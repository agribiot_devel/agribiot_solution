# Vagrant + Ansible scripts to install the AgriBIoT Virtual Machine

## What does it do?

Uses [Vagrant](https://www.vagrantup.com/intro) to manage the VM (create,
start, stop, destroy, reboot, provision, etc) and
[Ansible](https://www.ansible.com/) to provision it.

In this scope "provisionning" means making the configuration (installing
packages, tuning config files, etc). The nice thing with Ansible is that is
it idempotent (if well configured!). This means that running the provisioning
several times should always leave the system in the same state.

## What's needed to start?

Well, you need:

* Vagrant: I use the version from Buster's repository (v2.2.3 as of 09/09/20)
* Ansible: I use the version from Buster's repository (v2.7.7 as of 09/09/20)
* A Virtual Machine engine: Vagrant can manage several VM engines (VirtualBox,
  LibVirt-KVM, VMWare, etc). It is possible to specify which of them it should
  use. For the moment I didn't specified anything and it uses KVM (on my Debian
  Buster) and it works fine.

## How it Works?

* Move to the directory containing the `Vagrantfile`
* Use`vagrant up` to start the VM. If the VM doesn't exist, it wil be created
* Use `vagrant provision` to run provisioning scripts
* When the VM us up, use `vagrant ssh` to open a shell on the VM by SSH. This
  works automagically since Vagrant populate the VM with the necessary SSH keys
  when it creates the VM.
* Use`vagrant stop` to stop the VM.
* Use`vagrant destroy` to delete the VM.

In the current configuration, the provisioning scripts:

* install MariaDB, Grafana and development tools
* enable the grafana service (run on port 3000)
* The `Vagrantfile` forward the port 3000 of the VM to the port 3001 of the
  host. So that it is possible to connect to grafana from the host on
  `localhost:3000`.

## Lot of files. How is is organized?

First looks tricky, but finally quite simple:

* Vagrant entry point is the `Vagrantfile`. All VM configuration is in this file
* The provisioning section of the `Vagrantfile` (at the end of the file) refers
  to the Ansible Playbook located in the `ansible`directory.
* The Ansible entry point are the `playbook.yml` and the `inventory` files. The
  sooner is the top level file describing all provisioning actions. The later
  should describe the list of managed machines. In our case this file is a very
  simple since we only have one machine to administrate.
* The playbook calls roles and tasks. Roles are collections of tasks aiming at
  a specific goal (eg. installing and configuring mariaDD)  
  * each role has subdirectories
    * tasks is mandatory: it contains a `main.yaml`file describing the actions
      to perform
    * defaults (optional): is the place where variables can be defined as well
      as their default values
    * handlers: handlers are actions that can be triggered if a task is actually
      performed. Eg.: if a a task ask to install an already  package, it may not
      be necessary to enable the corresponding service.

## Need Help!

=> contact: christophe.couturier@irisa.fr

## Notes

* The `mysql` (MariaDB) role is largely inspired from
  [https://github.com/neikei/vagrant-debian-ansible-lemps]
* the `grafana` role is handwritten from scratch to fit at maximum to the
  AgriBIoT installation procedure.
  