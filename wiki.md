| tag | comment |
| ------ | ------ |
| v0.1 | first version deployed in vignoble cheneau |
| v0.2 | candidate for vignoble cheneau |



### Migrate from 0.1 to 0.2 :

./agribiot_db_dump.sh /opt/db_dump.sql


python3 manage.py makemigrations  
python3 manage.py migrate  
systemctl restart apache2.service


### Migration from v02 to v0.4: 

Export old Operation/Sensor : 
python3 manage.py shell < /root/export_v0_2_sensor_and_operation.py  > export_vc.json

Once updated use tools/import_from_v02json.py to import operation/sensor : 
1) First edit source filename in script, replace sample/json_dump_sensor_operation_chenau_03_03.json with export_vc.json
2) Replace simple quote by double quote (https://jsonformatter.curiousconcept.com/) 
3) import in new version :  python3 manage.py shell < tools/import_from_v02json.py


### Migration from v0.4 or v0.5 to v0.6: 

1) db dump : ./agribiot_db_dump.sh /opt/db_dump.sql

install dependancies 
pip3 install django_tables2 django_filter tablib

Copy following to migration folder :
agribiot/my-migration/0002_dimension_update_adding_dinmension.py
agribiot/my-migration/0004_dimension_update_remove_operation_value.py
agribiot/my-migration/0003_dimension_update_transfer_value.py

python3 manage.py migrate
Apply new permission :

python3 manage.py shell < tools/apply_dimension_permission.py

## SSH Tunnel 

/lib/systemd/system/secure-tunnel@.service :

```
[Unit]
Description=Setup a secure tunnel to %I
After=network.target

[Service]
Environment="LOCAL_ADDR=localhost"
EnvironmentFile=/etc/default/secure-tunnel@%i
ExecStart=/usr/bin/ssh -NT -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -T -R${LOCAL_PORT}:localhost:${REMOTE_PORT} ${TARGET} -p ${TARGET_PORT} -i ${KEY}


# Restart every >2 seconds to avoid StartLimitInterval failure
RestartSec=5
Restart=always

[Install]
WantedBy=multi-user.target
```


/etc/default/secure-tunnel@agribiot : 
```
TARGET=sshtun@82.65.196.68
LOCAL_PORT=22222
REMOTE_PORT=22
TARGET_PORT=2287
KEY=/home/agribiot/.ssh/id_rsa
```
Add the folowing to visudo :
```
%www-data ALL=(ALL) NOPASSWD:/usr/bin/systemctl
```
Enable :  
systemctl start secure-tunnel@agribiot.service  
Disable :  
systemctl start secure-tunnel@agribiot.service  

ssh to connected on :
ssh -p 22222 agribiot@localhost
