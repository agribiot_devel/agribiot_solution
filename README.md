# agribiot_solution

### Note about this project

AgriBIoT was a startup project lead by Oscar Roberto Bastos at the time of his untimely passing in 2022. To honor his legacy and in line with his values, we have decided to release AgriBIoT Application backend and frontend (this repository) and the [android application](https://gitlab.inria.fr/agribiot_devel/agribiot-android) into the public domain.  

Additional information related to the start-up project can be found [here](https://web.archive.org/web/20210703182926/https://agribiot.fr/en/index.html).  

Support for either projects can be obtained by contacting Simon Tropée (simon.tropee@sirocha.fr) or E4SE team projet leader Jean-Marie Bonnin (Jean-Marie.Bonnin@irisa.fr)

### How does it work? 

AgriBIoT is a Data collection solution at the service of the farmer

The solution integrates the advantages of three technological elements :
- A local storage and data visualitation tool :  by local storage we mean that data and information produced by the 
solution are not leaving the owner geo-location (e.g. to a cloud service). The owner is the only one able to decide which 
data can be shared to a tier solution/platform.
- A sensor network : aggregate from a diversity of connected objects on the farm
- RFID technology : A pool of RFID tag disseminate following the farmer need on his exploitation to keep track of his 
task and metric over time at the nearest position of his work place (e.g. into the field, on plants fence,...) 


This git repository contain the local storage and data visalisation tool that aggregate data from sensor and RFID tag.

RFID Tags and Sensor are defined as follow in the solution

A sensor will gather physical information from the field environment. Farmer will use a defined variety of sensor type 
(Humidity, Temperature, Rain gauge, etc...)
The Sensor database model will contain an entry for each of sensor type used on the farm.
RFID Tag will also be one entry of Sensor model in database.

Sensors will be placed in the farm environment at specific location.
Each physical sensor or RFID tag should be store in the database with his location.
Each existent sensor or RFID Tag deployed into the exploitation will have an entry in the Object database which store 
his own location.
Since an object in field can rely to multiple sensor type (e.g. a sensor that probe humidity and temperature), each one 
of his property will have an entry in Sensor_Object table which rely on his Sensor Type and Object position.

When a value is generated from sensor, it will be stored in Value table with a reference to his Sensor_Object



### Installation Using agribiot-box based on up-board


I consider agribiot-box ip has been configured into /etc/hosts and the remote ssh-server permit root login :

> stropee@agrobiot-01:~/work/agribiot_solution/agribiot-vm$ cat /etc/hosts  
    127.0.0.1   localhost  
    127.0.1.1   agrobiot-01  
    192.168.1.100   agribiot-box-01  
    192.168.1.43    agribiot-box-02  
    # The following lines are desirable for IPv6 capable hosts  
    ::1     localhost ip6-localhost ip6-loopback  
    ff02::1 ip6-allnodes  
    ff02::2 ip6-allrouters  

>root@agribiot-box-02:~# cat /etc/ssh/sshd_config | grep PermitRootLogin  
    PermitRootLogin yes  


Install rsync and ansible on remote box: 
>stropee@agrobiot-01:~/work/agribiot_solution/agribiot-vm$ ssh root@agribiot-box-02

>root@agribiot-box-02:~# apt-get install rsync ansible

Resync agribiot solution to remote box

>stropee@agrobiot-01:~/work$ rsync -rl agribiot_solution root@agribiot-box-02:

Run ansible playbook :


>stropee@agrobiot-01:~/work/agribiot_solution/agribiot-vm$ ssh root@agribiot-box-02

>root@agribiot-box-02:~# cd agribiot_solution/agribiot-vm/ansible/

Set hostname in ansible variable file : 
>root@agribiot-box-02:~/agribiot_solution/agribiot-vm/ansible# nano vars/main.yml   

Replace HOSTNAME_TO_SET :   
With your hostname :  
hostname: YOUR_HOSTNAME  

>root@agribiot-box-02:~/agribiot_solution/agribiot-vm/ansible# ansible-playbook playbook-box.yml 


Move project to http server folder : 

>cp -rf /root/agribiot_solution/agribiot-api /var/www/html/  

>cd /var/www/html/agribiot-api/  
>./migration_cleanup.sh ; ./migrate.sh  



### Installation Using vagrant

Using agribiot-vm provisioned virtual machine :  
> cd agribiot-vm  
> vagrant provision  
> vagrant up  

...  

Web application is now available on  
http://127.0.0.1:8080/

Application is initialised with farm, parcel, object and sensor and at two sensors value  
Default user to login with is "agribiot" password "6jwxnj9TGwfY5yr"  
Default adminitrator user for django is user : "root" password : "root"  



### Sync your local repository with your server 

From this repository on your own filesystem copy your modification to the remote web server folder :  
> rsync -rl agribiot-api root@<server ip>:/var/www/html/  

Then following modification type :  

If your modification impact only html/css/js page or Django View/serializer/url restart apache2 web server to refresh   
apache cache:  
Connect to your server as root :  
> ssh root@< server ip >  
Restart apache2  
> systemctl restart apache2  

If your modification impact Django model ("model.py") a django migration is required  
Connect to your server as root :  
> ssh root@< server ip>  
Move to django application folder :  
> cd /var/www/html/agribiot-api/   

Prepare migration :  
> python3 manage.py makemigrations  
Migrate :   
> python3 manage.py migrate  

In case of huge modification on existing model a database wipe could be needed (if the migration fail).  
This action will wipe database content and reinit it with default value (import script)  
>./migration_cleanup.sh ; ./migrate.sh  






#### Post a value 

Each Object in field is associated with an ObjectUser  
ObjectUser inherit from Django User model to manage permissions and authorisation  
In that way only an ObjectUser has the authorisation to push new value in Value table over API  
To post a new value we first need to retrieve the token of object that produce a value   

The following API endpoint return ObjectUser's token for and "object id" :  

<url>/api/object_token/<int:pk>/   
Example :   
http://127.0.0.1:8080/api/object_token/1/  
Return : 

{
    "object_id": 1,
    "uuid": "0001",
    "token": "c2e96d4497512453f69aee93f8005cdf02b7a386"
}

Using post-value.py tools :
>python3 agribiot-api/tools/post-value.py 
usage : post-value.py <url> <access_token> <sensor_object_id> <value>


> python3 agribiot-api/tools/post-value.py http://127.0.0.1:8080/api/value/ c2e96d4497512453f69aee93f8005cdf02b7a386 2 80  
http://127.0.0.1:8080/api/value/  
<Response [201]>  
{"id":44751,"created":"2021-01-06T11:07:10.451388Z","value":80.0,"sensor_object_id":2}  



### Dependencies and licenses



AgriBIoT Backend
| Name  | Licenses  | Project link |
|-------|-----------| -------------|
| Django  |  BSD 3 Clause | https://github.com/django/django/blob/main/LICENSE |
| Django-rest-framework  | Encode OSS Ltd. | https://github.com/encode/django-rest-framework/blob/master/LICENSE.md |
| psycopg2 | LGPL | https://pypi.org/project/psycopg2/ |
| Django-leaflet  |  Lesser GNU Public License | https://github.com/makinacorpus/django-leaflet |
| Django-extensions  | MIT License  | https://github.com/django-extensions/django-extensions/blob/main/LICENSE |
| Django-guardian  |   | https://github.com/django-guardian/django-guardian/blob/devel/LICENSE |
| djangorestframework-guardian  | BSD | https://github.com/rpkilby/django-rest-framework-guardian/blob/master/LICENSE |
| django-cors-headers  | MIT | https://github.com/adamchainz/django-cors-headers/blob/main/LICENSE |
| django-resized  | MIT License | https://github.com/un1t/django-resized |
| django-thumbs-v2  |  | https://github.com/rrmerugu/django-thumbs-v2/blob/master/LICENSE |
| PyNaCl  |  Apache License | https://github.com/pyca/pynacl/blob/main/LICENSE |
| Pillow  |  | https://github.com/python-pillow/Pillow/blob/master/LICENSE |
| django-filter | | https://github.com/carltongibson/django-filter/tree/main |
| django-table2 | | https://github.com/jieter/django-tables2 |




AgriBIoT Frontend : 

| Name  | Licenses  | Project link |
|-------|-----------| -------------|
| Volt Template | MIT | https://themesberg.com/licensing |
| leafletjs |  BSD 2-clauses (following wikipedia page)   | https://github.com/Leaflet/Leaflet |
| qrcodejs | MIT  | https://github.com/davidshimjs/qrcodejs |
| wellknownjs | equivalent to the BSD 2-Clause | https://github.com/mapbox/wellknown |
| jquery | | https://github.com/jquery/jquery/blob/main/LICENSE.txt | 
| bootstrap  | MIT | https://github.com/twbs/bootstrap/blob/main/LICENSE | 
| chartist | MIT & WTFPL | https://github.com/gionkunz/chartist-js | 
| simplebar  | MIT | https://github.com/Grsmto/simplebar/blob/master/LICENSE | 
| datepicker  | MIT | https://github.com/mymth/vanillajs-datepicker/blob/master/LICENSE | 
| buttons js | | https://buttons.github.io/ |
| qrcode js | MIT License | https://github.com/davidshimjs/qrcodejs/blob/master/LICENSE |

