#!/bin/bash

runuser -l postgres -c 'createdb agribiot'
echo "CREATE EXTENSION postgis;" | runuser -l postgres -c 'psql agribiot'
echo "CREATE EXTENSION plpython3u;" | runuser -l postgres -c 'psql agribiot'
