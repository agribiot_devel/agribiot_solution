#!/bin/bash
#runuser -l postgres -c 'createdb test_agribiot'
echo "CREATE EXTENSION postgis;" | runuser -l postgres -c 'psql'
echo "CREATE EXTENSION plpython3u;" | runuser -l postgres -c 'psql'
echo "ALTER USER agribiot superuser;" | runuser -l postgres -c 'psql'
