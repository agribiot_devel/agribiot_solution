/*
#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot JS
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
*/
class AgribiotInstance {

	constructor(url, name, token,color) {
		if(url == "local"){
			this.url = ""//document.location.origin;
		}else{
			this.url = url;
		}
		this.token = token;
		this.name = name; 
		this.color = color;
	}

	getRemoteInstances(callback){
		this._remote_instance_fetch("api/remoteinstance",callback);
	}
	getParcel(callback){
		this._remote_instance_fetch("api/parcel/",callback);
	}
	getSensorObjects(callback){
		this._remote_instance_fetch("api/sensor_object/",callback);
	}
	getValueBySensorObject(sensor_object_id,callback){
		this._remote_instance_fetch("api/value_by_sensor_object/"+sensor_object_id+"/",callback);
	}
	getValueBySensorObjectSinceDate(sensor_object_id,start_date,callback){
		this._remote_instance_fetch("api/value_by_sensor_object_since_date/"+sensor_object_id+"/"+start_date+"/",callback);
	}
	getValueBySensorObjectRangeDate(sensor_object_id,start_date,end_date,callback){
		this._remote_instance_fetch("api/value_by_sensor_object_range_date/"+sensor_object_id+"/"+start_date+"/"+end_date+"/",callback);
	}
	getValueBySensorObjectLastN(sensor_object_id,n,callback){
		this._remote_instance_fetch("api/value_by_sensor_object_last_n/"+sensor_object_id+"/"+n+"/",callback);
	}
	getOperationBySensorObject(sensor_object_id,callback){
		this._remote_instance_fetch("api/operation_by_sensor_object/"+sensor_object_id+"/",callback);		
	}
	getOperation(callback){
		this._remote_instance_fetch("api/operation/",callback)
	}
	getOperationImage(operation_image_id,callback){
		this._remote_instance_fetch("api/operation_image/"+operation_image_id+"/",callback);
	}
	getObjects(callback){
		this._remote_instance_fetch("api/object/",callback);
	}
	getObjectById(id,callback){
		this._remote_instance_fetch("api/object/"+id+"/",callback);
	}
	getObjectsInParcel(parcel_id,callback){
		this._remote_instance_fetch("api/object_in_parcel/"+parcel_id+"/",callback);
	}
	getSensorObjectsByObject(object_id,callback){
		this._remote_instance_fetch("api/sensor_object_by_object/"+object_id+"/",callback);
	}

	getAnnotationTemplate(callback){
		this._remote_instance_fetch("api/annotation_template/",callback);
	}
	getDimensionByAnnotationTemplate(annotation_template_id,callback){
		this._remote_instance_fetch("api/dimension_by_annotation_template/"+annotation_template_id+"/",callback);
	}

	getOperationFiltered(filter,callback){
		this._remote_instance_fetch("api/operation_filtered/?"+filter,callback);
	}
	getDimensionValue(id,callback){
		this._remote_instance_fetch("api/dimension_value/"+id+"/",callback);
	}
	getDimensionValues(callback){
		this._remote_instance_fetch("api/dimension_value/",callback);
	}
	getDimensions(callback){
		this._remote_instance_fetch("api/dimension/",callback);
	}
	getDimensionValueByAnnotationTemplate(id,callback){
		this._remote_instance_fetch("api/dimension_value_by_annotation_template/"+id+"/",callback);
	}
	getRisedAlarms(callback){
		this._remote_instance_fetch("api/rised_alarm/",callback);
	}

	updateObject(object,callback){
		var update_object_request = new XMLHttpRequest();
		const csrftoken = getCookie('csrftoken');
		update_object_request.open("PUT", 'api/object/'+object.id+'/', true);
		update_object_request.setRequestHeader('Content-Type', 'application/json');
		update_object_request.setRequestHeader('X-CSRFToken',csrftoken);
		update_object_request.onreadystatechange = function() {
			if (update_object_request.readyState == XMLHttpRequest.DONE) {
				try {
					if(JSON.parse(update_object_request.responseText).id == undefined){
						pop_up_error(update_object_request.responseText);
						return;
					}
					callback();
				} catch (e) {
					pop_up_error(e.toString());
				}
			}
		}
		update_object_request.send(JSON.stringify(object));

	}

	deleteSensorObject(id,callback){
		this._delete("sensor_object",id,callback);
	}
	deleteRisedAlarm(id,callback){
		this._delete("rised_alarm",id,callback);
	}
	deleteOperation(operation_id,callback){
		this._delete("operation",operation_id,callback);
	}
	_delete(api_endpoint,id,callback){
		var delete_request = new XMLHttpRequest();
		const csrftoken = getCookie('csrftoken');

		delete_request.open("DELETE", 'api/'+api_endpoint+'/'+id, true);
		delete_request.setRequestHeader('Content-Type', 'application/json');
		delete_request.setRequestHeader('X-CSRFToken',csrftoken);
		delete_request.onreadystatechange = function() {
			if (delete_request.readyState == XMLHttpRequest.DONE) {
				callback();
			}
		}
		delete_request.send();
	}
	_remote_instance_fetch(url, callback) {
	    var promises = []
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        myHeaders.append('Authorization', 'Token '+this.token);

        promises.push(fetch(this.url+url, {
            method: 'GET',
            headers: myHeaders,
        }).then(response => response.json()).then(function(response){
            return response;
        }))


	    Promise.all(promises).then(function(data){
	        data.forEach(function(element) {
	            if(element.detail != undefined){
	               pop_up_error(element.detail);
	               return;
	            }
	            callback(element);
	        })
	    });   

	}
}
