#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
from django import forms

from leaflet.forms.widgets import LeafletWidget
from leaflet.forms.fields import PolygonField
from django.forms.widgets import TextInput

from django.contrib.gis.db.models import PointField
from django.forms.models import ModelChoiceField
from agribiot.models import ForeignFarmUser
from agribiot.models import Sensor
from agribiot.models import Object
from agribiot.models import SensorObject
from agribiot.models import TargetType
from agribiot.models import RemoteAccess
from agribiot.models import RemoteInstance
from agribiot.models import AnnotationTemplate
from agribiot.models import AnnotationType

from agribiot.models import GatewayUser
from agribiot.models import RFIDDeviceUser
from django.contrib.auth.forms import UserCreationForm

from django.forms import inlineformset_factory
MY_CHOICES = (
       ('0', 'All'),
    ('1', 'Last value'),
    ('2', 'Date range'),
)

#https://stackoverflow.com/questions/6034047/one-to-many-inline-select-with-django-admin
"""
    
    We will set view permission on sensor object and linked values following the following filter :
    on a sensor list
    If geometry or object list is specified we will set the permission on each sensor_object that fit 
    Permissions should be either on all value, last value, or between a date range
    
"""
class RemoteAccessForm(forms.ModelForm):
    geometry = PolygonField(required=False)
    access_type = forms.ChoiceField(choices=MY_CHOICES)
    sensors = forms.ModelMultipleChoiceField(Sensor.objects.all(),required=True)
    objects = forms.ModelMultipleChoiceField(Object.objects.all(),required=False) 
    start_date = forms.DateTimeField(required=False)
    end_date = forms.DateTimeField(required=False)
    class Meta:
        model = RemoteAccess
        fields  = ('id', 'access_type','sensors','objects','user','geometry','start_date','end_date')
        widgets = {'geometry': LeafletWidget(),}

    def save(self, *args, **kwargs):
        selected_objects = self.cleaned_data['objects']
        selected_sensors = self.cleaned_data['sensors']
        remoteAccess_instance = RemoteAccess()
        remoteAccess_instance.access_type = self.instance.access_type
        remoteAccess_instance.user = self.instance.user
        remoteAccess_instance.geometry = self.instance.geometry
        if len(selected_objects) >0:
            remoteAccess_instance.target_type = TargetType.SPECIFIC_OBJECTS
        elif remoteAccess_instance.geometry != None:
            remoteAccess_instance.target_type = TargetType.GEOMETRY
        else:
            remoteAccess_instance.target_type = TargetType.ALL_OBJECTS
        remoteAccess_instance.start_date = self.instance.start_date
        remoteAccess_instance.end_date = self.instance.end_date 
        remoteAccess_instance.save()
        for sensor in selected_sensors:
            remoteAccess_instance.sensors.add(sensor)
        for object in selected_objects:
            remoteAccess_instance.selected_objects.add(object)
        remoteAccess_instance.commit_remote_access()
        return remoteAccess_instance



class ForeignFarmUserForm(forms.ModelForm):
    class Meta:
        model = ForeignFarmUser
        fields = ('username', 'url_origin', 'first_name', 'last_name', 'email')

class GatewayUserForm(forms.ModelForm):
    class Meta:
        model = GatewayUser
        fields = ('username',)

class RFIDDeviceUserForm(forms.ModelForm):
    class Meta:
        model = RFIDDeviceUser
        fields = ('username',)

class RemoteInstanceForm(forms.ModelForm):
    class Meta:
        model = RemoteInstance
        fields = ('name', 'token', 'url','color')
        widgets = {
            'color': TextInput(attrs={'type': 'color'}),
        }



class TaskForm(forms.ModelForm):
    class Meta:
        model = AnnotationTemplate
        fields = ('description','photo',)

class ObservationForm(forms.ModelForm):
    class Meta:
        model = AnnotationTemplate
        fields = ('description','photo',)
