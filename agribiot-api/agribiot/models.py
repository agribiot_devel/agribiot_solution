#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
import pytz

from django.utils import timezone
from datetime import datetime, timedelta
from datetime import date
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission
from django.utils.dateparse import parse_datetime
from django.db import models
from django.db import connection
from django.db.models.functions import Cast

from django.contrib.gis.db.models import PolygonField
from django.contrib.gis.db.models import PointField
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from guardian.models import UserObjectPermission

from enum import IntEnum
from django.db.models.fields.related import ManyToManyField
from django_thumbs.fields import ImageThumbsField

def add_classic_permission(permission_type,model,group):
    from django.contrib.contenttypes.models import ContentType
    from django.contrib.auth.models import Permission

    content_type = ContentType.objects.get(app_label='agribiot', model=model)
    permission = Permission.objects.get(
        codename=permission_type+'_'+model,
        content_type=content_type,
    )
    group.permissions.add(permission)
    
def add_custom_permission(permission_type,model,group):
    from django.contrib.contenttypes.models import ContentType
    from django.contrib.auth.models import Permission

    content_type = ContentType.objects.get(app_label='agribiot', model=model)
    permission = Permission.objects.get(
        codename=permission_type,
        content_type=content_type,
    )
    group.permissions.add(permission)

class AccessType(models.IntegerChoices):
        ALL = 0
        LAST_VALUE = 1
        DATE_RANGE= 2

class TargetType(models.IntegerChoices):
        ALL_OBJECTS = 0
        SPECIFIC_OBJECTS = 1
        GEOMETRY = 2

def assign_permission_to_sensor_object(sensor_object,remote_access):
    UserObjectPermission.objects.assign_perm('view_sensorobject', user_or_group=remote_access.user, obj=sensor_object)
    UserObjectPermission.objects.assign_perm('view_sensor', user_or_group=remote_access.user, obj=sensor_object.sensor_id)
    UserObjectPermission.objects.assign_perm('view_object', user_or_group=remote_access.user, obj=sensor_object.object_id)
    if remote_access.access_type == AccessType.ALL:
        UserObjectPermission.objects.assign_perm('view_all_value', user_or_group=remote_access.user, obj=sensor_object)
    elif remote_access.access_type == AccessType.LAST_VALUE:
        UserObjectPermission.objects.assign_perm('view_last_value', user_or_group=remote_access.user, obj=sensor_object)
    elif remote_access.access_type == AccessType.DATE_RANGE:
        UserObjectPermission.objects.assign_perm('view_date_range_value', user_or_group=remote_access.user, obj=sensor_object)
        end_date = remote_access.end_date
        if end_date == None:
            end_date = date.today()
        values = Value.objects.filter(sensor_object_id=sensor_object.id, created__range=(remote_access.start_date, end_date))
        for value in values:
            UserObjectPermission.objects.assign_perm('view_value', user_or_group=remote_access.user, obj=value)

def remove_permission_to_sensor_object(sensor_object,remote_access):
    UserObjectPermission.objects.remove_perm('view_sensorobject', user_or_group=remote_access.user, obj=sensor_object)
    # Avoid cross permissions settings removal
    if len(sensor_object.sensor_id.remoteaccess_set.filter(user=remote_access.user)) == 1:
        UserObjectPermission.objects.remove_perm('view_sensor', user_or_group=remote_access.user, obj=sensor_object.sensor_id)
    if len(sensor_object.object_id.remoteaccess_set.filter(user=remote_access.user)) == 1:
        UserObjectPermission.objects.remove_perm('view_object', user_or_group=remote_access.user, obj=sensor_object.object_id)
    if remote_access.access_type == AccessType.ALL:
        UserObjectPermission.objects.remove_perm('view_all_value', user_or_group=remote_access.user, obj=sensor_object)
    elif remote_access.access_type == AccessType.LAST_VALUE:
        UserObjectPermission.objects.remove_perm('view_all_value', user_or_group=remote_access.user, obj=sensor_object)
    elif remote_access.access_type == AccessType.DATE_RANGE:
        UserObjectPermission.objects.remove_perm('view_date_range_value', user_or_group=remote_access.user, obj=sensor_object)
        end_date = remote_access.end_date
        if end_date == None:
            end_date = date.today()
        values = Value.objects.filter(sensor_object_id=sensor_object.id, created__range=(remote_access.start_date, end_date))
        for value in values:
            UserObjectPermission.objects.remove_perm('view_value', user_or_group=remote_access.user, obj=value)

class GatewayUser(User):
    def save(self, *args, **kwargs):
        super(GatewayUser, self).save(*args, **kwargs)
        gatewayGroup, created = Group.objects.get_or_create(name='gatewayGroup')
        if created == True :
            add_custom_permission('access_object_token','object',gatewayGroup)
            add_classic_permission('view','sensorobject',gatewayGroup)
        gatewayGroup.user_set.add(self);
    class Meta:
        permissions = [
            ("access_gateway_user_token", "Can access auth token"),]

class RFIDDeviceUser(User):
    def save(self, *args, **kwargs):
        super(RFIDDeviceUser, self).save(*args, **kwargs)
        RFIDDeviceGroup, created = Group.objects.get_or_create(name='RFIDDeviceGroup')
        if created == True :
            add_classic_permission('view','annotationtemplate',RFIDDeviceGroup)
            add_classic_permission('add','annotationtemplate',RFIDDeviceGroup)
            add_classic_permission('view','operation',RFIDDeviceGroup)
            add_classic_permission('add','operation',RFIDDeviceGroup)
            add_classic_permission('view','operationimage',RFIDDeviceGroup)
            add_classic_permission('add','operationimage',RFIDDeviceGroup)
            add_classic_permission('view','sensorobject',RFIDDeviceGroup)
            add_classic_permission('add','sensorobject',RFIDDeviceGroup)
            add_classic_permission('change','sensorobject',RFIDDeviceGroup)
            add_classic_permission('view','object',RFIDDeviceGroup)
            add_classic_permission('add','object',RFIDDeviceGroup)
            add_classic_permission('change','object',RFIDDeviceGroup)
            add_classic_permission('view','dimension',RFIDDeviceGroup)
            add_classic_permission('add','dimension',RFIDDeviceGroup)
            add_classic_permission('view','dimensionvalue',RFIDDeviceGroup)
            add_classic_permission('add','dimensionvalue',RFIDDeviceGroup)
        RFIDDeviceGroup.user_set.add(self);
    class Meta:
        permissions = [
            ("access_rfiddevice_user_token", "Can access auth token"),]

class ForeignFarmUser(User):
    url_origin = models.CharField(max_length=100, blank=True, default='')
    def save(self, *args, **kwargs):
        if self.url_origin[-1] != "/":
            self.url_origin += "/"
        super(ForeignFarmUser, self).save(*args, **kwargs)
        foreignfarmGroup, created = Group.objects.get_or_create(name='foreignFarmGroup')
        if created == True :
            add_classic_permission('view','parcel',foreignfarmGroup)
            add_classic_permission('view','farm',foreignfarmGroup)

        foreignfarmGroup.user_set.add(self);
    class Meta:
        permissions = [
            ("access_foreign_user_token", "Can access auth token"),]

class ObjectUser(User):
    def save(self, *args, **kwargs):
        super(ObjectUser, self).save(*args, **kwargs)
        objectGroup, created = Group.objects.get_or_create(name='objectGroup')
        if created == True :
            add_classic_permission('add','value',objectGroup)
        objectGroup.user_set.add(self);

class RemoteInstance(models.Model):
    name = models.CharField(max_length=100, blank=True, default='')
    token = models.CharField(max_length=256, blank=True, default='')
    url = models.CharField(max_length=256, blank=True, default='')
    color = models.CharField(max_length=7,default='')
    def save(self, *args, **kwargs):
        if self.url[-1] != "/":
            self.url += "/"
        super(RemoteInstance, self).save(*args, **kwargs)
    class Meta:
        unique_together = ('name', 'url',)

class Farm(models.Model):
    name = models.CharField(max_length=100, blank=True, default='')

class Parcel(models.Model):
    name = models.CharField(max_length=100, blank=True, default='', unique=True)
    farm = models.ForeignKey('agribiot.Farm',on_delete=models.CASCADE)
    geometry = PolygonField(geography=True)

class Object(models.Model):
    position = PointField(geography=True)
    user_id = models.ForeignKey(ObjectUser, on_delete=models.CASCADE, null=True, db_column='user_id')
    uuid = models.CharField(max_length=100, unique=True, default='')
    description = models.TextField(blank=True, default='')
    def save(self, *args, **kwargs):
        objectUser, created = ObjectUser.objects.get_or_create(username=self.uuid);
        if created == True:
            objectUser.is_active = True
            objectUser.is_staff = False
            objectUser.username = self.uuid
            objectUser.set_password(self.uuid) # TODO : randomize
            objectUser.save()
            self.user_id = objectUser
        super(Object, self).save(*args, **kwargs)
    class Meta:
        permissions = [
            ("access_object_token", "Can access auth token"),]

class Sensor(models.Model):
    name = models.CharField(max_length=100, unique=True,blank=True, default='')
    unit = models.CharField(max_length=100, blank=True, default='')

class SensorObject(models.Model):
    object_id = models.ForeignKey('agribiot.Object',on_delete=models.CASCADE,db_column='object_id')
    sensor_id = models.ForeignKey('agribiot.Sensor',on_delete=models.DO_NOTHING,db_column='sensor_id')
    class Meta:
        unique_together = ('object_id', 'sensor_id',)
        permissions = (
            ('view_last_value', 'View last associated value/operation'),
            ('view_all_value', 'View all associated value/operation'),
            ('view_date_range_value', 'View all associated value/operation in date range'),
        )
    def save(self, *args, **kwargs):
        super(SensorObject, self).save(*args, **kwargs)
        remoteAccesss= RemoteAccess.objects.all();
        for remoteAccess in remoteAccesss:
            remoteAccess.check_new_sensor_object(self);

class Value(models.Model):
    created = models.DateTimeField(blank=True)
    value = models.FloatField()
    sensor_object_id = models.ForeignKey('agribiot.SensorObject',on_delete=models.CASCADE,db_column='sensor_object_id')
    class Meta:
        ordering = ['created']
    def save(self, *args, **kwargs):
        if not self.id and not self.created:
            self.created = timezone.now()
        super(Value, self).save(*args, **kwargs)
        date_now = timezone.now() - timedelta(minutes=1) # TODO 1 minute is arbitrary
        if isinstance(self.created,str) :
            if parse_datetime(self.created) < date_now:
                return;
        else:
            if self.created < date_now:
                return;
        
        for remote_access in RemoteAccess.objects.all():
            if remote_access.access_type == AccessType.DATE_RANGE and remote_access.end_date == None and self.sensor_object_id in remote_access.sensor_objects.all():
                UserObjectPermission.objects.assign_perm('view_value', user_or_group=remote_access.user, obj=self)

        triggers = AlarmTrigger.objects.all()
        test = 0;
        # Iterate over each alarm, then over each sensor object latest value to check wether alarm should be rise
        for trigger in triggers:
            alarms = trigger.alarmelement_set.all()
            for alarm in alarms:
                # Check if Object, Sensor and Geometry match
                # todo factorize
                if alarm.object_id == None and alarm.geometry == None:
                    sensor_objects = SensorObject.objects.filter(sensor_id=alarm.sensor_id)
                    for sensor_object in sensor_objects:
                        try:
                            latest_value = sensor_object.value_set.latest("created")
                        except Value.DoesNotExist:
                            continue
                        # Check if this alarm is not already rised for this value
                        if latest_value.risedalarm_set.filter(alarm_trigger_id=trigger).count() != 0:
                            continue
                        if alarm.trigger_type == "<=" and latest_value.value <= alarm.trigger_value:
                            test+=1;
                        elif  alarm.trigger_type == "<" and latest_value.value < alarm.trigger_value:
                            test+=1;
                        elif  alarm.trigger_type == ">=" and latest_value.value >= alarm.trigger_value:
                            test+=1;
                        elif  alarm.trigger_type == ">" and latest_value.value > alarm.trigger_value:
                            test+=1;
                        elif  alarm.trigger_type == "=" and latest_value.value == alarm.trigger_value:
                            test+=1;
                elif alarm.object_id != None:
                    sensor_objects = SensorObject.objects.filter(sensor_id=alarm.sensor_id,object_id=alarm.object_id)
                    for sensor_object in sensor_objects:
                        try:
                            latest_value = sensor_object.value_set.latest("created")
                        except Value.DoesNotExist:
                            continue
                        # Check if this alarm is not already rised for this value
                        if latest_value.risedalarm_set.filter(alarm_trigger_id=trigger).count() != 0:
                            continue
                        if alarm.trigger_type == "<=" and latest_value.value <= alarm.trigger_value:
                            test+=1;
                        elif  alarm.trigger_type == "<" and latest_value.value < alarm.trigger_value:
                            test+=1;
                        elif  alarm.trigger_type == ">=" and latest_value.value >= alarm.trigger_value:
                            test+=1;
                        elif  alarm.trigger_type == ">" and latest_value.value > alarm.trigger_value:
                            test+=1;
                        elif  alarm.trigger_type == "=" and latest_value.value == alarm.trigger_value:
                            test+=1;
                elif alarm.geometry != None:
                    sensor_objects = SensorObject.objects.filter(sensor_id=alarm.sensor_id)
                    for sensor_object in sensor_objects:
                        if alarm.geometry.contains(sensor_object.object_id.position):
                            try:
                                latest_value = sensor_object.value_set.latest("created")
                            except Value.DoesNotExist:
                                continue
                            # Check if this alarm is not already rised for this value
                            if latest_value.risedalarm_set.filter(alarm_trigger_id=trigger).count() != 0:
                                continue
                            if alarm.trigger_type == "<=" and latest_value.value <= alarm.trigger_value:
                                test+=1;
                                break # One sensor is enought for an area, break 
                            elif  alarm.trigger_type == "<" and latest_value.value < alarm.trigger_value:
                                test+=1;
                                break # One sensor is enought for an area, break 
                            elif  alarm.trigger_type == ">=" and latest_value.value >= alarm.trigger_value:
                                test+=1;
                                break # One sensor is enought for an area, break 
                            elif  alarm.trigger_type == ">" and latest_value.value > alarm.trigger_value:
                                test+=1;
                                break # One sensor is enought for an area, break 
                            elif  alarm.trigger_type == "=" and latest_value.value == alarm.trigger_value:
                                test+=1;
                                break # One sensor is enought for an area, break

            if test >= trigger.alarmelement_set.all().count():
                test = 0
                riseAlarm = RisedAlarm(alarm_trigger_id=trigger,value_id=self)
                riseAlarm.save()
#   Trigger type : 
#    INFEQUAL = "<="
#    INF = "<"
#    SUPEQUAL = ">="
#    SUP = ">"
#    EQUAL = "="
#    TODO : RANGE



class AlarmTrigger(models.Model):
    name = models.CharField(max_length=100, default='',unique=True)


class AlarmElement(models.Model):
    object_id = models.ForeignKey('agribiot.Object',on_delete=models.DO_NOTHING,db_column='object_id', blank=True,null=True)
    sensor_id = models.ForeignKey('agribiot.Sensor',on_delete=models.DO_NOTHING,db_column='sensor_id')
    trigger_type = models.CharField(max_length=100, default='')
    trigger_value = models.FloatField()
    geometry = PolygonField(geography=True,blank=True,null=True)
    alarm_trigger_id = models.ForeignKey(AlarmTrigger, on_delete=models.CASCADE,db_column='alarm_trigger_id')
    
class RisedAlarm (models.Model):
    created = models.DateTimeField(auto_now_add=True)
    alarm_trigger_id = models.ForeignKey('agribiot.AlarmTrigger',on_delete=models.CASCADE,db_column='alarm_trigger_id')
    value_id = models.ForeignKey('agribiot.Value',on_delete=models.DO_NOTHING,db_column='value_id')
    class Meta:
        ordering = ['created']
        

class RemoteAccess(models.Model):
    access_type = models.IntegerField(choices=AccessType.choices,default=AccessType.ALL)
    target_type = models.IntegerField(choices=TargetType.choices,default=TargetType.ALL_OBJECTS)
    user = models.ForeignKey('agribiot.ForeignFarmUser',on_delete=models.DO_NOTHING,db_column='foreign_users')
    sensor_objects = ManyToManyField('agribiot.SensorObject');
    sensors = ManyToManyField('agribiot.Sensor');
    selected_objects = ManyToManyField('agribiot.Object');
    geometry = PolygonField(geography=True, blank=True,null=True)
    start_date = models.DateTimeField(blank=True,null=True)
    end_date = models.DateTimeField(blank=True,null=True)

    def check_new_sensor_object(self,sensor_object):
        if self.target_type == TargetType.GEOMETRY:
            if sensor_object.object_id.position.within(self.geometry) == True:
                # If have that kind of sensor in sensors set add permission
                if len(self.sensors.filter(id=sensor_object.sensor_id.id)) >0:
                    self.sensor_objects.add(sensor_object);
                    assign_permission_to_sensor_object(sensor_object,self);
        elif self.target_type == TargetType.ALL_OBJECTS:
            # If have that kind of sensor in sensors set add permission
            if len(self.sensors.filter(id=sensor_object.sensor_id.id)) >0:
                    self.sensor_objects.add(sensor_object);
                    assign_permission_to_sensor_object(sensor_object,self);

    def commit_remote_access(self):
        if self.target_type == TargetType.GEOMETRY:
            self.selected_objects.set(Object.objects.annotate(geom=Cast('position', PointField())).filter(geom__within=self.geometry))
        for sensor in self.sensors.all():
            if len(self.selected_objects.all())> 0:
                for object in self.selected_objects.all():
                    sensorObjects = SensorObject.objects.filter(sensor_id=sensor.id, object_id=object.id);
                    for sensorObject in sensorObjects:
                        self.sensor_objects.add(sensorObject);
            else:
                sensorObjects = SensorObject.objects.filter(sensor_id=sensor.id);
                for sensorObject in sensorObjects:
                    self.sensor_objects.add(sensorObject);

from django.db.models.signals import pre_delete
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

@receiver(pre_delete, sender=RemoteAccess)
def remote_access_delete(sender, instance, using, **kwargs):
    for sensor_object in instance.sensor_objects.all():
         remove_permission_to_sensor_object(sensor_object,instance)

@receiver(m2m_changed, sender=RemoteAccess.sensor_objects.through)
def sensor_object_set_changed(sender, **kwargs):
    action = kwargs.pop('action', None)
    pk_set = kwargs.pop('pk_set', None)    
    if action == "post_add":
        if len(pk_set) >1:
            print("Fatal error on sensor_object_set_changed len(pk_set) > 1")
            return
        for id in pk_set:
            remoteAccess = kwargs.pop('instance', None)
            sensorObject = SensorObject.objects.get(id=id);
            assign_permission_to_sensor_object(sensorObject,remoteAccess)
    elif action == "post_remove":
        print("todo remove (edit mode not implemented)")

from django.utils.translation import gettext_lazy
class AnnotationType(models.TextChoices):
    TASK = "TASK", gettext_lazy("Task")
    OBSERVATION = "OBSV", gettext_lazy("Observation")

class AnnotationTemplate(models.Model):
    SIZES = (
        {'code': '100', 'wxh': '100x100'},
        {'code': '50', 'wxh': '50x50'},
        )
    annotation_type = models.CharField(
        max_length=4,
        choices=AnnotationType.choices,
        default=AnnotationType.TASK,
    )
    description = models.TextField(blank=True, default='',unique=True)
    photo = ImageThumbsField(upload_to='media/annotationtemplateimg', sizes=SIZES,null=True,blank=True)

class OperationImage(models.Model):
    SIZES = (
        {'code': '100', 'wxh': '100x100'},
        {'code': '50', 'wxh': '50x50'},
        )
    photo = ImageThumbsField(upload_to='media/operation', sizes=SIZES,null=True,blank=True)


class Operation(models.Model):
    imported = models.DateTimeField(auto_now_add=True)
    created = models.DateTimeField(blank=True)
    reader_uuid = models.CharField(max_length=100, blank=True, default='')
    tag_embedded_task_index = models.IntegerField()
    position = PointField(geography=True,blank=True) # Just in case
    accuracy = models.FloatField(blank=True)
    sensor_object_id = models.ForeignKey('agribiot.SensorObject',on_delete=models.CASCADE,db_column='sensor_object_id')
    annotation_template_id = models.ForeignKey('agribiot.AnnotationTemplate',on_delete=models.CASCADE,db_column='annotation_template_id')
    images = models.ManyToManyField('agribiot.OperationImage')
    class Meta:
        ordering = ['created']


class Dimension(models.Model):
    name = models.TextField(blank=False,unique=True)
    unit = models.CharField(max_length=100, blank=True, default='')

class DimensionValue(models.Model):
    value = models.FloatField()
    operation_id = models.ForeignKey('agribiot.Operation',on_delete=models.CASCADE,db_column='operation_id')
    dimension_id = models.ForeignKey('agribiot.Dimension',on_delete=models.CASCADE,db_column='dimension_id')
    class Meta:
        unique_together = ('operation_id', 'dimension_id',)

