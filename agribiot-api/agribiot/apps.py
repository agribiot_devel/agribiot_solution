#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
from django.apps import AppConfig


class AgribiotConfig(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'agribiot'
    def ready(self):
        from agribiot import signal
