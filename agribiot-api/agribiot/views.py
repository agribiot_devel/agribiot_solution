#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
import datetime
import pytz

import json 
import os, sys
import subprocess
from django.core import serializers
from django.db.models.functions import Cast
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import DeleteView
from django_filters.views import FilterView
from django_filters.rest_framework import DjangoFilterBackend
from django_tables2.views import SingleTableMixin
from django_tables2 import SingleTableView
from django_tables2.export.views import ExportMixin

from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.gis.db.models import PointField
from django.contrib.auth.mixins import PermissionRequiredMixin


from rest_framework.authtoken.models import Token
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.filters import SearchFilter, OrderingFilter

from rest_framework.response import Response
from rest_framework import status
from rest_framework_guardian import filters
from guardian.shortcuts import get_objects_for_user
from agribiot.models import Farm
from agribiot.models import Parcel
from agribiot.models import Object
from agribiot.models import Sensor
from agribiot.models import SensorObject
from agribiot.models import Value
from agribiot.models import AlarmTrigger
from agribiot.models import AlarmElement
from agribiot.models import RisedAlarm
from agribiot.models import ForeignFarmUser
from agribiot.models import RemoteAccess
from agribiot.models import RemoteInstance
from agribiot.models import AnnotationTemplate
from agribiot.models import AnnotationType
from agribiot.models import Operation
from agribiot.models import OperationImage
from agribiot.models import GatewayUser
from agribiot.models import RFIDDeviceUser
from agribiot.models import Dimension
from agribiot.models import DimensionValue



from agribiot.serializers import DimensionValueJoinSerializer, DimensionValueSerializer, FarmSerializer
from agribiot.serializers import ParcelSerializer
from agribiot.serializers import ObjectSerializer
from agribiot.serializers import SensorSerializer
from agribiot.serializers import SensorObjectSerializer
from agribiot.serializers import SensorObjectJoinSerializer
from agribiot.serializers import ValueSerializer
from agribiot.serializers import AlarmTriggerSerializer
from agribiot.serializers import AlarmTriggerJoinSerializer
from agribiot.serializers import AlarmElementSerializer
from agribiot.serializers import RisedAlarmSerializer
from agribiot.serializers import RemoteInstanceSerializer
from agribiot.serializers import AnnotationTemplateSerializer
from agribiot.serializers import OperationSerializer
from agribiot.serializers import OperationImageSerializer
from agribiot.serializers import OperationJoinSerializer
from agribiot.serializers import OperationJoinTidSerializer
from agribiot.serializers import DimensionSerializer
from agribiot.serializers import DimensionValueSerializer
from agribiot.serializers import DimensionValueJoinSerializer

from agribiot.permissions import DefaultDjangoModelPermissions
from agribiot.permissions import ObjectTokenDjangoModelPermissions
from agribiot.permissions import ForeignUserTokenDjangoModelPermissions
from agribiot.permissions import GatewayUserTokenDjangoModelPermissions
from agribiot.permissions import RFIDDeviceUserTokenDjangoModelPermissions

from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.exceptions import NotFound

from agribiot.form import RemoteAccessForm
from agribiot.form import ForeignFarmUserForm
from agribiot.form import RemoteInstanceForm
from agribiot.form import TaskForm
from agribiot.form import ObservationForm
from agribiot.form import GatewayUserForm
from agribiot.form import RFIDDeviceUserForm

from agribiot.filter import OperationFilter
from agribiot.tables import DimensionValueTable
from agribiot.filter import DimensionValueFilter

import os.path
import os
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
utc=pytz.UTC

def forbiden_access(request):
    #print("unauthorized access for user : " + request.user.username)
    raise PermissionDenied#forbiden_access(request)
def not_found(request):
    raise NotFound#forbiden_access(request)


@csrf_exempt
def farm_dump(request):
    """
    """
    if request.method == 'GET':
        farms = Farm.objects.all()
        serialized_farms = FarmSerializer(farms, many=True).data;
        my_json = []
        i = 0
        for farm in farms:
            my_json.append(serialized_farms[i])
            json_parcel = []
            j = 0
            parcels = farm.parcel_set.all()
            serialized_parcel = ParcelSerializer(parcels, many=True).data
            '''for parcel in parcels:
                json_parcel.append(serialized_parcel[j])
                probes = parcel.probe_set.all()
                json_parcel[j]['probes'] = ProbeSerializer(probes, many=True).data
                j = j +1
            '''    
            my_json[i]['parcels'] = json_parcel
            i = i + 1
        return JsonResponse(my_json, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        for farm in data:
            serializer = FarmSerializer(data=farm)
            if serializer.is_valid():
                serializer.save()
            for parcel in farm['parcels']:
                serializer = ParcelSerializer(data=parcel)
                if serializer.is_valid():
                    serializer.save()
                '''for probe in parcel['probes']:
                    serializer = ProbeSerializer(data=probe)
                    if serializer.is_valid():
                        serializer.save()'''
        return JsonResponse({"status":"ok"}, status=400)

def start_maintenance(request):
    if request.POST:
        subprocess.run('/usr/bin/sudo /usr/bin/systemctl start secure-tunnel@agribiot.service', shell=True)
    return render(request,'maintenance.html',{})

def stop_maintenance(request):
    if request.POST:
        subprocess.run('/usr/bin/sudo /usr/bin/systemctl stop secure-tunnel@agribiot.service', shell=True)
    return render(request,'maintenance.html',{})

class FarmList(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return Farm.objects.all()
    def get(self, request, format=None):
        farms = Farm.objects.all()
        serializer = FarmSerializer(farms, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = FarmSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class FarmDetail(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return Farm.objects.all()
    def get_farm(self, pk):
        try:
            return Farm.objects.get(pk=pk)
        except Farm.DoesNotExist:
            raise NotFound

    def get(self, request, pk, format=None):
        farm = self.get_farm(pk)
        serializer = FarmSerializer(farm)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        farm = self.get_farm(pk)
        serializer = FarmSerializer(farm, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        farm = self.get_farm(pk)
        farm.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ParcelList(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return Parcel.objects.all()
    def get(self, request, format=None):
        parcels = Parcel.objects.all()
        serializer = ParcelSerializer(parcels, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ParcelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ParcelDetail(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return Parcel.objects.all()
    def get_parcel(self, pk):
        try:
            return Parcel.objects.get(pk=pk)
        except Parcel.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        parcel = self.get_parcel(pk)
        if parcel == None:
            raise NotFound
        serializer = ParcelSerializer(parcel)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        parcel = self.get_parcel(pk)
        serializer = ParcelSerializer(parcel, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        parcel = self.get_parcel(pk)
        parcel.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



class ObjectList(APIView):
    def get_queryset(self):
        return Object.objects.all()
    def get(self, request, format=None):
        objects = get_objects_for_user(request.user,'view_object',klass=Object,accept_global_perms=True)
        serializer = ObjectSerializer(objects, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ObjectSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# todo custom permission
class ObjectToken(APIView):
    permission_classes = (ObjectTokenDjangoModelPermissions, )
    def get_queryset(self):
        return Object.objects.all()
    def get_object(self, pk):
        try:
            return Object.objects.get(pk=pk)
        except Object.DoesNotExist:
            raise NotFound;
    def get(self, request, pk, format=None):
        object = self.get_object(pk)
        if object == None:
            raise NotFound;
        token, created = Token.objects.get_or_create(user=object.user_id)
        data = {}
        data["object_id"] = object.id
        data["uuid"] = object.uuid
        data["token"] = str(token);
        return Response(data)

class ObjectInParcel(APIView):
    def get_queryset(self):
        return Object.objects.all()
    def get_object_in_parcel(self, user, pk):
        try:
            parcel = Parcel.objects.get(pk=pk)
            return  get_objects_for_user(user,'view_object',klass=Object,accept_global_perms=True).annotate(geom=Cast('position', PointField())).filter(geom__within=parcel.geometry)
        except Object.DoesNotExist:
            return None;

    def get(self, request, pk, format=None):
        object = self.get_object_in_parcel(request.user, pk)
        if object == None:
            raise NotFound;
        serializer = ObjectSerializer(object,many=True)
        return Response(serializer.data)

class ObjectDetail(APIView):
    def get_queryset(self):
        return Object.objects.all()
    def get_object(self, pk):
        try:
            return Object.objects.get(pk=pk)
        except Object.DoesNotExist:
            raise NotFound

    def get(self, request, pk, format=None):
        object = self.get_object(pk)
        serializer = ObjectSerializer(object)
        return Response(serializer.data)
    """
    def put(self, request, pk, format=None):
        object = self.get_object(pk)
        objectParsed = None;
        serializer = ObjectSerializer(objectParsed, data=request.data)
        if serializer.is_valid():
            object.position = objectParsed.position;
            object.save();
            serializer = ObjectSerializer(object);
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
"""
    def put(self, request, pk, format=None):
        object = self.get_object(pk)
        serializer = ObjectSerializer(object, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        object = self.get_object(pk)
        object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class SensorList(APIView):
    def get_queryset(self):
        return Sensor.objects.all()
    def get(self, request, format=None):
        sensors= get_objects_for_user(request.user,'view_sensor',klass=Sensor,accept_global_perms=True)
        serializer = SensorSerializer(sensors, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = SensorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class SensorDetail(APIView):
    def get_queryset(self):
        return Sensor.objects.all()

    def get(self, request, pk, format=None):
        try:
            sensor = get_objects_for_user(request.user,'view_sensor',klass=Sensor,accept_global_perms=True).get(id=pk);
            serializer = SensorSerializer(sensor)
            return Response(serializer.data)
        except Sensor.DoesNotExist:
            return forbiden_access(request)

    def put(self, request, pk, format=None):
        try:
            sensor = get_objects_for_user(request.user,'change_sensor',klass=Sensor,accept_global_perms=True).get(id=pk);
            serializer = SensorSerializer(sensor, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Sensor.DoesNotExist:
            return forbiden_access(request)

    def delete(self, request, pk, format=None):
        try:
            sensor = get_objects_for_user(request.user,'delete_sensor',klass=Sensor,accept_global_perms=True).get(id=pk)
            sensor.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Sensor.DoesNotExist:
            return forbiden_access(request)

class SensorObjectByObject(APIView):
    def get_queryset(self):
        return SensorObject.objects.all()
    def get_sensor_object_by_object(self, user, pk):
        try:
            return  get_objects_for_user(user,'view_sensorobject',klass=SensorObject,accept_global_perms=True).select_related('sensor_id', 'object_id').filter(object_id=pk)
        except SensorObject.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        sensor_objects = self.get_sensor_object_by_object(request.user,pk)
        if sensor_objects == None:
            raise NotFound
        serializer = SensorObjectJoinSerializer(sensor_objects, many=True)
        return Response(serializer.data)

class SensorObjectBySensor(APIView):
    def get_queryset(self):
        return SensorObject.objects.all()
    def get_sensor_object_by_sensor(self, user, pk):
        try:
            return  get_objects_for_user(user,'view_sensorobject',klass=SensorObject,accept_global_perms=True).select_related('sensor_id', 'object_id').filter(sensor_id=pk)
        except Object.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        sensor_objects = self.get_sensor_object_by_sensor(request.user,pk)
        if sensor_objects == None:
            raise NotFound
        serializer = SensorObjectJoinSerializer(sensor_objects, many=True)
        return Response(serializer.data)


class SensorObjectList(APIView):
    def get_queryset(self):
        return SensorObject.objects.all()
    def get(self, request, format=None):
        sensorsObjects = get_objects_for_user(request.user,'view_sensorobject',klass=SensorObject,accept_global_perms=True).select_related('sensor_id', 'object_id')
        serializer = SensorObjectJoinSerializer(sensorsObjects, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = SensorObjectSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


"""
Only list tags and allowing push sensor object for tag
"""

class SensorObjectTagList(APIView):
    parser_classes = [JSONParser]
    permission_classes = (DefaultDjangoModelPermissions, )

    def get_queryset(self):
        return SensorObject.objects.filter(sensor_id__name="tag")
    def get(self, request, format=None):
        sensorObjects = SensorObject.objects.filter(sensor_id__name="tag")
        serializer = SensorObjectJoinSerializer(sensorObjects, many=True)
        return Response(serializer.data)
    def parse_sensor_object(self,sensor_object_json):
        object_json = sensor_object_json['object_id']
        sensor = Sensor.objects.filter(name="tag").first()
        if sensor == None:
            sensor = Sensor(name="tag")
            sensor.save()
        lon = object_json['lon']
        lat = object_json['lat']
        object = Object.objects.filter(uuid=object_json['uuid']).first()
        if object == None:
            object = Object(uuid=object_json['uuid'], position="SRID=4326;POINT ("+str(lon)+" "+str(lat)+")",description=object_json['description'])
            object.save()
        else:
            object.position="SRID=4326;POINT ("+str(lon)+" "+str(lat)+")"
            object.description=object_json['description']
            object.save()

        sensorObject = SensorObject.objects.filter(object_id=object,sensor_id=sensor).first()
        if sensorObject == None:
            sensorObject = SensorObject(object_id=object,sensor_id=sensor)
            sensorObject.save()
        return sensorObject

    def post(self, request, format=None):
        data = request.data;
        sensor_object = self.parse_sensor_object(data)
        serializer = SensorObjectJoinSerializer(sensor_object)
        return Response(serializer.data,status=status.HTTP_201_CREATED)

class SensorObjectDetail(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return SensorObject.objects.all()
    def get_sensorObject(self, pk):
        try:
            return SensorObject.objects.get(pk=pk)
        except SensorObject.DoesNotExist:
            raise NotFound

    def get(self, request, pk, format=None):
        sensorObject = self.get_sensorObject(pk)
        serializer = SensorObjectSerializer(sensorObject)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        sensorObject = self.get_sensorObject(pk)
        serializer = SensorObjectSerializer(sensorObject, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        sensorObject = self.get_sensorObject(pk)
        sensorObject.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ValueBySensorObject(APIView):
    def get_queryset(self):
        return Value.objects.all()
    def get_value_by_sensor_object(self, pk):
        try:
            return Value.objects.filter(sensor_object_id=pk)
        except Value.DoesNotExist:
            return None
    def get_last_value_by_sensor_object(self, pk):
        try:
            return Value.objects.filter(sensor_object_id=pk).latest('created')
        except Value.DoesNotExist:
            return None
    def get(self, request, pk, format=None):
        if request.user.has_perm("view_all_value",SensorObject.objects.get(id=pk)) or request.user.has_perm("agribiot.view_value") :
            values = self.get_value_by_sensor_object(pk)
            if values == None:
                raise NotFound
            serializer = ValueSerializer(values, many=True)
            return Response(serializer.data)
        elif request.user.has_perm("view_last_value",SensorObject.objects.get(id=pk)):
            value = self.get_last_value_by_sensor_object(pk)
            if value == None:
                raise NotFound
            serializer = ValueSerializer(value, many=False)
            return Response([serializer.data])
        elif request.user.has_perm("view_date_range_value",SensorObject.objects.get(id=pk)):
            values = get_objects_for_user(request.user,'view_value',klass=Value,accept_global_perms=False)
            if values == None:
                raise NotFound
            serializer = ValueSerializer(values, many=True)
            return Response(serializer.data)
        else:
            return forbiden_access(request)

class ValueBySensorObjectlastN(APIView):
    def get_queryset(self):
        return Value.objects.all()
    def get_value_by_sensor_object_last_n(self, pk,n):
        try:
            return Value.objects.filter(sensor_object_id=pk).order_by('-id')[:n]
        except Value.DoesNotExist:
            return None
    def get_last_value_by_sensor_object(self, pk):
        try:
            return Value.objects.filter(sensor_object_id=pk).latest('created')
        except Value.DoesNotExist:
            return None
    def get(self, request, pk,n, format=None):
        if request.user.has_perm("view_all_value",SensorObject.objects.get(id=pk)) or request.user.has_perm("agribiot.view_value") :
            values = self.get_value_by_sensor_object_last_n(pk,n)
            if values == None:
                raise NotFound
            serializer = ValueSerializer(values, many=True)
            return Response(serializer.data)
        elif request.user.has_perm("view_last_value",SensorObject.objects.get(id=pk)):
            value = self.get_last_value_by_sensor_object(pk)
            if value == None:
                raise NotFound
            serializer = ValueSerializer(value, many=False)
            return Response([serializer.data])
        elif request.user.has_perm("view_date_range_value",SensorObject.objects.get(id=pk)):
            values = get_objects_for_user(request.user,'view_value',klass=Value,accept_global_perms=False).order_by('-id')[:n]
            if values == None:
                raise NotFound
            serializer = ValueSerializer(values, many=True)
            return Response(serializer.data)
        else:
            return forbiden_access(request)

class ValueBySensorObjectSinceDate(APIView):
    def get_queryset(self):
        return Value.objects.all()
    def get_value_by_sensor_object_since_date(self, pk, start_date):
        try:
            return Value.objects.filter(sensor_object_id=pk, created__gt=start_date)
        except Value.DoesNotExist:
            return None
    def get_last_value_by_sensor_object(self, pk):
        try:
            return Value.objects.filter(sensor_object_id=pk).latest('created')
        except Value.DoesNotExist:
            return None

    def get(self, request, pk, start_date, format=None):
        if request.user.has_perm("view_all_value",SensorObject.objects.get(id=pk)) or request.user.has_perm("agribiot.view_value"):
            datetime_start = utc.localize(start_date) 
            values = self.get_value_by_sensor_object_since_date(pk,datetime_start)
            if values == None:
                raise NotFound
            serializer = ValueSerializer(values, many=True)
            return Response(serializer.data)
        elif request.user.has_perm("view_last_value",SensorObject.objects.get(id=pk)):
            value = self.get_last_value_by_sensor_object(pk)
            if value == None:
                raise NotFound
            datetime_start = utc.localize(start_date) 
            if value.created < datetime_start:
                raise NotFound
            serializer = ValueSerializer(value, many=False)
            return Response([serializer.data])
        elif request.user.has_perm("view_date_range_value",SensorObject.objects.get(id=pk)):
            values = get_objects_for_user(request.user,'view_value',klass=Value,accept_global_perms=False)
            if values == None:
                raise NotFound
            serializer = ValueSerializer(values, many=True)
            return Response(serializer.data)
        else:
            return forbiden_access(request)


class ValueBySensorObjectRangeDate(APIView):
    def get_queryset(self):
        return Value.objects.all()
    def get_last_value_by_sensor_object(self, pk):
        try:
            return Value.objects.filter(sensor_object_id=pk).latest('created')
        except Value.DoesNotExist:
            return None
    def get(self, request, pk, start_date, end_date, format=None):
        if request.user.has_perm("view_all_value",SensorObject.objects.get(id=pk)) or request.user.has_perm("agribiot.view_value"):
            try:
                datetime_start = utc.localize(start_date) 
                datetime_end = utc.localize(end_date) 
                values = Value.objects.filter(sensor_object_id=pk, created__range=(datetime_start, datetime_end))
                serializer = ValueSerializer(values, many=True)
                return Response(serializer.data)
            except Value.DoesNotExist:
                raise NotFound
        elif request.user.has_perm("view_last_value",SensorObject.objects.get(id=pk)):
            value = self.get_last_value_by_sensor_object(pk)
            if value == None:
                raise NotFound
            datetime_start = utc.localize(start_date) 
            if value.created < datetime_start:
                raise NotFound
            serializer = ValueSerializer(value, many=False)
            return Response([serializer.data])
        elif request.user.has_perm("view_date_range_value",SensorObject.objects.get(id=pk)):
            values = get_objects_for_user(request.user,'view_value',klass=Value,accept_global_perms=False).filter(sensor_object_id=pk, created__range=(start_date, end_date))
            if values == None:
                raise NotFound
            serializer = ValueSerializer(values, many=True)
            return Response(serializer.data)
        else:
            return forbiden_access(request)


class ValueList(APIView):
    def get_queryset(self):
        return Value.objects.all()
    def get(self, request, format=None):
        values = get_objects_for_user(request.user,'view_value',klass=Value,accept_global_perms=True)
        serializer = ValueSerializer(values, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ValueSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

"""
class ValueDetail(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return Value.objects.all()
    def get_value(self, pk):
        try:
            return Value.objects.get(pk=pk)
        except Value.DoesNotExist:
            raise NotFound

    def get(self, request, pk, format=None):
        value = self.get_value(pk)
        serializer = ValueSerializer(value)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        value = self.get_value(pk)
        serializer = ValueSerializer(value, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        value = self.get_value(pk)
        value.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
"""

class AlarmTriggerList(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return AlarmTrigger.objects.all()
    def get(self, request, format=None):
        alarmTriggers = AlarmTrigger.objects.prefetch_related('alarmelement_set')
        serializer = AlarmTriggerJoinSerializer(alarmTriggers, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = AlarmTriggerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AlarmTriggerDetail(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return AlarmTrigger.objects.all()
    def get_alarmTrigger(self, pk):
        try:
            return AlarmTrigger.objects.get(pk=pk)
        except AlarmTrigger.DoesNotExist:
            raise NotFound

    def get(self, request, pk, format=None):
        alarmTrigger = self.get_alarmTrigger(pk)
        serializer = AlarmTriggerSerializer(alarmTrigger)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        alarmTrigger = self.get_alarmTrigger(pk)
        serializer = AlarmTriggerSerializer(alarmTrigger, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        alarmTrigger = self.get_alarmTrigger(pk)
        alarmTrigger.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    
class AlarmElementList(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return AlarmElement.objects.all()
    def get(self, request, format=None):
        alarmElements = AlarmElement.objects.all()
        serializer = AlarmElementSerializer(alarmElements, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = AlarmElementSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AlarmElementDetail(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return AlarmElement.objects.all()
    def get_alarmElement(self, pk):
        try:
            return AlarmElement.objects.get(pk=pk)
        except AlarmElement.DoesNotExist:
            raise NotFound

    def get(self, request, pk, format=None):
        alarmElement = self.get_alarmElement(pk)
        serializer = AlarmElementSerializer(alarmElement)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        alarmElement = self.get_alarmElement(pk)
        serializer = AlarmElementSerializer(alarmElement, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        alarmElement = self.get_alarmElement(pk)
        alarmElement.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class RisedAlarmList(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return RisedAlarm.objects.all()
    def get(self, request, format=None):
        risedAlarms = RisedAlarm.objects.all()
        serializer = RisedAlarmSerializer(risedAlarms, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = RisedAlarmSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class RisedAlarmDetail(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return RisedAlarm.objects.all()
    def get_risedAlarm(self, pk):
        try:
            return RisedAlarm.objects.get(pk=pk)
        except RisedAlarm.DoesNotExist:
            raise NotFound

    def get(self, request, pk, format=None):
        risedAlarm = self.get_risedAlarm(pk)
        serializer = RisedAlarmSerializer(risedAlarm)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        risedAlarm = self.get_risedAlarm(pk)
        serializer = RisedAlarmSerializer(risedAlarm, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        risedAlarm = self.get_risedAlarm(pk)
        risedAlarm.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ForeignFarmUserToken(APIView):
    permission_classes = (ForeignUserTokenDjangoModelPermissions, )
    def get_queryset(self):
        return ForeignFarmUser.objects.all()
    def get_user(self, pk):
        try:
            return ForeignFarmUser.objects.get(pk=pk)
        except ForeignFarmUser.DoesNotExist:
            raise NotFound
    def get(self, request, pk, format=None):
        foreignFarmUser = self.get_user(pk)
        if foreignFarmUser == None:
            raise NotFound;
        token, created = Token.objects.get_or_create(user=foreignFarmUser)
        data = {}
        data["user_id"] = foreignFarmUser.id
        data["token"] = str(token);
        return Response(data)


class ForeignFarmUserListView(PermissionRequiredMixin, ListView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.view_foreignfarmuser'
    model = ForeignFarmUser
    paginate_by = 100  # if pagination is desired
    def get_queryset(self):
        return ForeignFarmUser.objects.all()

class ForeignFarmUserCreate(PermissionRequiredMixin, CreateView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.add_foreignfarmuser'
    model = ForeignFarmUser
    form_class = ForeignFarmUserForm
    def get_queryset(self):
        return ForeignFarmUser.objects.all()

class ForeignFarmUserDelete(DeleteView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.delete_foreignfarmuser'
    model = ForeignFarmUser
    success_url = reverse_lazy('foreignfarmuser_list')

class RemoteAccessListView(PermissionRequiredMixin, ListView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.view_remoteaccess'
    model = RemoteAccess
    paginate_by = 100  # if pagination is desired
    def get_queryset(self):
        return RemoteAccess.objects.all()


class RemoteAccessCreate(CreateView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.add_remoteaccess'
    model = RemoteAccess
    form_class = RemoteAccessForm
    def get_queryset(self):
        return RemoteAccess.objects.all()

    def get_context(self, **kwargs):
        context = {'form': RemoteAccessForm()}
        return context


class RemoteAccessDelete(DeleteView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.delete_remoteaccess'
    model = RemoteAccess
    success_url = reverse_lazy('remoteaccess_list')

class RemoteInstanceListView(PermissionRequiredMixin, ListView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.view_remoteinstance'
    model = RemoteInstance
    paginate_by = 100  # if pagination is desired
    def get_queryset(self):
        return RemoteInstance.objects.all()

class RemoteInstanceCreate(PermissionRequiredMixin, CreateView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.add_remoteinstance'
    model = RemoteInstance
    form_class = RemoteInstanceForm
    def get_queryset(self):
        return RemoteInstance.objects.all()

class RemoteInstanceDelete(DeleteView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.delete_remoteinstance'
    model = RemoteInstance
    success_url = reverse_lazy('remoteinstance_list')


class RemoteInstanceList(APIView):
    def get_queryset(self):
        return RemoteInstance.objects.all()
    def get(self, request, format=None):
        remoteInstances = get_objects_for_user(request.user,'view_remoteinstance',klass=RemoteInstance,accept_global_perms=True)
        serializer = RemoteInstanceSerializer(remoteInstances, many=True)
        return Response(serializer.data)

class TaskListView(PermissionRequiredMixin, ListView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.view_annotationtemplate'
    model = AnnotationTemplate
    paginate_by = 100  # if pagination is desired
    def get_queryset(self):
        return AnnotationTemplate.objects.filter(annotation_type="TASK")
    def get_context_data(self,**kwargs):
        context = super(TaskListView,self).get_context_data(**kwargs)
        context['ctx'] = "Task"
        return context


class TaskCreate(PermissionRequiredMixin, CreateView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.add_annotationtemplate'
    model = AnnotationTemplate
    form_class = TaskForm
    def get_queryset(self):
        return AnnotationTemplate.objects.all()
    def get_context(self, **kwargs):
        context = {'form': TaskForm()}
        return context
    def get_context_data(self,**kwargs):
        context = super(TaskCreate,self).get_context_data(**kwargs)
        context['ctx'] = "Task"
        return context
    def form_valid(self, form):
        form.instance.annotation_type =  AnnotationType.TASK
        return super().form_valid(form)

class TaskDelete(DeleteView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.delete_annotationtemplate'
    model = AnnotationTemplate
    success_url = reverse_lazy('task_list')
    def get_context_data(self,**kwargs):
        context = super(TaskDelete,self).get_context_data(**kwargs)
        context['ctx'] = "Task"
        return context

class TaskList(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return AnnotationTemplate.objects.filter(annotation_type="TASK")
    def get(self, request, format=None):
        tasks =  AnnotationTemplate.objects.filter(annotation_type="TASK")
        serializer = AnnotationTemplateSerializer(tasks, many=True)
        return Response(serializer.data)


class ObservationListView(PermissionRequiredMixin, ListView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.view_annotationtemplate'
    model = AnnotationTemplate
    paginate_by = 100  # if pagination is desired
    def get_queryset(self):
        return AnnotationTemplate.objects.filter(annotation_type="OBSV")
    def get_context_data(self,**kwargs):
        context = super(ObservationListView,self).get_context_data(**kwargs)
        context['ctx'] = "Observation"
        return context

class ObservationCreate(PermissionRequiredMixin, CreateView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.add_annotationtemplate'
    model = AnnotationTemplate
    form_class = ObservationForm
    def get_queryset(self):
        return AnnotationTemplate.objects.all()
    def get_context(self, **kwargs):
        context = {'form': ObservationForm()}
        return context
    def get_context_data(self,**kwargs):
        context = super(ObservationCreate,self).get_context_data(**kwargs)
        context['ctx'] = "Observation"
        return context
    def form_valid(self, form):
        form.instance.annotation_type =  AnnotationType.OBSERVATION
        return super().form_valid(form)

class ObservationDelete(DeleteView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.delete_annotationtemplate'
    model = AnnotationTemplate
    success_url = reverse_lazy('observation_list')
    def get_context_data(self,**kwargs):
        context = super(ObservationDelete,self).get_context_data(**kwargs)
        context['ctx'] = "Observation"
        return context

class AnnotationTemplateList(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return AnnotationTemplate.objects.all()
    def get(self, request, format=None):
        annotationTemplates = AnnotationTemplate.objects.all()
        serializer = AnnotationTemplateSerializer(annotationTemplates, many=True)
        return Response(serializer.data)
    def post(self, request, format=None):
        if isinstance(request.data, dict):
            serializer = AnnotationTemplateSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        elif isinstance(request.data, list):
            serializer = AnnotationTemplateSerializer(data=request.data, many=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class DimensionValueListView(ExportMixin, SingleTableMixin, FilterView):
    permission_classes = (DefaultDjangoModelPermissions, )
    model = DimensionValue
    table_class = DimensionValueTable
    template_name = 'agribiot/dimensions.html'
    filterset_class = DimensionValueFilter



class OperationListFiltered(ListAPIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    #filterset_fields = ['annotation_template_id','sensor_object_id','accuracy']
    filterset_class = OperationFilter
    filter_backends = [DjangoFilterBackend, ]
    serializer_class = OperationJoinTidSerializer
    def get_queryset(self):
        return Operation.objects.all()



class OperationList(APIView):
    parser_classes = [JSONParser]
    permission_classes = (DefaultDjangoModelPermissions, )

    def get_queryset(self):
        return Operation.objects.all()
    def get(self, request, format=None):
        operations = Operation.objects.all()
        serializer = OperationJoinTidSerializer(operations, many=True)
        return Response(serializer.data)
        
    def parse_operation(self,operation_json):
        lon = 0.0
        lat = 0.0
        accuracy = 0.0
        if 'lon' in operation_json and 'lat' in operation_json and 'accuracy' in operation_json:
            lon = operation_json['lon']
            lat = operation_json['lat']
            accuracy=operation_json['accuracy']

        sensorObject = SensorObject.objects.get(id=operation_json['sensor_object_id'])
        annotationTemplate= AnnotationTemplate.objects.get(id=operation_json['annotation_template_id'])
        operationImage = OperationImage.objects.filter(id=operation_json['image_id']).first()
        operation = Operation.objects.filter(annotation_template_id=annotationTemplate,created=operation_json['created'],sensor_object_id=sensorObject).first()
        if operation == None:
            operation = Operation(sensor_object_id=sensorObject,
                                  tag_embedded_task_index = operation_json['tag_embedded_task_index'],
                                  created=operation_json['created'],
                                  annotation_template_id=annotationTemplate,
                                  position="SRID=4326;POINT ("+str(lon)+" "+str(lat)+")",
                                  accuracy=accuracy);
            operation.save()
            if operationImage != None:
                operation.images.add(operationImage)
        return operation

    def post(self, request, format=None):
        data = request.data;
        operation = self.parse_operation(data);
        serializer = OperationJoinTidSerializer(operation)
        return Response(serializer.data,status=status.HTTP_201_CREATED)

class OperationBySensorObject(APIView):
    def get_queryset(self):
        return Operation.objects.all()
    def get_operation_by_sensor_object(self, pk):
        try:
            return Operation.objects.filter(sensor_object_id=pk)
        except Operation.DoesNotExist:
            return None
    def get_last_operation_by_sensor_object(self, pk):
        try:
            return Operation.objects.filter(sensor_object_id=pk).latest('created')
        except Operation.DoesNotExist:
            return None
    def get(self, request, pk, format=None):
        if request.user.has_perm("view_all_operation",SensorObject.objects.get(id=pk)) or request.user.has_perm("agribiot.view_operation") :
            operations = self.get_operation_by_sensor_object(pk)
            if operations == None:
                raise NotFound
            serializer = OperationJoinSerializer(operations, many=True)
            return Response(serializer.data)
        elif request.user.has_perm("view_last_operation",SensorObject.objects.get(id=pk)):
            operation = self.get_last_operation_by_sensor_object(pk)
            if operation == None:
                raise NotFound
            serializer = OperationJoinSerializer(operation, many=False)
            return Response([serializer.data])
        elif request.user.has_perm("view_date_range_operation",SensorObject.objects.get(id=pk)):
            operations = get_objects_for_user(request.user,'view_operation',klass=Operation,accept_global_perms=False)
            if operations == None:
                raise NotFound
            serializer = OperationJoinSerializer(operations, many=True)
            return Response(serializer.data)
        else:
            return forbiden_access(request)


class OperationDetail(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return Operation.objects.all()
    def get_operation(self, pk):
        try:
            return Operation.objects.get(pk=pk)
        except Operation.DoesNotExist:
            raise NotFound

    def get(self, request, pk, format=None):
        operation = self.get_operation(pk)
        serializer = OperationJoinTidSerializer(operation)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        operation = self.get_operation(pk)
        serializer = OperationJoinTidSerializer(operation, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        operation = self.get_operation(pk)
        operation.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class OperationImageDetail(APIView):
    def get_queryset(self):
        return OperationImage.objects.all()
    def get(self, request, pk, format=None):
        try:
            operationImage = get_objects_for_user(request.user,'view_operationimage',klass=OperationImage,accept_global_perms=True).get(id=pk);
            serializer = OperationImageSerializer(operationImage)
            return Response(serializer.data)
        except OperationImage.DoesNotExist:
            return forbiden_access(request)

class OperationImageList(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return OperationImage.objects.all()
    def get(self, request, format=None):
        operationsImages = OperationImage.objects.all()
        serializer = OperationImageSerializer(operationsImages, many=True)
        return Response(serializer.data)
    def post(self, request, format=None):
        serializer = OperationImageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GatewayUserListView(PermissionRequiredMixin, ListView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.view_gatewayuser'
    model = GatewayUser
    paginate_by = 100  # if pagination is desired
    def get_queryset(self):
        return GatewayUser.objects.all()

class GatewayUserCreate(PermissionRequiredMixin, CreateView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.add_gatewayuser'
    model = GatewayUser
    form_class = GatewayUserForm
    def get_queryset(self):
        return GatewayUser.objects.all()

class GatewayUserDelete(DeleteView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.delete_gatewayuser'
    model = GatewayUser
    success_url = reverse_lazy('gatewayuser_list')

class GatewayUserToken(APIView):
    permission_classes = (GatewayUserTokenDjangoModelPermissions, )
    def get_queryset(self):
        return GatewayUser.objects.all()
    def get_user(self, pk):
        try:
            return GatewayUser.objects.get(pk=pk)
        except GatewayUser.DoesNotExist:
            raise NotFound
    def get(self, request, pk, format=None):
        gatewayUser = self.get_user(pk)
        if gatewayUser == None:
            raise NotFound
        token, created = Token.objects.get_or_create(user=gatewayUser)
        data = {}
        data["user_id"] = gatewayUser.id
        data["token"] = str(token)
        return Response(data)


class RFIDDeviceUserListView(PermissionRequiredMixin, ListView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.view_rfiddeviceuser'
    model = RFIDDeviceUser
    paginate_by = 100  # if pagination is desired
    def get_queryset(self):
        return RFIDDeviceUser.objects.all()

class RFIDDeviceUserCreate(PermissionRequiredMixin, CreateView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.add_rfiddeviceuser'
    model = RFIDDeviceUser
    form_class = RFIDDeviceUserForm
    def get_queryset(self):
        return RFIDDeviceUser.objects.all()

class RFIDDeviceUserDelete(DeleteView):
    permission_classes = (DefaultDjangoModelPermissions, )
    permission_required = 'agribiot.delete_rfiddeviceuser'
    model = RFIDDeviceUser
    success_url = reverse_lazy('rfiddeviceuser_list')

class RFIDDeviceUserToken(APIView):
    permission_classes = (RFIDDeviceUserTokenDjangoModelPermissions, )
    def get_queryset(self):
        return RFIDDeviceUser.objects.all()
    def get_user(self, pk):
        try:
            return RFIDDeviceUser.objects.get(pk=pk)
        except RFIDDeviceUser.DoesNotExist:
            raise NotFound
    def get(self, request, pk, format=None):
        rfidDeviceUser = self.get_user(pk)
        if rfidDeviceUser == None:
            raise NotFound;
        token, created = Token.objects.get_or_create(user=rfidDeviceUser)
        data = {}
        data["user_id"] = rfidDeviceUser.id
        data["token"] = str(token);
        return Response(data)


class DimensionValueListFiltered(ListAPIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    filterset_fields = ['dimension_id','value']
    filter_backends = [DjangoFilterBackend, OrderingFilter ]
    serializer_class = DimensionValueSerializer
    def get_queryset(self):
        return DimensionValue.objects.all()



class DimensionValueList(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return DimensionValue.objects.all()
    def get(self, request, format=None):
        dimensionValues = DimensionValue.objects.all()
        serializer = DimensionValueSerializer(dimensionValues, many=True)
        return Response(serializer.data)
    def post(self, request, format=None):
        serializer = DimensionValueSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DimensionValueByAnnotationTemplate(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return DimensionValue.objects.all()
    def get(self, request, pk, format=None):
        dimensionValues = DimensionValue.objects.filter(operation_id__annotation_template_id__id=pk)
        serializer = DimensionValueSerializer(dimensionValues, many=True)
        return Response(serializer.data)

class DimensionValueDetail(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return DimensionValue.objects.all()
    def get_dimensionValue(self, pk):
        try:
            return DimensionValue.objects.get(pk=pk)
        except DimensionValue.DoesNotExist:
            raise NotFound

    def get(self, request, pk, format=None):
        dimensionValue = self.get_dimensionValue(pk)
        serializer = DimensionValueSerializer(dimensionValue)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        dimensionValue = self.get_dimensionValue(pk)
        serializer = DimensionValueSerializer(dimensionValue, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        dimensionValue = self.get_dimensionValue(pk)
        dimensionValue.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class DimensionByAnnotationTemplate(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return Dimension.objects.all()
    def get(self, request, pk, format=None):
        dimensions = Dimension.objects.filter(dimensionvalue__operation_id__annotation_template_id__id=pk).distinct()
        serializer = DimensionSerializer(dimensions, many=True)
        return Response(serializer.data)

class DimensionList(APIView):
    permission_classes = (DefaultDjangoModelPermissions, )
    def get_queryset(self):
        return Dimension.objects.all()
    def get(self, request, format=None):
        dimensions = Dimension.objects.all()
        serializer = DimensionSerializer(dimensions, many=True)
        return Response(serializer.data)
    def post(self, request, format=None):
        serializer = DimensionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
"""
        f = open(SITE_ROOT+"/../res/rfid_pub.key","r")
        public_key = f.read()
        f.close()
        f = open(SITE_ROOT+"/../res/rfid.key","r")
        private_key = f.read()
        f.close()
        data["key"] = str(private_key);
        data["pub_key"] = str(public_key);
"""
