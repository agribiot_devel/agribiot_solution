#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
from django import template
from django.contrib.auth.models import Group 

register = template.Library()

@register.filter(name='has_group')
def has_group(user, group_name): 
    group = Group.objects.get(name=group_name) 
    return True if group in user.groups.all() else False
