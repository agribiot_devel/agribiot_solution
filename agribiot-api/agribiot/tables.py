# tutorial/tables.py
import django_tables2 as tables
from .models import Operation
from .models import DimensionValue

import itertools

class DimensionValueTable(tables.Table):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.counter = itertools.count()
    class Meta:
        model = DimensionValue
        template_name = "django_tables2/bootstrap.html"
        fields = ("operation_id.created","operation_id.sensor_object_id.object_id.uuid","operation_id.annotation_template_id.description", "dimension_id.name", "value" )


