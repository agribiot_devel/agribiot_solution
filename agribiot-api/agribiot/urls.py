#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
from django.conf import settings
from django.conf.urls import url
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.urls import path, register_converter
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from agribiot import views


admin.autodiscover()
admin.site.login = login_required(admin.site.login)

from datetime import datetime

class DateConverter:
    regex = '\d{4}-\d{2}-\d{2}'

    def to_python(self, value):
        return datetime.strptime(value, '%Y-%m-%d')

    def to_url(self, value):
        return value

class DateTimeConverter:
    regex = '\d{4}-\d{2}-\d{2}-\d{2}-\d{2}'

    def to_python(self, value):
        return datetime.strptime(value, '%Y-%m-%d-%H-%M')

    def to_url(self, value):
        return value

register_converter(DateConverter, 'yyyy')

register_converter(DateTimeConverter, 'yyyyhm')

urlpatterns = [
    url(r'^$', login_required(TemplateView.as_view(template_name='index.html')), name='home'),
    url(r'^maintenance.html', login_required(TemplateView.as_view(template_name='maintenance.html')), name='maintenance'),
    url(r'^create_alarm.html', login_required(TemplateView.as_view(template_name='create_alarm.html')), name='create_alarm'),
    url(r'^alarm.html', login_required(TemplateView.as_view(template_name='alarm.html')), name='alarm'),
    url(r'^manage_object.html', login_required(TemplateView.as_view(template_name='manage_object.html')), name='manage_object'),
    url(r'^manage_access.html', login_required(TemplateView.as_view(template_name='manage_access.html')), name='manage_access'),
    url(r'^sign-in.html', TemplateView.as_view(template_name='sign-in.html'), name='sign-in'),
    url(r'^operations.html', login_required(TemplateView.as_view(template_name='operations.html')), name='operations'),
    url(r'^admin/', admin.site.urls),
    url(r'^start_maintenance/$', views.start_maintenance, name='start_maintenance'),
    url(r'^stop_maintenance/$', views.stop_maintenance, name='stop_maintenance'),
    path('accounts/login/', auth_views.LoginView.as_view(redirect_authenticated_user=True), name='login'),
    path('accounts/logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('accounts/change-password/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('accounts/change-password/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('accounts/password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('accounts/password_reset/done', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('accounts/reset/<uidb64>/<token>/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_confirm'),
    path('accounts/reset/done', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_complete'),
    path('api/parcel/', views.ParcelList.as_view()),
    path('api/object/', views.ObjectList.as_view()),
    path('api/object_in_parcel/<int:pk>/', views.ObjectInParcel.as_view()),
    path('api/object/<int:pk>/', views.ObjectDetail.as_view()),
    path('api/object_token/<int:pk>/',views.ObjectToken.as_view()),
    path('api/sensor/', views.SensorList.as_view()),
    path('api/sensor_object/', views.SensorObjectList.as_view()),
    path('api/sensor_object_tag/',views.SensorObjectTagList.as_view()),
    path('api/sensor_object/<int:pk>/', views.SensorObjectDetail.as_view()),
    path('api/sensor_object_by_object/<int:pk>/',views.SensorObjectByObject.as_view()),
    path('api/value_by_sensor_object_last_n/<int:pk>/<int:n>/',views.ValueBySensorObjectlastN.as_view()),
    path('api/value_by_sensor_object_since_date/<int:pk>/<yyyyhm:start_date>/', views.ValueBySensorObjectSinceDate.as_view()),
    path('api/value_by_sensor_object_range_date/<int:pk>/<yyyyhm:start_date>/<yyyyhm:end_date>/', views.ValueBySensorObjectRangeDate.as_view()),
    path('api/value/', views.ValueList.as_view()),
    path('api/value_by_sensor_object/<int:pk>/', views.ValueBySensorObject.as_view()),
    path('api/alarm_trigger/', views.AlarmTriggerList.as_view()),
    path('api/alarm_trigger/<int:pk>/', views.AlarmTriggerDetail.as_view()),
    path('api/alarm_element/', views.AlarmElementList.as_view()),
    path('api/alarm_element/<int:pk>/', views.AlarmElementDetail.as_view()),
    path('api/rised_alarm/', views.RisedAlarmList.as_view()),
    path('api/rised_alarm/<int:pk>/', views.RisedAlarmDetail.as_view()),
    path('foreignfarmuser_list.html', login_required(views.ForeignFarmUserListView.as_view()), name='foreignfarmuser_list'),
    path('foreignfarmuser_create.html', login_required(views.ForeignFarmUserCreate.as_view(success_url="foreignfarmuser_list.html")), name='foreignfarmuser_create'),
    path('foreignfarmuser_delete/<int:pk>/', login_required(views.ForeignFarmUserDelete.as_view()), name='foreignfarmuser_delete'),
    path('api/foreignfarmuser_token/<int:pk>/',views.ForeignFarmUserToken.as_view()),
    path('remoteaccess_list.html', login_required(views.RemoteAccessListView.as_view()), name='remoteaccess_list'),
    path('remoteaccess_create.html', login_required(views.RemoteAccessCreate.as_view(success_url="remoteaccess_create.html")), name='remoteaccess_create'),
    path('remoteaccess_delete/<int:pk>/', login_required(views.RemoteAccessDelete.as_view()), name='remoteaccess_delete'),
    path('remoteinstance_list.html', login_required(views.RemoteInstanceListView.as_view()), name='remoteinstance_list'),
    path('remoteinstance_create.html', login_required(views.RemoteInstanceCreate.as_view(success_url="remoteinstance_list.html")), name='foreignfarmuser_create'),
    path('remoteinstance_delete/<int:pk>/', login_required(views.RemoteInstanceDelete.as_view()), name='remoteinstance_delete'),
    path('api/remoteinstance/', views.RemoteInstanceList.as_view()),
    path('task_list.html', login_required(views.TaskListView.as_view()), name='task_list'),
    path('task_create.html', login_required(views.TaskCreate.as_view(success_url="task_list.html")), name='task_create'),
    path('task_delete/<int:pk>/', login_required(views.TaskDelete.as_view()), name='task_delete'),
    path('observation_list.html', login_required(views.ObservationListView.as_view()), name='observation_list'),
    path('observation_create.html', login_required(views.ObservationCreate.as_view(success_url="observation_list.html")), name='observation_create'),
    path('observation_delete/<int:pk>/', login_required(views.ObservationDelete.as_view()), name='observation_delete'),
    path('api/task/', views.TaskList.as_view()),
    path('api/annotation_template/', views.AnnotationTemplateList.as_view()),
    path('api/operation/', views.OperationList.as_view()),
    path('api/operation/<int:pk>/', views.OperationDetail.as_view()),
    path('api/operation_filtered/', views.OperationListFiltered.as_view()),
    path('api/operation_by_sensor_object/<int:pk>/',views.OperationBySensorObject.as_view()),
    path('api/operation_image/',views.OperationImageList.as_view()),
    path('api/operation_image/<int:pk>/', views.OperationImageDetail.as_view()),

    path('api/dimension/',views.DimensionList.as_view()),
    path('api/dimension_by_annotation_template/<int:pk>/',views.DimensionByAnnotationTemplate.as_view()),
    path('api/dimension_value_filtered/',views.DimensionValueListFiltered.as_view()),

    path('api/dimension_value/',views.DimensionValueList.as_view()),
    path('api/dimension_value/<int:pk>/',views.DimensionValueDetail.as_view()),
    path('api/dimension_value_by_annotation_template/<int:pk>/',views.DimensionValueByAnnotationTemplate.as_view()),

    path('gatewayuser_list.html', login_required(views.GatewayUserListView.as_view()), name='gatewayuser_list'),
    path('gatewayuser_create.html', login_required(views.GatewayUserCreate.as_view(success_url="gatewayuser_list.html")), name='gatewayuser_create'),
    path('gatewayuser_delete/<int:pk>/', login_required(views.GatewayUserDelete.as_view()), name='gatewayuser_delete'),
    path('api/gatewayuser_token/<int:pk>/',views.GatewayUserToken.as_view()),
    path('rfiddeviceuser_list.html', login_required(views.RFIDDeviceUserListView.as_view()), name='rfiddeviceuser_list'),
    path('rfiddeviceuser_create.html', login_required(views.RFIDDeviceUserCreate.as_view(success_url="rfiddeviceuser_list.html")), name='rfiddeviceuser_create'),
    path('rfiddeviceuser_delete/<int:pk>/', login_required(views.RFIDDeviceUserDelete.as_view()), name='rfiddeviceuser_delete'),
    path('api/rfiddeviceuser_token/<int:pk>/',views.RFIDDeviceUserToken.as_view()),

    ] 


'''
accounts/password_reset/ [name='password_reset']
accounts/password_reset/done/ [name='password_reset_done']
accounts/reset/<uidb64>/<token>/ [name='password_reset_confirm']
accounts/reset/done/ [name='password_reset_complete']
'''

"""
    path('api/value/<int:pk>/', views.ValueDetail.as_view()),
    path('api/sensor_object_by_sensor/<int:pk>/',views.SensorObjectBySensor.as_view()),
    path('api/farm_dump/', views.farm_dump),
    path('api/farm/', views.FarmList.as_view()),
    path('api/farm/<int:pk>/', views.FarmDetail.as_view()),
    path('api/parcel/<int:pk>/', views.ParcelDetail.as_view()),
    path('api/sensor/<int:pk>/', views.SensorDetail.as_view()),
"""
