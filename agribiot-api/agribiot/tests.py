#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#

from django.test import TestCase
from django.db import connection
# Create your tests here.


from agribiot.models import Farm
from agribiot.models import Parcel
from agribiot.models import Object
from agribiot.models import Sensor
from agribiot.models import SensorObject
from agribiot.models import Value
from agribiot.models import AlarmTrigger
from agribiot.models import AlarmElement
from agribiot.models import RisedAlarm
from datetime import datetime


def setup_database():
    with connection.cursor() as c:
        c.execute("CREATE EXTENSION plpython3u;");


class AlarmTestCase(TestCase):
    def setUp(self):
        setup_database()
    # Test if value on sensor 1 rise alarm with 
    # > 
    # >= 
    # < 
    # <= 
    # =
    def test_alarm_sup(self):
        # Create a sensor object 
        object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3298044204711914 44.53526920483945)")
        object1.save()
        sensor1 = Sensor(name="Température",unit="°C");
        sensor1.save();
        sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1);
        sensorObject1.save()
        alarmTrigger=AlarmTrigger();
        alarmTrigger.save();
        alarmEl = AlarmElement(sensor_id=sensor1,trigger_type=">",trigger_value=21,alarm_trigger_id=alarmTrigger).save();
        Value(value=19, sensor_object_id=sensorObject1).save();
        Value(value=21, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject1).save();
        self.assertEqual(AlarmTrigger.objects.all().count(),1)
        self.assertEqual(Value.objects.all().count(),3)
        self.assertEqual(RisedAlarm.objects.all().count(),1)

    def test_alarm_sup_equal(self):
        # Create a sensor object 
        object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3298044204711914 44.53526920483945)")
        object1.save()
        sensor1 = Sensor(name="Température",unit="°C");
        sensor1.save();
        sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1);
        sensorObject1.save()
        alarmTrigger=AlarmTrigger();
        alarmTrigger.save();
        alarmEl = AlarmElement(sensor_id=sensor1,trigger_type=">=",trigger_value=21,alarm_trigger_id=alarmTrigger).save();
        Value(value=19, sensor_object_id=sensorObject1).save();
        Value(value=21, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject1).save();
        self.assertEqual(AlarmTrigger.objects.all().count(),1)
        self.assertEqual(Value.objects.all().count(),3)
        self.assertEqual(RisedAlarm.objects.all().count(),2)

    def test_alarm_inf(self):
        # Create a sensor object 
        object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3298044204711914 44.53526920483945)")
        object1.save()
        sensor1 = Sensor(name="Température",unit="°C");
        sensor1.save();
        sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1);
        sensorObject1.save()
        alarmTrigger=AlarmTrigger();
        alarmTrigger.save();
        alarmEl = AlarmElement(sensor_id=sensor1,trigger_type="<",trigger_value=21,alarm_trigger_id=alarmTrigger).save();
        Value(value=19, sensor_object_id=sensorObject1).save();
        Value(value=21, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject1).save();
        self.assertEqual(AlarmTrigger.objects.all().count(),1)
        self.assertEqual(Value.objects.all().count(),3)
        self.assertEqual(RisedAlarm.objects.all().count(),1)

    def test_alarm_inf_equal(self):
        # Create a sensor object 
        object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3298044204711914 44.53526920483945)")
        object1.save()
        sensor1 = Sensor(name="Température",unit="°C");
        sensor1.save();
        sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1);
        sensorObject1.save()
        alarmTrigger=AlarmTrigger();
        alarmTrigger.save();
        alarmEl = AlarmElement(sensor_id=sensor1,trigger_type="<=",trigger_value=21,alarm_trigger_id=alarmTrigger).save();
        Value(value=19, sensor_object_id=sensorObject1).save();
        Value(value=21, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject1).save();
        self.assertEqual(AlarmTrigger.objects.all().count(),1)
        self.assertEqual(Value.objects.all().count(),3)
        self.assertEqual(RisedAlarm.objects.all().count(),2)

    def test_alarm_equal(self):
        # Create a sensor object 
        object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3298044204711914 44.53526920483945)")
        object1.save()
        sensor1 = Sensor(name="Température",unit="°C");
        sensor1.save();
        sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1);
        sensorObject1.save()
        alarmTrigger=AlarmTrigger();
        alarmTrigger.save();
        alarmEl = AlarmElement(sensor_id=sensor1,trigger_type="=",trigger_value=21,alarm_trigger_id=alarmTrigger).save();
        Value(value=19, sensor_object_id=sensorObject1).save();
        Value(value=21, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject1).save();
        self.assertEqual(AlarmTrigger.objects.all().count(),1)
        self.assertEqual(Value.objects.all().count(),3)
        self.assertEqual(RisedAlarm.objects.all().count(),1)

   # Test if value on sensor 1 and sensor 2 rise alarm only for value > on sensor 1

    
    def test_alarm_simple_sensor(self):
        # Create a sensor object 
        object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3298044204711914 44.53526920483945)")
        object1.save()
        sensor1 = Sensor(name="Température",unit="°C");
        sensor1.save();
        sensor2 = Sensor(name="Humidité",unit="%");
        sensor2.save();
        sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1);
        sensorObject1.save()
        sensorObject2 = SensorObject(object_id=object1,sensor_id=sensor2);
        sensorObject2.save()

        alarmTrigger=AlarmTrigger();
        alarmTrigger.save();
        alarmEl = AlarmElement(sensor_id=sensor1,trigger_type=">",trigger_value=21,alarm_trigger_id=alarmTrigger).save();
        Value(value=19, sensor_object_id=sensorObject1).save();
        Value(value=21, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject2).save();

        self.assertEqual(AlarmTrigger.objects.all().count(),1)
        self.assertEqual(Value.objects.all().count(),4)
        self.assertEqual(RisedAlarm.objects.all().count(),1)


   # Test if value on sensor 1 object 1 and sensor 1 object 2 rise alarm only for value > on sensor 1 object 1
    
    def test_alarm_simple_sensor_object(self):
        # Create a sensor object 
        object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3298044204711914 44.53526920483945)")
        object1.save()
        object2 = Object(uuid="0002", position="SRID=4326;POINT (-0.324246883392334 44.53427499318332)")
        object2.save()
        sensor1 = Sensor(name="Température",unit="°C");
        sensor1.save();
        sensor2 = Sensor(name="Humidité",unit="%");
        sensor2.save();
        sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1);
        sensorObject1.save()
        sensorObject2 = SensorObject(object_id=object2,sensor_id=sensor1);
        sensorObject2.save()

        alarmTrigger=AlarmTrigger();
        alarmTrigger.save();
        alarmEl = AlarmElement(sensor_id=sensor1,object_id=object1,trigger_type=">",trigger_value=21,alarm_trigger_id=alarmTrigger).save();
        Value(value=19, sensor_object_id=sensorObject1).save();
        Value(value=21, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject2).save();

        self.assertEqual(AlarmTrigger.objects.all().count(),1)
        self.assertEqual(Value.objects.all().count(),4)
        print("rised alarm count" + str(RisedAlarm.objects.all().count()))
        self.assertEqual(RisedAlarm.objects.all().count(),1)
        
        
   # Test geo alarm on sensor id 1, object 1 and 2 should rise alarm, 3 not
    
    def test_alarm_geo_sensor(self):
        # Create a sensor object 
        object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3298044204711914 44.53526920483945)")
        object1.save()
        object2 = Object(uuid="0002", position="SRID=4326;POINT (-0.324246883392334 44.53427499318332)")
        object2.save()
        object3 = Object(uuid="0003", position="SRID=4326;POINT (-0.332293510390771 44.53226356967177)")
        object3.save()

        sensor1 = Sensor(name="Température",unit="°C");
        sensor1.save();
        sensor2 = Sensor(name="Humidité",unit="%");
        sensor2.save();
        
        sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1);
        sensorObject1.save()
        sensorObject2 = SensorObject(object_id=object2,sensor_id=sensor1);
        sensorObject2.save()
        sensorObject3 = SensorObject(object_id=object3,sensor_id=sensor1);
        sensorObject3.save()
        sensorObject4 = SensorObject(object_id=object1,sensor_id=sensor2);
        sensorObject4.save()

        alarmTrigger=AlarmTrigger();
        alarmTrigger.save();
        alarmEl = AlarmElement(sensor_id=sensor1,trigger_type=">",trigger_value=21,alarm_trigger_id=alarmTrigger,geometry="SRID=4326;POLYGON ((-0.3301296 44.5320302, -0.3299398 44.5319698, -0.3299007 44.5320147, -0.3289336 44.5317099, -0.3288858 44.5316792, -0.3248068 44.5303878, -0.3246711 44.5303881, -0.323601 44.53449, -0.3311754 44.5358754, -0.3314803 44.5355151, -0.3303854 44.5352196, -0.3312454 44.5337272, -0.3311367 44.5336972, -0.3311431 44.5336865, -0.3294704 44.5331927, -0.3301296 44.5320302))").save();
        Value(value=19, sensor_object_id=sensorObject1).save();
        Value(value=21, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject2).save();
        Value(value=21.1, sensor_object_id=sensorObject3).save();
        Value(value=50.1, sensor_object_id=sensorObject4).save();

        self.assertEqual(AlarmTrigger.objects.all().count(),1)
        self.assertEqual(Value.objects.all().count(),6)
        self.assertEqual(RisedAlarm.objects.all().count(),2)
        
        
        
        # Test multiple alarm
    def test_alarm_multiple(self):
        # Create a sensor object 
        object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3298044204711914 44.53526920483945)")
        object1.save()
        object2 = Object(uuid="0002", position="SRID=4326;POINT (-0.324246883392334 44.53427499318332)")
        object2.save()
        object3 = Object(uuid="0003", position="SRID=4326;POINT (-0.332293510390771 44.53226356967177)")
        object3.save()
        sensor1 = Sensor(name="Température",unit="°C");
        sensor1.save();
        sensor2 = Sensor(name="Humidité",unit="%");
        sensor2.save();
        
        
        sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1); # object 1 temperature
        sensorObject1.save()
        sensorObject2 = SensorObject(object_id=object2,sensor_id=sensor1); # object 2 temperature, same geo area than object 1
        sensorObject2.save()
        sensorObject3 = SensorObject(object_id=object3,sensor_id=sensor2); # object 3 humidity outside geo area than object 1 and 2 
        sensorObject3.save()
        sensorObject4 = SensorObject(object_id=object1,sensor_id=sensor2); # Object 1 humidity
        sensorObject4.save()
        
        
        sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1);
        sensorObject1.save()
        alarmTrigger=AlarmTrigger();
        
        # Create an alarm that should rise if in  a geometry area temperature > 21 and object 3 humidity over 70%
        alarmTrigger.save();
        alarmEl = AlarmElement(sensor_id=sensor1,trigger_type=">",trigger_value=21,alarm_trigger_id=alarmTrigger,geometry="SRID=4326;POLYGON ((-0.3301296 44.5320302, -0.3299398 44.5319698, -0.3299007 44.5320147, -0.3289336 44.5317099, -0.3288858 44.5316792, -0.3248068 44.5303878, -0.3246711 44.5303881, -0.323601 44.53449, -0.3311754 44.5358754, -0.3314803 44.5355151, -0.3303854 44.5352196, -0.3312454 44.5337272, -0.3311367 44.5336972, -0.3311431 44.5336865, -0.3294704 44.5331927, -0.3301296 44.5320302))").save();
        alarmEl = AlarmElement(sensor_id=sensor2,object_id=object3,trigger_type=">",trigger_value=70,alarm_trigger_id=alarmTrigger).save();


        Value(value=19, sensor_object_id=sensorObject1).save();
        Value(value=21.2, sensor_object_id=sensorObject1).save();
        Value(value=21.1, sensor_object_id=sensorObject2).save();
        Value(value=71.1, sensor_object_id=sensorObject3).save();
        Value(value=71.1, sensor_object_id=sensorObject4).save();

        self.assertEqual(AlarmTrigger.objects.all().count(),1)
        self.assertEqual(RisedAlarm.objects.all().count(),1)
        
