#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
from corsheaders.signals import check_request_enabled

from agribiot.models import ForeignFarmUser


def cors_allow_mysites(sender, request, **kwargs):
#    print("cors_allow_mysites")
#    print(request.headers['Origin']);
    
    if ForeignFarmUser.objects.filter(url_origin=request.headers['Origin']).exists() == True or ForeignFarmUser.objects.filter(url_origin=request.headers['Origin']+"/").exists():
        return True
    else :
        return False

check_request_enabled.connect(cors_allow_mysites)
