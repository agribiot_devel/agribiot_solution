from django.core.management.base import BaseCommand, CommandError
from agribiot.models import Object

class Command(BaseCommand):
    help = 'Closes the specified Object to display'

    def add_arguments(self, parser):
        parser.add_argument('object_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        for object_id in options['object_ids']:
            try:
                my_object = Object.objects.get(pk=object_id)
            except Object.DoesNotExist:
                raise CommandError('Object "%s" does not exist' % object_ids)

            print(my_object)

