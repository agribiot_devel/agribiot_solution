from django.core.management.base import BaseCommand, CommandError
from agribiot.models import Object
from agribiot.models import Operation

class Command(BaseCommand):
    help = 'Replace linked tag\'s operation by another object'

    def add_arguments(self, parser):
        parser.add_argument('old_uuid', type=str)
        parser.add_argument('new_uuid', type=str)
    def handle(self, *args, **options):
        old_uuid=options['old_uuid']
        new_uuid=options['new_uuid']
        
        try:
            old_object = Object.objects.filter(uuid=old_uuid).first()
            new_object = Object.objects.filter(uuid=new_uuid).first()
            operations = Operation.objects.filter(sensor_object_id=old_object.sensorobject_set.first().id)
            for operation in operations:
                operation.sensor_object_id = new_object.sensorobject_set.first()
                operation.save()
        except Object.DoesNotExist:
            raise CommandError('Object "%s" does not exist' % old_uuid)


