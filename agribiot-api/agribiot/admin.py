#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
from django.contrib import admin

# Register your models here.


from django.contrib.auth.models import Permission
from django.contrib.gis.admin import OSMGeoAdmin
from .models import Farm
from .models import Parcel
from .models import Object
from .models import Sensor
from .models import SensorObject
from .models import Value
from .models import AlarmTrigger
from .models import AlarmElement
from .models import RisedAlarm
from .models import RemoteAccess
from .models import AnnotationTemplate
from .models import ObjectUser
from .models import Operation
from .models import OperationImage
from .models import Dimension
from .models import DimensionValue

from guardian.admin import GuardedModelAdmin

admin.site.register(Permission)

class MyAdmin(OSMGeoAdmin,GuardedModelAdmin):
    pass

@admin.register(Farm)
class FarmAdmin(MyAdmin):
    list_display = ('id', 'name',)

@admin.register(Parcel)
class ParcelAdmin(MyAdmin):
    list_display = ('id', 'name','farm','geometry')

@admin.register(Object)
class ObjectAdmin(MyAdmin):
    list_display = ('id', 'position','uuid')

@admin.register(Sensor)
class SensorAdmin(MyAdmin):
    list_display = ('id', 'name', 'unit')

@admin.register(SensorObject)
class SensorObjectAdmin(MyAdmin):
    list_display = ('id', 'sensor_id', 'object_id')
    
    
@admin.register(Value)
class ValueAdmin(MyAdmin):
    list_display = ('id', 'value', 'sensor_object_id')

@admin.register(AlarmTrigger)
class AlarmTriggerAdmin(GuardedModelAdmin):
    list_display = ('id', 'name',)

@admin.register(AlarmElement)
class AlarmElementAdmin(MyAdmin):
    list_display = ('id', 'sensor_id', 'object_id','trigger_type','trigger_value','geometry','alarm_trigger_id')

@admin.register(RisedAlarm)
class RisedAlarmAdmin(MyAdmin):
    list_display = ('id', 'created', 'alarm_trigger_id','value_id')


@admin.register(RemoteAccess)
class RemoteAccessAdmin(MyAdmin):
#    list_display = ('id', 'access_type', 'user','geometry','sensor_id_set','object_id_set','start_time','end_time')
    list_display = ('id', 'access_type', 'user','geometry','start_date','end_date')

@admin.register(AnnotationTemplate)
class AnnotationTemplateAdmin(MyAdmin):
    list_display = ('id', 'description', 'annotation_type','photo')


@admin.register(OperationImage)
class OperationImageAdmin(MyAdmin):
    list_display = ('id','photo')

@admin.register(Operation)
class OperationAdmin(MyAdmin):
    list_display = ('id','imported','created','reader_uuid','tag_embedded_task_index','sensor_object_id','annotation_template_id')


@admin.register(Dimension)
class DimensionAdmin(MyAdmin):
    list_display = ('id',  'name', 'unit')

@admin.register(DimensionValue)
class DimensionValueAdmin(MyAdmin):
    list_display = ('id',  'value', 'operation_id', 'dimension_id')

 