#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
from rest_framework import serializers
from agribiot.models import Parcel
from agribiot.models import Farm
from agribiot.models import Object
from agribiot.models import Sensor
from agribiot.models import SensorObject
from agribiot.models import Value
from agribiot.models import AlarmTrigger
from agribiot.models import AlarmElement
from agribiot.models import RisedAlarm
from agribiot.models import RemoteInstance
from agribiot.models import AnnotationTemplate
from agribiot.models import Operation
from agribiot.models import OperationImage
from agribiot.models import Dimension
from agribiot.models import DimensionValue

class FarmSerializer(serializers.ModelSerializer):
	class Meta:
		model = Farm
		fields = ['id','name']

class ParcelSerializer(serializers.ModelSerializer):
	class Meta:
		model = Parcel
		fields = ['id','name','geometry', 'farm']

class ObjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Object
        fields = ['id','position','uuid','user_id','description']

class SensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sensor
        fields = ['id','name','unit']

class SensorObjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = SensorObject
        fields = ['id','object_id','sensor_id']

class SensorObjectJoinSerializer(serializers.ModelSerializer):
    object_id = ObjectSerializer()
    sensor_id = SensorSerializer()
    class Meta:
        model = SensorObject
        fields = ['id','object_id','sensor_id']

class ValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Value
        fields = ['id','created','value','sensor_object_id']

class AlarmTriggerSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlarmTrigger
        fields = ['id','name', 'alarmelement_set']

class AlarmElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlarmElement
        fields = ['id','object_id','sensor_id','trigger_type', 'trigger_value','geometry', 'alarm_trigger_id']
        
class AlarmElementJoinSerializer(serializers.ModelSerializer):
    sensor_id = SensorSerializer();
    object_id = ObjectSerializer(read_only=True);
    class Meta:
        model = AlarmElement
        fields = ['id','object_id','sensor_id','trigger_type', 'trigger_value','geometry', 'alarm_trigger_id']


class AlarmTriggerJoinSerializer(serializers.ModelSerializer):
    alarmelement_set = AlarmElementJoinSerializer(many=True,read_only=True)
    class Meta:
        model = AlarmTrigger
        fields = ['id','name', 'alarmelement_set']


class RisedAlarmSerializer(serializers.ModelSerializer):
    value_id = ValueSerializer();
    alarm_trigger_id = AlarmTriggerSerializer()
    class Meta:
        model = RisedAlarm
        fields = ['id','created','alarm_trigger_id','value_id']


class RemoteInstanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = RemoteInstance
        fields = ['id','name','token','url','color']

"""
AnnotationTemplate Serialisers 
"""


class AnnotationTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnotationTemplate
        fields = ['id','description','annotation_type', 'photo']

"""
Dimension Serialisers 
"""

class DimensionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dimension
        fields = ['id',  'name', 'unit']

"""
Dimension Value Serialisers 
"""

class DimensionValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = DimensionValue
        fields = ['id',  'value', 'operation_id', 'dimension_id']

class DimensionValueJoinSerializer(serializers.ModelSerializer):
    dimension_id = DimensionSerializer()
    class Meta:
        model = DimensionValue
        fields = ['id',  'value', 'operation_id', 'dimension_id']

"""
Operation Serialisers 
"""

class OperationJoinSerializer(serializers.ModelSerializer):
    annotation_template_id = AnnotationTemplateSerializer();
    class Meta:
        model = Operation
        fields = ['id','imported','created','reader_uuid','tag_embedded_task_index','sensor_object_id','annotation_template_id','position','accuracy','images']

class OperationJoinSensorObjectSerializer(serializers.ModelSerializer):
    sensor_object_id = SensorObjectJoinSerializer();
    class Meta:
        model = Operation
        fields = ['id','imported','created','reader_uuid','tag_embedded_task_index','sensor_object_id','annotation_template_id','position','accuracy','images']


class OperationJoinTidSerializer(serializers.ModelSerializer):
    tid = serializers.SerializerMethodField()
    class Meta:
        model = Operation
        fields = ['id','imported','created','reader_uuid','tag_embedded_task_index','sensor_object_id','annotation_template_id','tid','position','accuracy','images','dimensionvalue_set']
    def get_tid(self, obj):
        return obj.sensor_object_id.object_id.uuid;

class OperationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Operation
        fields = ['id','imported','created','reader_uuid','tag_embedded_task_index','sensor_object_id','annotation_template_id','position','accuracy','images']

"""
AnnotationTemplateSerializerJoinAll Serialisers 
"""
class AnnotationTemplateSerializerJoinAll(serializers.ModelSerializer):
    operation_set = OperationJoinSensorObjectSerializer(many=True,read_only=True)
    class Meta:
        model = AnnotationTemplate
        fields = ['id','description','annotation_type', 'photo','operation_set']

"""
Operation Image Serialisers 
"""
class OperationImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = OperationImage
        fields = ['id',  'photo']


