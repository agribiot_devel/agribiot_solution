import django_filters
from django_filters import rest_framework as djrf_filters

from agribiot.models import DimensionValue
from agribiot.models import AnnotationTemplate
from agribiot.models import Operation
from agribiot.models import Dimension
from agribiot.models import Object
from django import forms

class DimensionValueFilter(django_filters.FilterSet):
    created_range =django_filters.DateTimeFromToRangeFilter(
        field_name="operation_id__created",
        label="Date de création",
        lookup_expr='icontains',
        widget=django_filters.widgets.RangeWidget(
            attrs={
                'placeholder': 'jour/mois/année',
            },
        ),
    )
    tag_uuid = django_filters.ModelMultipleChoiceFilter(
        queryset=Object.objects.all(),
        field_name="operation_id__sensor_object_id__object_id__uuid",
        to_field_name="uuid",
        label="tag uuid",
        label_suffix="",
        widget=forms.SelectMultiple(),
    )
    dimension_id = django_filters.ModelMultipleChoiceFilter(
        queryset=Dimension.objects.all(),
        field_name="dimension_id__name",
        to_field_name="name",
        label="Dimension",
        label_suffix="",
        widget=forms.SelectMultiple(),
    )
    type = django_filters.ModelMultipleChoiceFilter(
        queryset=AnnotationTemplate.objects.all(),
        field_name="operation_id__annotation_template_id__description",
        to_field_name="description",
        label="Type",
        label_suffix="",
        widget=forms.SelectMultiple(),
    )

    class Meta:
        model = DimensionValue
        fields = ['created_range', 'tag_uuid', 'dimension_id','value','type']

    #filterset_fields = ['annotation_template_id','sensor_object_id','accuracy']

class OperationFilter(djrf_filters.FilterSet):
    created_range =django_filters.DateTimeFromToRangeFilter(
        field_name="created",
        label="Date de création",
        lookup_expr='icontains',
        widget=django_filters.widgets.RangeWidget(
            attrs={
                'placeholder': 'jour/mois/année',
            },
        ),
    )
    sensor_object_id = django_filters.ModelMultipleChoiceFilter(
        queryset=Object.objects.all(),
        field_name="sensor_object_id",
        to_field_name="id",
        label="sensor_object_id",
        label_suffix="",
        widget=forms.SelectMultiple(),
    )
    annotation_template_id = django_filters.ModelMultipleChoiceFilter(
        queryset=AnnotationTemplate.objects.all(),
        field_name="annotation_template_id",
        to_field_name="id",
        label="Type",
        label_suffix="",
        widget=forms.SelectMultiple(),
    )
    class Meta:
        model = Operation
        fields = ['created_range', 'sensor_object_id','annotation_template_id']
