#
# Projet AgriBIoT - Centre de Recherche INRIA Rennes
# INRIA Startup Studio - 2020-2021
# Copyright : AgriBIoT (c) 2020-2021
# Module Name :
# Agribiot-api
# Author : Tropée Simon (simon.tropee@gmail.com)
# Oscar Roberto Bastos (roberto@bastos-reseach.fr)
# Licence : GNU/GPL V3 see : www.gnu.org/licenses/gpl-3.0.en.html
#
from rest_framework.permissions import (DjangoModelPermissions, DjangoObjectPermissions)
import copy
from rest_framework import permissions
from guardian.shortcuts import get_perms

class DefaultDjangoModelPermissions(DjangoModelPermissions):
    def __init__(self):
        self.perms_map = copy.deepcopy(self.perms_map) # you need deepcopy when you inherit a dictionary type 
        self.perms_map['GET'] = ['%(app_label)s.view_%(model_name)s'] # Get is not setted by default (put/post/delete is set) 


class ObjectTokenDjangoModelPermissions(DjangoModelPermissions):
    def __init__(self):
        self.perms_map = copy.deepcopy(self.perms_map) # you need deepcopy when you inherit a dictionary type 
        self.perms_map['GET'] = ['%(app_label)s.access_object_token']

class ForeignUserTokenDjangoModelPermissions(DjangoModelPermissions):
    def __init__(self):
        self.perms_map = copy.deepcopy(self.perms_map) # you need deepcopy when you inherit a dictionary type 
        self.perms_map['GET'] = ['%(app_label)s.access_foreign_user_token']

class GatewayUserTokenDjangoModelPermissions(DjangoModelPermissions):
    def __init__(self):
        self.perms_map = copy.deepcopy(self.perms_map) # you need deepcopy when you inherit a dictionary type 
        self.perms_map['GET'] = ['%(app_label)s.access_gateway_user_token']

class RFIDDeviceUserTokenDjangoModelPermissions(DjangoModelPermissions):
    def __init__(self):
        self.perms_map = copy.deepcopy(self.perms_map) # you need deepcopy when you inherit a dictionary type 
        self.perms_map['GET'] = ['%(app_label)s.access_rfiddevice_user_token']
"""
Owners of the object or admins can do anything.
Everyone else can do nothing.
"""
class UserPermissionsObj(DjangoModelPermissions):
    pass


