#!/bin/bash
runuser -l postgres -c '/usr/bin/dropdb agribiot'
runuser -l postgres -c '/usr/bin/dropdb test_agribiot'
find . -path "*/migrations/*.py" -not -name "__init__.py" -not -name "0001_initial.py" -delete
find . -path "*/migrations/*.pyc"  -delete
