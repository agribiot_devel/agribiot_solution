import json
from datetime import datetime, timedelta
from django.utils import timezone
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
from django.contrib.auth.models import User
from agribiot.models import Farm
from agribiot.models import Parcel
from agribiot.models import Object
from agribiot.models import Sensor
from agribiot.models import SensorObject
from agribiot.models import Value
from agribiot.models import AlarmTrigger
from agribiot.models import RisedAlarm


sensorObject1 = SensorObject.objects.get(object_id=1,sensor_id=1);
sensorObject2 = SensorObject.objects.get(object_id=1,sensor_id=2);
with open('tools/value.json') as json_file:
    data = json.load(json_file)
    nbr_value = len(data)
    d = timezone.now() - timedelta(hours=nbr_value)
    for value in data:
        #print(d.strftime('%Y-%m-%d %H:%M:%S.%f+00'))
        if value['sensor_object_id'] == 1:
            Value(created=d.strftime('%Y-%m-%d %H:%M:%S%z'), value=value['value'], sensor_object_id=sensorObject1).save();
        elif value['sensor_object_id'] == 2:
            Value(created=d.strftime('%Y-%m-%d %H:%M:%S%z'), value=value['value'], sensor_object_id=sensorObject2).save();
        d += timedelta(hours=1)
