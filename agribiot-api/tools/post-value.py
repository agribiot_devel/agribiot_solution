import json
import requests
import sys

if len(sys.argv) != 5:
	print("usage : post-value.py <url> <access_token> <sensor_object_id> <value>")
	sys.exit()

print(str(sys.argv[1]))

temperature = {"value":float(sys.argv[4]),"sensor_object_id":int(sys.argv[3])}
# convert into JSON:
json_value = json.dumps(temperature)
headers = {'Content-type': 'application/json','Authorization': "Token "+sys.argv[2]}

x = requests.post(str(sys.argv[1]), headers=headers, data = json_value)

print(x)
if(x.status_code != 200):
	print(x.text)

