from agribiot.models import Farm
from agribiot.models import Parcel
from agribiot.models import Object
from agribiot.models import Sensor
from agribiot.models import SensorObject
from agribiot.models import Value
from agribiot.models import AlarmElement
from agribiot.models import AlarmTrigger
from agribiot.models import RisedAlarm
from agribiot.models import GatewayUser
from agribiot.models import ObjectUser
from agribiot.models import ForeignFarmUser
from agribiot.models import RFIDDeviceUser
from agribiot.models import RemoteAccess
from agribiot.models import TargetType
from agribiot.models import Task
from agribiot.models import Operation
from agribiot.models import AccessType
from agribiot.models import add_classic_permission
from agribiot.models import add_custom_permission
from agribiot.serializers import FarmSerializer
from agribiot.serializers import ParcelSerializer
from agribiot.serializers import ObjectSerializer
from agribiot.serializers import SensorSerializer
from agribiot.serializers import SensorObjectSerializer
from agribiot.serializers import SensorObjectJoinSerializer
from agribiot.serializers import ValueSerializer
from agribiot.serializers import AlarmTriggerSerializer
from agribiot.serializers import AlarmTriggerJoinSerializer
from agribiot.serializers import AlarmElementSerializer
from agribiot.serializers import RisedAlarmSerializer
from agribiot.serializers import RemoteInstanceSerializer
from agribiot.serializers import TaskSerializer
from agribiot.serializers import OperationSerializer
from agribiot.serializers import OperationJoinSerializer
from agribiot.serializers import OperationJoinTidSerializer


sensorObjects = SensorObject.objects.all();

mySensorObject = sensorObjects.first();

sensor_object_list = []

for sensorObject in sensorObjects:
    sensor_object_el = {};
    sensor_object_el['object_id'] = ObjectSerializer(sensorObject.object_id).data;
    sensor_object_el['sensor_id'] = SensorSerializer(sensorObject.sensor_id).data;
    
    operation_list = []
    operations = sensorObject.operation_set.all()
    for operation in operations:
        operation_dict = OperationSerializer(operation).data;
        operation_dict['task_id'] = TaskSerializer(operation.task_id).data
        operation_dict['sensor_object_id'] = SensorObjectSerializer(operation.sensor_object_id).data

        operation_list.append(operation_dict)
        
    sensor_object_el['operations'] = operation_list
    sensor_object_list.append(sensor_object_el)

print(sensor_object_list);
