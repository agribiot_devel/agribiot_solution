import json
import requests
import sys

if len(sys.argv) != 3:
	print("usage : pypost.py <url> <file>")
	sys.exit()

print(str(sys.argv[1]))

with open(str(sys.argv[2]), 'r') as content_file:
    content = content_file.read()
    x = requests.post(str(sys.argv[1]), data = content)

print(x)

