
import json
import requests
import sys
import gzip
if len(sys.argv) != 3:
    print("usage : "+sys.argv[0] +"<code insee> <parcelid>")
    print("use https://france-cadastre.fr/cadastre/sauternes to get parcel id and insee code")
    sys.exit()
code_insee = str(sys.argv[1])
departement = str(sys.argv[1])[0:2]
parcelid = sys.argv[2]
print("Departement "+ departement);
print("Code insee "+ code_insee);
print("Parcel "+ parcelid);
url = "https://cadastre.data.gouv.fr/data/etalab-cadastre/2020-10-01/geojson/communes/"+departement+"/"+code_insee+"/cadastre-"+code_insee+"-parcelles.json.gz"
x = requests.get(url)

result_json = json.loads(gzip.decompress(x.content));
for parcel in result_json["features"]:
    if parcel["id"] == sys.argv[2]:
        srid_str ="SRID=4326;POLYGON (("
        for lat_lon in parcel["geometry"]["coordinates"][0]:
            srid_str += str(lat_lon[0]) + " " + str(lat_lon[1])+ ","
        srid_str = srid_str[:-1]
        srid_str += "))"
        print("Parcel(name=\""+parcelid+"\",farm=farm,geometry=\""+ srid_str+"\").save()");
        
        