#!/bin/bash


if [ $# -eq 0 ]
  then
    echo "usage ./$1 <input_file>"
fi

runuser -l postgres -c 'createdb agribiot'
echo "CREATE EXTENSION postgis;" | runuser -l postgres -c 'psql agribiot'
runuser -l postgres -c 'psql agribiot' < $1
