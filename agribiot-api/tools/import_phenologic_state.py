
from agribiot.models import AnnotationTemplate
from agribiot.models import AnnotationType
from django.core.files import File  # you need this somewhere
from shutil import copyfile
import glob, os
for file in glob.glob("*.txt"):
    print(file)

#os.chdir("")
try:
    os.mkdir("media/")
    os.mkdir("media/annotationtemplateimg/")
except FileExistsError:
    pass
for i in range(ord('A'),ord('K')+1):
    my_file = glob.glob("sample/phenologique/stade-"+chr(i)+"*.png")[0]
    print(my_file)
    stade_name = os.path.splitext(os.path.basename(my_file))[0]
    print(stade_name)
    copyfile(glob.glob("sample/phenologique/stade-"+chr(i)+"*.png")[0], "media/annotationtemplateimg/"+os.path.basename(my_file))
    try:
        cur_obs = AnnotationTemplate.objects.get(description=stade_name)
    except AnnotationTemplate.DoesNotExist:
        cur_obs = AnnotationTemplate(description=stade_name,annotation_type=AnnotationType.OBSERVATION)
        cur_obs.photo.save(os.path.basename(my_file),
                File(open("media/annotationtemplateimg/"+os.path.basename(my_file), 'rb'))
            )
        cur_obs.save()
