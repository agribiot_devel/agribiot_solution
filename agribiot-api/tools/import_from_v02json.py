from agribiot.models import Farm
from agribiot.models import Parcel
from agribiot.models import Object
from agribiot.models import Sensor
from agribiot.models import SensorObject
from agribiot.models import Value
from agribiot.models import AlarmTrigger
from agribiot.models import AlarmElement
from agribiot.models import RisedAlarm
from agribiot.models import ForeignFarmUser
from agribiot.models import RemoteAccess
from agribiot.models import RemoteInstance
from agribiot.models import AnnotationTemplate
from agribiot.models import AnnotationType
from agribiot.models import Operation
from agribiot.models import GatewayUser
from agribiot.models import RFIDDeviceUser

from agribiot.serializers import FarmSerializer
from agribiot.serializers import ParcelSerializer
from agribiot.serializers import ObjectSerializer
from agribiot.serializers import SensorSerializer
from agribiot.serializers import SensorObjectSerializer
from agribiot.serializers import SensorObjectJoinSerializer
from agribiot.serializers import ValueSerializer
from agribiot.serializers import AlarmTriggerSerializer
from agribiot.serializers import AlarmTriggerJoinSerializer
from agribiot.serializers import AlarmElementSerializer
from agribiot.serializers import RisedAlarmSerializer
from agribiot.serializers import RemoteInstanceSerializer
from agribiot.serializers import AnnotationTemplateSerializer
from agribiot.serializers import OperationSerializer
from agribiot.serializers import OperationJoinSerializer
from agribiot.serializers import OperationJoinTidSerializer
import json
from datetime import datetime
from django.utils.dateparse import parse_datetime


with open('sample/json_dump_sensor_operation_chenau_03_03.json') as json_file:
    data = json.load(json_file)

    for sensorObject in data:
        #print(sensorObject);
        cur_object = sensorObject['object_id']
        try:
            my_object = Object.objects.get(uuid=cur_object['uuid'])
        except Object.DoesNotExist:
            my_object = Object(uuid=cur_object['uuid'],position=cur_object['position'],description=cur_object['description'])
            my_object.save()
        cur_sensor = sensorObject['sensor_id']
        try:
            my_sensor = Sensor.objects.get(id=cur_sensor['id'])
        except Sensor.DoesNotExist:
            my_sensor = Sensor(name=cur_sensor['name'],unit=cur_sensor['unit'])
            my_sensor.save()
        try:
            my_sensor_object = SensorObject.objects.get(object_id=my_object,sensor_id=my_sensor)
        except SensorObject.DoesNotExist:
            my_sensor_object = SensorObject(object_id=my_object,sensor_id=my_sensor)
            my_sensor_object.save()
        operations = sensorObject['operations']
        for operation in operations:
            try:
                cur_task = AnnotationTemplate.objects.get(description=operation['task_id']['description'])
            except AnnotationTemplate.DoesNotExist:
                cur_task = AnnotationTemplate(description=operation['task_id']['description'],annotation_type=AnnotationType.TASK)
                cur_task.save()
            try:
                cur_operation = Operation.objects.get(
                    created=parse_datetime(operation['created']),
                    sensor_object_id=my_sensor_object.id,
                    tag_embedded_task_index=operation['tag_embedded_task_index'])
                print(cur_operation)

            except Operation.DoesNotExist:
                cur_operation = Operation(created=parse_datetime(operation['created']),
                    reader_uuid='',
                    tag_embedded_task_index=operation['tag_embedded_task_index'],
                    value=operation['value'],
                    sensor_object_id=my_sensor_object,
                    annotation_template_id=cur_task,
                    position=operation['position'],
                    accuracy=operation['accuracy'])
                cur_operation.save();
        

