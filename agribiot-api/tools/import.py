import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
from django.contrib.auth.models import User     # where User lives
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from agribiot.models import Farm
from agribiot.models import Parcel
from agribiot.models import Object
from agribiot.models import Sensor
from agribiot.models import SensorObject
from agribiot.models import Value
from agribiot.models import AlarmElement
from agribiot.models import AlarmTrigger
from agribiot.models import RisedAlarm
from agribiot.models import GatewayUser
from agribiot.models import ObjectUser
from agribiot.models import ForeignFarmUser
from agribiot.models import RFIDDeviceUser
from agribiot.models import RemoteAccess
from agribiot.models import TargetType
from agribiot.models import AnnotationTemplate
from agribiot.models import Operation
from agribiot.models import AccessType
from agribiot.models import add_classic_permission
from agribiot.models import add_custom_permission
import uuid 
import nacl.encoding
import nacl.signing
def import_value_json():
    from agribiot.models import SensorObject
    from agribiot.models import Value
    import json
    from datetime import datetime, timedelta
    from django.utils import timezone
    sensorObject1 = SensorObject.objects.get(object_id=1,sensor_id=1);
    sensorObject2 = SensorObject.objects.get(object_id=1,sensor_id=2);
    with open('tools/value.json') as json_file:
        data = json.load(json_file)
        nbr_value = len(data)
        d = timezone.now() - timedelta(hours=nbr_value)
        for value in data:
            #print(d.strftime('%Y-%m-%d %H:%M:%S.%f+00'))
            if value['sensor_object_id'] == 1:
                Value(created=d.strftime('%Y-%m-%d %H:%M:%S%z'), value=value['value'], sensor_object_id=sensorObject1).save();
            elif value['sensor_object_id'] == 2:
                Value(created=d.strftime('%Y-%m-%d %H:%M:%S%z'), value=value['value'], sensor_object_id=sensorObject2).save();
            d += timedelta(hours=1)


farmUserGroup, created = Group.objects.get_or_create(name='farmUserGroup')
superuser = User()
superuser.is_active = True
superuser.is_superuser = True
superuser.is_staff = True
superuser.username = 'root'
superuser.email = 'root@root.fr'
superuser.set_password('6jwxnj9TGwfY5yr')
superuser.save()

normalUser = User();
normalUser.is_active = True
normalUser.is_staff = False
normalUser.username = 'agribiot'
normalUser.email = 'normal@user.fr'
normalUser.set_password('6jwxnj9TGwfY5yr')
normalUser.save()
farmUserGroup.user_set.add(normalUser);

gateway1 = GatewayUser();
gateway1.is_active = True
gateway1.is_staff = False
gateway1.username = 'gateway'
gateway1.email = 'normal@gateway.fr'
gateway1.save()

rfidDevice1 = RFIDDeviceUser();
rfidDevice1.is_active = True
rfidDevice1.is_staff = False
rfidDevice1.username = 'rfidDevice1'
rfidDevice1.email = 'normal@rfidDevice1.fr'
rfidDevice1.save()

add_classic_permission('view','farm',farmUserGroup)
add_classic_permission('view','parcel',farmUserGroup)
add_classic_permission('view','value',farmUserGroup)

add_classic_permission('add','sensor',farmUserGroup)
add_classic_permission('view','sensor',farmUserGroup)
add_classic_permission('change','sensor',farmUserGroup)
add_classic_permission('delete','sensor',farmUserGroup)

add_classic_permission('add','object',farmUserGroup)
add_classic_permission('view','object',farmUserGroup)
add_classic_permission('change','object',farmUserGroup)
add_classic_permission('delete','object',farmUserGroup)

add_classic_permission('view','sensorobject',farmUserGroup)
add_classic_permission('add','sensorobject',farmUserGroup)
add_classic_permission('change','sensorobject',farmUserGroup)
add_classic_permission('delete','sensorobject',farmUserGroup)

add_classic_permission('view','alarmelement',farmUserGroup)
add_classic_permission('add','alarmelement',farmUserGroup)
add_classic_permission('change','alarmelement',farmUserGroup)
add_classic_permission('delete','alarmelement',farmUserGroup)

add_classic_permission('view','alarmtrigger',farmUserGroup)
add_classic_permission('add','alarmtrigger',farmUserGroup)
add_classic_permission('change','alarmtrigger',farmUserGroup)
add_classic_permission('delete','alarmtrigger',farmUserGroup)

add_classic_permission('view','risedalarm',farmUserGroup)
add_classic_permission('add','risedalarm',farmUserGroup)
add_classic_permission('change','risedalarm',farmUserGroup)
add_classic_permission('delete','risedalarm',farmUserGroup)

add_classic_permission('add','foreignfarmuser',farmUserGroup)
add_classic_permission('view','foreignfarmuser',farmUserGroup)
add_classic_permission('delete','foreignfarmuser',farmUserGroup)
add_classic_permission('change','foreignfarmuser',farmUserGroup)

add_classic_permission('add','remoteaccess',farmUserGroup)
add_classic_permission('view','remoteaccess',farmUserGroup)
add_classic_permission('delete','remoteaccess',farmUserGroup)
add_classic_permission('change','remoteaccess',farmUserGroup)

add_classic_permission('add','remoteinstance',farmUserGroup)
add_classic_permission('change','remoteinstance',farmUserGroup)
add_classic_permission('view','remoteinstance',farmUserGroup)
add_classic_permission('delete','remoteinstance',farmUserGroup)

add_classic_permission('add','annotationtemplate',farmUserGroup)
add_classic_permission('change','annotationtemplate',farmUserGroup)
add_classic_permission('view','annotationtemplate',farmUserGroup)
add_classic_permission('delete','annotationtemplate',farmUserGroup)

add_classic_permission('add','operation',farmUserGroup)
add_classic_permission('change','operation',farmUserGroup)
add_classic_permission('view','operation',farmUserGroup)
add_classic_permission('delete','operation',farmUserGroup)

add_classic_permission('add','rfiddeviceuser',farmUserGroup)
add_classic_permission('change','rfiddeviceuser',farmUserGroup)
add_classic_permission('view','rfiddeviceuser',farmUserGroup)
add_classic_permission('delete','rfiddeviceuser',farmUserGroup)

add_classic_permission('add','gatewayuser',farmUserGroup)
add_classic_permission('change','gatewayuser',farmUserGroup)
add_classic_permission('view','gatewayuser',farmUserGroup)
add_classic_permission('delete','gatewayuser',farmUserGroup)

add_classic_permission('add','operationimage',farmUserGroup)
add_classic_permission('change','operationimage',farmUserGroup)
add_classic_permission('view','operationimage',farmUserGroup)
add_classic_permission('delete','operationimage',farmUserGroup)

add_classic_permission('view','dimension',farmUserGroup)
add_classic_permission('add','dimension',farmUserGroup)
add_classic_permission('change','dimension',farmUserGroup)
add_classic_permission('delete','dimension',farmUserGroup)

add_classic_permission('view','dimensionvalue',farmUserGroup)
add_classic_permission('add','dimensionvalue',farmUserGroup)
add_classic_permission('change','dimensionvalue',farmUserGroup)
add_classic_permission('delete','dimensionvalue',farmUserGroup)

add_custom_permission('access_object_token','object',farmUserGroup)
add_custom_permission("access_foreign_user_token", 'foreignfarmuser', farmUserGroup);
add_custom_permission("access_rfiddevice_user_token", 'rfiddeviceuser', farmUserGroup);
add_custom_permission("access_gateway_user_token", 'gatewayuser', farmUserGroup);

device_uuid = uuid.getnode()
device_uuid = None
#Import some data
sensor1 = Sensor(name="Température",unit="°C");
sensor1.save();
sensor2 = Sensor(name="Humidité",unit="%");
sensor2.save();
sensor3 = Sensor(name="tag",unit="");
sensor3.save();



# Generate a new signing key
"""
signing_key_original = nacl.signing.SigningKey.generate()
encoded_private_key = signing_key_original.encode(encoder=nacl.encoding.Base64Encoder)
encoded_public_key = signing_key_original.verify_key.encode(encoder=nacl.encoding.Base64Encoder)

f = open("res/rfid.key", "wb")
f.write(encoded_private_key)
f.close()
f = open("res/rfid_pub.key", "wb")
f.write(encoded_public_key)
f.close()
"""

if device_uuid == 30911737940:
    print("Simon agribiot-box-02");
    farm = Farm(name="Château Sauterne 2")
    farm.save()
    Parcel(name="335040000B0088",farm=farm,geometry="SRID=4326;POLYGON ((-0.3424766 44.5373832 , -0.341076 44.5370947 , -0.3397453 44.5368371 , -0.3394908 44.5367797 , -0.3393492 44.5367693 , -0.3390829 44.5367843 , -0.3388111 44.5367651 , -0.3375551 44.5366611 , -0.3372096 44.5366298 , -0.336952 44.5369344 , -0.3367964 44.5370521 , -0.3363151 44.5372506 , -0.3362152 44.5372906 , -0.3361709 44.5373247 , -0.336903 44.5374658 , -0.3377089 44.5376211 , -0.3376213 44.5378534 , -0.3394649 44.5379449 , -0.3397338 44.5379691 , -0.3398441 44.5379838 , -0.3400307 44.5380312 , -0.3401642 44.5380899 , -0.3411331 44.5385503 , -0.3413919 44.5386705 , -0.3417188 44.5387202 , -0.3420466 44.5387685 , -0.3421751 44.538787 , -0.3424456 44.538085 , -0.3424983 44.5379426 , -0.3421895 44.5378823 , -0.3424766 44.5373832))").save()
    Parcel(name="335040000B0086",farm=farm,geometry="SRID=4326;POLYGON ((-0.3424766 44.5373832 , -0.3426912 44.5374274 , -0.3428943 44.5369273 , -0.3429053 44.5368716 , -0.3429029 44.5368137 , -0.3427609 44.5364087 , -0.3427343 44.5362258 , -0.3427367 44.5361445 , -0.3422237 44.5360262 , -0.3419287 44.5359576 , -0.3411183 44.53577 , -0.340846 44.5357072 , -0.3408138 44.5356647 , -0.3404662 44.5356009 , -0.3403849 44.535582 , -0.340067 44.5361886 , -0.3400864 44.5361906 , -0.3397453 44.5368371 , -0.341076 44.5370947 , -0.3424766 44.5373832))").save()
    Parcel(name="335040000B0297",farm=farm,geometry="SRID=4326;POLYGON ((-0.3447963 44.5362292 , -0.3448575 44.5361198 , -0.3435853 44.5356719 , -0.3431025 44.5366209 , -0.3429286 44.536576 , -0.3430071 44.5368361 , -0.3430129 44.5368784 , -0.3430066 44.5369282 , -0.3429409 44.5370931 , -0.3431222 44.5371363 , -0.3433334 44.5371868 , -0.3441351 44.5373782 , -0.3447963 44.5362292))").save()
    Parcel(name="335040000B0293",farm=farm,geometry="SRID=4326;POLYGON ((-0.3462306 44.5367254 , -0.3460864 44.5366923 , -0.3456525 44.5365591 , -0.3447963 44.5362292 , -0.3441351 44.5373782 , -0.3439777 44.5376517 , -0.3439634 44.5376768 , -0.3447687 44.5378374 , -0.3455546 44.5379941 , -0.3458046 44.5374105 , -0.3458921 44.537426 , -0.3462306 44.5367254))").save()
    Parcel(name="335040000B0299",farm=farm,geometry="SRID=4326;POLYGON ((-0.3429755 44.5354593,-0.3435853 44.5356719,-0.3448575 44.5361198,-0.3447963 44.5362292,-0.3456525 44.5365591,-0.3460864 44.5366923,-0.3462306 44.5367254,-0.3472047 44.536957,-0.3480324 44.537155,-0.3479553 44.5373095,-0.3487609 44.5374521,-0.3495841 44.5376,-0.3501477 44.5377005,-0.3504352 44.5379683,-0.3504851 44.5377923,-0.3505607 44.5376279,-0.350566 44.5375874,-0.350578 44.5375792,-0.35062 44.5372548,-0.3506127 44.5370934,-0.3506047 44.536934,-0.350597 44.5368003,-0.3499625 44.5367917,-0.3497609 44.5367958,-0.3486522 44.5367495,-0.3484846 44.5367386,-0.3480775 44.5367088,-0.3475465 44.536614,-0.346863 44.536515,-0.345945 44.5361824,-0.3453341 44.5359602,-0.3446092 44.535691,-0.3430987 44.5349643,-0.3430543 44.5349696,-0.3430267 44.5349835,-0.3430094 44.5350004,-0.3429436 44.5351263,-0.3429755 44.5354593))").save()
    Parcel(name="335040000B0288",farm=farm,geometry="SRID=4326;POLYGON ((-0.3444142 44.5389458,-0.3447143 44.5380074,-0.343953 44.5378846,-0.3439202 44.5378923,-0.3438203 44.5381988,-0.3437734 44.5381873,-0.3436833 44.5381653,-0.3436859 44.5381583,-0.34362 44.5381452,-0.3434824 44.538522,-0.3431242 44.5384575,-0.3432587 44.5380722,-0.343199 44.5380596,-0.3431967 44.5380664,-0.3431042 44.538052,-0.3425952 44.5379458,-0.3421895 44.5390006,-0.343541 44.5392068,-0.3443004 44.5392979,-0.3444142 44.5389458))").save()


    object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3405976295471192 44.53772407014981)")
    object1.save()
    object2 = Object(uuid="0002", position="SRID=4326;POINT (-0.3422176837921143 44.53700521065002)")
    object2.save()
    object3 = Object(uuid="0003", position="SRID=4326;POINT (-0.3402221202850342 44.53653871197109)")
    object3.save()
    object4 = Object(uuid="0004", position="SRID=4326;POINT (-0.3429687023162842 44.53848880455612)")
    object4.save() 
    object5 = Object(uuid="0005", position="SRID=4326;POINT (-0.3438484668731689 44.5369975631608)")
    object5.save()
    object6 = Object(uuid="0006", position="SRID=4326;POINT (-0.345146656036377 44.53690579321212)")
    object6.save()
    
    sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1).save();
    sensorObject2 = SensorObject(object_id=object1,sensor_id=sensor2).save();
    sensorObject3 = SensorObject(object_id=object2,sensor_id=sensor1).save();
    sensorObject4 = SensorObject(object_id=object2,sensor_id=sensor2).save();
    sensorObject5 = SensorObject(object_id=object3,sensor_id=sensor1).save();
    sensorObject6 = SensorObject(object_id=object3,sensor_id=sensor2).save();
    sensorObject7 = SensorObject(object_id=object4,sensor_id=sensor1).save();
    sensorObject8 = SensorObject(object_id=object4,sensor_id=sensor2).save();
    sensorObject9 = SensorObject(object_id=object5,sensor_id=sensor3).save();
    sensorObject10 = SensorObject(object_id=object6,sensor_id=sensor3).save();
    import_value_json();
elif device_uuid == 30911905455: # 30911905455 or vagrant
    print("Simon agribiot-box-01 or vagrant");
    farm = Farm(name="Ch\u00e2teau Guiraud")
    farm.save()
    
    # Populate parcels 
    Parcel(name= "335040000D0474",geometry="SRID=4326;POLYGON ((-0.3301296 44.5320302, -0.3299398 44.5319698, -0.3299007 44.5320147, -0.3289336 44.5317099, -0.3288858 44.5316792, -0.3248068 44.5303878, -0.3246711 44.5303881, -0.323601 44.53449, -0.3311754 44.5358754, -0.3314803 44.5355151, -0.3303854 44.5352196, -0.3312454 44.5337272, -0.3311367 44.5336972, -0.3311431 44.5336865, -0.3294704 44.5331927, -0.3301296 44.5320302))",farm=farm).save();
    Parcel(name= "335040000D0458", geometry="SRID=4326;POLYGON ((-0.3314803 44.5355151, -0.3315136 44.5354755, -0.33165 44.5353615, -0.3318914 44.5351884, -0.3324505 44.5347134, -0.3327917 44.5341815, -0.3326749 44.5341474, -0.3312454 44.5337272, -0.3303854 44.5352196, -0.3314803 44.5355151))",farm=farm).save();
    Parcel(name= "335040000D0486",geometry="SRID=4326;POLYGON ((-0.3271208 44.5309755, -0.3280439 44.5295135, -0.3286073 44.5286259, -0.3286184 44.5285375, -0.3284742 44.5285248, -0.3252317 44.5282399, -0.3247388 44.5301287, -0.3248606 44.5302542, -0.3271208 44.5309755))",farm=farm).save();
    Parcel(name= "335040000D0478",geometry="SRID=4326;POLYGON ((-0.3300097 44.5309021, -0.3301958 44.5300672, -0.3294974 44.5300289, -0.3280439 44.5295135, -0.3271208 44.5309755, -0.329805 44.5318319, -0.3299269 44.5312666, -0.3300097 44.5309021))",farm=farm).save();
    Parcel(name= "335040000D0453",geometry="SRID=4326;POLYGON ((-0.3301958 44.5300672, -0.3304765 44.5287631, -0.3306735 44.5278871, -0.3296273 44.5278557, -0.3287553 44.5278046, -0.3287127 44.5277931, -0.3286211 44.5285164, -0.3286184 44.5285375, -0.3286073 44.5286259, -0.3280439 44.5295135, -0.3294974 44.5300289, -0.3301958 44.5300672))",farm=farm).save();
    Parcel(name= "335040000D0491",geometry="SRID=4326;POLYGON ((-0.3372737 44.5305438, -0.3365758 44.5304202, -0.3360163 44.5303737, -0.3333848 44.5303377, -0.3301958 44.5300672, -0.3300097 44.5309021, -0.3308945 44.5310039, -0.3308146 44.5311788, -0.3306605 44.5314293, -0.3304091 44.5313735, -0.3303431 44.5316459, -0.3303782 44.5317137, -0.3304407 44.5317832, -0.3314123 44.5320936, -0.3312826 44.5323018, -0.3319216 44.5325084, -0.331826 44.5325214, -0.3318046 44.53256, -0.3318001 44.5325695, -0.331917 44.5326046, -0.3319662 44.5325186, -0.3322598 44.5326054, -0.3324286 44.5322704, -0.3333997 44.5325575, -0.3338736 44.5326976, -0.3340571 44.5327519, -0.3341605 44.5327824, -0.335636 44.5320123, -0.3376694 44.5313768, -0.3379681 44.5312354, -0.338208 44.5309035, -0.3372339 44.5307173, -0.3372737 44.5305438))",farm=farm).save();
    Parcel(name= "335040000D0452",geometry="SRID=4326;POLYGON ((-0.3333848 44.5303377, -0.3337889 44.5289739, -0.334404 44.5290281, -0.334726 44.5279969, -0.3311283 44.5279009, -0.3306735 44.5278871, -0.3304765 44.5287631, -0.3301958 44.5300672, -0.3333848 44.5303377))",farm=farm).save();
    Parcel(name= "335040000D0482",geometry="SRID=4326;POLYGON ((-0.3383445 44.5307147, -0.3384638 44.5305344, -0.3386219 44.530348, -0.3395554 44.5294461, -0.33811 44.5293322, -0.3378111 44.5293079, -0.3351173 44.529089, -0.3348255 44.5290653, -0.334404 44.5290281, -0.3337889 44.5289739, -0.3333848 44.5303377, -0.3360163 44.5303737, -0.3365758 44.5304202, -0.3372737 44.5305438, -0.3373839 44.5300631, -0.3377514 44.5300994, -0.3376727 44.5303219, -0.3376375 44.5303961, -0.3376063 44.5304711, -0.3375792 44.5305471, -0.3375678 44.5305839, -0.3383445 44.5307147))",farm=farm).save();
    Parcel(name= "335040000D0475",geometry="SRID=4326;POLYGON ((-0.3301296 44.5320302, -0.3318046 44.53256, -0.331826 44.5325214, -0.3301504 44.5319937, -0.3301296 44.5320302))",farm=farm).save();
    Parcel(name= "335040000D0484",geometry="SRID=4326;POLYGON ((-0.3380652 44.5338983, -0.3380068 44.531713, -0.3373948 44.5317173, -0.3374029 44.5315452, -0.3358327 44.5320151, -0.3356187 44.5321092, -0.3337617 44.5330692, -0.3334664 44.5332944, -0.3334384 44.5333381, -0.334364 44.5336173, -0.3349742 44.5337371, -0.3353424 44.5337704, -0.3366847 44.5337875, -0.3380652 44.5338983))",farm=farm).save();
    Parcel(name= "335040000D0455", geometry="SRID=4326;POLYGON ((-0.3323872 44.5360509, -0.3334283 44.536218, -0.3336335 44.5362747, -0.3345301 44.5361515, -0.3350539 44.5360536, -0.3363181 44.5355683, -0.3363427 44.5350289, -0.336997 44.5351387, -0.3371828 44.5344296, -0.3380834 44.534575, -0.3380652 44.5338983, -0.3366847 44.5337875, -0.3353424 44.5337704, -0.3349742 44.5337371, -0.334364 44.5336173, -0.3334384 44.5333381, -0.3325489 44.5347311, -0.3322447 44.5349878, -0.3319371 44.5357529, -0.3324941 44.5358591, -0.3323872 44.5360509))",farm=farm).save();
    Parcel(name= "335040000D0472", geometry="SRID=4326;POLYGON ((-0.3341256 44.5383725, -0.3343287 44.5380755, -0.334845 44.5382479, -0.3355732 44.5376805, -0.3361626 44.5372654, -0.3368229 44.5369396, -0.3375756 44.536089, -0.3372709 44.5361287, -0.3369607 44.5352396, -0.3370648 44.535153, -0.3373059 44.5350518, -0.337976 44.5348084, -0.3380834 44.534575, -0.3371828 44.5344296, -0.336997 44.5351387, -0.3363427 44.5350289, -0.3363181 44.5355683, -0.3350539 44.5360536, -0.3345301 44.5361515, -0.3336335 44.5362747, -0.3334283 44.536218, -0.3323872 44.5360509, -0.3323757 44.5360652, -0.3321257 44.5360038, -0.3318592 44.5359666, -0.3317431 44.5359427, -0.3314042 44.5358729, -0.3313453 44.5358375, -0.3312746 44.5359287, -0.3308514 44.5365625, -0.3309717 44.5365778, -0.3311143 44.536596, -0.3316827 44.5366682, -0.3313511 44.537542, -0.332731 44.5379647, -0.3341256 44.5383725))",farm=farm).save();
    Parcel(name= "335040000D0494", geometry="SRID=4326;POLYGON ((-0.3312454 44.5337272, -0.3326749 44.5341474, -0.3328612 44.5338503, -0.332981 44.5338863, -0.3332676 44.533439, -0.3333601 44.5332947, -0.3334648 44.5331584, -0.333889 44.5329292, -0.3341605 44.5327824, -0.3340571 44.5327519, -0.3338736 44.5326976, -0.3333997 44.5325575, -0.3324286 44.5322704, -0.3322598 44.5326054, -0.3319662 44.5325186, -0.331917 44.5326046, -0.3318001 44.5325695, -0.3315889 44.5329287, -0.33155 44.532995, -0.3311431 44.5336865, -0.3311367 44.5336972, -0.3312454 44.5337272))",farm=farm).save();
    Parcel(name= "335040000D0476",geometry="SRID=4326;POLYGON ((-0.3301296 44.5320302, -0.3294704 44.5331927, -0.3311431 44.5336865, -0.33155 44.532995, -0.3315889 44.5329287, -0.3318001 44.5325695, -0.3318046 44.53256, -0.3301296 44.5320302))",farm=farm).save()
    #Populate object
    object1 = Object(uuid="0001", position="SRID=4326;POINT (-0.3298044204711914 44.53526920483945)")
    object1.save()
    object2 = Object(uuid="0002", position="SRID=4326;POINT (-0.324246883392334 44.53427499318332)")
    object2.save()
    object3 = Object(uuid="0003", position="SRID=4326;POINT (-0.3296864032745361 44.53211061219065)")
    object3.save()
    object4 = Object(uuid="0004", position="SRID=4326;POINT (-0.325169563293457 44.53071098730988)")
    object4.save() 
    object5 = Object(uuid="0005", position="SRID=4326;POINT (-0.3274869918823242 44.53321193271217)")
    object5.save()
    
    
    object6 = Object(uuid="0006", position="SRID=4326;POINT (-0.332293510390771 44.53226356967177)")
    object6.save()
    
    object7 = Object(uuid="0007", position="SRID=4326;POINT (-0.3341388701927709 44.53215649601647)")
    object7.save()
    
    
    sensorObject1 = SensorObject(object_id=object1,sensor_id=sensor1);
    sensorObject1.save()
    alarmTrigger=AlarmTrigger();
    alarmTrigger.save();
    alarmEl = AlarmElement(sensor_id=sensor1,trigger_type=">",trigger_value=21,alarm_trigger_id=alarmTrigger,geometry="SRID=4326;POLYGON ((-0.3301296 44.5320302, -0.3299398 44.5319698, -0.3299007 44.5320147, -0.3289336 44.5317099, -0.3288858 44.5316792, -0.3248068 44.5303878, -0.3246711 44.5303881, -0.323601 44.53449, -0.3311754 44.5358754, -0.3314803 44.5355151, -0.3303854 44.5352196, -0.3312454 44.5337272, -0.3311367 44.5336972, -0.3311431 44.5336865, -0.3294704 44.5331927, -0.3301296 44.5320302))").save();
    
    sensorObject2 = SensorObject(object_id=object1,sensor_id=sensor2);
    sensorObject2.save()
    
 
    
    
    
    sensorObject3 = SensorObject(object_id=object2,sensor_id=sensor1);
    sensorObject3.save()
    sensorObject2 = SensorObject(object_id=object2,sensor_id=sensor2);
    sensorObject2.save()
    
    sensorObject3 = SensorObject(object_id=object3,sensor_id=sensor1);
    sensorObject3.save()
    sensorObject2 = SensorObject(object_id=object3,sensor_id=sensor2);
    sensorObject2.save()
    
    sensorObject3 = SensorObject(object_id=object4,sensor_id=sensor1);
    sensorObject3.save()
    sensorObject2 = SensorObject(object_id=object4,sensor_id=sensor2);
    sensorObject2.save()
    
    sensorObject3 = SensorObject(object_id=object5,sensor_id=sensor1);
    sensorObject3.save()
    sensorObject2 = SensorObject(object_id=object5,sensor_id=sensor2);
    sensorObject2.save()
    
    sensorObject3 = SensorObject(object_id=object6,sensor_id=sensor3);
    sensorObject3.save()
    value = Value(value=1,sensor_object_id=sensorObject3)
    value.save()
    
    value = Value(value=2,sensor_object_id=sensorObject3)
    value.save()
    
    sensorObject3 = SensorObject(object_id=object7,sensor_id=sensor3);
    sensorObject3.save()
    value = Value(value=3,sensor_object_id=sensorObject3)
    value.save()
    

    import_value_json()
    """
    foreignUser = ForeignFarmUser.objects.get(username = 'foreignUser')
    
    remoteAccess=RemoteAccess(user=foreignUser,target_type=TargetType.GEOMETRY,geometry="POLYGON((-0.33340930938720703 44.53437747443309,-0.33340930938720703 44.536732959602794,-0.3280448913574219 44.536732959602794,-0.3280448913574219 44.53437747443309,-0.33340930938720703 44.53437747443309))");
    remoteAccess.save()
    remoteAccess.sensors.add(sensor1);
    remoteAccess.commit_remote_access();
    
    
    remoteAccess=RemoteAccess(user=foreignUser,target_type=TargetType.SPECIFIC_OBJECTS);
    remoteAccess.save()
    remoteAccess.selected_objects.add(object6)
    remoteAccess.selected_objects.add(object7)
    remoteAccess.sensors.add(sensor3);
    remoteAccess.sensors.add(sensor3);
    remoteAccess.commit_remote_access();
    """

