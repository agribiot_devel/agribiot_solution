import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
from django.contrib.auth.models import User     # where User lives
from agribiot.models import Farm
from agribiot.models import Parcel
from agribiot.models import Object
from agribiot.models import Sensor
from agribiot.models import SensorObject
from agribiot.models import Value
from agribiot.models import AlarmTrigger
from agribiot.models import RisedAlarm
from datetime import datetime


sensorObject1 = SensorObject.objects.get(object_id=1,sensor_id=1);
sensorObject2 = SensorObject.objects.get(object_id=1,sensor_id=2);



Value(created=datetime.strptime("2020-10-06 13:32:05.494729+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:06.494648+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.2999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:09.52145+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:10.49293+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.2000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:13.526733+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:14.504382+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.1000022888183594, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:17.505259+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:18.518224+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.1000022888183594, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:21.52185+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:22.508546+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:25.515466+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:26.516158+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:29.519732+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:30.519687+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:33.526779+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:34.537422+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:37.522477+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:38.545572+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:41.53213+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:42.533681+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:45.536282+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:46.53625+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=57.9000015258789062, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:49.561067+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:50.540019+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:53.544771+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:54.545299+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.2000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:32:57.570698+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:32:58.575757+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.2000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:33:01.566606+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:33:02.558+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.2000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:33:05.586996+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:33:06.577051+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.2000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:33:09.565233+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:33:10.558473+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.2000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:34:29.659383+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:34:30.659576+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.1000022888183594, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:34:33.636637+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:34:34.662741+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.1000022888183594, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:34:37.669132+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:34:38.667864+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.1000022888183594, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:39:13.940805+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:39:14.949624+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.1000022888183594, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:39:17.923547+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:39:18.949148+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.1000022888183594, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:39:21.930045+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:39:22.926852+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.1000022888183594, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:39:25.932096+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:39:26.929922+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.1000022888183594, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 13:39:29.962536+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 13:39:30.928787+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=58.1000022888183594, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:03:52.477501+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:03:55.482437+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:03:56.482127+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:03:59.498782+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:00.497835+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:03.51722+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:04.489712+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:07.486528+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:08.488579+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:11.509618+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:12.497085+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:15.501982+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:16.512661+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:19.504439+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:20.505533+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:23.508403+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:24.508519+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:27.5099+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:28.512757+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:31.516729+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:32.517162+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:35.520449+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:36.520099+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:39.548698+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:40.523904+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:43.527568+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:44.527788+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:47.532014+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:48.531319+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:51.536807+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:52.535593+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7000007629394531, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:55.536722+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:04:56.539913+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:04:59.543881+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:05:00.544075+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:05:03.547093+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:05:04.547768+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.9000015258789062, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:05:07.551673+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:05:08.548657+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.7999992370605469, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:05:11.55619+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:05:12.568095+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.9000015258789062, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:05:15.558841+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.2000007629394531, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:05:16.559079+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.9000015258789062, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:05:19.58561+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
Value(created=datetime.strptime("2020-10-06 15:05:20.562971+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=56.9000015258789062, sensor_object_id=sensorObject2).save();
Value(created=datetime.strptime("2020-10-06 15:05:23.567796+00",'%Y-%m-%d %H:%M:%S.%f+00'), value=21.1000003814697266, sensor_object_id=sensorObject1).save();
