#!/bin/bash
./create_database.sh
mkdir media
mkdir media/annotationtemplateimg/
mkdir media/operation/
chown -R www-data:www-data media

python3 manage.py makemigrations  agribiot &&  python3 manage.py migrate && python3 manage.py collectstatic --no-input
python3 manage.py shell < tools/import.py
python3 manage.py shell < tools/import_phenologic_state.py
#python3 manage.py shell < tools/importValueJson.py
