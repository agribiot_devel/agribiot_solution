--
-- PostgreSQL database dump
--

-- Dumped from database version 11.10 (Debian 11.10-0+deb10u1)
-- Dumped by pg_dump version 11.10 (Debian 11.10-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectp_permission_id_71807bfc_fk_auth_perm;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectp_content_type_id_2e892405_fk_django_co;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobject_permission_id_36572738_fk_auth_perm;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobject_group_id_4bbbfb62_fk_auth_grou;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobject_content_type_id_7ade36b8_fk_django_co;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
ALTER TABLE ONLY public.agribiot_value DROP CONSTRAINT agribiot_value_sensor_object_id_97097252_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_sensor_id_4a836e2c_fk_agribiot_sensor_id;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_object_id_fcf9bc39_fk_agribiot_object_id;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_value_id_49e5444e_fk_agribiot_value_id;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_alarm_trigger_id_d6008608_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_rfiddeviceuser DROP CONSTRAINT agribiot_rfiddeviceuser_user_ptr_id_6733a39c_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteacces_sensorobject_id_3a831118_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteacces_sensor_id_a56784c0_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteacces_remoteaccess_id_b12c6c90_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteacces_remoteaccess_id_a33d91e7_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteacces_remoteaccess_id_382e8a77_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteacces_object_id_b8286ddc_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess DROP CONSTRAINT agribiot_remoteacces_foreign_users_207df1dc_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_parcel DROP CONSTRAINT agribiot_parcel_farm_id_63c02727_fk_agribiot_farm_id;
ALTER TABLE ONLY public.agribiot_operation DROP CONSTRAINT agribiot_operation_task_id_13228df0_fk_agribiot_task_id;
ALTER TABLE ONLY public.agribiot_operation DROP CONSTRAINT agribiot_operation_sensor_object_id_cb2c3e2f_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_objectuser DROP CONSTRAINT agribiot_objectuser_user_ptr_id_d9b15f29_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_user_id_69163bcb_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_gatewayuser DROP CONSTRAINT agribiot_gatewayuser_user_ptr_id_c1b9d969_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_foreignfarmuser DROP CONSTRAINT agribiot_foreignfarmuser_user_ptr_id_d62330a8_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelement_sensor_id_ab7c0bfe_fk_agribiot_sensor_id;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelement_object_id_c292df7b_fk_agribiot_object_id;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelemen_alarm_trigger_id_9ba193db_fk_agribiot_;
DROP INDEX public.guardian_userobjectpermission_user_id_d5c1e964;
DROP INDEX public.guardian_userobjectpermission_permission_id_71807bfc;
DROP INDEX public.guardian_userobjectpermission_content_type_id_2e892405;
DROP INDEX public.guardian_us_content_179ed2_idx;
DROP INDEX public.guardian_groupobjectpermission_permission_id_36572738;
DROP INDEX public.guardian_groupobjectpermission_group_id_4bbbfb62;
DROP INDEX public.guardian_groupobjectpermission_content_type_id_7ade36b8;
DROP INDEX public.guardian_gr_content_ae6aec_idx;
DROP INDEX public.django_session_session_key_c0390e0f_like;
DROP INDEX public.django_session_expire_date_a5c62663;
DROP INDEX public.django_admin_log_user_id_c564eba6;
DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
DROP INDEX public.authtoken_token_key_10f0b77e_like;
DROP INDEX public.auth_user_username_6821ab7c_like;
DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
DROP INDEX public.auth_user_groups_group_id_97559544;
DROP INDEX public.auth_permission_content_type_id_2f476e4b;
DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
DROP INDEX public.auth_group_name_a6ea08ec_like;
DROP INDEX public.agribiot_value_sensor_object_id_97097252;
DROP INDEX public.agribiot_sensorobject_sensor_id_4a836e2c;
DROP INDEX public.agribiot_sensorobject_object_id_fcf9bc39;
DROP INDEX public.agribiot_sensor_name_56d3fb1c_like;
DROP INDEX public.agribiot_risedalarm_value_id_49e5444e;
DROP INDEX public.agribiot_risedalarm_alarm_trigger_id_d6008608;
DROP INDEX public.agribiot_remoteaccess_sensors_sensor_id_a56784c0;
DROP INDEX public.agribiot_remoteaccess_sensors_remoteaccess_id_a33d91e7;
DROP INDEX public.agribiot_remoteaccess_sensor_objects_sensorobject_id_3a831118;
DROP INDEX public.agribiot_remoteaccess_sensor_objects_remoteaccess_id_b12c6c90;
DROP INDEX public.agribiot_remoteaccess_selected_objects_remoteaccess_id_382e8a77;
DROP INDEX public.agribiot_remoteaccess_selected_objects_object_id_b8286ddc;
DROP INDEX public.agribiot_remoteaccess_geometry_id;
DROP INDEX public.agribiot_remoteaccess_foreign_users_207df1dc;
DROP INDEX public.agribiot_parcel_geometry_id;
DROP INDEX public.agribiot_parcel_farm_id_63c02727;
DROP INDEX public.agribiot_operation_task_id_13228df0;
DROP INDEX public.agribiot_operation_sensor_object_id_cb2c3e2f;
DROP INDEX public.agribiot_operation_position_id;
DROP INDEX public.agribiot_object_uuid_a3a0d421_like;
DROP INDEX public.agribiot_object_user_id_69163bcb;
DROP INDEX public.agribiot_object_position_id;
DROP INDEX public.agribiot_alarmtrigger_name_2a404727_like;
DROP INDEX public.agribiot_alarmelement_sensor_id_ab7c0bfe;
DROP INDEX public.agribiot_alarmelement_object_id_c292df7b;
DROP INDEX public.agribiot_alarmelement_geometry_id;
DROP INDEX public.agribiot_alarmelement_alarm_trigger_id_9ba193db;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectpermission_pkey;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectpermi_user_id_permission_id_ob_b0b3d2fc_uniq;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobjectpermission_pkey;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobjectperm_group_id_permission_id_o_3f189f7c_uniq;
ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_user_id_key;
ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
ALTER TABLE ONLY public.agribiot_value DROP CONSTRAINT agribiot_value_pkey;
ALTER TABLE ONLY public.agribiot_task DROP CONSTRAINT agribiot_task_pkey;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_pkey;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_object_id_sensor_id_a7d20fea_uniq;
ALTER TABLE ONLY public.agribiot_sensor DROP CONSTRAINT agribiot_sensor_pkey;
ALTER TABLE ONLY public.agribiot_sensor DROP CONSTRAINT agribiot_sensor_name_key;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_pkey;
ALTER TABLE ONLY public.agribiot_rfiddeviceuser DROP CONSTRAINT agribiot_rfiddeviceuser_pkey;
ALTER TABLE ONLY public.agribiot_remoteinstance DROP CONSTRAINT agribiot_remoteinstance_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteaccess_sensors_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteaccess_sensor_objects_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteaccess_selected_objects_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensorob_81219c57_uniq;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensor_i_0da2fc55_uniq;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_object_i_6a4c0d2b_uniq;
ALTER TABLE ONLY public.agribiot_remoteaccess DROP CONSTRAINT agribiot_remoteaccess_pkey;
ALTER TABLE ONLY public.agribiot_parcel DROP CONSTRAINT agribiot_parcel_pkey;
ALTER TABLE ONLY public.agribiot_operation DROP CONSTRAINT agribiot_operation_pkey;
ALTER TABLE ONLY public.agribiot_objectuser DROP CONSTRAINT agribiot_objectuser_pkey;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_uuid_key;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_pkey;
ALTER TABLE ONLY public.agribiot_gatewayuser DROP CONSTRAINT agribiot_gatewayuser_pkey;
ALTER TABLE ONLY public.agribiot_foreignfarmuser DROP CONSTRAINT agribiot_foreignfarmuser_pkey;
ALTER TABLE ONLY public.agribiot_farm DROP CONSTRAINT agribiot_farm_pkey;
ALTER TABLE ONLY public.agribiot_alarmtrigger DROP CONSTRAINT agribiot_alarmtrigger_pkey;
ALTER TABLE ONLY public.agribiot_alarmtrigger DROP CONSTRAINT agribiot_alarmtrigger_name_key;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelement_pkey;
ALTER TABLE public.guardian_userobjectpermission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.guardian_groupobjectpermission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_value ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_task ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_sensorobject ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_sensor ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_risedalarm ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteinstance ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess_sensors ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess_sensor_objects ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess_selected_objects ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_parcel ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_operation ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_object ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_farm ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_alarmtrigger ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_alarmelement ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.guardian_userobjectpermission_id_seq;
DROP TABLE public.guardian_userobjectpermission;
DROP SEQUENCE public.guardian_groupobjectpermission_id_seq;
DROP TABLE public.guardian_groupobjectpermission;
DROP TABLE public.django_session;
DROP SEQUENCE public.django_migrations_id_seq;
DROP TABLE public.django_migrations;
DROP SEQUENCE public.django_content_type_id_seq;
DROP TABLE public.django_content_type;
DROP SEQUENCE public.django_admin_log_id_seq;
DROP TABLE public.django_admin_log;
DROP TABLE public.authtoken_token;
DROP SEQUENCE public.auth_user_user_permissions_id_seq;
DROP TABLE public.auth_user_user_permissions;
DROP SEQUENCE public.auth_user_id_seq;
DROP SEQUENCE public.auth_user_groups_id_seq;
DROP TABLE public.auth_user_groups;
DROP TABLE public.auth_user;
DROP SEQUENCE public.auth_permission_id_seq;
DROP TABLE public.auth_permission;
DROP SEQUENCE public.auth_group_permissions_id_seq;
DROP TABLE public.auth_group_permissions;
DROP SEQUENCE public.auth_group_id_seq;
DROP TABLE public.auth_group;
DROP SEQUENCE public.agribiot_value_id_seq;
DROP TABLE public.agribiot_value;
DROP SEQUENCE public.agribiot_task_id_seq;
DROP TABLE public.agribiot_task;
DROP SEQUENCE public.agribiot_sensorobject_id_seq;
DROP TABLE public.agribiot_sensorobject;
DROP SEQUENCE public.agribiot_sensor_id_seq;
DROP TABLE public.agribiot_sensor;
DROP SEQUENCE public.agribiot_risedalarm_id_seq;
DROP TABLE public.agribiot_risedalarm;
DROP TABLE public.agribiot_rfiddeviceuser;
DROP SEQUENCE public.agribiot_remoteinstance_id_seq;
DROP TABLE public.agribiot_remoteinstance;
DROP SEQUENCE public.agribiot_remoteaccess_sensors_id_seq;
DROP TABLE public.agribiot_remoteaccess_sensors;
DROP SEQUENCE public.agribiot_remoteaccess_sensor_objects_id_seq;
DROP TABLE public.agribiot_remoteaccess_sensor_objects;
DROP SEQUENCE public.agribiot_remoteaccess_selected_objects_id_seq;
DROP TABLE public.agribiot_remoteaccess_selected_objects;
DROP SEQUENCE public.agribiot_remoteaccess_id_seq;
DROP TABLE public.agribiot_remoteaccess;
DROP SEQUENCE public.agribiot_parcel_id_seq;
DROP TABLE public.agribiot_parcel;
DROP SEQUENCE public.agribiot_operation_id_seq;
DROP TABLE public.agribiot_operation;
DROP TABLE public.agribiot_objectuser;
DROP SEQUENCE public.agribiot_object_id_seq;
DROP TABLE public.agribiot_object;
DROP TABLE public.agribiot_gatewayuser;
DROP TABLE public.agribiot_foreignfarmuser;
DROP SEQUENCE public.agribiot_farm_id_seq;
DROP TABLE public.agribiot_farm;
DROP SEQUENCE public.agribiot_alarmtrigger_id_seq;
DROP TABLE public.agribiot_alarmtrigger;
DROP SEQUENCE public.agribiot_alarmelement_id_seq;
DROP TABLE public.agribiot_alarmelement;
DROP EXTENSION postgis;
DROP EXTENSION plpython3u;
--
-- Name: plpython3u; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpython3u WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpython3u; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpython3u IS 'PL/Python3U untrusted procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agribiot_alarmelement; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_alarmelement (
    id integer NOT NULL,
    trigger_type character varying(100) NOT NULL,
    trigger_value double precision NOT NULL,
    geometry public.geography(Polygon,4326),
    alarm_trigger_id integer NOT NULL,
    object_id integer,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_alarmelement OWNER TO agribiot;

--
-- Name: agribiot_alarmelement_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_alarmelement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_alarmelement_id_seq OWNER TO agribiot;

--
-- Name: agribiot_alarmelement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_alarmelement_id_seq OWNED BY public.agribiot_alarmelement.id;


--
-- Name: agribiot_alarmtrigger; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_alarmtrigger (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_alarmtrigger OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_alarmtrigger_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_alarmtrigger_id_seq OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_alarmtrigger_id_seq OWNED BY public.agribiot_alarmtrigger.id;


--
-- Name: agribiot_farm; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_farm (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_farm OWNER TO agribiot;

--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_farm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_farm_id_seq OWNER TO agribiot;

--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_farm_id_seq OWNED BY public.agribiot_farm.id;


--
-- Name: agribiot_foreignfarmuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_foreignfarmuser (
    user_ptr_id integer NOT NULL,
    url_origin character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_foreignfarmuser OWNER TO agribiot;

--
-- Name: agribiot_gatewayuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_gatewayuser (
    user_ptr_id integer NOT NULL
);


ALTER TABLE public.agribiot_gatewayuser OWNER TO agribiot;

--
-- Name: agribiot_object; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_object (
    id integer NOT NULL,
    "position" public.geography(Point,4326) NOT NULL,
    uuid character varying(100) NOT NULL,
    description text NOT NULL,
    user_id integer
);


ALTER TABLE public.agribiot_object OWNER TO agribiot;

--
-- Name: agribiot_object_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_object_id_seq OWNER TO agribiot;

--
-- Name: agribiot_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_object_id_seq OWNED BY public.agribiot_object.id;


--
-- Name: agribiot_objectuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_objectuser (
    user_ptr_id integer NOT NULL
);


ALTER TABLE public.agribiot_objectuser OWNER TO agribiot;

--
-- Name: agribiot_operation; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_operation (
    id integer NOT NULL,
    imported timestamp with time zone NOT NULL,
    created timestamp with time zone NOT NULL,
    reader_uuid character varying(100) NOT NULL,
    tag_embedded_task_index integer NOT NULL,
    value double precision NOT NULL,
    "position" public.geography(Point,4326) NOT NULL,
    accuracy double precision NOT NULL,
    sensor_object_id integer NOT NULL,
    task_id integer NOT NULL
);


ALTER TABLE public.agribiot_operation OWNER TO agribiot;

--
-- Name: agribiot_operation_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_operation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_operation_id_seq OWNER TO agribiot;

--
-- Name: agribiot_operation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_operation_id_seq OWNED BY public.agribiot_operation.id;


--
-- Name: agribiot_parcel; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_parcel (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    geometry public.geography(Polygon,4326) NOT NULL,
    farm_id integer NOT NULL
);


ALTER TABLE public.agribiot_parcel OWNER TO agribiot;

--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_parcel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_parcel_id_seq OWNER TO agribiot;

--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_parcel_id_seq OWNED BY public.agribiot_parcel.id;


--
-- Name: agribiot_remoteaccess; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess (
    id integer NOT NULL,
    access_type integer NOT NULL,
    target_type integer NOT NULL,
    geometry public.geography(Polygon,4326),
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    foreign_users integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_id_seq OWNED BY public.agribiot_remoteaccess.id;


--
-- Name: agribiot_remoteaccess_selected_objects; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess_selected_objects (
    id integer NOT NULL,
    remoteaccess_id integer NOT NULL,
    object_id integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess_selected_objects OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_selected_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_selected_objects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_selected_objects_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_selected_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_selected_objects_id_seq OWNED BY public.agribiot_remoteaccess_selected_objects.id;


--
-- Name: agribiot_remoteaccess_sensor_objects; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess_sensor_objects (
    id integer NOT NULL,
    remoteaccess_id integer NOT NULL,
    sensorobject_id integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess_sensor_objects OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensor_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_sensor_objects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_sensor_objects_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensor_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_sensor_objects_id_seq OWNED BY public.agribiot_remoteaccess_sensor_objects.id;


--
-- Name: agribiot_remoteaccess_sensors; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess_sensors (
    id integer NOT NULL,
    remoteaccess_id integer NOT NULL,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess_sensors OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensors_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_sensors_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_sensors_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_sensors_id_seq OWNED BY public.agribiot_remoteaccess_sensors.id;


--
-- Name: agribiot_remoteinstance; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteinstance (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    token character varying(256) NOT NULL,
    url character varying(256) NOT NULL,
    color character varying(7) NOT NULL
);


ALTER TABLE public.agribiot_remoteinstance OWNER TO agribiot;

--
-- Name: agribiot_remoteinstance_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteinstance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteinstance_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteinstance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteinstance_id_seq OWNED BY public.agribiot_remoteinstance.id;


--
-- Name: agribiot_rfiddeviceuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_rfiddeviceuser (
    user_ptr_id integer NOT NULL
);


ALTER TABLE public.agribiot_rfiddeviceuser OWNER TO agribiot;

--
-- Name: agribiot_risedalarm; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_risedalarm (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    alarm_trigger_id integer NOT NULL,
    value_id integer NOT NULL
);


ALTER TABLE public.agribiot_risedalarm OWNER TO agribiot;

--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_risedalarm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_risedalarm_id_seq OWNER TO agribiot;

--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_risedalarm_id_seq OWNED BY public.agribiot_risedalarm.id;


--
-- Name: agribiot_sensor; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_sensor (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    unit character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_sensor OWNER TO agribiot;

--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_sensor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_sensor_id_seq OWNER TO agribiot;

--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_sensor_id_seq OWNED BY public.agribiot_sensor.id;


--
-- Name: agribiot_sensorobject; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_sensorobject (
    id integer NOT NULL,
    object_id integer NOT NULL,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_sensorobject OWNER TO agribiot;

--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_sensorobject_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_sensorobject_id_seq OWNER TO agribiot;

--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_sensorobject_id_seq OWNED BY public.agribiot_sensorobject.id;


--
-- Name: agribiot_task; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_task (
    id integer NOT NULL,
    description text NOT NULL
);


ALTER TABLE public.agribiot_task OWNER TO agribiot;

--
-- Name: agribiot_task_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_task_id_seq OWNER TO agribiot;

--
-- Name: agribiot_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_task_id_seq OWNED BY public.agribiot_task.id;


--
-- Name: agribiot_value; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_value (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    value double precision NOT NULL,
    sensor_object_id integer NOT NULL
);


ALTER TABLE public.agribiot_value OWNER TO agribiot;

--
-- Name: agribiot_value_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_value_id_seq OWNER TO agribiot;

--
-- Name: agribiot_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_value_id_seq OWNED BY public.agribiot_value.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO agribiot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO agribiot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO agribiot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO agribiot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO agribiot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO agribiot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO agribiot;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO agribiot;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO agribiot;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO agribiot;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO agribiot;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO agribiot;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO agribiot;

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO agribiot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO agribiot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO agribiot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO agribiot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO agribiot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO agribiot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO agribiot;

--
-- Name: guardian_groupobjectpermission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.guardian_groupobjectpermission (
    id integer NOT NULL,
    object_pk character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.guardian_groupobjectpermission OWNER TO agribiot;

--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.guardian_groupobjectpermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guardian_groupobjectpermission_id_seq OWNER TO agribiot;

--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.guardian_groupobjectpermission_id_seq OWNED BY public.guardian_groupobjectpermission.id;


--
-- Name: guardian_userobjectpermission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.guardian_userobjectpermission (
    id integer NOT NULL,
    object_pk character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    permission_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.guardian_userobjectpermission OWNER TO agribiot;

--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.guardian_userobjectpermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guardian_userobjectpermission_id_seq OWNER TO agribiot;

--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.guardian_userobjectpermission_id_seq OWNED BY public.guardian_userobjectpermission.id;


--
-- Name: agribiot_alarmelement id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement ALTER COLUMN id SET DEFAULT nextval('public.agribiot_alarmelement_id_seq'::regclass);


--
-- Name: agribiot_alarmtrigger id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger ALTER COLUMN id SET DEFAULT nextval('public.agribiot_alarmtrigger_id_seq'::regclass);


--
-- Name: agribiot_farm id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_farm ALTER COLUMN id SET DEFAULT nextval('public.agribiot_farm_id_seq'::regclass);


--
-- Name: agribiot_object id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object ALTER COLUMN id SET DEFAULT nextval('public.agribiot_object_id_seq'::regclass);


--
-- Name: agribiot_operation id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation ALTER COLUMN id SET DEFAULT nextval('public.agribiot_operation_id_seq'::regclass);


--
-- Name: agribiot_parcel id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel ALTER COLUMN id SET DEFAULT nextval('public.agribiot_parcel_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess_selected_objects id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_selected_objects_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess_sensor_objects id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_sensor_objects_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess_sensors id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_sensors_id_seq'::regclass);


--
-- Name: agribiot_remoteinstance id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteinstance ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteinstance_id_seq'::regclass);


--
-- Name: agribiot_risedalarm id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm ALTER COLUMN id SET DEFAULT nextval('public.agribiot_risedalarm_id_seq'::regclass);


--
-- Name: agribiot_sensor id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor ALTER COLUMN id SET DEFAULT nextval('public.agribiot_sensor_id_seq'::regclass);


--
-- Name: agribiot_sensorobject id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject ALTER COLUMN id SET DEFAULT nextval('public.agribiot_sensorobject_id_seq'::regclass);


--
-- Name: agribiot_task id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_task ALTER COLUMN id SET DEFAULT nextval('public.agribiot_task_id_seq'::regclass);


--
-- Name: agribiot_value id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value ALTER COLUMN id SET DEFAULT nextval('public.agribiot_value_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: guardian_groupobjectpermission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission ALTER COLUMN id SET DEFAULT nextval('public.guardian_groupobjectpermission_id_seq'::regclass);


--
-- Name: guardian_userobjectpermission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission ALTER COLUMN id SET DEFAULT nextval('public.guardian_userobjectpermission_id_seq'::regclass);


--
-- Data for Name: agribiot_alarmelement; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_alarmelement (id, trigger_type, trigger_value, geometry, alarm_trigger_id, object_id, sensor_id) FROM stdin;
\.


--
-- Data for Name: agribiot_alarmtrigger; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_alarmtrigger (id, name) FROM stdin;
\.


--
-- Data for Name: agribiot_farm; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_farm (id, name) FROM stdin;
\.


--
-- Data for Name: agribiot_foreignfarmuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_foreignfarmuser (user_ptr_id, url_origin) FROM stdin;
\.


--
-- Data for Name: agribiot_gatewayuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_gatewayuser (user_ptr_id) FROM stdin;
4
\.


--
-- Data for Name: agribiot_object; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_object (id, "position", uuid, description, user_id) FROM stdin;
3	0101000020E6100000010000C026C8F4BFD604AE435A924740	E2003412012C0100	Muscadet	8
5	0101000020E61000000100008073BBF4BFFFF17EA241924740	E2003412012E0100	Sauvignon	10
6	0101000020E6100000FFFFFF7FDBBCF4BF97490B014A924740	E2003412012F0100	Muscadet Champ Garreau	\N
8	0101000020E61000001F4B1FBAA0BEF4BFC2CDCFFA81924740	E200341201381700	Merlot	12
4	0101000020E610000001000040E3C7F4BFFFF17EA241924740	E2003412012A0100	Cru Mouzillon-Tillières	9
9	0101000020E6100000FCD6615FC5BCF4BFD5BD28544A924740	E2003412012C1700		13
10	0101000020E610000061AE32408FBEF4BFB16D516683924740	E200341201300100		14
\.


--
-- Data for Name: agribiot_objectuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_objectuser (user_ptr_id) FROM stdin;
8
9
10
11
12
13
14
\.


--
-- Data for Name: agribiot_operation; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_operation (id, imported, created, reader_uuid, tag_embedded_task_index, value, "position", accuracy, sensor_object_id, task_id) FROM stdin;
3	2021-02-10 14:32:21.290093+01	2021-02-10 15:09:45.239+01		2	0	0101000020E610000086284C0502CAF4BF82FA96395D924740	5.69999980926513672	3	3
4	2021-02-10 14:32:21.332823+01	2021-02-10 15:12:29.613+01		2	0	0101000020E6100000AF7451078CC8F4BF00E8305F5E924740	1.5	3	3
5	2021-02-10 14:32:21.985652+01	2021-02-10 15:15:25.908+01		2	0	0101000020E610000092FD0600A1C8F4BF84CB3D3747924740	1.29999995231628418	4	3
6	2021-02-10 14:32:22.635979+01	2021-02-10 15:20:19.332+01		2	0	0101000020E61000008CCE0CF5CEBBF4BFDCD7817346924740	1.5	5	3
7	2021-02-10 14:32:23.1884+01	2021-02-10 15:22:30.566+01		2	0	0101000020E6100000FCD6615FC5BCF4BF4970109A4A924740	1.10000002384185791	6	3
8	2021-02-10 14:32:23.8511+01	2021-02-10 15:25:51.946+01		4	0	0101000020E61000001F4B1FBAA0BEF4BFC2CDCFFA81924740	1.5	7	3
9	2021-03-03 13:17:20.833722+01	2021-03-03 10:33:53.778+01		2	0	0101000020E61000008870B9E7E6C8F4BF935A15CE5B924740	2.09999990463256836	3	7
10	2021-03-03 13:17:20.901092+01	2021-03-03 10:37:49.764+01		2	0	0101000020E61000009F4471B4D0BBF4BF6B300DC347924740	2.5	5	7
11	2021-03-03 13:17:43.19064+01	2021-03-03 10:34:05.927+01		2	0	0101000020E6100000A45B9C1E01C9F4BF5E1941505B924740	1.79999995231628418	3	8
12	2021-03-03 13:17:43.258192+01	2021-03-03 10:38:04.643+01		2	0	0101000020E6100000CEA5B8AAECBBF4BFB7019F1F46924740	1.5	5	8
13	2021-03-03 14:39:38.104331+01	2021-03-03 15:19:23.629+01		2	0	0101000020E61000001E65D7D0E0C9F4BFED71CC9F5C924740	3.70000004768371582	3	6
14	2021-03-03 14:39:38.169193+01	2021-03-03 15:20:59.069+01		2	0	0101000020E610000004DA301D4DC8F4BF95362F6147924740	1.79999995231628418	4	6
15	2021-03-03 14:39:38.200078+01	2021-03-03 15:21:21.601+01		2	0	0101000020E61000007F130A1170C8F4BF3E1F788F46924740	1.5	4	6
16	2021-03-03 14:39:38.231848+01	2021-03-03 15:25:05.886+01		2	0	0101000020E6100000F41DE9F9C0BBF4BFD5B2B5BE48924740	2.90000009536743164	5	6
17	2021-03-03 14:39:38.263622+01	2021-03-03 15:26:08.572+01		2	0	0101000020E6100000BBA3EC1A1ABCF4BFF788981249924740	2.90000009536743164	6	6
18	2021-03-03 14:39:39.006489+01	2021-03-03 15:26:39.839+01		3	0	0101000020E6100000FCD6615FC5BCF4BFD5BD28544A924740	1.39999997615814209	8	6
19	2021-03-03 14:39:39.65089+01	2021-03-03 15:32:50.786+01		2	0	0101000020E610000061AE32408FBEF4BFB16D516683924740	2.09999990463256836	9	6
\.


--
-- Data for Name: agribiot_parcel; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_parcel (id, name, geometry, farm_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess (id, access_type, target_type, geometry, start_date, end_date, foreign_users) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess_selected_objects; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess_selected_objects (id, remoteaccess_id, object_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess_sensor_objects; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess_sensor_objects (id, remoteaccess_id, sensorobject_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess_sensors; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess_sensors (id, remoteaccess_id, sensor_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteinstance; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteinstance (id, name, token, url, color) FROM stdin;
\.


--
-- Data for Name: agribiot_rfiddeviceuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_rfiddeviceuser (user_ptr_id) FROM stdin;
5
\.


--
-- Data for Name: agribiot_risedalarm; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_risedalarm (id, created, alarm_trigger_id, value_id) FROM stdin;
\.


--
-- Data for Name: agribiot_sensor; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_sensor (id, name, unit) FROM stdin;
1	Température	°C
2	Humidité	%
3	tag	
\.


--
-- Data for Name: agribiot_sensorobject; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_sensorobject (id, object_id, sensor_id) FROM stdin;
3	3	3
4	4	3
5	5	3
6	6	3
7	8	3
8	9	3
9	10	3
\.


--
-- Data for Name: agribiot_task; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_task (id, description) FROM stdin;
3	Taillage
4	Pliage
5	Broyage
6	test
7	Réparation faite
8	fils tendus
\.


--
-- Data for Name: agribiot_value; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_value (id, created, value, sensor_object_id) FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_group (id, name) FROM stdin;
1	farmUserGroup
2	gatewayGroup
3	RFIDDeviceGroup
4	objectGroup
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	2	55
2	2	76
3	3	83
4	3	103
5	3	100
6	1	40
7	1	99
8	1	87
9	1	72
10	1	54
11	1	51
12	1	52
13	1	53
14	1	76
15	1	73
16	1	74
17	1	75
18	1	107
19	1	104
20	1	105
21	1	106
22	1	36
23	1	33
24	1	34
25	1	35
26	1	91
27	1	88
28	1	89
29	1	90
30	1	41
31	1	44
32	1	43
33	1	92
34	1	95
35	1	94
36	1	60
37	1	63
38	1	62
39	1	80
40	1	83
41	1	82
42	1	100
43	1	103
44	1	102
45	1	64
46	1	65
47	1	67
48	1	66
49	1	46
50	1	47
51	1	49
52	1	48
53	1	55
54	1	45
55	1	68
56	1	50
57	4	84
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add Token	7	add_token
26	Can change Token	7	change_token
27	Can delete Token	7	delete_token
28	Can view Token	7	view_token
29	Can add token	8	add_tokenproxy
30	Can change token	8	change_tokenproxy
31	Can delete token	8	delete_tokenproxy
32	Can view token	8	view_tokenproxy
33	Can add alarm trigger	9	add_alarmtrigger
34	Can change alarm trigger	9	change_alarmtrigger
35	Can delete alarm trigger	9	delete_alarmtrigger
36	Can view alarm trigger	9	view_alarmtrigger
37	Can add farm	10	add_farm
38	Can change farm	10	change_farm
39	Can delete farm	10	delete_farm
40	Can view farm	10	view_farm
41	Can add foreign farm user	11	add_foreignfarmuser
42	Can change foreign farm user	11	change_foreignfarmuser
43	Can delete foreign farm user	11	delete_foreignfarmuser
44	Can view foreign farm user	11	view_foreignfarmuser
45	Can access auth token	11	access_foreign_user_token
46	Can add gateway user	12	add_gatewayuser
47	Can change gateway user	12	change_gatewayuser
48	Can delete gateway user	12	delete_gatewayuser
49	Can view gateway user	12	view_gatewayuser
50	Can access auth token	12	access_gateway_user_token
51	Can add object	13	add_object
52	Can change object	13	change_object
53	Can delete object	13	delete_object
54	Can view object	13	view_object
55	Can access auth token	13	access_object_token
56	Can add user	14	add_objectuser
57	Can change user	14	change_objectuser
58	Can delete user	14	delete_objectuser
59	Can view user	14	view_objectuser
60	Can add remote instance	15	add_remoteinstance
61	Can change remote instance	15	change_remoteinstance
62	Can delete remote instance	15	delete_remoteinstance
63	Can view remote instance	15	view_remoteinstance
64	Can add rfid device user	16	add_rfiddeviceuser
65	Can change rfid device user	16	change_rfiddeviceuser
66	Can delete rfid device user	16	delete_rfiddeviceuser
67	Can view rfid device user	16	view_rfiddeviceuser
68	Can access auth token	16	access_rfiddevice_user_token
69	Can add sensor	17	add_sensor
70	Can change sensor	17	change_sensor
71	Can delete sensor	17	delete_sensor
72	Can view sensor	17	view_sensor
73	Can add sensor object	18	add_sensorobject
74	Can change sensor object	18	change_sensorobject
75	Can delete sensor object	18	delete_sensorobject
76	Can view sensor object	18	view_sensorobject
77	View last associated value/operation	18	view_last_value
78	View all associated value/operation	18	view_all_value
79	View all associated value/operation in date range	18	view_date_range_value
80	Can add task	19	add_task
81	Can change task	19	change_task
82	Can delete task	19	delete_task
83	Can view task	19	view_task
84	Can add value	20	add_value
85	Can change value	20	change_value
86	Can delete value	20	delete_value
87	Can view value	20	view_value
88	Can add rised alarm	21	add_risedalarm
89	Can change rised alarm	21	change_risedalarm
90	Can delete rised alarm	21	delete_risedalarm
91	Can view rised alarm	21	view_risedalarm
92	Can add remote access	22	add_remoteaccess
93	Can change remote access	22	change_remoteaccess
94	Can delete remote access	22	delete_remoteaccess
95	Can view remote access	22	view_remoteaccess
96	Can add parcel	23	add_parcel
97	Can change parcel	23	change_parcel
98	Can delete parcel	23	delete_parcel
99	Can view parcel	23	view_parcel
100	Can add operation	24	add_operation
101	Can change operation	24	change_operation
102	Can delete operation	24	delete_operation
103	Can view operation	24	view_operation
104	Can add alarm element	25	add_alarmelement
105	Can change alarm element	25	change_alarmelement
106	Can delete alarm element	25	delete_alarmelement
107	Can view alarm element	25	view_alarmelement
108	Can add group object permission	26	add_groupobjectpermission
109	Can change group object permission	26	change_groupobjectpermission
110	Can delete group object permission	26	delete_groupobjectpermission
111	Can view group object permission	26	view_groupobjectpermission
112	Can add user object permission	27	add_userobjectpermission
113	Can change user object permission	27	change_userobjectpermission
114	Can delete user object permission	27	delete_userobjectpermission
115	Can view user object permission	27	view_userobjectpermission
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	!cQuSW2sAXjdsIVeD72AcfSIo3GDYk6rk0wDcKhdq	\N	f	AnonymousUser				f	t	2021-02-08 17:46:46.891942+01
4		\N	f	gateway			normal@gateway.fr	f	t	2021-02-08 17:46:52.382601+01
5		\N	f	rfidDevice1			normal@rfidDevice1.fr	f	t	2021-02-08 17:46:52.438167+01
2	pbkdf2_sha256$216000$Lb8Y7fqRuERL$VmGfrO4v4n4rlVxeF4dhLij1PF5Yjejl5AKDINb+jq8=	2021-02-10 14:03:05.985582+01	t	root			root@root.fr	t	t	2021-02-08 17:46:51.154818+01
8	pbkdf2_sha256$216000$sE7zCSpQXlmY$ZV4d5GkcjHxkD8RJYFAHUey1Hs4zrFUrpK9RFmlI9ps=	\N	f	E2003412012C0100				f	t	2021-02-10 14:32:20.516099+01
9	pbkdf2_sha256$216000$0dypXL18zCyK$I63px9J58aqkLZfMCCZ52v7iUjNiL0sXRZGrYGmUWTI=	\N	f	E2003412012A0100				f	t	2021-02-10 14:32:21.347877+01
10	pbkdf2_sha256$216000$zsu4DViqjrBi$GeTIPy701pDhP5u+fMMRdXezwGbvBOR60rXHR3Cfx+M=	\N	f	E2003412012E0100				f	t	2021-02-10 14:32:22.001391+01
11	pbkdf2_sha256$216000$XA4CVk6lIdSZ$1G0lrudfGdaTmle9enD0/Ub+qUUQOqsPhs4mRL0QE3I=	\N	f	E2003412012F0100				f	t	2021-02-10 14:32:22.651576+01
12	pbkdf2_sha256$216000$edDI7Sfbfqe5$y+sA6Vdc6HJayed6l1ONFLYsd0Axpx4d4c+9ExbsgzA=	\N	f	E200341201381700				f	t	2021-02-10 14:32:23.207256+01
3	pbkdf2_sha256$216000$wsNBopgFP9Ao$Np4samdDMlxJXRuUwscsYj2hWxi0JwKYRuzHB6gVznw=	2021-03-03 14:02:57.101736+01	f	agribiot			normal@user.fr	f	t	2021-02-08 17:46:51.793819+01
13	pbkdf2_sha256$216000$xGCCcwQA5Zjq$DiQsl5Y07jAdX0RCIQ1Cpy7/SymIPvZbl8cXPSfMZmA=	\N	f	E2003412012C1700				f	t	2021-03-03 14:39:38.291981+01
14	pbkdf2_sha256$216000$1IaflvRhACye$jlywPz4QsloPJmA2pVlWR4MLkOL+ZsJ7g8DlgFZVB44=	\N	f	E200341201300100				f	t	2021-03-03 14:39:39.021613+01
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
1	3	1
2	4	2
3	5	3
8	8	4
10	9	4
12	10	4
14	11	4
15	12	4
18	13	4
20	14	4
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
eac38ce677ac816d9e6064d2e713f7019ca51342	2021-02-08 17:48:20.462099+01	5
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2021-02-08 17:47:56.660306+01	3	agribiot	2	[{"changed": {"fields": ["password"]}}]	4	2
2	2021-02-10 14:03:27.048239+01	2	Object object (2)	3		13	2
3	2021-02-10 14:03:27.086398+01	1	Object object (1)	3		13	2
4	2021-02-10 14:03:37.277549+01	6	E2003412012B0100	3		4	2
5	2021-02-10 14:03:37.311887+01	7	E200341201301700	3		4	2
6	2021-02-10 14:03:45.649648+01	2	Task object (2)	3		19	2
7	2021-02-10 14:03:45.683017+01	1	Task object (1)	3		19	2
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	authtoken	token
8	authtoken	tokenproxy
9	agribiot	alarmtrigger
10	agribiot	farm
11	agribiot	foreignfarmuser
12	agribiot	gatewayuser
13	agribiot	object
14	agribiot	objectuser
15	agribiot	remoteinstance
16	agribiot	rfiddeviceuser
17	agribiot	sensor
18	agribiot	sensorobject
19	agribiot	task
20	agribiot	value
21	agribiot	risedalarm
22	agribiot	remoteaccess
23	agribiot	parcel
24	agribiot	operation
25	agribiot	alarmelement
26	guardian	groupobjectpermission
27	guardian	userobjectpermission
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-02-08 17:46:43.089844+01
2	auth	0001_initial	2021-02-08 17:46:43.233005+01
3	admin	0001_initial	2021-02-08 17:46:43.388253+01
4	admin	0002_logentry_remove_auto_add	2021-02-08 17:46:43.44933+01
5	admin	0003_logentry_add_action_flag_choices	2021-02-08 17:46:43.482311+01
6	contenttypes	0002_remove_content_type_name	2021-02-08 17:46:43.558505+01
7	auth	0002_alter_permission_name_max_length	2021-02-08 17:46:43.624639+01
8	auth	0003_alter_user_email_max_length	2021-02-08 17:46:43.659744+01
9	auth	0004_alter_user_username_opts	2021-02-08 17:46:43.693059+01
10	auth	0005_alter_user_last_login_null	2021-02-08 17:46:43.735781+01
11	auth	0006_require_contenttypes_0002	2021-02-08 17:46:43.744649+01
12	auth	0007_alter_validators_add_error_messages	2021-02-08 17:46:43.780893+01
13	auth	0008_alter_user_username_max_length	2021-02-08 17:46:43.827193+01
14	auth	0009_alter_user_last_name_max_length	2021-02-08 17:46:43.867174+01
15	auth	0010_alter_group_name_max_length	2021-02-08 17:46:43.908445+01
16	auth	0011_update_proxy_permissions	2021-02-08 17:46:43.948864+01
17	auth	0012_alter_user_first_name_max_length	2021-02-08 17:46:43.98349+01
18	agribiot	0001_initial	2021-02-08 17:46:45.061134+01
19	authtoken	0001_initial	2021-02-08 17:46:45.464141+01
20	authtoken	0002_auto_20160226_1747	2021-02-08 17:46:45.732102+01
21	authtoken	0003_tokenproxy	2021-02-08 17:46:45.776902+01
22	guardian	0001_initial	2021-02-08 17:46:46.138379+01
23	guardian	0002_generic_permissions_index	2021-02-08 17:46:46.3416+01
24	sessions	0001_initial	2021-02-08 17:46:46.370462+01
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
pzvprih70miwqy6xcl5bfgi621o151ns	.eJxVjEEOgjAQRe_StWk6M1CoS_eegcy0g0VNSSisjHdXEha6_e-9_zIDb2setqrLMCVzNmROv5twfGjZQbpzuc02zmVdJrG7Yg9a7XVO-rwc7t9B5pq_NTCFvtVRkHGktsMeVEJC8ty4AMkHDwKkERshAA3dGBFRlFyMznnz_gDTLDdZ:1l9SZ9:wnxNFNli7XYNgGw4vg6z4rQRwsValJMximWMZvAvIxY	2021-02-23 13:56:27.225987+01
hhrnych9nkzpa8e6wah5fmufdy7zzsii	.eJxVjEEOgjAQRe_StWk6M1CoS_eegcy0g0VNSSisjHdXEha6_e-9_zIDb2setqrLMCVzNmROv5twfGjZQbpzuc02zmVdJrG7Yg9a7XVO-rwc7t9B5pq_NTCFvtVRkHGktsMeVEJC8ty4AMkHDwKkERshAA3dGBFRlFyMznnz_gDTLDdZ:1l9p6E:TMCKdUqTLXZa8ARcVvrgQhikufeFlQGjjcKN4asYdrc	2021-02-24 14:00:06.857086+01
9vn503nfy3jm2uzha7wp88hmps6zekfu	.eJxVjEsOwiAUAO_C2hBoHz-X7nsG8niAVA1NSrsy3t2SdKHbmcm8mcd9K35vafVzZFc2sMsvC0jPVLuID6z3hdNSt3UOvCf8tI1PS0yv29n-DQq20rcBAGPSGR2RIBttUnJ0BgJlCyRcADyUsRqssHmQUioySoijSqPW7PMF96k3dQ:1l9p98:2-dczOmbXbZxa1-Ci4dfxkkR1HUoq_aGmoG6KxwU4sU	2021-02-24 14:03:06.009367+01
o17c410s28s36emp20xl6wd8kh533kfx	.eJxVjEEOgjAQRe_StWk6M1CoS_eegcy0g0VNSSisjHdXEha6_e-9_zIDb2setqrLMCVzNmROv5twfGjZQbpzuc02zmVdJrG7Yg9a7XVO-rwc7t9B5pq_NTCFvtVRkHGktsMeVEJC8ty4AMkHDwKkERshAA3dGBFRlFyMznnz_gDTLDdZ:1lHMtB:uQCz5WxzbHAxIbW4KBCe5vhQThIfrD-WCTjztetJ7a8	2021-03-17 09:29:49.272036+01
27a1rj8kfwiw4gzswzp49gikyddkzlft	.eJxVjEEOgjAQRe_StWk6M1CoS_eegcy0g0VNSSisjHdXEha6_e-9_zIDb2setqrLMCVzNmROv5twfGjZQbpzuc02zmVdJrG7Yg9a7XVO-rwc7t9B5pq_NTCFvtVRkHGktsMeVEJC8ty4AMkHDwKkERshAA3dGBFRlFyMznnz_gDTLDdZ:1lHR9V:sbcBL8bvomSIF4ttIQEd9IGuzVG_P_mHMMTtreXeY2k	2021-03-17 14:02:57.11958+01
\.


--
-- Data for Name: guardian_groupobjectpermission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.guardian_groupobjectpermission (id, object_pk, content_type_id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: guardian_userobjectpermission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.guardian_userobjectpermission (id, object_pk, content_type_id, permission_id, user_id) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Name: agribiot_alarmelement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_alarmelement_id_seq', 1, false);


--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_alarmtrigger_id_seq', 1, false);


--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_farm_id_seq', 1, false);


--
-- Name: agribiot_object_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_object_id_seq', 10, true);


--
-- Name: agribiot_operation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_operation_id_seq', 19, true);


--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_parcel_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_selected_objects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_selected_objects_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_sensor_objects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_sensor_objects_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_sensors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_sensors_id_seq', 1, false);


--
-- Name: agribiot_remoteinstance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteinstance_id_seq', 1, false);


--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_risedalarm_id_seq', 1, false);


--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_sensor_id_seq', 3, true);


--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_sensorobject_id_seq', 9, true);


--
-- Name: agribiot_task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_task_id_seq', 8, true);


--
-- Name: agribiot_value_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_value_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 4, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 57, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 115, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 21, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 14, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 7, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 27, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 24, true);


--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.guardian_groupobjectpermission_id_seq', 1, false);


--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.guardian_userobjectpermission_id_seq', 1, false);


--
-- Name: agribiot_alarmelement agribiot_alarmelement_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelement_pkey PRIMARY KEY (id);


--
-- Name: agribiot_alarmtrigger agribiot_alarmtrigger_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger
    ADD CONSTRAINT agribiot_alarmtrigger_name_key UNIQUE (name);


--
-- Name: agribiot_alarmtrigger agribiot_alarmtrigger_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger
    ADD CONSTRAINT agribiot_alarmtrigger_pkey PRIMARY KEY (id);


--
-- Name: agribiot_farm agribiot_farm_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_farm
    ADD CONSTRAINT agribiot_farm_pkey PRIMARY KEY (id);


--
-- Name: agribiot_foreignfarmuser agribiot_foreignfarmuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_foreignfarmuser
    ADD CONSTRAINT agribiot_foreignfarmuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_gatewayuser agribiot_gatewayuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_gatewayuser
    ADD CONSTRAINT agribiot_gatewayuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_object agribiot_object_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_pkey PRIMARY KEY (id);


--
-- Name: agribiot_object agribiot_object_uuid_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_uuid_key UNIQUE (uuid);


--
-- Name: agribiot_objectuser agribiot_objectuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_objectuser
    ADD CONSTRAINT agribiot_objectuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_operation agribiot_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation
    ADD CONSTRAINT agribiot_operation_pkey PRIMARY KEY (id);


--
-- Name: agribiot_parcel agribiot_parcel_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel
    ADD CONSTRAINT agribiot_parcel_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess agribiot_remoteaccess_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess
    ADD CONSTRAINT agribiot_remoteaccess_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteaccess_se_remoteaccess_id_object_i_6a4c0d2b_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_object_i_6a4c0d2b_uniq UNIQUE (remoteaccess_id, object_id);


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteaccess_se_remoteaccess_id_sensor_i_0da2fc55_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensor_i_0da2fc55_uniq UNIQUE (remoteaccess_id, sensor_id);


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteaccess_se_remoteaccess_id_sensorob_81219c57_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensorob_81219c57_uniq UNIQUE (remoteaccess_id, sensorobject_id);


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteaccess_selected_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteaccess_selected_objects_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteaccess_sensor_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteaccess_sensor_objects_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteaccess_sensors_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteaccess_sensors_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteinstance agribiot_remoteinstance_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteinstance
    ADD CONSTRAINT agribiot_remoteinstance_pkey PRIMARY KEY (id);


--
-- Name: agribiot_rfiddeviceuser agribiot_rfiddeviceuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_rfiddeviceuser
    ADD CONSTRAINT agribiot_rfiddeviceuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_risedalarm agribiot_risedalarm_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_pkey PRIMARY KEY (id);


--
-- Name: agribiot_sensor agribiot_sensor_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor
    ADD CONSTRAINT agribiot_sensor_name_key UNIQUE (name);


--
-- Name: agribiot_sensor agribiot_sensor_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor
    ADD CONSTRAINT agribiot_sensor_pkey PRIMARY KEY (id);


--
-- Name: agribiot_sensorobject agribiot_sensorobject_object_id_sensor_id_a7d20fea_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_object_id_sensor_id_a7d20fea_uniq UNIQUE (object_id, sensor_id);


--
-- Name: agribiot_sensorobject agribiot_sensorobject_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_pkey PRIMARY KEY (id);


--
-- Name: agribiot_task agribiot_task_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_task
    ADD CONSTRAINT agribiot_task_pkey PRIMARY KEY (id);


--
-- Name: agribiot_value agribiot_value_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value
    ADD CONSTRAINT agribiot_value_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: guardian_groupobjectpermission guardian_groupobjectperm_group_id_permission_id_o_3f189f7c_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobjectperm_group_id_permission_id_o_3f189f7c_uniq UNIQUE (group_id, permission_id, object_pk);


--
-- Name: guardian_groupobjectpermission guardian_groupobjectpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobjectpermission_pkey PRIMARY KEY (id);


--
-- Name: guardian_userobjectpermission guardian_userobjectpermi_user_id_permission_id_ob_b0b3d2fc_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermi_user_id_permission_id_ob_b0b3d2fc_uniq UNIQUE (user_id, permission_id, object_pk);


--
-- Name: guardian_userobjectpermission guardian_userobjectpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermission_pkey PRIMARY KEY (id);


--
-- Name: agribiot_alarmelement_alarm_trigger_id_9ba193db; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_alarm_trigger_id_9ba193db ON public.agribiot_alarmelement USING btree (alarm_trigger_id);


--
-- Name: agribiot_alarmelement_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_geometry_id ON public.agribiot_alarmelement USING gist (geometry);


--
-- Name: agribiot_alarmelement_object_id_c292df7b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_object_id_c292df7b ON public.agribiot_alarmelement USING btree (object_id);


--
-- Name: agribiot_alarmelement_sensor_id_ab7c0bfe; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_sensor_id_ab7c0bfe ON public.agribiot_alarmelement USING btree (sensor_id);


--
-- Name: agribiot_alarmtrigger_name_2a404727_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmtrigger_name_2a404727_like ON public.agribiot_alarmtrigger USING btree (name varchar_pattern_ops);


--
-- Name: agribiot_object_position_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_position_id ON public.agribiot_object USING gist ("position");


--
-- Name: agribiot_object_user_id_69163bcb; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_user_id_69163bcb ON public.agribiot_object USING btree (user_id);


--
-- Name: agribiot_object_uuid_a3a0d421_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_uuid_a3a0d421_like ON public.agribiot_object USING btree (uuid varchar_pattern_ops);


--
-- Name: agribiot_operation_position_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_position_id ON public.agribiot_operation USING gist ("position");


--
-- Name: agribiot_operation_sensor_object_id_cb2c3e2f; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_sensor_object_id_cb2c3e2f ON public.agribiot_operation USING btree (sensor_object_id);


--
-- Name: agribiot_operation_task_id_13228df0; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_task_id_13228df0 ON public.agribiot_operation USING btree (task_id);


--
-- Name: agribiot_parcel_farm_id_63c02727; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_parcel_farm_id_63c02727 ON public.agribiot_parcel USING btree (farm_id);


--
-- Name: agribiot_parcel_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_parcel_geometry_id ON public.agribiot_parcel USING gist (geometry);


--
-- Name: agribiot_remoteaccess_foreign_users_207df1dc; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_foreign_users_207df1dc ON public.agribiot_remoteaccess USING btree (foreign_users);


--
-- Name: agribiot_remoteaccess_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_geometry_id ON public.agribiot_remoteaccess USING gist (geometry);


--
-- Name: agribiot_remoteaccess_selected_objects_object_id_b8286ddc; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_selected_objects_object_id_b8286ddc ON public.agribiot_remoteaccess_selected_objects USING btree (object_id);


--
-- Name: agribiot_remoteaccess_selected_objects_remoteaccess_id_382e8a77; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_selected_objects_remoteaccess_id_382e8a77 ON public.agribiot_remoteaccess_selected_objects USING btree (remoteaccess_id);


--
-- Name: agribiot_remoteaccess_sensor_objects_remoteaccess_id_b12c6c90; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensor_objects_remoteaccess_id_b12c6c90 ON public.agribiot_remoteaccess_sensor_objects USING btree (remoteaccess_id);


--
-- Name: agribiot_remoteaccess_sensor_objects_sensorobject_id_3a831118; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensor_objects_sensorobject_id_3a831118 ON public.agribiot_remoteaccess_sensor_objects USING btree (sensorobject_id);


--
-- Name: agribiot_remoteaccess_sensors_remoteaccess_id_a33d91e7; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensors_remoteaccess_id_a33d91e7 ON public.agribiot_remoteaccess_sensors USING btree (remoteaccess_id);


--
-- Name: agribiot_remoteaccess_sensors_sensor_id_a56784c0; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensors_sensor_id_a56784c0 ON public.agribiot_remoteaccess_sensors USING btree (sensor_id);


--
-- Name: agribiot_risedalarm_alarm_trigger_id_d6008608; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_risedalarm_alarm_trigger_id_d6008608 ON public.agribiot_risedalarm USING btree (alarm_trigger_id);


--
-- Name: agribiot_risedalarm_value_id_49e5444e; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_risedalarm_value_id_49e5444e ON public.agribiot_risedalarm USING btree (value_id);


--
-- Name: agribiot_sensor_name_56d3fb1c_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensor_name_56d3fb1c_like ON public.agribiot_sensor USING btree (name varchar_pattern_ops);


--
-- Name: agribiot_sensorobject_object_id_fcf9bc39; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensorobject_object_id_fcf9bc39 ON public.agribiot_sensorobject USING btree (object_id);


--
-- Name: agribiot_sensorobject_sensor_id_4a836e2c; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensorobject_sensor_id_4a836e2c ON public.agribiot_sensorobject USING btree (sensor_id);


--
-- Name: agribiot_value_sensor_object_id_97097252; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_value_sensor_object_id_97097252 ON public.agribiot_value USING btree (sensor_object_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: guardian_gr_content_ae6aec_idx; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_gr_content_ae6aec_idx ON public.guardian_groupobjectpermission USING btree (content_type_id, object_pk);


--
-- Name: guardian_groupobjectpermission_content_type_id_7ade36b8; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_groupobjectpermission_content_type_id_7ade36b8 ON public.guardian_groupobjectpermission USING btree (content_type_id);


--
-- Name: guardian_groupobjectpermission_group_id_4bbbfb62; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_groupobjectpermission_group_id_4bbbfb62 ON public.guardian_groupobjectpermission USING btree (group_id);


--
-- Name: guardian_groupobjectpermission_permission_id_36572738; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_groupobjectpermission_permission_id_36572738 ON public.guardian_groupobjectpermission USING btree (permission_id);


--
-- Name: guardian_us_content_179ed2_idx; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_us_content_179ed2_idx ON public.guardian_userobjectpermission USING btree (content_type_id, object_pk);


--
-- Name: guardian_userobjectpermission_content_type_id_2e892405; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_userobjectpermission_content_type_id_2e892405 ON public.guardian_userobjectpermission USING btree (content_type_id);


--
-- Name: guardian_userobjectpermission_permission_id_71807bfc; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_userobjectpermission_permission_id_71807bfc ON public.guardian_userobjectpermission USING btree (permission_id);


--
-- Name: guardian_userobjectpermission_user_id_d5c1e964; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_userobjectpermission_user_id_d5c1e964 ON public.guardian_userobjectpermission USING btree (user_id);


--
-- Name: agribiot_alarmelement agribiot_alarmelemen_alarm_trigger_id_9ba193db_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelemen_alarm_trigger_id_9ba193db_fk_agribiot_ FOREIGN KEY (alarm_trigger_id) REFERENCES public.agribiot_alarmtrigger(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_alarmelement agribiot_alarmelement_object_id_c292df7b_fk_agribiot_object_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelement_object_id_c292df7b_fk_agribiot_object_id FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_alarmelement agribiot_alarmelement_sensor_id_ab7c0bfe_fk_agribiot_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelement_sensor_id_ab7c0bfe_fk_agribiot_sensor_id FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_foreignfarmuser agribiot_foreignfarmuser_user_ptr_id_d62330a8_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_foreignfarmuser
    ADD CONSTRAINT agribiot_foreignfarmuser_user_ptr_id_d62330a8_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_gatewayuser agribiot_gatewayuser_user_ptr_id_c1b9d969_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_gatewayuser
    ADD CONSTRAINT agribiot_gatewayuser_user_ptr_id_c1b9d969_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_object agribiot_object_user_id_69163bcb_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_user_id_69163bcb_fk_agribiot_ FOREIGN KEY (user_id) REFERENCES public.agribiot_objectuser(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_objectuser agribiot_objectuser_user_ptr_id_d9b15f29_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_objectuser
    ADD CONSTRAINT agribiot_objectuser_user_ptr_id_d9b15f29_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_operation agribiot_operation_sensor_object_id_cb2c3e2f_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation
    ADD CONSTRAINT agribiot_operation_sensor_object_id_cb2c3e2f_fk_agribiot_ FOREIGN KEY (sensor_object_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_operation agribiot_operation_task_id_13228df0_fk_agribiot_task_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation
    ADD CONSTRAINT agribiot_operation_task_id_13228df0_fk_agribiot_task_id FOREIGN KEY (task_id) REFERENCES public.agribiot_task(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_parcel agribiot_parcel_farm_id_63c02727_fk_agribiot_farm_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel
    ADD CONSTRAINT agribiot_parcel_farm_id_63c02727_fk_agribiot_farm_id FOREIGN KEY (farm_id) REFERENCES public.agribiot_farm(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess agribiot_remoteacces_foreign_users_207df1dc_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess
    ADD CONSTRAINT agribiot_remoteacces_foreign_users_207df1dc_fk_agribiot_ FOREIGN KEY (foreign_users) REFERENCES public.agribiot_foreignfarmuser(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteacces_object_id_b8286ddc_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteacces_object_id_b8286ddc_fk_agribiot_ FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteacces_remoteaccess_id_382e8a77_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteacces_remoteaccess_id_382e8a77_fk_agribiot_ FOREIGN KEY (remoteaccess_id) REFERENCES public.agribiot_remoteaccess(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteacces_remoteaccess_id_a33d91e7_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteacces_remoteaccess_id_a33d91e7_fk_agribiot_ FOREIGN KEY (remoteaccess_id) REFERENCES public.agribiot_remoteaccess(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteacces_remoteaccess_id_b12c6c90_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteacces_remoteaccess_id_b12c6c90_fk_agribiot_ FOREIGN KEY (remoteaccess_id) REFERENCES public.agribiot_remoteaccess(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteacces_sensor_id_a56784c0_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteacces_sensor_id_a56784c0_fk_agribiot_ FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteacces_sensorobject_id_3a831118_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteacces_sensorobject_id_3a831118_fk_agribiot_ FOREIGN KEY (sensorobject_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_rfiddeviceuser agribiot_rfiddeviceuser_user_ptr_id_6733a39c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_rfiddeviceuser
    ADD CONSTRAINT agribiot_rfiddeviceuser_user_ptr_id_6733a39c_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_risedalarm agribiot_risedalarm_alarm_trigger_id_d6008608_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_alarm_trigger_id_d6008608_fk_agribiot_ FOREIGN KEY (alarm_trigger_id) REFERENCES public.agribiot_alarmtrigger(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_risedalarm agribiot_risedalarm_value_id_49e5444e_fk_agribiot_value_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_value_id_49e5444e_fk_agribiot_value_id FOREIGN KEY (value_id) REFERENCES public.agribiot_value(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_sensorobject agribiot_sensorobject_object_id_fcf9bc39_fk_agribiot_object_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_object_id_fcf9bc39_fk_agribiot_object_id FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_sensorobject agribiot_sensorobject_sensor_id_4a836e2c_fk_agribiot_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_sensor_id_4a836e2c_fk_agribiot_sensor_id FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_value agribiot_value_sensor_object_id_97097252_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value
    ADD CONSTRAINT agribiot_value_sensor_object_id_97097252_fk_agribiot_ FOREIGN KEY (sensor_object_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobjectpermission guardian_groupobject_content_type_id_7ade36b8_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobject_content_type_id_7ade36b8_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobjectpermission guardian_groupobject_group_id_4bbbfb62_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobject_group_id_4bbbfb62_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobjectpermission guardian_groupobject_permission_id_36572738_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobject_permission_id_36572738_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjectpermission guardian_userobjectp_content_type_id_2e892405_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectp_content_type_id_2e892405_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjectpermission guardian_userobjectp_permission_id_71807bfc_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectp_permission_id_71807bfc_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjectpermission guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

