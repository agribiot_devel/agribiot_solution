--
-- PostgreSQL database dump
--

-- Dumped from database version 11.9 (Debian 11.9-0+deb10u1)
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectp_permission_id_71807bfc_fk_auth_perm;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectp_content_type_id_2e892405_fk_django_co;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobject_permission_id_36572738_fk_auth_perm;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobject_group_id_4bbbfb62_fk_auth_grou;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobject_content_type_id_7ade36b8_fk_django_co;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
ALTER TABLE ONLY public.agribiot_value DROP CONSTRAINT agribiot_value_sensor_object_id_97097252_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_sensor_id_4a836e2c_fk_agribiot_sensor_id;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_object_id_fcf9bc39_fk_agribiot_object_id;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_value_id_49e5444e_fk_agribiot_value_id;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_alarm_trigger_id_d6008608_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_rfiddeviceuser DROP CONSTRAINT agribiot_rfiddeviceuser_user_ptr_id_6733a39c_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteacces_sensorobject_id_3a831118_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteacces_sensor_id_a56784c0_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteacces_remoteaccess_id_b12c6c90_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteacces_remoteaccess_id_a33d91e7_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteacces_remoteaccess_id_382e8a77_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteacces_object_id_b8286ddc_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess DROP CONSTRAINT agribiot_remoteacces_foreign_users_207df1dc_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_parcel DROP CONSTRAINT agribiot_parcel_farm_id_63c02727_fk_agribiot_farm_id;
ALTER TABLE ONLY public.agribiot_operation DROP CONSTRAINT agribiot_operation_sensor_object_id_cb2c3e2f_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_operation DROP CONSTRAINT agribiot_operation_annotation_template__af355ffe_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_objectuser DROP CONSTRAINT agribiot_objectuser_user_ptr_id_d9b15f29_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_user_id_69163bcb_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_gatewayuser DROP CONSTRAINT agribiot_gatewayuser_user_ptr_id_c1b9d969_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_foreignfarmuser DROP CONSTRAINT agribiot_foreignfarmuser_user_ptr_id_d62330a8_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelement_sensor_id_ab7c0bfe_fk_agribiot_sensor_id;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelement_object_id_c292df7b_fk_agribiot_object_id;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelemen_alarm_trigger_id_9ba193db_fk_agribiot_;
DROP INDEX public.guardian_userobjectpermission_user_id_d5c1e964;
DROP INDEX public.guardian_userobjectpermission_permission_id_71807bfc;
DROP INDEX public.guardian_userobjectpermission_content_type_id_2e892405;
DROP INDEX public.guardian_us_content_179ed2_idx;
DROP INDEX public.guardian_groupobjectpermission_permission_id_36572738;
DROP INDEX public.guardian_groupobjectpermission_group_id_4bbbfb62;
DROP INDEX public.guardian_groupobjectpermission_content_type_id_7ade36b8;
DROP INDEX public.guardian_gr_content_ae6aec_idx;
DROP INDEX public.django_session_session_key_c0390e0f_like;
DROP INDEX public.django_session_expire_date_a5c62663;
DROP INDEX public.django_admin_log_user_id_c564eba6;
DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
DROP INDEX public.authtoken_token_key_10f0b77e_like;
DROP INDEX public.auth_user_username_6821ab7c_like;
DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
DROP INDEX public.auth_user_groups_group_id_97559544;
DROP INDEX public.auth_permission_content_type_id_2f476e4b;
DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
DROP INDEX public.auth_group_name_a6ea08ec_like;
DROP INDEX public.agribiot_value_sensor_object_id_97097252;
DROP INDEX public.agribiot_sensorobject_sensor_id_4a836e2c;
DROP INDEX public.agribiot_sensorobject_object_id_fcf9bc39;
DROP INDEX public.agribiot_sensor_name_56d3fb1c_like;
DROP INDEX public.agribiot_risedalarm_value_id_49e5444e;
DROP INDEX public.agribiot_risedalarm_alarm_trigger_id_d6008608;
DROP INDEX public.agribiot_remoteaccess_sensors_sensor_id_a56784c0;
DROP INDEX public.agribiot_remoteaccess_sensors_remoteaccess_id_a33d91e7;
DROP INDEX public.agribiot_remoteaccess_sensor_objects_sensorobject_id_3a831118;
DROP INDEX public.agribiot_remoteaccess_sensor_objects_remoteaccess_id_b12c6c90;
DROP INDEX public.agribiot_remoteaccess_selected_objects_remoteaccess_id_382e8a77;
DROP INDEX public.agribiot_remoteaccess_selected_objects_object_id_b8286ddc;
DROP INDEX public.agribiot_remoteaccess_geometry_id;
DROP INDEX public.agribiot_remoteaccess_foreign_users_207df1dc;
DROP INDEX public.agribiot_parcel_geometry_id;
DROP INDEX public.agribiot_parcel_farm_id_63c02727;
DROP INDEX public.agribiot_operation_task_id_13228df0;
DROP INDEX public.agribiot_operation_sensor_object_id_cb2c3e2f;
DROP INDEX public.agribiot_operation_position_id;
DROP INDEX public.agribiot_object_uuid_a3a0d421_like;
DROP INDEX public.agribiot_object_user_id_69163bcb;
DROP INDEX public.agribiot_object_position_id;
DROP INDEX public.agribiot_alarmtrigger_name_2a404727_like;
DROP INDEX public.agribiot_alarmelement_sensor_id_ab7c0bfe;
DROP INDEX public.agribiot_alarmelement_object_id_c292df7b;
DROP INDEX public.agribiot_alarmelement_geometry_id;
DROP INDEX public.agribiot_alarmelement_alarm_trigger_id_9ba193db;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectpermission_pkey;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectpermi_user_id_permission_id_ob_b0b3d2fc_uniq;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobjectpermission_pkey;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobjectperm_group_id_permission_id_o_3f189f7c_uniq;
ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_user_id_key;
ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
ALTER TABLE ONLY public.agribiot_value DROP CONSTRAINT agribiot_value_pkey;
ALTER TABLE ONLY public.agribiot_annotationtemplate DROP CONSTRAINT agribiot_task_pkey;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_pkey;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_object_id_sensor_id_a7d20fea_uniq;
ALTER TABLE ONLY public.agribiot_sensor DROP CONSTRAINT agribiot_sensor_pkey;
ALTER TABLE ONLY public.agribiot_sensor DROP CONSTRAINT agribiot_sensor_name_key;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_pkey;
ALTER TABLE ONLY public.agribiot_rfiddeviceuser DROP CONSTRAINT agribiot_rfiddeviceuser_pkey;
ALTER TABLE ONLY public.agribiot_remoteinstance DROP CONSTRAINT agribiot_remoteinstance_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteaccess_sensors_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteaccess_sensor_objects_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteaccess_selected_objects_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensorob_81219c57_uniq;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensor_i_0da2fc55_uniq;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_object_i_6a4c0d2b_uniq;
ALTER TABLE ONLY public.agribiot_remoteaccess DROP CONSTRAINT agribiot_remoteaccess_pkey;
ALTER TABLE ONLY public.agribiot_parcel DROP CONSTRAINT agribiot_parcel_pkey;
ALTER TABLE ONLY public.agribiot_operation DROP CONSTRAINT agribiot_operation_pkey;
ALTER TABLE ONLY public.agribiot_objectuser DROP CONSTRAINT agribiot_objectuser_pkey;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_uuid_key;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_pkey;
ALTER TABLE ONLY public.agribiot_gatewayuser DROP CONSTRAINT agribiot_gatewayuser_pkey;
ALTER TABLE ONLY public.agribiot_foreignfarmuser DROP CONSTRAINT agribiot_foreignfarmuser_pkey;
ALTER TABLE ONLY public.agribiot_farm DROP CONSTRAINT agribiot_farm_pkey;
ALTER TABLE ONLY public.agribiot_alarmtrigger DROP CONSTRAINT agribiot_alarmtrigger_pkey;
ALTER TABLE ONLY public.agribiot_alarmtrigger DROP CONSTRAINT agribiot_alarmtrigger_name_key;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelement_pkey;
ALTER TABLE public.guardian_userobjectpermission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.guardian_groupobjectpermission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_value ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_sensorobject ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_sensor ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_risedalarm ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteinstance ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess_sensors ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess_sensor_objects ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess_selected_objects ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_parcel ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_operation ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_object ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_farm ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_annotationtemplate ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_alarmtrigger ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_alarmelement ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.guardian_userobjectpermission_id_seq;
DROP TABLE public.guardian_userobjectpermission;
DROP SEQUENCE public.guardian_groupobjectpermission_id_seq;
DROP TABLE public.guardian_groupobjectpermission;
DROP TABLE public.django_session;
DROP SEQUENCE public.django_migrations_id_seq;
DROP TABLE public.django_migrations;
DROP SEQUENCE public.django_content_type_id_seq;
DROP TABLE public.django_content_type;
DROP SEQUENCE public.django_admin_log_id_seq;
DROP TABLE public.django_admin_log;
DROP TABLE public.authtoken_token;
DROP SEQUENCE public.auth_user_user_permissions_id_seq;
DROP TABLE public.auth_user_user_permissions;
DROP SEQUENCE public.auth_user_id_seq;
DROP SEQUENCE public.auth_user_groups_id_seq;
DROP TABLE public.auth_user_groups;
DROP TABLE public.auth_user;
DROP SEQUENCE public.auth_permission_id_seq;
DROP TABLE public.auth_permission;
DROP SEQUENCE public.auth_group_permissions_id_seq;
DROP TABLE public.auth_group_permissions;
DROP SEQUENCE public.auth_group_id_seq;
DROP TABLE public.auth_group;
DROP SEQUENCE public.agribiot_value_id_seq;
DROP TABLE public.agribiot_value;
DROP SEQUENCE public.agribiot_task_id_seq;
DROP SEQUENCE public.agribiot_sensorobject_id_seq;
DROP TABLE public.agribiot_sensorobject;
DROP SEQUENCE public.agribiot_sensor_id_seq;
DROP TABLE public.agribiot_sensor;
DROP SEQUENCE public.agribiot_risedalarm_id_seq;
DROP TABLE public.agribiot_risedalarm;
DROP TABLE public.agribiot_rfiddeviceuser;
DROP SEQUENCE public.agribiot_remoteinstance_id_seq;
DROP TABLE public.agribiot_remoteinstance;
DROP SEQUENCE public.agribiot_remoteaccess_sensors_id_seq;
DROP TABLE public.agribiot_remoteaccess_sensors;
DROP SEQUENCE public.agribiot_remoteaccess_sensor_objects_id_seq;
DROP TABLE public.agribiot_remoteaccess_sensor_objects;
DROP SEQUENCE public.agribiot_remoteaccess_selected_objects_id_seq;
DROP TABLE public.agribiot_remoteaccess_selected_objects;
DROP SEQUENCE public.agribiot_remoteaccess_id_seq;
DROP TABLE public.agribiot_remoteaccess;
DROP SEQUENCE public.agribiot_parcel_id_seq;
DROP TABLE public.agribiot_parcel;
DROP SEQUENCE public.agribiot_operation_id_seq;
DROP TABLE public.agribiot_operation;
DROP TABLE public.agribiot_objectuser;
DROP SEQUENCE public.agribiot_object_id_seq;
DROP TABLE public.agribiot_object;
DROP TABLE public.agribiot_gatewayuser;
DROP TABLE public.agribiot_foreignfarmuser;
DROP SEQUENCE public.agribiot_farm_id_seq;
DROP TABLE public.agribiot_farm;
DROP TABLE public.agribiot_annotationtemplate;
DROP SEQUENCE public.agribiot_alarmtrigger_id_seq;
DROP TABLE public.agribiot_alarmtrigger;
DROP SEQUENCE public.agribiot_alarmelement_id_seq;
DROP TABLE public.agribiot_alarmelement;
DROP EXTENSION postgis;
DROP EXTENSION plpython3u;
--
-- Name: plpython3u; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpython3u WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpython3u; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpython3u IS 'PL/Python3U untrusted procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agribiot_alarmelement; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_alarmelement (
    id integer NOT NULL,
    trigger_type character varying(100) NOT NULL,
    trigger_value double precision NOT NULL,
    geometry public.geography(Polygon,4326),
    alarm_trigger_id integer NOT NULL,
    object_id integer,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_alarmelement OWNER TO agribiot;

--
-- Name: agribiot_alarmelement_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_alarmelement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_alarmelement_id_seq OWNER TO agribiot;

--
-- Name: agribiot_alarmelement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_alarmelement_id_seq OWNED BY public.agribiot_alarmelement.id;


--
-- Name: agribiot_alarmtrigger; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_alarmtrigger (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_alarmtrigger OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_alarmtrigger_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_alarmtrigger_id_seq OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_alarmtrigger_id_seq OWNED BY public.agribiot_alarmtrigger.id;


--
-- Name: agribiot_annotationtemplate; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_annotationtemplate (
    id integer NOT NULL,
    description text NOT NULL,
    annotation_type character varying(4) NOT NULL
);


ALTER TABLE public.agribiot_annotationtemplate OWNER TO agribiot;

--
-- Name: agribiot_farm; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_farm (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_farm OWNER TO agribiot;

--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_farm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_farm_id_seq OWNER TO agribiot;

--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_farm_id_seq OWNED BY public.agribiot_farm.id;


--
-- Name: agribiot_foreignfarmuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_foreignfarmuser (
    user_ptr_id integer NOT NULL,
    url_origin character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_foreignfarmuser OWNER TO agribiot;

--
-- Name: agribiot_gatewayuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_gatewayuser (
    user_ptr_id integer NOT NULL
);


ALTER TABLE public.agribiot_gatewayuser OWNER TO agribiot;

--
-- Name: agribiot_object; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_object (
    id integer NOT NULL,
    "position" public.geography(Point,4326) NOT NULL,
    uuid character varying(100) NOT NULL,
    description text NOT NULL,
    user_id integer
);


ALTER TABLE public.agribiot_object OWNER TO agribiot;

--
-- Name: agribiot_object_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_object_id_seq OWNER TO agribiot;

--
-- Name: agribiot_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_object_id_seq OWNED BY public.agribiot_object.id;


--
-- Name: agribiot_objectuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_objectuser (
    user_ptr_id integer NOT NULL
);


ALTER TABLE public.agribiot_objectuser OWNER TO agribiot;

--
-- Name: agribiot_operation; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_operation (
    id integer NOT NULL,
    imported timestamp with time zone NOT NULL,
    created timestamp with time zone NOT NULL,
    reader_uuid character varying(100) NOT NULL,
    tag_embedded_task_index integer NOT NULL,
    value double precision NOT NULL,
    "position" public.geography(Point,4326) NOT NULL,
    accuracy double precision NOT NULL,
    sensor_object_id integer NOT NULL,
    annotation_template_id integer NOT NULL
);


ALTER TABLE public.agribiot_operation OWNER TO agribiot;

--
-- Name: agribiot_operation_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_operation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_operation_id_seq OWNER TO agribiot;

--
-- Name: agribiot_operation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_operation_id_seq OWNED BY public.agribiot_operation.id;


--
-- Name: agribiot_parcel; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_parcel (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    geometry public.geography(Polygon,4326) NOT NULL,
    farm_id integer NOT NULL
);


ALTER TABLE public.agribiot_parcel OWNER TO agribiot;

--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_parcel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_parcel_id_seq OWNER TO agribiot;

--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_parcel_id_seq OWNED BY public.agribiot_parcel.id;


--
-- Name: agribiot_remoteaccess; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess (
    id integer NOT NULL,
    access_type integer NOT NULL,
    target_type integer NOT NULL,
    geometry public.geography(Polygon,4326),
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    foreign_users integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_id_seq OWNED BY public.agribiot_remoteaccess.id;


--
-- Name: agribiot_remoteaccess_selected_objects; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess_selected_objects (
    id integer NOT NULL,
    remoteaccess_id integer NOT NULL,
    object_id integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess_selected_objects OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_selected_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_selected_objects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_selected_objects_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_selected_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_selected_objects_id_seq OWNED BY public.agribiot_remoteaccess_selected_objects.id;


--
-- Name: agribiot_remoteaccess_sensor_objects; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess_sensor_objects (
    id integer NOT NULL,
    remoteaccess_id integer NOT NULL,
    sensorobject_id integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess_sensor_objects OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensor_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_sensor_objects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_sensor_objects_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensor_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_sensor_objects_id_seq OWNED BY public.agribiot_remoteaccess_sensor_objects.id;


--
-- Name: agribiot_remoteaccess_sensors; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess_sensors (
    id integer NOT NULL,
    remoteaccess_id integer NOT NULL,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess_sensors OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensors_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_sensors_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_sensors_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_sensors_id_seq OWNED BY public.agribiot_remoteaccess_sensors.id;


--
-- Name: agribiot_remoteinstance; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteinstance (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    token character varying(256) NOT NULL,
    url character varying(256) NOT NULL,
    color character varying(7) NOT NULL
);


ALTER TABLE public.agribiot_remoteinstance OWNER TO agribiot;

--
-- Name: agribiot_remoteinstance_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteinstance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteinstance_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteinstance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteinstance_id_seq OWNED BY public.agribiot_remoteinstance.id;


--
-- Name: agribiot_rfiddeviceuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_rfiddeviceuser (
    user_ptr_id integer NOT NULL
);


ALTER TABLE public.agribiot_rfiddeviceuser OWNER TO agribiot;

--
-- Name: agribiot_risedalarm; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_risedalarm (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    alarm_trigger_id integer NOT NULL,
    value_id integer NOT NULL
);


ALTER TABLE public.agribiot_risedalarm OWNER TO agribiot;

--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_risedalarm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_risedalarm_id_seq OWNER TO agribiot;

--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_risedalarm_id_seq OWNED BY public.agribiot_risedalarm.id;


--
-- Name: agribiot_sensor; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_sensor (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    unit character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_sensor OWNER TO agribiot;

--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_sensor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_sensor_id_seq OWNER TO agribiot;

--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_sensor_id_seq OWNED BY public.agribiot_sensor.id;


--
-- Name: agribiot_sensorobject; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_sensorobject (
    id integer NOT NULL,
    object_id integer NOT NULL,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_sensorobject OWNER TO agribiot;

--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_sensorobject_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_sensorobject_id_seq OWNER TO agribiot;

--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_sensorobject_id_seq OWNED BY public.agribiot_sensorobject.id;


--
-- Name: agribiot_task_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_task_id_seq OWNER TO agribiot;

--
-- Name: agribiot_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_task_id_seq OWNED BY public.agribiot_annotationtemplate.id;


--
-- Name: agribiot_value; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_value (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    value double precision NOT NULL,
    sensor_object_id integer NOT NULL
);


ALTER TABLE public.agribiot_value OWNER TO agribiot;

--
-- Name: agribiot_value_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_value_id_seq OWNER TO agribiot;

--
-- Name: agribiot_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_value_id_seq OWNED BY public.agribiot_value.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO agribiot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO agribiot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO agribiot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO agribiot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO agribiot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO agribiot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO agribiot;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO agribiot;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO agribiot;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO agribiot;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO agribiot;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO agribiot;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO agribiot;

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO agribiot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO agribiot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO agribiot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO agribiot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO agribiot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO agribiot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO agribiot;

--
-- Name: guardian_groupobjectpermission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.guardian_groupobjectpermission (
    id integer NOT NULL,
    object_pk character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.guardian_groupobjectpermission OWNER TO agribiot;

--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.guardian_groupobjectpermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guardian_groupobjectpermission_id_seq OWNER TO agribiot;

--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.guardian_groupobjectpermission_id_seq OWNED BY public.guardian_groupobjectpermission.id;


--
-- Name: guardian_userobjectpermission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.guardian_userobjectpermission (
    id integer NOT NULL,
    object_pk character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    permission_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.guardian_userobjectpermission OWNER TO agribiot;

--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.guardian_userobjectpermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guardian_userobjectpermission_id_seq OWNER TO agribiot;

--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.guardian_userobjectpermission_id_seq OWNED BY public.guardian_userobjectpermission.id;


--
-- Name: agribiot_alarmelement id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement ALTER COLUMN id SET DEFAULT nextval('public.agribiot_alarmelement_id_seq'::regclass);


--
-- Name: agribiot_alarmtrigger id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger ALTER COLUMN id SET DEFAULT nextval('public.agribiot_alarmtrigger_id_seq'::regclass);


--
-- Name: agribiot_annotationtemplate id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_annotationtemplate ALTER COLUMN id SET DEFAULT nextval('public.agribiot_task_id_seq'::regclass);


--
-- Name: agribiot_farm id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_farm ALTER COLUMN id SET DEFAULT nextval('public.agribiot_farm_id_seq'::regclass);


--
-- Name: agribiot_object id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object ALTER COLUMN id SET DEFAULT nextval('public.agribiot_object_id_seq'::regclass);


--
-- Name: agribiot_operation id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation ALTER COLUMN id SET DEFAULT nextval('public.agribiot_operation_id_seq'::regclass);


--
-- Name: agribiot_parcel id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel ALTER COLUMN id SET DEFAULT nextval('public.agribiot_parcel_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess_selected_objects id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_selected_objects_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess_sensor_objects id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_sensor_objects_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess_sensors id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_sensors_id_seq'::regclass);


--
-- Name: agribiot_remoteinstance id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteinstance ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteinstance_id_seq'::regclass);


--
-- Name: agribiot_risedalarm id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm ALTER COLUMN id SET DEFAULT nextval('public.agribiot_risedalarm_id_seq'::regclass);


--
-- Name: agribiot_sensor id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor ALTER COLUMN id SET DEFAULT nextval('public.agribiot_sensor_id_seq'::regclass);


--
-- Name: agribiot_sensorobject id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject ALTER COLUMN id SET DEFAULT nextval('public.agribiot_sensorobject_id_seq'::regclass);


--
-- Name: agribiot_value id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value ALTER COLUMN id SET DEFAULT nextval('public.agribiot_value_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: guardian_groupobjectpermission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission ALTER COLUMN id SET DEFAULT nextval('public.guardian_groupobjectpermission_id_seq'::regclass);


--
-- Name: guardian_userobjectpermission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission ALTER COLUMN id SET DEFAULT nextval('public.guardian_userobjectpermission_id_seq'::regclass);


--
-- Data for Name: agribiot_alarmelement; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_alarmelement (id, trigger_type, trigger_value, geometry, alarm_trigger_id, object_id, sensor_id) FROM stdin;
\.


--
-- Data for Name: agribiot_alarmtrigger; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_alarmtrigger (id, name) FROM stdin;
\.


--
-- Data for Name: agribiot_annotationtemplate; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_annotationtemplate (id, description, annotation_type) FROM stdin;
3	Taillage	TASK
4	Pliage	TASK
5	Broyage	TASK
6	test	TASK
7	Réparation faite	TASK
8	fils tendus	TASK
\.


--
-- Data for Name: agribiot_farm; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_farm (id, name) FROM stdin;
\.


--
-- Data for Name: agribiot_foreignfarmuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_foreignfarmuser (user_ptr_id, url_origin) FROM stdin;
\.


--
-- Data for Name: agribiot_gatewayuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_gatewayuser (user_ptr_id) FROM stdin;
4
\.


--
-- Data for Name: agribiot_object; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_object (id, "position", uuid, description, user_id) FROM stdin;
3	0101000020E6100000010000C026C8F4BFD604AE435A924740	E2003412012C0100	Muscadet	8
5	0101000020E61000000100008073BBF4BFFFF17EA241924740	E2003412012E0100	Sauvignon	10
6	0101000020E6100000FFFFFF7FDBBCF4BF97490B014A924740	E2003412012F0100	Muscadet Champ Garreau	\N
8	0101000020E61000001F4B1FBAA0BEF4BFC2CDCFFA81924740	E200341201381700	Merlot	12
4	0101000020E610000001000040E3C7F4BFFFF17EA241924740	E2003412012A0100	Cru Mouzillon-Tillières	9
9	0101000020E6100000FCD6615FC5BCF4BFD5BD28544A924740	E2003412012C1700		13
10	0101000020E610000061AE32408FBEF4BFB16D516683924740	E200341201300100		14
11	0101000020E61000000100000032C8F4BFADDBC58065924740	0001		15
\.


--
-- Data for Name: agribiot_objectuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_objectuser (user_ptr_id) FROM stdin;
8
9
10
11
12
13
14
15
\.


--
-- Data for Name: agribiot_operation; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_operation (id, imported, created, reader_uuid, tag_embedded_task_index, value, "position", accuracy, sensor_object_id, annotation_template_id) FROM stdin;
3	2021-02-10 07:32:21.290093-06	2021-02-10 08:09:45.239-06		2	0	0101000020E610000086284C0502CAF4BF82FA96395D924740	5.69999980926513672	3	3
4	2021-02-10 07:32:21.332823-06	2021-02-10 08:12:29.613-06		2	0	0101000020E6100000AF7451078CC8F4BF00E8305F5E924740	1.5	3	3
5	2021-02-10 07:32:21.985652-06	2021-02-10 08:15:25.908-06		2	0	0101000020E610000092FD0600A1C8F4BF84CB3D3747924740	1.29999995231628418	4	3
6	2021-02-10 07:32:22.635979-06	2021-02-10 08:20:19.332-06		2	0	0101000020E61000008CCE0CF5CEBBF4BFDCD7817346924740	1.5	5	3
7	2021-02-10 07:32:23.1884-06	2021-02-10 08:22:30.566-06		2	0	0101000020E6100000FCD6615FC5BCF4BF4970109A4A924740	1.10000002384185791	6	3
8	2021-02-10 07:32:23.8511-06	2021-02-10 08:25:51.946-06		4	0	0101000020E61000001F4B1FBAA0BEF4BFC2CDCFFA81924740	1.5	7	3
9	2021-03-03 06:17:20.833722-06	2021-03-03 03:33:53.778-06		2	0	0101000020E61000008870B9E7E6C8F4BF935A15CE5B924740	2.09999990463256836	3	7
10	2021-03-03 06:17:20.901092-06	2021-03-03 03:37:49.764-06		2	0	0101000020E61000009F4471B4D0BBF4BF6B300DC347924740	2.5	5	7
11	2021-03-03 06:17:43.19064-06	2021-03-03 03:34:05.927-06		2	0	0101000020E6100000A45B9C1E01C9F4BF5E1941505B924740	1.79999995231628418	3	8
12	2021-03-03 06:17:43.258192-06	2021-03-03 03:38:04.643-06		2	0	0101000020E6100000CEA5B8AAECBBF4BFB7019F1F46924740	1.5	5	8
13	2021-03-03 07:39:38.104331-06	2021-03-03 08:19:23.629-06		2	0	0101000020E61000001E65D7D0E0C9F4BFED71CC9F5C924740	3.70000004768371582	3	6
14	2021-03-03 07:39:38.169193-06	2021-03-03 08:20:59.069-06		2	0	0101000020E610000004DA301D4DC8F4BF95362F6147924740	1.79999995231628418	4	6
15	2021-03-03 07:39:38.200078-06	2021-03-03 08:21:21.601-06		2	0	0101000020E61000007F130A1170C8F4BF3E1F788F46924740	1.5	4	6
16	2021-03-03 07:39:38.231848-06	2021-03-03 08:25:05.886-06		2	0	0101000020E6100000F41DE9F9C0BBF4BFD5B2B5BE48924740	2.90000009536743164	5	6
17	2021-03-03 07:39:38.263622-06	2021-03-03 08:26:08.572-06		2	0	0101000020E6100000BBA3EC1A1ABCF4BFF788981249924740	2.90000009536743164	6	6
\.


--
-- Data for Name: agribiot_parcel; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_parcel (id, name, geometry, farm_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess (id, access_type, target_type, geometry, start_date, end_date, foreign_users) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess_selected_objects; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess_selected_objects (id, remoteaccess_id, object_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess_sensor_objects; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess_sensor_objects (id, remoteaccess_id, sensorobject_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess_sensors; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess_sensors (id, remoteaccess_id, sensor_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteinstance; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteinstance (id, name, token, url, color) FROM stdin;
\.


--
-- Data for Name: agribiot_rfiddeviceuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_rfiddeviceuser (user_ptr_id) FROM stdin;
5
\.


--
-- Data for Name: agribiot_risedalarm; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_risedalarm (id, created, alarm_trigger_id, value_id) FROM stdin;
\.


--
-- Data for Name: agribiot_sensor; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_sensor (id, name, unit) FROM stdin;
1	Température	°C
2	Humidité	%
3	tag	
\.


--
-- Data for Name: agribiot_sensorobject; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_sensorobject (id, object_id, sensor_id) FROM stdin;
3	3	3
4	4	3
5	5	3
6	6	3
7	8	3
10	11	1
11	11	2
\.


--
-- Data for Name: agribiot_value; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_value (id, created, value, sensor_object_id) FROM stdin;
1	2021-03-24 05:28:13.104602-05	22.7000007629394531	10
2	2021-03-24 05:28:13.845625-05	45.1000022888183594	11
3	2021-03-24 05:28:16.613179-05	22.7000007629394531	10
4	2021-03-24 05:28:17.61319-05	44.6000022888183594	11
5	2021-03-24 05:28:20.621836-05	22.7000007629394531	10
6	2021-03-24 05:28:21.598942-05	44.2000007629394531	11
7	2021-03-24 05:28:24.622498-05	22.8000011444091797	10
8	2021-03-24 05:28:25.610585-05	44	11
9	2021-03-24 05:28:28.63079-05	22.8000011444091797	10
10	2021-03-24 05:28:29.627229-05	43.9000015258789062	11
11	2021-03-24 05:28:32.657834-05	22.8000011444091797	10
12	2021-03-24 05:28:33.624973-05	43.7000007629394531	11
13	2021-03-24 05:28:36.632056-05	22.8000011444091797	10
14	2021-03-24 05:28:37.630684-05	43.6000022888183594	11
15	2021-03-24 05:28:40.639831-05	22.8000011444091797	10
16	2021-03-24 05:28:41.627148-05	43.6000022888183594	11
17	2021-03-24 05:28:44.643754-05	22.8000011444091797	10
18	2021-03-24 05:28:45.648396-05	43.5	11
19	2021-03-24 05:28:48.661452-05	22.8999996185302734	10
20	2021-03-24 05:28:49.647279-05	43.5	11
21	2021-03-24 05:28:52.657916-05	22.8999996185302734	10
22	2021-03-24 05:28:53.655133-05	43.4000015258789062	11
23	2021-03-24 05:28:56.651895-05	22.8999996185302734	10
24	2021-03-24 05:28:57.654663-05	43.4000015258789062	11
25	2021-03-24 05:29:00.662142-05	22.8999996185302734	10
26	2021-03-24 05:29:01.683398-05	43.4000015258789062	11
27	2021-03-24 05:29:04.662734-05	22.8999996185302734	10
28	2021-03-24 05:29:05.670289-05	43.2999992370605469	11
29	2021-03-24 05:29:08.663813-05	22.8999996185302734	10
30	2021-03-24 05:29:09.664498-05	43.2999992370605469	11
31	2021-03-24 05:29:12.670063-05	22.8999996185302734	10
32	2021-03-24 05:29:13.671392-05	43.2999992370605469	11
33	2021-03-24 05:29:16.655216-05	23	10
34	2021-03-24 05:29:17.678783-05	43.2999992370605469	11
35	2021-03-24 05:29:20.667088-05	23	10
36	2021-03-24 05:29:21.68048-05	43.2000007629394531	11
37	2021-03-24 05:29:24.689724-05	23	10
38	2021-03-24 05:29:25.700994-05	43.2000007629394531	11
39	2021-03-24 05:29:28.695224-05	23	10
40	2021-03-24 05:29:29.700879-05	43.1000022888183594	11
41	2021-03-24 05:29:32.702938-05	23	10
42	2021-03-24 05:29:33.695918-05	43.1000022888183594	11
43	2021-03-24 05:29:36.70055-05	23	10
44	2021-03-24 05:29:37.688579-05	43.1000022888183594	11
45	2021-03-24 05:29:40.706928-05	23.1000003814697266	10
46	2021-03-24 05:29:41.704725-05	43.5	11
47	2021-03-24 05:29:44.721892-05	23	10
48	2021-03-24 05:29:45.719692-05	43.6000022888183594	11
49	2021-03-24 05:29:48.723263-05	23	10
50	2021-03-24 05:29:52.759014-05	23	10
51	2021-03-24 05:29:53.764162-05	44.1000022888183594	11
52	2021-03-24 05:29:56.765226-05	23	10
53	2021-03-24 05:29:57.756094-05	45.6000022888183594	11
54	2021-03-24 05:30:00.769449-05	23	10
55	2021-03-24 05:30:01.76467-05	46	11
56	2021-03-24 05:30:04.768747-05	23	10
57	2021-03-24 05:30:05.773744-05	45.9000015258789062	11
58	2021-03-24 05:30:08.773405-05	23	10
59	2021-03-24 05:30:09.779443-05	45.7999992370605469	11
60	2021-03-24 05:30:12.776061-05	23.1000003814697266	10
61	2021-03-24 05:30:13.783171-05	46.1000022888183594	11
62	2021-03-24 05:30:16.787336-05	23.1000003814697266	10
63	2021-03-24 05:30:17.78474-05	45.7999992370605469	11
64	2021-03-24 05:30:20.786795-05	23.1000003814697266	10
65	2021-03-24 05:30:21.779312-05	45.2000007629394531	11
66	2021-03-24 05:30:24.791114-05	23.1000003814697266	10
67	2021-03-24 05:30:25.790781-05	44.6000022888183594	11
68	2021-03-24 05:30:28.782885-05	23.2000007629394531	10
69	2021-03-24 05:30:29.7964-05	44.2000007629394531	11
70	2021-03-24 05:30:32.794103-05	23.2000007629394531	10
71	2021-03-24 05:30:33.794065-05	43.7999992370605469	11
72	2021-03-24 05:30:36.783834-05	23.2000007629394531	10
73	2021-03-24 05:30:37.799555-05	43.5	11
74	2021-03-24 05:30:40.807879-05	23.2000007629394531	10
75	2021-03-24 05:30:41.80668-05	43.4000015258789062	11
76	2021-03-24 05:30:44.790751-05	23.3000011444091797	10
77	2021-03-24 05:30:45.80506-05	43.2999992370605469	11
78	2021-03-24 05:30:48.805896-05	23.2000007629394531	10
79	2021-03-24 05:30:49.816174-05	43.1000022888183594	11
80	2021-03-24 05:30:52.824254-05	23.3000011444091797	10
81	2021-03-24 05:30:53.817685-05	43.1000022888183594	11
82	2021-03-24 05:30:56.816207-05	23.3000011444091797	10
83	2021-03-24 05:30:57.821307-05	43	11
84	2021-03-24 05:31:00.81565-05	23.3000011444091797	10
85	2021-03-24 05:31:01.826674-05	43	11
86	2021-03-24 05:31:04.840148-05	23.3000011444091797	10
87	2021-03-24 05:31:05.831637-05	42.9000015258789062	11
88	2021-03-24 05:31:08.838832-05	23.3000011444091797	10
89	2021-03-24 05:31:09.817324-05	42.9000015258789062	11
90	2021-03-24 05:31:12.842567-05	23.3000011444091797	10
91	2021-03-24 05:31:13.834622-05	42.7999992370605469	11
92	2021-03-24 05:31:16.844801-05	23.3999996185302734	10
93	2021-03-24 05:31:17.84361-05	42.7000007629394531	11
94	2021-03-24 05:31:20.843304-05	23.3999996185302734	10
95	2021-03-24 05:31:21.847255-05	42.7000007629394531	11
96	2021-03-24 05:31:24.858096-05	23.3999996185302734	10
97	2021-03-24 05:31:25.856414-05	42.6000022888183594	11
98	2021-03-24 05:31:28.855039-05	23.3999996185302734	10
99	2021-03-24 05:31:29.855537-05	42.5	11
100	2021-03-24 05:31:32.861325-05	23.3999996185302734	10
101	2021-03-24 05:31:33.865336-05	42.5	11
102	2021-03-24 05:31:36.861804-05	23.3999996185302734	10
103	2021-03-24 05:31:37.871718-05	42.4000015258789062	11
104	2021-03-24 05:31:40.864197-05	23.3999996185302734	10
105	2021-03-24 05:31:41.868902-05	42.4000015258789062	11
106	2021-03-24 05:31:44.876242-05	23.5	10
107	2021-03-24 05:31:45.877273-05	42.4000015258789062	11
108	2021-03-24 05:31:48.879011-05	23.5	10
109	2021-03-24 05:31:49.884693-05	42.2999992370605469	11
110	2021-03-24 05:31:52.882609-05	23.5	10
111	2021-03-24 05:31:53.882409-05	42.2999992370605469	11
112	2021-03-24 05:31:56.88856-05	23.5	10
113	2021-03-24 05:31:57.88487-05	42.2000007629394531	11
114	2021-03-24 05:32:00.891766-05	23.5	10
115	2021-03-24 05:32:01.892217-05	42.2000007629394531	11
116	2021-03-24 05:32:04.894386-05	23.5	10
117	2021-03-24 05:32:05.901624-05	42.1000022888183594	11
118	2021-03-24 05:32:08.903748-05	23.6000003814697266	10
119	2021-03-24 05:32:09.894589-05	42.1000022888183594	11
120	2021-03-24 05:32:12.898818-05	23.6000003814697266	10
121	2021-03-24 05:32:13.905253-05	42.1000022888183594	11
122	2021-03-24 05:32:16.904153-05	23.6000003814697266	10
123	2021-03-24 05:32:17.910422-05	42	11
124	2021-03-24 05:32:20.913105-05	23.6000003814697266	10
125	2021-03-24 05:32:21.91492-05	42	11
126	2021-03-24 05:32:24.917794-05	23.6000003814697266	10
127	2021-03-24 05:32:25.916215-05	41.9000015258789062	11
128	2021-03-24 05:32:28.902141-05	23.6000003814697266	10
129	2021-03-24 05:32:29.926453-05	41.9000015258789062	11
130	2021-03-24 05:32:32.92189-05	23.6000003814697266	10
131	2021-03-24 05:32:33.920413-05	41.7999992370605469	11
132	2021-03-24 05:32:36.929092-05	23.6000003814697266	10
133	2021-03-24 05:32:37.928453-05	41.7999992370605469	11
134	2021-03-24 05:32:40.936426-05	23.7000007629394531	10
135	2021-03-24 05:32:41.927245-05	41.7999992370605469	11
136	2021-03-24 05:32:44.932724-05	23.6000003814697266	10
137	2021-03-24 05:32:45.927032-05	41.7000007629394531	11
138	2021-03-24 05:32:48.947097-05	23.7000007629394531	10
139	2021-03-24 05:32:49.942165-05	41.7000007629394531	11
140	2021-03-24 05:32:52.946574-05	23.7000007629394531	10
141	2021-03-24 05:32:53.942644-05	41.6000022888183594	11
142	2021-03-24 05:32:56.946162-05	23.7000007629394531	10
143	2021-03-24 05:32:57.92137-05	41.6000022888183594	11
144	2021-03-24 05:33:00.955779-05	23.7000007629394531	10
145	2021-03-24 05:33:01.942452-05	41.6000022888183594	11
146	2021-03-24 05:33:04.967395-05	23.7000007629394531	10
147	2021-03-24 05:33:05.95458-05	41.5	11
148	2021-03-24 05:33:08.967676-05	23.7000007629394531	10
149	2021-03-24 05:33:09.965626-05	41.4000015258789062	11
150	2021-03-24 05:33:12.970383-05	23.8000011444091797	10
151	2021-03-24 05:33:13.97088-05	41.5	11
152	2021-03-24 05:33:16.972989-05	23.7000007629394531	10
153	2021-03-24 05:33:17.973458-05	41.4000015258789062	11
154	2021-03-24 05:33:20.981036-05	23.8000011444091797	10
155	2021-03-24 05:33:21.979236-05	41.4000015258789062	11
156	2021-03-24 05:33:24.994281-05	23.8000011444091797	10
157	2021-03-24 05:33:25.983176-05	41.2999992370605469	11
158	2021-03-24 05:33:29.000525-05	23.8000011444091797	10
159	2021-03-24 05:33:29.995218-05	41.2999992370605469	11
160	2021-03-24 05:33:32.997449-05	23.8000011444091797	10
161	2021-03-24 05:33:33.990578-05	41.2000007629394531	11
162	2021-03-24 05:33:36.988006-05	23.8000011444091797	10
163	2021-03-24 05:33:37.995047-05	41.2000007629394531	11
164	2021-03-24 05:33:40.99784-05	23.8000011444091797	10
165	2021-03-24 05:33:41.987786-05	41.2000007629394531	11
166	2021-03-24 05:33:45.006238-05	23.8000011444091797	10
167	2021-03-24 05:33:46.017705-05	41.1000022888183594	11
168	2021-03-24 05:33:49.016831-05	23.8000011444091797	10
169	2021-03-24 05:33:50.02135-05	41.1000022888183594	11
170	2021-03-24 05:33:53.01796-05	23.8000011444091797	10
171	2021-03-24 05:33:54.017452-05	41	11
172	2021-03-24 05:33:57.003964-05	23.8999996185302734	10
173	2021-03-24 05:33:58.012619-05	41	11
174	2021-03-24 05:34:01.023579-05	23.8999996185302734	10
175	2021-03-24 05:34:02.029333-05	41	11
176	2021-03-24 05:34:05.0185-05	23.8999996185302734	10
177	2021-03-24 05:34:06.032644-05	41	11
178	2021-03-24 05:34:09.036991-05	23.8999996185302734	10
179	2021-03-24 05:34:10.029012-05	40.9000015258789062	11
180	2021-03-24 05:34:13.03229-05	23.8999996185302734	10
181	2021-03-24 05:34:14.033759-05	40.9000015258789062	11
182	2021-03-24 05:34:17.046172-05	23.8999996185302734	10
183	2021-03-24 05:34:18.049507-05	40.7999992370605469	11
184	2021-03-24 05:34:21.035147-05	23.8999996185302734	10
185	2021-03-24 05:34:22.046241-05	40.7999992370605469	11
186	2021-03-24 05:34:25.040209-05	23.8999996185302734	10
187	2021-03-24 05:34:26.047428-05	40.7999992370605469	11
188	2021-03-24 05:34:29.045527-05	24	10
189	2021-03-24 05:34:30.055903-05	40.7999992370605469	11
190	2021-03-24 05:34:33.039306-05	24	10
191	2021-03-24 05:34:34.056778-05	40.7999992370605469	11
192	2021-03-24 05:34:37.038742-05	24	10
193	2021-03-24 05:34:38.06782-05	40.7000007629394531	11
194	2021-03-24 05:34:41.061688-05	24	10
195	2021-03-24 05:34:42.068145-05	40.7000007629394531	11
196	2021-03-24 05:34:45.070908-05	24	10
197	2021-03-24 05:34:46.0628-05	40.7000007629394531	11
198	2021-03-24 05:34:49.07832-05	24	10
199	2021-03-24 05:34:50.064627-05	40.6000022888183594	11
200	2021-03-24 05:34:53.090371-05	24	10
201	2021-03-24 05:34:54.084501-05	40.6000022888183594	11
202	2021-03-24 05:34:57.085959-05	24	10
203	2021-03-24 05:34:58.070765-05	40.6000022888183594	11
204	2021-03-24 05:35:01.086603-05	24	10
205	2021-03-24 05:35:02.083153-05	40.6000022888183594	11
206	2021-03-24 05:35:05.0838-05	24	10
207	2021-03-24 05:35:06.083972-05	40.5	11
208	2021-03-24 05:35:09.093951-05	24	10
209	2021-03-24 05:35:10.070028-05	40.5	11
210	2021-03-24 05:35:13.096155-05	24	10
211	2021-03-24 05:35:14.088667-05	40.4000015258789062	11
212	2021-03-24 05:35:17.103094-05	24.1000003814697266	10
213	2021-03-24 05:35:18.105289-05	40.5	11
214	2021-03-24 05:35:21.111707-05	24.1000003814697266	10
215	2021-03-24 05:35:22.110559-05	40.4000015258789062	11
216	2021-03-24 05:35:25.115029-05	24.1000003814697266	10
217	2021-03-24 05:35:26.114165-05	40.4000015258789062	11
218	2021-03-24 05:35:29.1145-05	24.1000003814697266	10
219	2021-03-24 05:35:30.119225-05	40.2999992370605469	11
220	2021-03-24 05:35:33.108521-05	24.1000003814697266	10
221	2021-03-24 05:35:34.122515-05	40.2999992370605469	11
222	2021-03-24 05:35:37.125456-05	24.1000003814697266	10
223	2021-03-24 05:35:38.132619-05	40.2999992370605469	11
224	2021-03-24 05:35:41.137567-05	24.1000003814697266	10
225	2021-03-24 05:35:42.129823-05	40.2000007629394531	11
226	2021-03-24 05:35:45.120954-05	24.1000003814697266	10
227	2021-03-24 05:35:46.137791-05	40.2000007629394531	11
228	2021-03-24 05:35:49.126929-05	24.1000003814697266	10
229	2021-03-24 05:35:50.12945-05	40.2000007629394531	11
230	2021-03-24 05:35:53.140315-05	24.2000007629394531	10
231	2021-03-24 05:35:54.137738-05	40.2000007629394531	11
232	2021-03-24 05:35:57.144249-05	24.2000007629394531	10
233	2021-03-24 05:35:58.143426-05	40.2000007629394531	11
234	2021-03-24 05:36:01.14873-05	24.2000007629394531	10
235	2021-03-24 05:36:02.147302-05	40.1000022888183594	11
236	2021-03-24 05:36:05.157023-05	24.2000007629394531	10
237	2021-03-24 05:36:06.159126-05	40.1000022888183594	11
238	2021-03-24 05:36:09.155542-05	24.2000007629394531	10
239	2021-03-24 05:36:10.153734-05	40.1000022888183594	11
240	2021-03-24 05:36:13.163469-05	24.2000007629394531	10
241	2021-03-24 05:36:14.162053-05	40.1000022888183594	11
242	2021-03-24 05:36:17.164682-05	24.2000007629394531	10
243	2021-03-24 05:36:18.170167-05	40	11
244	2021-03-24 05:36:21.169288-05	24.2000007629394531	10
245	2021-03-24 05:36:22.176676-05	40	11
246	2021-03-24 05:36:25.170458-05	24.2000007629394531	10
247	2021-03-24 05:36:26.17593-05	40	11
248	2021-03-24 05:36:29.178988-05	24.2000007629394531	10
249	2021-03-24 05:36:30.178695-05	40	11
250	2021-03-24 05:36:33.187342-05	24.2000007629394531	10
251	2021-03-24 05:36:34.181098-05	39.9000015258789062	11
252	2021-03-24 05:36:37.186174-05	24.2000007629394531	10
253	2021-03-24 05:36:38.183305-05	39.9000015258789062	11
254	2021-03-24 05:36:41.190213-05	24.3000011444091797	10
255	2021-03-24 05:36:42.192172-05	39.9000015258789062	11
256	2021-03-24 05:36:45.189965-05	24.2000007629394531	10
257	2021-03-24 05:36:46.200787-05	39.7999992370605469	11
258	2021-03-24 05:36:49.191062-05	24.3000011444091797	10
259	2021-03-24 05:36:50.203029-05	39.9000015258789062	11
260	2021-03-24 05:36:53.200865-05	24.3000011444091797	10
261	2021-03-24 05:36:54.198183-05	39.7999992370605469	11
262	2021-03-24 05:36:57.204715-05	24.3000011444091797	10
263	2021-03-24 05:36:58.208325-05	39.7999992370605469	11
264	2021-03-24 05:37:01.21663-05	24.3000011444091797	10
265	2021-03-24 05:37:02.209914-05	39.7000007629394531	11
266	2021-03-24 05:37:05.217305-05	24.3000011444091797	10
267	2021-03-24 05:37:06.216694-05	39.7000007629394531	11
268	2021-03-24 05:37:09.21341-05	24.3000011444091797	10
269	2021-03-24 05:37:10.219382-05	39.7000007629394531	11
270	2021-03-24 05:37:13.217666-05	24.3000011444091797	10
271	2021-03-24 05:37:14.216987-05	39.7000007629394531	11
272	2021-03-24 05:37:17.215307-05	24.3000011444091797	10
273	2021-03-24 05:37:18.232724-05	39.7000007629394531	11
274	2021-03-24 05:37:21.205938-05	24.3000011444091797	10
275	2021-03-24 05:37:22.228743-05	39.6000022888183594	11
276	2021-03-24 05:37:25.241347-05	24.3000011444091797	10
277	2021-03-24 05:37:26.233088-05	39.6000022888183594	11
278	2021-03-24 05:37:29.243146-05	24.3000011444091797	10
279	2021-03-24 05:37:30.24016-05	39.6000022888183594	11
280	2021-03-24 05:37:33.249815-05	24.3000011444091797	10
281	2021-03-24 05:37:34.242438-05	39.5	11
282	2021-03-24 05:37:37.248874-05	24.3000011444091797	10
283	2021-03-24 05:37:38.250347-05	39.5	11
284	2021-03-24 05:37:41.252111-05	24.3000011444091797	10
285	2021-03-24 05:37:42.255536-05	39.5	11
286	2021-03-24 05:37:45.257669-05	24.3000011444091797	10
287	2021-03-24 05:37:46.262795-05	39.5	11
288	2021-03-24 05:37:49.266975-05	24.3000011444091797	10
289	2021-03-24 05:37:50.266966-05	39.5	11
290	2021-03-24 05:37:53.270548-05	24.3999996185302734	10
291	2021-03-24 05:37:54.267754-05	39.5	11
292	2021-03-24 05:37:57.267464-05	24.3999996185302734	10
293	2021-03-24 05:37:58.269316-05	39.5	11
294	2021-03-24 05:38:01.281287-05	24.3999996185302734	10
295	2021-03-24 05:38:02.275163-05	39.4000015258789062	11
296	2021-03-24 05:38:05.280885-05	24.3999996185302734	10
297	2021-03-24 05:38:06.276425-05	39.4000015258789062	11
298	2021-03-24 05:38:09.294156-05	24.3999996185302734	10
299	2021-03-24 05:38:10.269582-05	39.4000015258789062	11
300	2021-03-24 05:38:13.298463-05	24.3999996185302734	10
301	2021-03-24 05:38:14.260607-05	39.4000015258789062	11
302	2021-03-24 05:38:17.296413-05	24.3999996185302734	10
303	2021-03-24 05:38:18.277187-05	39.4000015258789062	11
304	2021-03-24 05:38:21.303159-05	24.3999996185302734	10
305	2021-03-24 05:38:22.290389-05	39.2999992370605469	11
306	2021-03-24 05:38:25.302503-05	24.3999996185302734	10
307	2021-03-24 05:38:26.314236-05	39.2999992370605469	11
308	2021-03-24 05:38:29.309087-05	24.3999996185302734	10
309	2021-03-24 05:38:30.309673-05	39.2999992370605469	11
310	2021-03-24 05:38:33.310414-05	24.3999996185302734	10
311	2021-03-24 05:38:34.315716-05	39.2999992370605469	11
312	2021-03-24 05:38:37.318794-05	24.3999996185302734	10
313	2021-03-24 05:38:38.315423-05	39.2000007629394531	11
314	2021-03-24 05:38:41.321277-05	24.3999996185302734	10
315	2021-03-24 05:38:42.324676-05	39.2000007629394531	11
316	2021-03-24 05:38:45.321513-05	24.3999996185302734	10
317	2021-03-24 05:38:46.32639-05	39.2000007629394531	11
318	2021-03-24 05:38:49.326284-05	24.3999996185302734	10
319	2021-03-24 05:38:50.330772-05	39.2000007629394531	11
320	2021-03-24 05:38:53.340796-05	24.3999996185302734	10
321	2021-03-24 05:38:54.340967-05	39.2000007629394531	11
322	2021-03-24 05:38:57.335187-05	24.5	10
323	2021-03-24 05:38:58.331706-05	39.2000007629394531	11
324	2021-03-24 05:39:01.34958-05	24.5	10
325	2021-03-24 05:39:02.342572-05	39.2000007629394531	11
326	2021-03-24 05:39:05.342901-05	24.5	10
327	2021-03-24 05:39:06.343158-05	39.2000007629394531	11
328	2021-03-24 05:39:09.351484-05	24.5	10
329	2021-03-24 05:39:10.367132-05	39.1000022888183594	11
330	2021-03-24 05:39:13.359162-05	24.3999996185302734	10
331	2021-03-24 05:39:14.362713-05	39.1000022888183594	11
332	2021-03-24 05:39:17.367509-05	24.5	10
333	2021-03-24 05:39:18.373589-05	39.1000022888183594	11
334	2021-03-24 05:39:21.366982-05	24.5	10
335	2021-03-24 05:39:22.363991-05	39.1000022888183594	11
336	2021-03-24 05:39:25.370288-05	24.5	10
337	2021-03-24 05:39:26.377444-05	39.1000022888183594	11
338	2021-03-24 05:39:29.37546-05	24.5	10
339	2021-03-24 05:39:30.379693-05	39.1000022888183594	11
340	2021-03-24 05:39:33.385488-05	24.5	10
341	2021-03-24 05:39:34.379833-05	39	11
342	2021-03-24 05:39:37.377032-05	24.5	10
343	2021-03-24 05:39:38.397688-05	39	11
344	2021-03-24 05:39:41.386295-05	24.5	10
345	2021-03-24 05:39:42.383626-05	39	11
346	2021-03-24 05:39:45.38037-05	24.5	10
347	2021-03-24 05:39:46.397363-05	39	11
348	2021-03-24 05:39:49.389646-05	24.5	10
349	2021-03-24 05:39:50.397559-05	39	11
350	2021-03-24 05:39:53.414689-05	24.5	10
351	2021-03-24 05:39:54.399048-05	38.9000015258789062	11
352	2021-03-24 05:39:57.402941-05	24.5	10
353	2021-03-24 05:39:58.406901-05	38.9000015258789062	11
354	2021-03-24 05:40:01.405765-05	24.5	10
355	2021-03-24 05:40:02.412522-05	38.9000015258789062	11
356	2021-03-24 05:40:05.415669-05	24.5	10
357	2021-03-24 05:40:06.415348-05	38.9000015258789062	11
358	2021-03-24 05:40:09.423193-05	24.5	10
359	2021-03-24 05:40:10.431351-05	38.7999992370605469	11
360	2021-03-24 05:40:13.41933-05	24.5	10
361	2021-03-24 05:40:14.427362-05	38.7999992370605469	11
362	2021-03-24 05:40:17.431827-05	24.5	10
363	2021-03-24 05:40:18.435671-05	38.7999992370605469	11
364	2021-03-24 05:40:21.440157-05	24.5	10
365	2021-03-24 05:40:22.442198-05	38.7999992370605469	11
366	2021-03-24 05:40:25.436402-05	24.5	10
367	2021-03-24 05:40:26.435521-05	38.7999992370605469	11
368	2021-03-24 05:40:29.443688-05	24.5	10
369	2021-03-24 05:40:30.443055-05	38.7999992370605469	11
370	2021-03-24 05:40:33.443612-05	24.5	10
371	2021-03-24 05:40:34.447437-05	38.7000007629394531	11
372	2021-03-24 05:40:37.445952-05	24.6000003814697266	10
373	2021-03-24 05:40:38.455335-05	38.7999992370605469	11
374	2021-03-24 05:40:41.447478-05	24.5	10
375	2021-03-24 05:40:42.458529-05	38.7000007629394531	11
376	2021-03-24 05:40:45.46232-05	24.6000003814697266	10
377	2021-03-24 05:40:46.458584-05	38.7000007629394531	11
378	2021-03-24 05:40:49.47109-05	24.6000003814697266	10
379	2021-03-24 05:40:50.468712-05	38.7000007629394531	11
380	2021-03-24 05:40:53.466813-05	24.6000003814697266	10
381	2021-03-24 05:40:54.469934-05	38.7000007629394531	11
382	2021-03-24 05:40:57.470625-05	24.6000003814697266	10
383	2021-03-24 05:40:58.470773-05	38.7000007629394531	11
384	2021-03-24 05:41:01.473601-05	24.6000003814697266	10
385	2021-03-24 05:41:02.46893-05	38.7000007629394531	11
386	2021-03-24 05:41:05.482539-05	24.6000003814697266	10
387	2021-03-24 05:41:06.480806-05	38.7000007629394531	11
388	2021-03-24 05:41:09.478902-05	24.6000003814697266	10
389	2021-03-24 05:41:10.48485-05	38.6000022888183594	11
390	2021-03-24 05:41:13.478752-05	24.6000003814697266	10
391	2021-03-24 05:41:14.488673-05	38.6000022888183594	11
392	2021-03-24 05:41:17.491574-05	24.6000003814697266	10
393	2021-03-24 05:41:18.48853-05	38.6000022888183594	11
394	2021-03-24 05:41:21.495427-05	24.6000003814697266	10
395	2021-03-24 05:41:22.488522-05	38.6000022888183594	11
396	2021-03-24 05:41:25.502145-05	24.6000003814697266	10
397	2021-03-24 05:41:26.494421-05	38.6000022888183594	11
398	2021-03-24 05:41:29.506794-05	24.6000003814697266	10
399	2021-03-24 05:41:30.508814-05	38.6000022888183594	11
400	2021-03-24 05:41:33.507238-05	24.6000003814697266	10
401	2021-03-24 05:41:34.512349-05	38.6000022888183594	11
402	2021-03-24 05:41:37.507028-05	24.6000003814697266	10
403	2021-03-24 05:41:38.515466-05	38.6000022888183594	11
404	2021-03-24 05:41:41.51059-05	24.6000003814697266	10
405	2021-03-24 05:41:42.513933-05	38.6000022888183594	11
406	2021-03-24 05:41:45.517419-05	24.6000003814697266	10
407	2021-03-24 05:41:46.522454-05	38.6000022888183594	11
408	2021-03-24 05:41:49.519559-05	24.6000003814697266	10
409	2021-03-24 05:41:50.52684-05	38.6000022888183594	11
410	2021-03-24 05:41:53.526224-05	24.6000003814697266	10
411	2021-03-24 05:41:54.525255-05	38.6000022888183594	11
412	2021-03-24 05:41:57.531218-05	24.6000003814697266	10
413	2021-03-24 05:41:58.533609-05	38.5	11
414	2021-03-24 05:42:01.528911-05	24.7000007629394531	10
415	2021-03-24 05:42:02.542724-05	38.5	11
416	2021-03-24 05:42:05.514513-05	24.6000003814697266	10
417	2021-03-24 05:42:06.541929-05	38.5	11
418	2021-03-24 05:42:09.522614-05	24.6000003814697266	10
419	2021-03-24 05:42:10.543983-05	38.5	11
420	2021-03-24 05:42:13.521242-05	24.7000007629394531	10
421	2021-03-24 05:42:14.551885-05	38.5	11
422	2021-03-24 05:42:17.520748-05	24.6000003814697266	10
423	2021-03-24 05:42:18.552221-05	38.4000015258789062	11
424	2021-03-24 05:42:21.533817-05	24.6000003814697266	10
425	2021-03-24 05:42:22.565163-05	38.4000015258789062	11
426	2021-03-24 05:42:25.539749-05	24.7000007629394531	10
427	2021-03-24 05:42:26.564643-05	38.4000015258789062	11
428	2021-03-24 05:42:29.548277-05	24.7000007629394531	10
429	2021-03-24 05:42:30.564553-05	38.4000015258789062	11
430	2021-03-24 05:42:33.563497-05	24.7000007629394531	10
431	2021-03-24 05:42:34.574655-05	38.5	11
432	2021-03-24 05:42:37.571709-05	24.6000003814697266	10
433	2021-03-24 05:42:38.575225-05	38.4000015258789062	11
434	2021-03-24 05:42:41.566917-05	24.7000007629394531	10
435	2021-03-24 05:42:42.572141-05	38.5	11
436	2021-03-24 05:42:45.575887-05	24.7000007629394531	10
437	2021-03-24 05:42:46.581431-05	38.4000015258789062	11
438	2021-03-24 05:42:49.58482-05	24.7000007629394531	10
439	2021-03-24 05:42:50.584391-05	38.4000015258789062	11
440	2021-03-24 05:42:53.590412-05	24.7000007629394531	10
441	2021-03-24 05:42:54.594484-05	38.4000015258789062	11
442	2021-03-24 05:42:57.597903-05	24.7000007629394531	10
443	2021-03-24 05:42:58.591309-05	38.4000015258789062	11
444	2021-03-24 05:43:01.600705-05	24.7000007629394531	10
445	2021-03-24 05:43:02.593331-05	38.4000015258789062	11
446	2021-03-24 05:43:05.610359-05	24.7000007629394531	10
447	2021-03-24 05:43:06.611869-05	38.4000015258789062	11
448	2021-03-24 05:43:09.605658-05	24.7000007629394531	10
449	2021-03-24 05:43:10.60804-05	38.4000015258789062	11
450	2021-03-24 05:43:13.613597-05	24.7000007629394531	10
451	2021-03-24 05:43:14.612914-05	38.4000015258789062	11
452	2021-03-24 05:43:17.62504-05	24.7000007629394531	10
453	2021-03-24 05:43:18.617378-05	38.2999992370605469	11
454	2021-03-24 05:43:21.631177-05	24.7000007629394531	10
455	2021-03-24 05:43:22.624285-05	38.4000015258789062	11
456	2021-03-24 05:43:25.619639-05	24.7000007629394531	10
457	2021-03-24 05:43:26.619177-05	38.2999992370605469	11
458	2021-03-24 05:43:29.626947-05	24.7000007629394531	10
459	2021-03-24 05:43:30.627799-05	38.2999992370605469	11
460	2021-03-24 05:43:33.643818-05	24.7000007629394531	10
461	2021-03-24 05:43:34.639789-05	38.2999992370605469	11
462	2021-03-24 05:43:37.646527-05	24.7000007629394531	10
463	2021-03-24 05:43:38.640438-05	38.2999992370605469	11
464	2021-03-24 05:43:41.641899-05	24.7000007629394531	10
465	2021-03-24 05:43:42.649948-05	38.2999992370605469	11
466	2021-03-24 05:43:45.645567-05	24.7000007629394531	10
467	2021-03-24 05:43:46.647203-05	38.2999992370605469	11
468	2021-03-24 05:43:49.663561-05	24.7000007629394531	10
469	2021-03-24 05:43:50.644817-05	38.2999992370605469	11
470	2021-03-24 05:43:53.667208-05	24.7000007629394531	10
471	2021-03-24 05:43:54.653528-05	38.2000007629394531	11
472	2021-03-24 05:43:57.657149-05	24.7000007629394531	10
473	2021-03-24 05:43:58.674759-05	38.2000007629394531	11
474	2021-03-24 05:44:01.653907-05	24.7000007629394531	10
475	2021-03-24 05:44:02.678193-05	38.2000007629394531	11
476	2021-03-24 05:44:05.675496-05	24.8000011444091797	10
477	2021-03-24 05:44:06.674077-05	38.2999992370605469	11
478	2021-03-24 05:44:09.687928-05	24.7000007629394531	10
479	2021-03-24 05:44:10.680803-05	38.2000007629394531	11
480	2021-03-24 05:44:13.67935-05	24.7000007629394531	10
481	2021-03-24 05:44:14.672149-05	38.2000007629394531	11
482	2021-03-24 05:44:17.686765-05	24.7000007629394531	10
483	2021-03-24 05:44:18.687387-05	38.2000007629394531	11
484	2021-03-24 05:44:21.702154-05	24.7000007629394531	10
485	2021-03-24 05:44:22.686169-05	38.1000022888183594	11
486	2021-03-24 05:44:25.696969-05	24.7000007629394531	10
487	2021-03-24 05:44:26.69768-05	38.1000022888183594	11
488	2021-03-24 05:44:29.709196-05	24.8000011444091797	10
489	2021-03-24 05:44:30.70031-05	38.2000007629394531	11
490	2021-03-24 05:44:33.711417-05	24.8000011444091797	10
491	2021-03-24 05:44:34.707424-05	38.2000007629394531	11
492	2021-03-24 05:44:37.723865-05	24.8000011444091797	10
493	2021-03-24 05:44:38.70677-05	38.2000007629394531	11
494	2021-03-24 05:44:41.721642-05	24.7000007629394531	10
495	2021-03-24 05:44:42.718483-05	38.1000022888183594	11
496	2021-03-24 05:44:45.720574-05	24.8000011444091797	10
497	2021-03-24 05:44:46.709198-05	38.1000022888183594	11
498	2021-03-24 05:44:49.729126-05	24.8000011444091797	10
499	2021-03-24 05:44:50.727989-05	38.1000022888183594	11
500	2021-03-24 05:44:53.732553-05	24.8000011444091797	10
501	2021-03-24 05:44:54.737613-05	38.1000022888183594	11
502	2021-03-24 05:44:57.737947-05	24.8000011444091797	10
503	2021-03-24 05:44:58.7245-05	38.1000022888183594	11
504	2021-03-24 05:45:01.750032-05	24.8000011444091797	10
505	2021-03-24 05:45:02.715454-05	38.1000022888183594	11
506	2021-03-24 05:45:05.740442-05	24.8000011444091797	10
507	2021-03-24 05:45:06.717448-05	38.1000022888183594	11
508	2021-03-24 05:45:09.75282-05	24.8000011444091797	10
509	2021-03-24 05:45:10.721105-05	38.1000022888183594	11
510	2021-03-24 05:45:13.752839-05	24.8000011444091797	10
511	2021-03-24 05:45:14.729057-05	38.1000022888183594	11
512	2021-03-24 05:45:17.75987-05	24.8000011444091797	10
513	2021-03-24 05:45:18.732863-05	38	11
514	2021-03-24 05:45:21.765102-05	24.8000011444091797	10
515	2021-03-24 05:45:22.739203-05	38	11
516	2021-03-24 05:45:25.766265-05	24.8999996185302734	10
517	2021-03-24 05:45:26.730667-05	38.1000022888183594	11
518	2021-03-24 05:45:29.765148-05	24.8000011444091797	10
519	2021-03-24 05:45:30.754081-05	38	11
520	2021-03-24 05:45:33.772149-05	24.8000011444091797	10
521	2021-03-24 05:45:34.749341-05	38	11
522	2021-03-24 05:45:37.774977-05	24.8000011444091797	10
523	2021-03-24 05:45:38.755593-05	38	11
524	2021-03-24 05:45:41.786073-05	24.8000011444091797	10
525	2021-03-24 05:45:42.758407-05	37.9000015258789062	11
526	2021-03-24 05:45:45.781394-05	24.8999996185302734	10
527	2021-03-24 05:45:46.764254-05	38	11
528	2021-03-24 05:45:49.793742-05	24.8999996185302734	10
529	2021-03-24 05:45:50.762134-05	38	11
530	2021-03-24 05:45:53.801544-05	24.8999996185302734	10
531	2021-03-24 05:45:54.780575-05	37.9000015258789062	11
532	2021-03-24 05:45:57.801395-05	24.8000011444091797	10
533	2021-03-24 05:45:58.77267-05	37.9000015258789062	11
534	2021-03-24 05:46:01.802223-05	24.8000011444091797	10
535	2021-03-24 05:46:02.785578-05	37.9000015258789062	11
536	2021-03-24 05:46:05.812454-05	24.8999996185302734	10
537	2021-03-24 05:46:06.785654-05	37.9000015258789062	11
538	2021-03-24 05:46:09.812024-05	24.8000011444091797	10
539	2021-03-24 05:46:10.789316-05	37.9000015258789062	11
540	2021-03-24 05:46:13.815004-05	24.8000011444091797	10
541	2021-03-24 05:46:14.795411-05	37.9000015258789062	11
542	2021-03-24 05:46:17.8262-05	24.8000011444091797	10
543	2021-03-24 05:46:18.800116-05	37.9000015258789062	11
544	2021-03-24 05:46:21.827433-05	24.8000011444091797	10
545	2021-03-24 05:46:22.801862-05	37.9000015258789062	11
546	2021-03-24 05:46:25.838745-05	24.8000011444091797	10
547	2021-03-24 05:46:26.809728-05	37.7999992370605469	11
548	2021-03-24 05:46:29.837649-05	24.8000011444091797	10
549	2021-03-24 05:46:30.810803-05	37.7999992370605469	11
550	2021-03-24 05:46:33.845738-05	24.8999996185302734	10
551	2021-03-24 05:46:34.813161-05	37.9000015258789062	11
552	2021-03-24 05:46:37.840355-05	24.8999996185302734	10
553	2021-03-24 05:46:38.82125-05	37.9000015258789062	11
554	2021-03-24 05:46:41.851305-05	24.8999996185302734	10
555	2021-03-24 05:46:42.818261-05	37.9000015258789062	11
556	2021-03-24 05:46:45.84854-05	24.8999996185302734	10
557	2021-03-24 05:46:46.826323-05	37.7999992370605469	11
558	2021-03-24 05:46:49.866105-05	24.8999996185302734	10
559	2021-03-24 05:46:50.830826-05	37.7999992370605469	11
560	2021-03-24 05:46:53.859336-05	24.8999996185302734	10
561	2021-03-24 05:46:54.837092-05	37.7999992370605469	11
562	2021-03-24 05:46:57.870818-05	24.8999996185302734	10
563	2021-03-24 05:46:58.835362-05	37.7999992370605469	11
564	2021-03-24 05:47:01.884328-05	24.8999996185302734	10
565	2021-03-24 05:47:02.847699-05	37.7999992370605469	11
566	2021-03-24 05:47:05.871983-05	24.8999996185302734	10
567	2021-03-24 05:47:06.856226-05	37.7999992370605469	11
568	2021-03-24 05:47:09.877924-05	24.8999996185302734	10
569	2021-03-24 05:47:10.858298-05	37.7999992370605469	11
570	2021-03-24 05:47:13.883059-05	24.8999996185302734	10
571	2021-03-24 05:47:14.858116-05	37.7999992370605469	11
572	2021-03-24 05:47:17.889124-05	24.8999996185302734	10
573	2021-03-24 05:47:18.864507-05	37.7999992370605469	11
574	2021-03-24 05:47:21.901309-05	24.8999996185302734	10
575	2021-03-24 05:47:22.863737-05	37.7999992370605469	11
576	2021-03-24 05:47:25.894287-05	24.8999996185302734	10
577	2021-03-24 05:47:26.866724-05	37.7999992370605469	11
578	2021-03-24 05:47:29.896301-05	24.8999996185302734	10
579	2021-03-24 05:47:30.873945-05	37.7999992370605469	11
580	2021-03-24 05:47:33.900654-05	24.8999996185302734	10
581	2021-03-24 05:47:34.880403-05	37.7999992370605469	11
582	2021-03-24 05:47:37.912209-05	24.8999996185302734	10
583	2021-03-24 05:47:38.889497-05	37.7999992370605469	11
584	2021-03-24 05:47:41.917385-05	24.8999996185302734	10
585	2021-03-24 05:47:42.883087-05	37.7999992370605469	11
586	2021-03-24 05:47:45.919636-05	24.8999996185302734	10
587	2021-03-24 05:47:46.898331-05	37.7000007629394531	11
588	2021-03-24 05:47:49.932194-05	24.8999996185302734	10
589	2021-03-24 05:47:50.896085-05	37.7000007629394531	11
590	2021-03-24 05:47:53.929393-05	24.8999996185302734	10
591	2021-03-24 05:47:54.902114-05	37.7000007629394531	11
592	2021-03-24 05:47:57.934352-05	24.8999996185302734	10
593	2021-03-24 05:47:58.914765-05	37.7000007629394531	11
594	2021-03-24 05:48:01.937596-05	24.8999996185302734	10
595	2021-03-24 05:48:02.930123-05	37.7000007629394531	11
596	2021-03-24 05:48:05.945448-05	24.8999996185302734	10
597	2021-03-24 05:48:06.926739-05	37.7000007629394531	11
598	2021-03-24 05:48:09.952659-05	24.8999996185302734	10
599	2021-03-24 05:48:10.934509-05	37.7000007629394531	11
600	2021-03-24 05:48:13.950111-05	24.8999996185302734	10
601	2021-03-24 05:48:14.92737-05	37.7000007629394531	11
602	2021-03-24 05:48:17.952967-05	24.8999996185302734	10
603	2021-03-24 05:48:18.936689-05	37.7000007629394531	11
604	2021-03-24 05:48:21.968196-05	24.8999996185302734	10
605	2021-03-24 05:48:22.933432-05	37.7000007629394531	11
606	2021-03-24 05:48:25.962711-05	24.8999996185302734	10
607	2021-03-24 05:48:26.93345-05	37.7000007629394531	11
608	2021-03-24 05:48:29.963075-05	24.8999996185302734	10
609	2021-03-24 05:48:30.950888-05	37.7000007629394531	11
610	2021-03-24 05:48:33.973606-05	24.8999996185302734	10
611	2021-03-24 05:48:34.948288-05	37.7000007629394531	11
612	2021-03-24 05:48:37.97677-05	24.8999996185302734	10
613	2021-03-24 05:48:38.965622-05	37.7000007629394531	11
614	2021-03-24 05:48:41.979215-05	24.8999996185302734	10
615	2021-03-24 05:48:42.966164-05	37.7000007629394531	11
616	2021-03-24 05:48:45.989417-05	24.8999996185302734	10
617	2021-03-24 05:48:46.977239-05	37.7000007629394531	11
618	2021-03-24 05:48:49.991378-05	24.8999996185302734	10
619	2021-03-24 05:48:50.981036-05	37.7000007629394531	11
620	2021-03-24 05:48:54.003188-05	24.8999996185302734	10
621	2021-03-24 05:48:54.987866-05	37.7000007629394531	11
622	2021-03-24 05:48:57.994922-05	24.8999996185302734	10
623	2021-03-24 05:48:58.988826-05	37.6000022888183594	11
624	2021-03-24 05:49:02.002165-05	24.8999996185302734	10
625	2021-03-24 05:49:02.977251-05	37.7000007629394531	11
626	2021-03-24 05:49:06.008126-05	24.8999996185302734	10
627	2021-03-24 05:49:06.989794-05	37.7000007629394531	11
628	2021-03-24 05:49:10.00833-05	24.8999996185302734	10
629	2021-03-24 05:49:10.992848-05	37.6000022888183594	11
630	2021-03-24 05:49:14.02741-05	24.8999996185302734	10
631	2021-03-24 05:49:15.005429-05	37.6000022888183594	11
632	2021-03-24 05:49:18.021633-05	24.8999996185302734	10
633	2021-03-24 05:49:19.003467-05	37.7000007629394531	11
634	2021-03-24 05:49:22.031809-05	24.8999996185302734	10
635	2021-03-24 05:49:23.012434-05	37.6000022888183594	11
636	2021-03-24 05:49:26.031572-05	24.8999996185302734	10
637	2021-03-24 05:49:27.028605-05	37.6000022888183594	11
638	2021-03-24 05:49:30.030201-05	24.8999996185302734	10
639	2021-03-24 05:49:31.01666-05	37.6000022888183594	11
640	2021-03-24 05:49:34.039877-05	24.8999996185302734	10
641	2021-03-24 05:49:35.025737-05	37.6000022888183594	11
642	2021-03-24 05:49:38.045602-05	24.8999996185302734	10
643	2021-03-24 05:49:39.032268-05	37.6000022888183594	11
644	2021-03-24 05:49:42.051104-05	24.8999996185302734	10
645	2021-03-24 05:49:43.057063-05	37.6000022888183594	11
646	2021-03-24 05:49:46.055112-05	24.8999996185302734	10
647	2021-03-24 05:49:47.050226-05	37.6000022888183594	11
648	2021-03-24 05:49:50.063457-05	24.8999996185302734	10
649	2021-03-24 05:49:51.056013-05	37.6000022888183594	11
650	2021-03-24 05:49:54.063157-05	24.8999996185302734	10
651	2021-03-24 05:49:55.062705-05	37.6000022888183594	11
652	2021-03-24 05:49:58.06679-05	24.8999996185302734	10
653	2021-03-24 05:49:59.0724-05	37.6000022888183594	11
654	2021-03-24 05:50:02.070119-05	24.8999996185302734	10
655	2021-03-24 05:50:03.062947-05	37.6000022888183594	11
656	2021-03-24 05:50:06.076394-05	24.8999996185302734	10
657	2021-03-24 05:50:07.06976-05	37.6000022888183594	11
658	2021-03-24 05:50:10.088017-05	24.8999996185302734	10
659	2021-03-24 05:50:11.069609-05	37.6000022888183594	11
660	2021-03-24 05:50:14.094065-05	24.8999996185302734	10
661	2021-03-24 05:50:15.083118-05	37.6000022888183594	11
662	2021-03-24 05:50:18.095856-05	24.8999996185302734	10
663	2021-03-24 05:50:19.089307-05	37.6000022888183594	11
664	2021-03-24 05:50:22.086749-05	24.8999996185302734	10
665	2021-03-24 05:50:23.085407-05	37.6000022888183594	11
666	2021-03-24 05:50:26.105188-05	24.8999996185302734	10
667	2021-03-24 05:50:27.104801-05	37.6000022888183594	11
668	2021-03-24 05:50:30.112357-05	24.8999996185302734	10
669	2021-03-24 05:50:31.115251-05	37.6000022888183594	11
670	2021-03-24 05:50:34.105866-05	24.8999996185302734	10
671	2021-03-24 05:50:35.112106-05	37.6000022888183594	11
672	2021-03-24 05:50:38.104799-05	24.8999996185302734	10
673	2021-03-24 05:50:39.107277-05	37.6000022888183594	11
674	2021-03-24 05:50:42.118617-05	24.8999996185302734	10
675	2021-03-24 05:50:43.109908-05	37.5	11
676	2021-03-24 05:50:46.118471-05	25	10
677	2021-03-24 05:50:47.117581-05	37.6000022888183594	11
678	2021-03-24 05:50:50.128637-05	24.8999996185302734	10
679	2021-03-24 05:50:51.120647-05	37.5	11
680	2021-03-24 05:50:54.123136-05	24.8999996185302734	10
681	2021-03-24 05:50:55.130242-05	37.5	11
682	2021-03-24 05:50:58.137748-05	24.8999996185302734	10
683	2021-03-24 05:50:59.137342-05	37.5	11
684	2021-03-24 05:51:02.145403-05	24.8999996185302734	10
685	2021-03-24 05:51:03.140569-05	37.5	11
686	2021-03-24 05:51:06.151144-05	25	10
687	2021-03-24 05:51:07.151023-05	37.5	11
688	2021-03-24 05:51:10.155424-05	24.8999996185302734	10
689	2021-03-24 05:51:11.150487-05	37.5	11
690	2021-03-24 05:51:14.148446-05	25	10
691	2021-03-24 05:51:15.152932-05	37.5	11
692	2021-03-24 05:51:18.169693-05	25	10
693	2021-03-24 05:51:19.159226-05	37.5	11
694	2021-03-24 05:51:22.161084-05	25	10
695	2021-03-24 05:51:23.163333-05	37.5	11
696	2021-03-24 05:51:26.163649-05	25	10
697	2021-03-24 05:51:27.170836-05	37.5	11
698	2021-03-24 05:51:30.17162-05	25	10
699	2021-03-24 05:51:31.16851-05	37.5	11
700	2021-03-24 05:51:34.1768-05	25	10
701	2021-03-24 05:51:35.179717-05	37.5	11
702	2021-03-24 05:51:38.181772-05	25	10
703	2021-03-24 05:51:39.177375-05	37.5	11
704	2021-03-24 05:51:42.185364-05	25	10
705	2021-03-24 05:51:43.176538-05	37.5	11
706	2021-03-24 05:51:46.19582-05	25	10
707	2021-03-24 05:51:47.196034-05	37.5	11
708	2021-03-24 05:51:50.194521-05	25	10
709	2021-03-24 05:51:51.195821-05	37.5	11
710	2021-03-24 05:51:54.196184-05	25	10
711	2021-03-24 05:51:55.193805-05	37.4000015258789062	11
712	2021-03-24 05:51:58.197365-05	25	10
713	2021-03-24 05:51:59.20308-05	37.4000015258789062	11
714	2021-03-24 05:52:02.181961-05	25	10
715	2021-03-24 05:52:03.210277-05	37.4000015258789062	11
716	2021-03-24 05:52:06.188083-05	25	10
717	2021-03-24 05:52:07.211015-05	37.4000015258789062	11
718	2021-03-24 05:52:10.186071-05	25	10
719	2021-03-24 05:52:11.217424-05	37.4000015258789062	11
720	2021-03-24 05:52:14.189941-05	25	10
721	2021-03-24 05:52:15.217964-05	37.4000015258789062	11
722	2021-03-24 05:52:18.211184-05	25	10
723	2021-03-24 05:52:19.219366-05	37.4000015258789062	11
724	2021-03-24 05:52:22.217749-05	25	10
725	2021-03-24 05:52:23.224539-05	37.4000015258789062	11
726	2021-03-24 05:52:26.226337-05	25	10
727	2021-03-24 05:52:27.234081-05	37.4000015258789062	11
728	2021-03-24 05:52:30.245614-05	25	10
729	2021-03-24 05:52:31.238137-05	37.4000015258789062	11
730	2021-03-24 05:52:34.238069-05	25	10
731	2021-03-24 05:52:35.248875-05	37.2999992370605469	11
732	2021-03-24 05:52:38.243219-05	25	10
733	2021-03-24 05:52:39.244487-05	37.2999992370605469	11
734	2021-03-24 05:52:42.249169-05	25	10
735	2021-03-24 05:52:43.259472-05	37.2999992370605469	11
736	2021-03-24 05:52:46.255015-05	25	10
737	2021-03-24 05:52:47.257235-05	37.2999992370605469	11
738	2021-03-24 05:52:50.256502-05	25	10
739	2021-03-24 05:52:51.26105-05	37.2999992370605469	11
740	2021-03-24 05:52:54.263566-05	25	10
741	2021-03-24 05:52:55.257709-05	37.2999992370605469	11
742	2021-03-24 05:52:58.261047-05	25	10
743	2021-03-24 05:52:59.267999-05	37.2999992370605469	11
744	2021-03-24 05:53:02.265878-05	25.1000003814697266	10
745	2021-03-24 05:53:03.278827-05	37.2999992370605469	11
746	2021-03-24 05:53:06.27249-05	25	10
747	2021-03-24 05:53:07.277518-05	37.2999992370605469	11
748	2021-03-24 05:53:10.276806-05	25	10
749	2021-03-24 05:53:11.277666-05	37.2999992370605469	11
750	2021-03-24 05:53:14.287566-05	25	10
751	2021-03-24 05:53:15.293468-05	37.2999992370605469	11
752	2021-03-24 05:53:18.285379-05	25	10
753	2021-03-24 05:53:19.297517-05	37.2000007629394531	11
754	2021-03-24 05:53:22.290686-05	25	10
755	2021-03-24 05:53:23.291377-05	37.2000007629394531	11
756	2021-03-24 05:53:26.290744-05	25	10
757	2021-03-24 05:53:27.29795-05	37.2000007629394531	11
758	2021-03-24 05:53:30.296503-05	25	10
759	2021-03-24 05:53:31.301852-05	37.2000007629394531	11
760	2021-03-24 05:53:34.315547-05	25	10
761	2021-03-24 05:53:35.314353-05	37.2000007629394531	11
762	2021-03-24 05:53:38.311762-05	25	10
763	2021-03-24 05:53:39.312667-05	37.2000007629394531	11
764	2021-03-24 05:53:42.313018-05	25	10
765	2021-03-24 05:53:43.314606-05	37.2000007629394531	11
766	2021-03-24 05:53:46.315443-05	25	10
767	2021-03-24 05:53:47.324377-05	37.2999992370605469	11
768	2021-03-24 05:53:50.326104-05	25	10
769	2021-03-24 05:53:51.328308-05	37.2999992370605469	11
770	2021-03-24 05:53:54.335865-05	25	10
771	2021-03-24 05:53:55.323004-05	37.2999992370605469	11
772	2021-03-24 05:53:58.340023-05	25	10
773	2021-03-24 05:53:59.347988-05	37.2999992370605469	11
774	2021-03-24 05:54:02.334062-05	25	10
775	2021-03-24 05:54:03.344566-05	37.2999992370605469	11
776	2021-03-24 05:54:06.349618-05	25	10
777	2021-03-24 05:54:07.351109-05	37.4000015258789062	11
778	2021-03-24 05:54:10.350774-05	25	10
779	2021-03-24 05:54:11.348086-05	37.4000015258789062	11
780	2021-03-24 05:54:14.3545-05	25	10
781	2021-03-24 05:54:15.353076-05	37.4000015258789062	11
782	2021-03-24 05:54:18.356426-05	25	10
783	2021-03-24 05:54:19.356181-05	37.4000015258789062	11
784	2021-03-24 05:54:22.371911-05	25	10
785	2021-03-24 05:54:23.356625-05	37.4000015258789062	11
786	2021-03-24 05:54:26.364836-05	25	10
787	2021-03-24 05:54:27.371721-05	37.2999992370605469	11
788	2021-03-24 05:54:30.366585-05	25	10
789	2021-03-24 05:54:31.371907-05	37.2999992370605469	11
790	2021-03-24 05:54:34.376375-05	25	10
791	2021-03-24 05:54:35.372534-05	37.2999992370605469	11
792	2021-03-24 05:54:38.382913-05	25.1000003814697266	10
793	2021-03-24 05:54:39.371316-05	37.2999992370605469	11
794	2021-03-24 05:54:42.3914-05	25	10
795	2021-03-24 05:54:43.390446-05	37.2999992370605469	11
796	2021-03-24 05:54:46.391852-05	25.1000003814697266	10
797	2021-03-24 05:54:47.394328-05	37.2999992370605469	11
798	2021-03-24 05:54:50.392355-05	25	10
799	2021-03-24 05:54:51.402967-05	37.2999992370605469	11
800	2021-03-24 05:54:54.395304-05	25	10
801	2021-03-24 05:54:55.400771-05	37.2000007629394531	11
802	2021-03-24 05:54:58.408453-05	25	10
803	2021-03-24 05:54:59.403118-05	37.2999992370605469	11
804	2021-03-24 05:55:02.403735-05	25	10
805	2021-03-24 05:55:03.420709-05	37.2000007629394531	11
806	2021-03-24 05:55:06.417746-05	25	10
807	2021-03-24 05:55:07.407182-05	37.2000007629394531	11
808	2021-03-24 05:55:10.418826-05	25	10
809	2021-03-24 05:55:11.42245-05	37.2000007629394531	11
810	2021-03-24 05:55:14.418508-05	25	10
811	2021-03-24 05:55:15.418022-05	37.2000007629394531	11
812	2021-03-24 05:55:18.417725-05	25	10
813	2021-03-24 05:55:19.430154-05	37.2000007629394531	11
814	2021-03-24 05:55:22.444267-05	25	10
815	2021-03-24 05:55:23.419991-05	37.2000007629394531	11
816	2021-03-24 05:55:26.439448-05	25	10
817	2021-03-24 05:55:27.429875-05	37.2999992370605469	11
818	2021-03-24 05:55:30.442944-05	25	10
819	2021-03-24 05:55:31.427899-05	37.2999992370605469	11
820	2021-03-24 05:55:34.445537-05	25	10
821	2021-03-24 05:55:35.451825-05	37.2999992370605469	11
822	2021-03-24 05:55:38.458132-05	25	10
823	2021-03-24 05:55:39.452927-05	37.2999992370605469	11
824	2021-03-24 05:55:42.448172-05	25	10
825	2021-03-24 05:55:43.451348-05	37.2999992370605469	11
826	2021-03-24 05:55:46.454018-05	25	10
827	2021-03-24 05:55:47.447672-05	37.2999992370605469	11
828	2021-03-24 05:55:50.475314-05	25.1000003814697266	10
829	2021-03-24 05:55:51.466983-05	37.2999992370605469	11
830	2021-03-24 05:55:54.466263-05	25.1000003814697266	10
831	2021-03-24 05:55:55.466748-05	37.2999992370605469	11
832	2021-03-24 05:55:58.463442-05	25.1000003814697266	10
833	2021-03-24 05:55:59.474914-05	37.2999992370605469	11
834	2021-03-24 05:56:02.480233-05	25	10
835	2021-03-24 05:56:03.477437-05	37.2000007629394531	11
836	2021-03-24 05:56:06.489098-05	25.1000003814697266	10
837	2021-03-24 05:56:07.481735-05	37.2999992370605469	11
838	2021-03-24 05:56:10.483034-05	25.1000003814697266	10
839	2021-03-24 05:56:11.483575-05	37.2999992370605469	11
840	2021-03-24 05:56:14.492897-05	25.1000003814697266	10
841	2021-03-24 05:56:15.488546-05	37.2000007629394531	11
842	2021-03-24 05:56:18.495388-05	25.1000003814697266	10
843	2021-03-24 05:56:19.49967-05	37.2000007629394531	11
844	2021-03-24 05:56:22.4945-05	25.1000003814697266	10
845	2021-03-24 05:56:23.504599-05	37.2000007629394531	11
846	2021-03-24 05:56:26.500392-05	25.1000003814697266	10
847	2021-03-24 05:56:27.493862-05	37.2000007629394531	11
848	2021-03-24 05:56:30.515522-05	25.1000003814697266	10
849	2021-03-24 05:56:31.490288-05	37.2000007629394531	11
850	2021-03-24 05:56:34.513209-05	25.1000003814697266	10
851	2021-03-24 05:56:35.479557-05	37.2000007629394531	11
852	2021-03-24 05:56:38.516266-05	25	10
853	2021-03-24 05:56:39.487081-05	37.1000022888183594	11
854	2021-03-24 05:56:42.517714-05	25.1000003814697266	10
855	2021-03-24 05:56:43.495917-05	37.2000007629394531	11
856	2021-03-24 05:56:46.513789-05	25.1000003814697266	10
857	2021-03-24 05:56:47.507807-05	37.2000007629394531	11
858	2021-03-24 05:56:50.531439-05	25	10
859	2021-03-24 05:56:51.525398-05	37.1000022888183594	11
860	2021-03-24 05:56:54.531201-05	25.1000003814697266	10
861	2021-03-24 05:56:55.523556-05	37.1000022888183594	11
862	2021-03-24 05:56:58.536923-05	25	10
863	2021-03-24 05:56:59.537752-05	37.1000022888183594	11
864	2021-03-24 05:57:02.544045-05	25.1000003814697266	10
865	2021-03-24 05:57:03.544554-05	37.1000022888183594	11
866	2021-03-24 05:57:06.547894-05	25.1000003814697266	10
867	2021-03-24 05:57:07.542756-05	37.1000022888183594	11
868	2021-03-24 05:57:10.552115-05	25.1000003814697266	10
869	2021-03-24 05:57:11.547624-05	37.1000022888183594	11
870	2021-03-24 05:57:14.558901-05	25.1000003814697266	10
871	2021-03-24 05:57:15.558526-05	37.1000022888183594	11
872	2021-03-24 05:57:18.559099-05	25.1000003814697266	10
873	2021-03-24 05:57:19.566319-05	37.1000022888183594	11
874	2021-03-24 05:57:22.559492-05	25.1000003814697266	10
875	2021-03-24 05:57:23.5619-05	37.1000022888183594	11
876	2021-03-24 05:57:26.566725-05	25.1000003814697266	10
877	2021-03-24 05:57:27.571025-05	37.1000022888183594	11
878	2021-03-24 05:57:30.570509-05	25.1000003814697266	10
879	2021-03-24 05:57:31.582976-05	37.1000022888183594	11
880	2021-03-24 05:57:34.573032-05	25.1000003814697266	10
881	2021-03-24 05:57:35.578474-05	37.1000022888183594	11
882	2021-03-24 05:57:38.552576-05	25.1000003814697266	10
883	2021-03-24 05:57:39.583433-05	37.1000022888183594	11
884	2021-03-24 05:57:42.576598-05	25.1000003814697266	10
885	2021-03-24 05:57:43.57743-05	37.1000022888183594	11
886	2021-03-24 05:57:46.56361-05	25.1000003814697266	10
887	2021-03-24 05:57:47.588035-05	37	11
888	2021-03-24 05:57:50.582819-05	25.1000003814697266	10
889	2021-03-24 05:57:51.592743-05	37.1000022888183594	11
890	2021-03-24 05:57:54.585221-05	25.1000003814697266	10
891	2021-03-24 05:57:55.594235-05	37.1000022888183594	11
892	2021-03-24 05:57:58.601923-05	25.1000003814697266	10
893	2021-03-24 05:57:59.605166-05	37.1000022888183594	11
894	2021-03-24 05:58:02.607252-05	25.1000003814697266	10
895	2021-03-24 05:58:03.61272-05	37.1000022888183594	11
896	2021-03-24 05:58:06.60314-05	25.1000003814697266	10
897	2021-03-24 05:58:07.609379-05	37.1000022888183594	11
898	2021-03-24 05:58:10.620692-05	25.1000003814697266	10
899	2021-03-24 05:58:11.610148-05	37	11
900	2021-03-24 05:58:14.621413-05	25.1000003814697266	10
901	2021-03-24 05:58:15.6211-05	37	11
902	2021-03-24 05:58:18.625081-05	25.1000003814697266	10
903	2021-03-24 05:58:19.644194-05	37	11
904	2021-03-24 05:58:22.627126-05	25.1000003814697266	10
905	2021-03-24 05:58:23.634353-05	37	11
906	2021-03-24 05:58:26.633342-05	25.1000003814697266	10
907	2021-03-24 05:58:27.641022-05	37	11
908	2021-03-24 05:58:30.632667-05	25.1000003814697266	10
909	2021-03-24 05:58:31.63245-05	37	11
910	2021-03-24 05:58:34.632793-05	25.1000003814697266	10
911	2021-03-24 05:58:35.639761-05	37	11
912	2021-03-24 05:58:38.642891-05	25.1000003814697266	10
913	2021-03-24 05:58:39.643061-05	37	11
914	2021-03-24 05:58:42.646809-05	25.1000003814697266	10
915	2021-03-24 05:58:43.652934-05	37	11
916	2021-03-24 05:58:46.645889-05	25.1000003814697266	10
917	2021-03-24 05:58:47.659504-05	37	11
918	2021-03-24 05:58:50.648639-05	25.1000003814697266	10
919	2021-03-24 05:58:51.655387-05	37	11
920	2021-03-24 05:58:54.665419-05	25.1000003814697266	10
921	2021-03-24 05:58:55.660451-05	37	11
922	2021-03-24 05:58:58.663867-05	25.1000003814697266	10
923	2021-03-24 05:58:59.672325-05	37	11
924	2021-03-24 05:59:02.676663-05	25.1000003814697266	10
925	2021-03-24 05:59:03.680698-05	37	11
926	2021-03-24 05:59:06.674545-05	25.1000003814697266	10
927	2021-03-24 05:59:07.679378-05	37	11
928	2021-03-24 05:59:10.689062-05	25.1000003814697266	10
929	2021-03-24 05:59:11.685058-05	37	11
930	2021-03-24 05:59:14.694775-05	25.1000003814697266	10
931	2021-03-24 05:59:15.693497-05	37	11
932	2021-03-24 05:59:18.700176-05	25.1000003814697266	10
933	2021-03-24 05:59:19.706751-05	37	11
934	2021-03-24 05:59:22.695873-05	25.1000003814697266	10
935	2021-03-24 05:59:23.700834-05	36.9000015258789062	11
936	2021-03-24 05:59:26.698713-05	25.1000003814697266	10
937	2021-03-24 05:59:27.707534-05	36.9000015258789062	11
938	2021-03-24 05:59:30.711969-05	25.1000003814697266	10
939	2021-03-24 05:59:31.705153-05	36.9000015258789062	11
940	2021-03-24 05:59:34.71407-05	25.1000003814697266	10
941	2021-03-24 05:59:35.710256-05	36.9000015258789062	11
942	2021-03-24 05:59:38.719179-05	25.1000003814697266	10
943	2021-03-24 05:59:39.719919-05	36.9000015258789062	11
944	2021-03-24 05:59:42.724029-05	25.1000003814697266	10
945	2021-03-24 05:59:43.721563-05	36.9000015258789062	11
946	2021-03-24 05:59:46.730954-05	25.1000003814697266	10
947	2021-03-24 05:59:47.725511-05	36.7999992370605469	11
948	2021-03-24 05:59:50.729895-05	25.1000003814697266	10
949	2021-03-24 05:59:51.732004-05	36.9000015258789062	11
950	2021-03-24 05:59:54.732166-05	25.1000003814697266	10
951	2021-03-24 05:59:55.743128-05	36.7999992370605469	11
952	2021-03-24 05:59:58.742903-05	25.1000003814697266	10
953	2021-03-24 05:59:59.74349-05	36.7999992370605469	11
954	2021-03-24 06:00:02.746978-05	25.1000003814697266	10
955	2021-03-24 06:00:03.740587-05	36.7999992370605469	11
956	2021-03-24 06:00:06.736446-05	25.1000003814697266	10
957	2021-03-24 06:00:07.75086-05	36.7999992370605469	11
958	2021-03-24 06:00:10.750398-05	25.1000003814697266	10
959	2021-03-24 06:00:11.750644-05	36.7999992370605469	11
960	2021-03-24 06:00:14.762323-05	25.1000003814697266	10
961	2021-03-24 06:00:15.758883-05	36.7999992370605469	11
962	2021-03-24 06:00:18.760722-05	25.1000003814697266	10
963	2021-03-24 06:00:19.750672-05	36.7999992370605469	11
964	2021-03-24 06:00:22.761539-05	25.1000003814697266	10
965	2021-03-24 06:00:23.773025-05	36.7999992370605469	11
966	2021-03-24 06:00:26.768355-05	25.1000003814697266	10
967	2021-03-24 06:00:27.778229-05	36.7999992370605469	11
968	2021-03-24 06:00:30.773722-05	25.1000003814697266	10
969	2021-03-24 06:00:31.774692-05	36.7999992370605469	11
970	2021-03-24 06:00:34.777035-05	25.1000003814697266	10
971	2021-03-24 06:00:35.753264-05	36.7999992370605469	11
972	2021-03-24 06:00:38.779235-05	25.1000003814697266	10
973	2021-03-24 06:00:39.752559-05	36.7999992370605469	11
974	2021-03-24 06:00:42.798855-05	25.1000003814697266	10
975	2021-03-24 06:00:43.772749-05	36.7999992370605469	11
976	2021-03-24 06:00:46.800453-05	25.1000003814697266	10
977	2021-03-24 06:00:47.784766-05	36.7999992370605469	11
978	2021-03-24 06:00:50.791334-05	25.1000003814697266	10
979	2021-03-24 06:00:51.78813-05	36.7999992370605469	11
980	2021-03-24 06:00:54.817206-05	25.1000003814697266	10
981	2021-03-24 06:00:55.784062-05	36.7999992370605469	11
982	2021-03-24 06:00:58.811209-05	25.1000003814697266	10
983	2021-03-24 06:00:59.804736-05	36.7999992370605469	11
984	2021-03-24 06:01:02.815524-05	25.1000003814697266	10
985	2021-03-24 06:01:03.818497-05	36.7999992370605469	11
986	2021-03-24 06:01:06.820317-05	25.1000003814697266	10
987	2021-03-24 06:01:07.812252-05	36.7999992370605469	11
988	2021-03-24 06:01:10.817201-05	25.1000003814697266	10
989	2021-03-24 06:01:11.821586-05	36.7999992370605469	11
990	2021-03-24 06:01:14.8236-05	25.1000003814697266	10
991	2021-03-24 06:01:15.823259-05	36.7999992370605469	11
992	2021-03-24 06:01:18.823562-05	25.1000003814697266	10
993	2021-03-24 06:01:19.834039-05	36.7999992370605469	11
994	2021-03-24 06:01:22.825529-05	25.1000003814697266	10
995	2021-03-24 06:01:23.828919-05	36.7999992370605469	11
996	2021-03-24 06:01:26.819685-05	25.1000003814697266	10
997	2021-03-24 06:01:27.842024-05	36.7999992370605469	11
998	2021-03-24 06:01:30.815298-05	25.1000003814697266	10
999	2021-03-24 06:01:31.830575-05	36.7999992370605469	11
1000	2021-03-24 06:01:34.841314-05	25.1000003814697266	10
1001	2021-03-24 06:01:35.839154-05	36.7999992370605469	11
1002	2021-03-24 06:01:38.840794-05	25.1000003814697266	10
1003	2021-03-24 06:01:39.841948-05	36.7999992370605469	11
1004	2021-03-24 06:01:42.849378-05	25.1000003814697266	10
1005	2021-03-24 06:01:43.85681-05	36.7999992370605469	11
1006	2021-03-24 06:01:46.860065-05	25.1000003814697266	10
1007	2021-03-24 06:01:47.852106-05	36.7999992370605469	11
1008	2021-03-24 06:01:50.854416-05	25.1000003814697266	10
1009	2021-03-24 06:01:51.864044-05	36.7999992370605469	11
1010	2021-03-24 06:01:54.870091-05	25.1000003814697266	10
1011	2021-03-24 06:01:55.864599-05	36.7999992370605469	11
1012	2021-03-24 06:01:58.879611-05	25.1000003814697266	10
1013	2021-03-24 06:01:59.862401-05	36.7999992370605469	11
1014	2021-03-24 06:02:02.879565-05	25.1000003814697266	10
1015	2021-03-24 06:02:03.873962-05	36.7999992370605469	11
1016	2021-03-24 06:02:06.864668-05	25.1000003814697266	10
1017	2021-03-24 06:02:07.876544-05	36.7999992370605469	11
1018	2021-03-24 06:02:10.882968-05	25.1000003814697266	10
1019	2021-03-24 06:02:11.885839-05	36.9000015258789062	11
1020	2021-03-24 06:02:14.885202-05	25.1000003814697266	10
1021	2021-03-24 06:02:15.881101-05	36.9000015258789062	11
1022	2021-03-24 06:02:18.896252-05	25.1000003814697266	10
1023	2021-03-24 06:02:19.891173-05	36.7999992370605469	11
1024	2021-03-24 06:02:22.898262-05	25.1000003814697266	10
1025	2021-03-24 06:02:23.895675-05	36.7999992370605469	11
1026	2021-03-24 06:02:26.904368-05	25.1000003814697266	10
1027	2021-03-24 06:02:27.900823-05	36.7999992370605469	11
1028	2021-03-24 06:02:30.900131-05	25.1000003814697266	10
1029	2021-03-24 06:02:31.909242-05	36.7999992370605469	11
1030	2021-03-24 06:02:34.907449-05	25.1000003814697266	10
1031	2021-03-24 06:02:35.909773-05	36.7999992370605469	11
1032	2021-03-24 06:02:38.913243-05	25.1000003814697266	10
1033	2021-03-24 06:02:39.917283-05	36.7999992370605469	11
1034	2021-03-24 06:02:42.910362-05	25.1000003814697266	10
1035	2021-03-24 06:02:43.914077-05	36.7999992370605469	11
1036	2021-03-24 06:02:46.917375-05	25.1000003814697266	10
1037	2021-03-24 06:02:47.90817-05	36.7999992370605469	11
1038	2021-03-24 06:02:50.926933-05	25.1000003814697266	10
1039	2021-03-24 06:02:51.923614-05	36.7999992370605469	11
1040	2021-03-24 06:02:54.930164-05	25.1000003814697266	10
1041	2021-03-24 06:02:55.931334-05	36.7999992370605469	11
1042	2021-03-24 06:02:58.932484-05	25.1000003814697266	10
1043	2021-03-24 06:02:59.93525-05	36.7999992370605469	11
1044	2021-03-24 06:03:02.938869-05	25.1000003814697266	10
1045	2021-03-24 06:03:03.932896-05	36.7999992370605469	11
1046	2021-03-24 06:03:06.944757-05	25.1000003814697266	10
1047	2021-03-24 06:03:07.939928-05	36.7999992370605469	11
1048	2021-03-24 06:03:10.946412-05	25	10
1049	2021-03-24 06:03:11.94366-05	36.7999992370605469	11
1050	2021-03-24 06:03:14.946722-05	25.1000003814697266	10
1051	2021-03-24 06:03:15.948839-05	36.7999992370605469	11
1052	2021-03-24 06:03:18.952282-05	25.1000003814697266	10
1053	2021-03-24 06:03:19.954563-05	36.7999992370605469	11
1054	2021-03-24 06:03:22.962952-05	25.1000003814697266	10
1055	2021-03-24 06:03:23.960831-05	36.7999992370605469	11
1056	2021-03-24 06:03:26.963395-05	25.1000003814697266	10
1057	2021-03-24 06:03:27.963644-05	36.7999992370605469	11
1058	2021-03-24 06:03:30.96671-05	25.1000003814697266	10
1059	2021-03-24 06:03:31.957006-05	36.7999992370605469	11
1060	2021-03-24 06:03:34.984225-05	25.1000003814697266	10
1061	2021-03-24 06:03:35.97006-05	36.7999992370605469	11
1062	2021-03-24 06:03:38.973834-05	25.1000003814697266	10
1063	2021-03-24 06:03:39.975166-05	36.7999992370605469	11
1064	2021-03-24 06:03:42.969076-05	25.1000003814697266	10
1065	2021-03-24 06:03:43.972297-05	36.7999992370605469	11
1066	2021-03-24 06:03:46.985495-05	25.1000003814697266	10
1067	2021-03-24 06:03:47.971041-05	36.7999992370605469	11
1068	2021-03-24 06:03:50.987201-05	25.1000003814697266	10
1069	2021-03-24 06:03:51.969052-05	36.7999992370605469	11
1070	2021-03-24 06:03:54.98434-05	25.1000003814697266	10
1071	2021-03-24 06:03:55.965144-05	36.7999992370605469	11
1072	2021-03-24 06:03:58.996363-05	25.1000003814697266	10
1073	2021-03-24 06:03:59.968983-05	36.7999992370605469	11
1074	2021-03-24 06:04:02.983894-05	25.1000003814697266	10
1075	2021-03-24 06:04:03.988894-05	36.7999992370605469	11
1076	2021-03-24 06:04:07.002966-05	25.1000003814697266	10
1077	2021-03-24 06:04:08.000781-05	36.7999992370605469	11
1078	2021-03-24 06:04:11.009829-05	25.1000003814697266	10
1079	2021-03-24 06:04:11.999073-05	36.7999992370605469	11
1080	2021-03-24 06:04:15.007795-05	25.1000003814697266	10
1081	2021-03-24 06:04:15.995148-05	36.7999992370605469	11
1082	2021-03-24 06:04:19.012028-05	25.1000003814697266	10
1083	2021-03-24 06:04:20.019695-05	36.7999992370605469	11
1084	2021-03-24 06:04:23.023799-05	25.1000003814697266	10
1085	2021-03-24 06:04:24.019473-05	36.7999992370605469	11
1086	2021-03-24 06:04:27.027327-05	25.1000003814697266	10
1087	2021-03-24 06:04:28.027316-05	36.7999992370605469	11
1088	2021-03-24 06:04:31.015846-05	25.1000003814697266	10
1089	2021-03-24 06:04:32.027533-05	36.7999992370605469	11
1090	2021-03-24 06:04:35.027797-05	25.1000003814697266	10
1091	2021-03-24 06:04:36.033603-05	36.7999992370605469	11
1092	2021-03-24 06:04:39.017299-05	25.1000003814697266	10
1093	2021-03-24 06:04:40.032265-05	36.7999992370605469	11
1094	2021-03-24 06:04:43.013655-05	25.1000003814697266	10
1095	2021-03-24 06:04:44.042086-05	36.7999992370605469	11
1096	2021-03-24 06:04:47.03852-05	25.1000003814697266	10
1097	2021-03-24 06:04:48.046046-05	36.7999992370605469	11
1098	2021-03-24 06:04:51.046197-05	25.1000003814697266	10
1099	2021-03-24 06:04:52.046767-05	36.7999992370605469	11
1100	2021-03-24 06:04:55.052821-05	25.1000003814697266	10
1101	2021-03-24 06:04:56.051016-05	36.7999992370605469	11
1102	2021-03-24 06:04:59.058775-05	25.1000003814697266	10
1103	2021-03-24 06:05:00.044912-05	36.7999992370605469	11
1104	2021-03-24 06:05:03.068128-05	25.1000003814697266	10
1105	2021-03-24 06:05:04.056186-05	36.7999992370605469	11
1106	2021-03-24 06:05:07.069277-05	25.1000003814697266	10
1107	2021-03-24 06:05:08.069753-05	36.7999992370605469	11
1108	2021-03-24 06:05:11.070879-05	25.1000003814697266	10
1109	2021-03-24 06:05:12.071909-05	36.7999992370605469	11
1110	2021-03-24 06:05:15.065931-05	25.1000003814697266	10
1111	2021-03-24 06:05:16.069316-05	36.7999992370605469	11
1112	2021-03-24 06:05:19.081385-05	25.1000003814697266	10
1113	2021-03-24 06:05:20.077679-05	36.7999992370605469	11
1114	2021-03-24 06:05:23.086038-05	25.1000003814697266	10
1115	2021-03-24 06:05:24.082937-05	36.7999992370605469	11
1116	2021-03-24 06:05:27.093281-05	25.1000003814697266	10
1117	2021-03-24 06:05:28.083274-05	36.7999992370605469	11
1118	2021-03-24 06:05:31.09364-05	25.1000003814697266	10
1119	2021-03-24 06:05:32.093471-05	36.7999992370605469	11
1120	2021-03-24 06:05:35.09593-05	25.1000003814697266	10
1121	2021-03-24 06:05:36.097776-05	36.7999992370605469	11
1122	2021-03-24 06:05:39.111012-05	25.1000003814697266	10
1123	2021-03-24 06:05:40.110084-05	36.7999992370605469	11
1124	2021-03-24 06:05:43.09567-05	25.1000003814697266	10
1125	2021-03-24 06:05:44.107039-05	36.7999992370605469	11
1126	2021-03-24 06:05:47.111437-05	25.1000003814697266	10
1127	2021-03-24 06:05:48.108168-05	36.7999992370605469	11
1128	2021-03-24 06:05:51.121749-05	25.1000003814697266	10
1129	2021-03-24 06:05:52.117482-05	36.7999992370605469	11
1130	2021-03-24 06:05:55.128814-05	25.1000003814697266	10
1131	2021-03-24 06:05:56.118866-05	36.7999992370605469	11
1132	2021-03-24 06:05:59.113497-05	25.1000003814697266	10
1133	2021-03-24 06:06:00.121812-05	36.7999992370605469	11
1134	2021-03-24 06:06:03.122733-05	25.1000003814697266	10
1135	2021-03-24 06:06:04.126912-05	36.7999992370605469	11
1136	2021-03-24 06:06:07.124777-05	25.1000003814697266	10
1137	2021-03-24 06:06:08.129503-05	36.7999992370605469	11
1138	2021-03-24 06:06:11.121731-05	25.1000003814697266	10
1139	2021-03-24 06:06:12.148512-05	36.7999992370605469	11
1140	2021-03-24 06:06:15.137251-05	25.1000003814697266	10
1141	2021-03-24 06:06:16.138038-05	36.7999992370605469	11
1142	2021-03-24 06:06:19.140727-05	25.1000003814697266	10
1143	2021-03-24 06:06:20.137931-05	36.7000007629394531	11
1144	2021-03-24 06:06:23.151779-05	25.1000003814697266	10
1145	2021-03-24 06:06:24.146933-05	36.7000007629394531	11
1146	2021-03-24 06:06:27.154395-05	25.1000003814697266	10
1147	2021-03-24 06:06:28.153333-05	36.7000007629394531	11
1148	2021-03-24 06:06:31.15048-05	25	10
1149	2021-03-24 06:06:32.155142-05	36.7000007629394531	11
1150	2021-03-24 06:06:35.146696-05	25.1000003814697266	10
1151	2021-03-24 06:06:36.15142-05	36.7000007629394531	11
1152	2021-03-24 06:06:39.163737-05	25.1000003814697266	10
1153	2021-03-24 06:06:40.16997-05	36.7000007629394531	11
1154	2021-03-24 06:06:43.165108-05	25	10
1155	2021-03-24 06:06:44.174827-05	36.7000007629394531	11
1156	2021-03-24 06:06:47.174204-05	25.1000003814697266	10
1157	2021-03-24 06:06:48.180372-05	36.7000007629394531	11
1158	2021-03-24 06:06:51.186294-05	25.1000003814697266	10
1159	2021-03-24 06:06:52.187189-05	36.7000007629394531	11
1160	2021-03-24 06:06:55.184289-05	25.1000003814697266	10
1161	2021-03-24 06:06:56.165452-05	36.7000007629394531	11
1162	2021-03-24 06:06:59.188617-05	25.1000003814697266	10
1163	2021-03-24 06:07:00.157116-05	36.7000007629394531	11
1164	2021-03-24 06:07:03.197273-05	25.1000003814697266	10
1165	2021-03-24 06:07:04.174717-05	36.7000007629394531	11
1166	2021-03-24 06:07:07.203245-05	25.1000003814697266	10
1167	2021-03-24 06:07:08.197507-05	36.7000007629394531	11
1168	2021-03-24 06:07:11.206094-05	25.1000003814697266	10
1169	2021-03-24 06:07:12.206088-05	36.7000007629394531	11
1170	2021-03-24 06:07:15.201583-05	25	10
1171	2021-03-24 06:07:16.20091-05	36.7000007629394531	11
1172	2021-03-24 06:07:19.204705-05	25.1000003814697266	10
1173	2021-03-24 06:07:20.20615-05	36.7000007629394531	11
1174	2021-03-24 06:07:23.215611-05	25.1000003814697266	10
1175	2021-03-24 06:07:24.211383-05	36.7000007629394531	11
1176	2021-03-24 06:07:27.213528-05	25.1000003814697266	10
1177	2021-03-24 06:07:28.215982-05	36.7000007629394531	11
1178	2021-03-24 06:07:31.228324-05	25.1000003814697266	10
1179	2021-03-24 06:07:32.219909-05	36.7000007629394531	11
1180	2021-03-24 06:07:35.229106-05	25.1000003814697266	10
1181	2021-03-24 06:07:36.226388-05	36.7000007629394531	11
1182	2021-03-24 06:07:39.207889-05	25.1000003814697266	10
1183	2021-03-24 06:07:40.229959-05	36.7000007629394531	11
1184	2021-03-24 06:07:43.212391-05	25.1000003814697266	10
1185	2021-03-24 06:07:44.230522-05	36.7000007629394531	11
1186	2021-03-24 06:07:47.229521-05	25.1000003814697266	10
1187	2021-03-24 06:07:48.241333-05	36.7000007629394531	11
1188	2021-03-24 06:07:51.242816-05	25.1000003814697266	10
1189	2021-03-24 06:07:52.248626-05	36.7000007629394531	11
1190	2021-03-24 06:07:55.261661-05	25.1000003814697266	10
1191	2021-03-24 06:07:56.250907-05	36.7000007629394531	11
1192	2021-03-24 06:07:59.257274-05	25.1000003814697266	10
1193	2021-03-24 06:08:00.250038-05	36.7000007629394531	11
1194	2021-03-24 06:08:03.253021-05	25.1000003814697266	10
1195	2021-03-24 06:08:04.261035-05	36.7000007629394531	11
1196	2021-03-24 06:08:07.26416-05	25.1000003814697266	10
1197	2021-03-24 06:08:08.27633-05	36.7000007629394531	11
1198	2021-03-24 06:08:11.268836-05	25.1000003814697266	10
1199	2021-03-24 06:08:12.265059-05	36.7000007629394531	11
1200	2021-03-24 06:08:15.273101-05	25.1000003814697266	10
1201	2021-03-24 06:08:16.273509-05	36.7000007629394531	11
1202	2021-03-24 06:08:19.273619-05	25.1000003814697266	10
1203	2021-03-24 06:08:20.274485-05	36.7000007629394531	11
1204	2021-03-24 06:08:23.28956-05	25.1000003814697266	10
1205	2021-03-24 06:08:24.281364-05	36.7000007629394531	11
1206	2021-03-24 06:08:27.285288-05	25.1000003814697266	10
1207	2021-03-24 06:08:28.28952-05	36.6000022888183594	11
1208	2021-03-24 06:08:31.294277-05	25.1000003814697266	10
1209	2021-03-24 06:08:32.282888-05	36.6000022888183594	11
1210	2021-03-24 06:08:35.299037-05	25.1000003814697266	10
1211	2021-03-24 06:08:36.301056-05	36.6000022888183594	11
1212	2021-03-24 06:08:39.296533-05	25.1000003814697266	10
1213	2021-03-24 06:08:40.301889-05	36.6000022888183594	11
1214	2021-03-24 06:08:43.306181-05	25.1000003814697266	10
1215	2021-03-24 06:08:44.312344-05	36.6000022888183594	11
1216	2021-03-24 06:08:47.30854-05	25.1000003814697266	10
1217	2021-03-24 06:08:48.303822-05	36.7000007629394531	11
1218	2021-03-24 06:08:51.314302-05	25.1000003814697266	10
1219	2021-03-24 06:08:52.313715-05	36.6000022888183594	11
1220	2021-03-24 06:08:55.310732-05	25.1000003814697266	10
1221	2021-03-24 06:08:56.319279-05	36.6000022888183594	11
1222	2021-03-24 06:08:59.318043-05	25.1000003814697266	10
1223	2021-03-24 06:09:00.323918-05	36.6000022888183594	11
1224	2021-03-24 06:09:03.312725-05	25.1000003814697266	10
1225	2021-03-24 06:09:04.32058-05	36.7000007629394531	11
1226	2021-03-24 06:09:07.336837-05	25.1000003814697266	10
1227	2021-03-24 06:09:08.330552-05	36.6000022888183594	11
1228	2021-03-24 06:09:11.331076-05	25.1000003814697266	10
1229	2021-03-24 06:09:12.341694-05	36.7000007629394531	11
1230	2021-03-24 06:09:15.331099-05	25.1000003814697266	10
1231	2021-03-24 06:09:16.336968-05	36.6000022888183594	11
1232	2021-03-24 06:09:19.345426-05	25.1000003814697266	10
1233	2021-03-24 06:09:20.348272-05	36.7000007629394531	11
1234	2021-03-24 06:09:23.344745-05	25.1000003814697266	10
1235	2021-03-24 06:09:24.342944-05	36.7000007629394531	11
1236	2021-03-24 06:09:27.349301-05	25	10
1237	2021-03-24 06:09:28.354905-05	36.6000022888183594	11
1238	2021-03-24 06:09:31.358605-05	25	10
1239	2021-03-24 06:09:32.350183-05	36.6000022888183594	11
1240	2021-03-24 06:09:35.371747-05	25.1000003814697266	10
1241	2021-03-24 06:09:36.341498-05	36.7000007629394531	11
1242	2021-03-24 06:09:39.37183-05	25.1000003814697266	10
1243	2021-03-24 06:09:40.339686-05	36.7000007629394531	11
1244	2021-03-24 06:09:43.37178-05	25.1000003814697266	10
1245	2021-03-24 06:09:44.369641-05	36.7000007629394531	11
1246	2021-03-24 06:09:47.372785-05	25	10
1247	2021-03-24 06:09:48.384517-05	36.6000022888183594	11
1248	2021-03-24 06:09:51.387132-05	25.1000003814697266	10
1249	2021-03-24 06:09:52.374896-05	36.7000007629394531	11
1250	2021-03-24 06:09:55.380319-05	25.1000003814697266	10
1251	2021-03-24 06:09:56.393605-05	36.7000007629394531	11
1252	2021-03-24 06:09:59.387254-05	25.1000003814697266	10
1253	2021-03-24 06:10:00.386406-05	36.7000007629394531	11
1254	2021-03-24 06:10:03.394957-05	25.1000003814697266	10
1255	2021-03-24 06:10:04.393421-05	36.7000007629394531	11
1256	2021-03-24 06:10:07.405062-05	25.1000003814697266	10
1257	2021-03-24 06:10:08.39193-05	36.7000007629394531	11
1258	2021-03-24 06:10:11.385914-05	25.1000003814697266	10
1259	2021-03-24 06:10:12.398773-05	36.7000007629394531	11
1260	2021-03-24 06:10:15.381217-05	25.1000003814697266	10
1261	2021-03-24 06:10:16.400183-05	36.7000007629394531	11
1262	2021-03-24 06:10:19.402235-05	25.1000003814697266	10
1263	2021-03-24 06:10:20.403122-05	36.7000007629394531	11
1264	2021-03-24 06:10:23.418086-05	25.1000003814697266	10
1265	2021-03-24 06:10:24.412916-05	36.7000007629394531	11
1266	2021-03-24 06:10:27.416563-05	25.1000003814697266	10
1267	2021-03-24 06:10:28.413131-05	36.6000022888183594	11
1268	2021-03-24 06:10:31.425695-05	25.1000003814697266	10
1269	2021-03-24 06:10:32.426491-05	36.6000022888183594	11
1270	2021-03-24 06:10:35.433508-05	25.1000003814697266	10
1271	2021-03-24 06:10:36.431118-05	36.6000022888183594	11
1272	2021-03-24 06:10:39.440266-05	25.1000003814697266	10
1273	2021-03-24 06:10:40.441057-05	36.7000007629394531	11
1274	2021-03-24 06:10:43.451109-05	25.1000003814697266	10
1275	2021-03-24 06:10:44.431816-05	36.6000022888183594	11
1276	2021-03-24 06:10:47.450146-05	25.1000003814697266	10
1277	2021-03-24 06:10:48.446289-05	36.6000022888183594	11
1278	2021-03-24 06:10:51.440022-05	25.1000003814697266	10
1279	2021-03-24 06:10:52.447364-05	36.6000022888183594	11
1280	2021-03-24 06:10:55.469553-05	25.1000003814697266	10
1281	2021-03-24 06:10:56.463393-05	36.6000022888183594	11
1282	2021-03-24 06:10:59.463031-05	25.1000003814697266	10
1283	2021-03-24 06:11:00.456855-05	36.6000022888183594	11
1284	2021-03-24 06:11:03.459082-05	25.1000003814697266	10
1285	2021-03-24 06:11:04.448248-05	36.6000022888183594	11
1286	2021-03-24 06:11:07.470713-05	25.1000003814697266	10
1287	2021-03-24 06:11:08.480072-05	36.6000022888183594	11
1288	2021-03-24 06:11:11.463106-05	25.1000003814697266	10
1289	2021-03-24 06:11:12.463353-05	36.6000022888183594	11
1290	2021-03-24 06:11:15.469017-05	25.1000003814697266	10
1291	2021-03-24 06:11:16.466902-05	36.6000022888183594	11
1292	2021-03-24 06:11:19.480601-05	25.1000003814697266	10
1293	2021-03-24 06:11:20.47932-05	36.6000022888183594	11
1294	2021-03-24 06:11:23.501136-05	25	10
1295	2021-03-24 06:11:24.48433-05	36.6000022888183594	11
1296	2021-03-24 06:11:27.48286-05	25.1000003814697266	10
1297	2021-03-24 06:11:28.487536-05	36.6000022888183594	11
1298	2021-03-24 06:11:31.49569-05	25.1000003814697266	10
1299	2021-03-24 06:11:32.495117-05	36.6000022888183594	11
1300	2021-03-24 06:11:35.499398-05	25.1000003814697266	10
1301	2021-03-24 06:11:36.497961-05	36.6000022888183594	11
1302	2021-03-24 06:11:39.500657-05	25.1000003814697266	10
1303	2021-03-24 06:11:40.507463-05	36.6000022888183594	11
1304	2021-03-24 06:11:43.507162-05	25.1000003814697266	10
1305	2021-03-24 06:11:44.511512-05	36.6000022888183594	11
1306	2021-03-24 06:11:47.51523-05	25.1000003814697266	10
1307	2021-03-24 06:11:48.518114-05	36.6000022888183594	11
1308	2021-03-24 06:11:51.516628-05	25.1000003814697266	10
1309	2021-03-24 06:11:52.517592-05	36.6000022888183594	11
1310	2021-03-24 06:11:55.51235-05	25.1000003814697266	10
1311	2021-03-24 06:11:56.527561-05	36.6000022888183594	11
1312	2021-03-24 06:11:59.532665-05	25.1000003814697266	10
1313	2021-03-24 06:12:00.521525-05	36.6000022888183594	11
1314	2021-03-24 06:12:03.525788-05	25.1000003814697266	10
1315	2021-03-24 06:12:04.507749-05	36.6000022888183594	11
1316	2021-03-24 06:12:07.536054-05	25	10
1317	2021-03-24 06:12:08.516102-05	36.6000022888183594	11
1318	2021-03-24 06:12:11.537531-05	25.1000003814697266	10
1319	2021-03-24 06:12:12.529853-05	36.6000022888183594	11
1320	2021-03-24 06:12:15.550383-05	25.1000003814697266	10
1321	2021-03-24 06:12:16.532376-05	36.6000022888183594	11
1322	2021-03-24 06:12:19.55128-05	25	10
1323	2021-03-24 06:12:20.551253-05	36.5	11
1324	2021-03-24 06:12:23.564609-05	25.1000003814697266	10
1325	2021-03-24 06:12:24.558406-05	36.6000022888183594	11
1326	2021-03-24 06:12:27.566646-05	25	10
1327	2021-03-24 06:12:28.553164-05	36.6000022888183594	11
1328	2021-03-24 06:12:31.571672-05	25.1000003814697266	10
1329	2021-03-24 06:12:32.567585-05	36.6000022888183594	11
1330	2021-03-24 06:12:35.56179-05	25.1000003814697266	10
1331	2021-03-24 06:12:36.569839-05	36.6000022888183594	11
1332	2021-03-24 06:12:39.561897-05	25.1000003814697266	10
1333	2021-03-24 06:12:40.572606-05	36.6000022888183594	11
1334	2021-03-24 06:12:43.549825-05	25.1000003814697266	10
1335	2021-03-24 06:12:44.576822-05	36.6000022888183594	11
1336	2021-03-24 06:12:47.585878-05	25.1000003814697266	10
1337	2021-03-24 06:12:48.582052-05	36.6000022888183594	11
1338	2021-03-24 06:12:51.589079-05	25.1000003814697266	10
1339	2021-03-24 06:12:52.58751-05	36.6000022888183594	11
1340	2021-03-24 06:12:55.589769-05	25.1000003814697266	10
1341	2021-03-24 06:12:56.58923-05	36.6000022888183594	11
1342	2021-03-24 06:12:59.591132-05	25.1000003814697266	10
1343	2021-03-24 06:13:00.594713-05	36.6000022888183594	11
1344	2021-03-24 06:13:03.593201-05	25.1000003814697266	10
1345	2021-03-24 06:13:04.596147-05	36.6000022888183594	11
1346	2021-03-24 06:13:07.595801-05	25.1000003814697266	10
1347	2021-03-24 06:13:08.601348-05	36.6000022888183594	11
1348	2021-03-24 06:13:11.619954-05	25.1000003814697266	10
1349	2021-03-24 06:13:12.607402-05	36.6000022888183594	11
1350	2021-03-24 06:13:15.612232-05	25.1000003814697266	10
1351	2021-03-24 06:13:16.611809-05	36.6000022888183594	11
1352	2021-03-24 06:13:19.617342-05	25.1000003814697266	10
1353	2021-03-24 06:13:20.621184-05	36.6000022888183594	11
1354	2021-03-24 06:13:23.615845-05	25.1000003814697266	10
1355	2021-03-24 06:13:24.616473-05	36.6000022888183594	11
1356	2021-03-24 06:13:27.617829-05	25.1000003814697266	10
1357	2021-03-24 06:13:28.620687-05	36.6000022888183594	11
1358	2021-03-24 06:13:31.630898-05	25.1000003814697266	10
1359	2021-03-24 06:13:32.631676-05	36.6000022888183594	11
1360	2021-03-24 06:13:35.637001-05	25.1000003814697266	10
1361	2021-03-24 06:13:36.636703-05	36.6000022888183594	11
1362	2021-03-24 06:13:39.641945-05	25.1000003814697266	10
1363	2021-03-24 06:13:40.639846-05	36.6000022888183594	11
1364	2021-03-24 06:13:43.650798-05	25.1000003814697266	10
1365	2021-03-24 06:13:44.645421-05	36.6000022888183594	11
1366	2021-03-24 06:13:47.638779-05	25.1000003814697266	10
1367	2021-03-24 06:13:48.655005-05	36.6000022888183594	11
1368	2021-03-24 06:13:51.659942-05	25.1000003814697266	10
1369	2021-03-24 06:13:52.650877-05	36.6000022888183594	11
1370	2021-03-24 06:13:55.651825-05	25.1000003814697266	10
1371	2021-03-24 06:13:56.664156-05	36.6000022888183594	11
1372	2021-03-24 06:13:59.667235-05	25.1000003814697266	10
1373	2021-03-24 06:14:00.668133-05	36.6000022888183594	11
1374	2021-03-24 06:14:03.666386-05	25.1000003814697266	10
1375	2021-03-24 06:14:04.670191-05	36.6000022888183594	11
1376	2021-03-24 06:14:07.664369-05	25.1000003814697266	10
1377	2021-03-24 06:14:08.668615-05	36.5	11
1378	2021-03-24 06:14:11.677074-05	25.1000003814697266	10
1379	2021-03-24 06:14:12.675031-05	36.5	11
1380	2021-03-24 06:14:15.680255-05	25.1000003814697266	10
1381	2021-03-24 06:14:16.679458-05	36.5	11
1382	2021-03-24 06:14:19.681796-05	25.1000003814697266	10
1383	2021-03-24 06:14:20.68928-05	36.5	11
1384	2021-03-24 06:14:23.684362-05	25.1000003814697266	10
1385	2021-03-24 06:14:24.691549-05	36.5	11
1386	2021-03-24 06:14:27.700912-05	25.1000003814697266	10
1387	2021-03-24 06:14:28.672707-05	36.4000015258789062	11
1388	2021-03-24 06:14:31.699473-05	25.1000003814697266	10
1389	2021-03-24 06:14:32.682847-05	36.4000015258789062	11
1390	2021-03-24 06:14:35.707366-05	25.1000003814697266	10
1391	2021-03-24 06:14:36.701038-05	36.5	11
1392	2021-03-24 06:14:39.707016-05	25.1000003814697266	10
1393	2021-03-24 06:14:40.71433-05	36.5	11
1394	2021-03-24 06:14:43.709023-05	25.1000003814697266	10
1395	2021-03-24 06:14:44.707908-05	36.5	11
1396	2021-03-24 06:14:47.717521-05	25.1000003814697266	10
1397	2021-03-24 06:14:48.719891-05	36.5	11
1398	2021-03-24 06:14:51.720649-05	25.1000003814697266	10
1399	2021-03-24 06:14:52.713551-05	36.5	11
1400	2021-03-24 06:14:55.724966-05	25.1000003814697266	10
1401	2021-03-24 06:14:56.727717-05	36.5	11
1402	2021-03-24 06:14:59.720478-05	25.1000003814697266	10
1403	2021-03-24 06:15:00.728785-05	36.5	11
1404	2021-03-24 06:15:03.702507-05	25.1000003814697266	10
1405	2021-03-24 06:15:04.729361-05	36.5	11
1406	2021-03-24 06:15:07.73528-05	25.1000003814697266	10
1407	2021-03-24 06:15:08.735931-05	36.5	11
1408	2021-03-24 06:15:11.734692-05	25.1000003814697266	10
1409	2021-03-24 06:15:12.732251-05	36.5	11
1410	2021-03-24 06:15:15.744482-05	25.1000003814697266	10
1411	2021-03-24 06:15:21.760805-05	36.5	11
1412	2021-03-24 06:15:22.009614-05	25.1000003814697266	10
1413	2021-03-24 06:15:22.280297-05	36.5	11
1414	2021-03-24 06:15:23.756619-05	25.1000003814697266	10
1415	2021-03-24 06:15:24.76673-05	36.5	11
1416	2021-03-24 06:15:27.765217-05	25.1000003814697266	10
1417	2021-03-24 06:15:28.765156-05	36.5	11
1418	2021-03-24 06:15:31.774333-05	25.1000003814697266	10
1419	2021-03-24 06:15:32.775416-05	36.5	11
1420	2021-03-24 06:15:35.772016-05	25.1000003814697266	10
1421	2021-03-24 06:15:36.773472-05	36.5	11
1422	2021-03-24 06:15:39.770336-05	25.1000003814697266	10
1423	2021-03-24 06:15:40.777054-05	36.5	11
1424	2021-03-24 06:15:43.78673-05	25.1000003814697266	10
1425	2021-03-24 06:15:44.790351-05	36.5	11
1426	2021-03-24 06:15:47.78914-05	25.1000003814697266	10
1427	2021-03-24 06:15:48.786656-05	36.5	11
1428	2021-03-24 06:15:51.798982-05	25.1000003814697266	10
1429	2021-03-24 06:15:52.796397-05	36.5	11
1430	2021-03-24 06:15:55.7934-05	25.1000003814697266	10
1431	2021-03-24 06:15:56.801262-05	36.5	11
1432	2021-03-24 06:15:59.806167-05	25.1000003814697266	10
1433	2021-03-24 06:16:00.801327-05	36.5	11
1434	2021-03-24 06:16:03.806612-05	25.1000003814697266	10
1435	2021-03-24 06:16:04.812511-05	36.5	11
1436	2021-03-24 06:16:07.812347-05	25.1000003814697266	10
1437	2021-03-24 06:16:08.804726-05	36.5	11
1438	2021-03-24 06:16:11.82321-05	25.1000003814697266	10
1439	2021-03-24 06:16:12.805385-05	36.5	11
1440	2021-03-24 06:16:15.828669-05	25.1000003814697266	10
1441	2021-03-24 06:16:16.8181-05	36.5	11
1442	2021-03-24 06:16:19.819995-05	25.1000003814697266	10
1443	2021-03-24 06:16:20.817337-05	36.5	11
1444	2021-03-24 06:16:23.823619-05	25.1000003814697266	10
1445	2021-03-24 06:16:24.821287-05	36.5	11
1446	2021-03-24 06:16:27.825507-05	25.1000003814697266	10
1447	2021-03-24 06:16:28.845586-05	36.5	11
1448	2021-03-24 06:16:31.832065-05	25.1000003814697266	10
1449	2021-03-24 06:16:32.828758-05	36.5	11
1450	2021-03-24 06:16:35.832657-05	25.1000003814697266	10
1451	2021-03-24 06:16:36.835531-05	36.5	11
1452	2021-03-24 06:16:39.837785-05	25.1000003814697266	10
1453	2021-03-24 06:16:40.832053-05	36.5	11
1454	2021-03-24 06:16:43.854412-05	25.1000003814697266	10
1455	2021-03-24 06:16:44.819172-05	36.5	11
1456	2021-03-24 06:16:47.863503-05	25.1000003814697266	10
1457	2021-03-24 06:16:48.850359-05	36.5	11
1458	2021-03-24 06:16:51.855185-05	25.1000003814697266	10
1459	2021-03-24 06:16:52.851544-05	36.5	11
1460	2021-03-24 06:16:55.86952-05	25.1000003814697266	10
1461	2021-03-24 06:16:56.858733-05	36.5	11
1462	2021-03-24 06:16:59.867082-05	25.1000003814697266	10
1463	2021-03-24 06:17:00.866113-05	36.5	11
1464	2021-03-24 06:17:03.872705-05	25.1000003814697266	10
1465	2021-03-24 06:17:04.864987-05	36.5	11
1466	2021-03-24 06:17:07.877844-05	25.1000003814697266	10
1467	2021-03-24 06:17:08.881866-05	36.5	11
1468	2021-03-24 06:17:11.882388-05	25.1000003814697266	10
1469	2021-03-24 06:17:12.881361-05	36.5	11
1470	2021-03-24 06:17:15.854346-05	25.1000003814697266	10
1471	2021-03-24 06:17:16.875676-05	36.5	11
1472	2021-03-24 06:17:19.878403-05	25.1000003814697266	10
1473	2021-03-24 06:17:20.88195-05	36.4000015258789062	11
1474	2021-03-24 06:17:23.883156-05	25.1000003814697266	10
1475	2021-03-24 06:17:24.894707-05	36.4000015258789062	11
1476	2021-03-24 06:17:27.89252-05	25.1000003814697266	10
1477	2021-03-24 06:17:28.895496-05	36.4000015258789062	11
1478	2021-03-24 06:17:31.901247-05	25.1000003814697266	10
1479	2021-03-24 06:17:32.903848-05	36.4000015258789062	11
1480	2021-03-24 06:17:35.904873-05	25.1000003814697266	10
1481	2021-03-24 06:17:36.90684-05	36.4000015258789062	11
1482	2021-03-24 06:17:39.906894-05	25.1000003814697266	10
1483	2021-03-24 06:17:40.906826-05	36.4000015258789062	11
1484	2021-03-24 06:17:43.914827-05	25.1000003814697266	10
1485	2021-03-24 06:17:44.911705-05	36.4000015258789062	11
1486	2021-03-24 06:17:47.91126-05	25.1000003814697266	10
1487	2021-03-24 06:17:48.915198-05	36.4000015258789062	11
1488	2021-03-24 06:17:51.928017-05	25.1000003814697266	10
1489	2021-03-24 06:17:52.924597-05	36.4000015258789062	11
1490	2021-03-24 06:17:55.926802-05	25.1000003814697266	10
1491	2021-03-24 06:17:56.93301-05	36.4000015258789062	11
1492	2021-03-24 06:17:59.934572-05	25.1000003814697266	10
1493	2021-03-24 06:18:00.932106-05	36.4000015258789062	11
1494	2021-03-24 06:18:03.943386-05	25.1000003814697266	10
1495	2021-03-24 06:18:04.94686-05	36.5	11
1496	2021-03-24 06:18:07.942679-05	25.1000003814697266	10
1497	2021-03-24 06:18:08.950805-05	36.5	11
1498	2021-03-24 06:18:11.943456-05	25.1000003814697266	10
1499	2021-03-24 06:18:12.948868-05	36.4000015258789062	11
1500	2021-03-24 06:18:15.9469-05	25.1000003814697266	10
1501	2021-03-24 06:18:16.948579-05	36.5	11
1502	2021-03-24 06:18:19.952246-05	25.1000003814697266	10
1503	2021-03-24 06:18:20.949107-05	36.4000015258789062	11
1504	2021-03-24 06:18:23.966635-05	25.1000003814697266	10
1505	2021-03-24 06:18:24.955751-05	36.5	11
1506	2021-03-24 06:18:27.961611-05	25.1000003814697266	10
1507	2021-03-24 06:18:28.96497-05	36.4000015258789062	11
1508	2021-03-24 06:18:31.962333-05	25.1000003814697266	10
1509	2021-03-24 06:18:32.968821-05	36.5	11
1510	2021-03-24 06:18:35.953486-05	25.1000003814697266	10
1511	2021-03-24 06:18:36.974154-05	36.4000015258789062	11
1512	2021-03-24 06:18:39.980381-05	25.1000003814697266	10
1513	2021-03-24 06:18:40.975443-05	36.4000015258789062	11
1514	2021-03-24 06:18:43.982358-05	25.1000003814697266	10
1515	2021-03-24 06:18:44.975242-05	36.4000015258789062	11
1516	2021-03-24 06:18:47.98954-05	25.1000003814697266	10
1517	2021-03-24 06:18:48.991642-05	36.4000015258789062	11
1518	2021-03-24 06:18:51.987589-05	25.1000003814697266	10
1519	2021-03-24 06:18:52.968165-05	36.4000015258789062	11
1520	2021-03-24 06:18:55.984164-05	25.1000003814697266	10
1521	2021-03-24 06:18:56.967276-05	36.4000015258789062	11
1522	2021-03-24 06:19:00.002929-05	25.1000003814697266	10
1523	2021-03-24 06:19:00.994514-05	36.4000015258789062	11
1524	2021-03-24 06:19:04.005936-05	25.1000003814697266	10
1525	2021-03-24 06:19:05.001345-05	36.4000015258789062	11
1526	2021-03-24 06:19:08.003342-05	25.1000003814697266	10
1527	2021-03-24 06:19:09.006994-05	36.4000015258789062	11
1528	2021-03-24 06:19:12.016978-05	25.1000003814697266	10
1529	2021-03-24 06:19:13.004344-05	36.4000015258789062	11
1530	2021-03-24 06:19:16.021796-05	25.1000003814697266	10
1531	2021-03-24 06:19:17.014082-05	36.4000015258789062	11
1532	2021-03-24 06:19:20.025225-05	25.1000003814697266	10
1533	2021-03-24 06:19:21.023064-05	36.4000015258789062	11
1534	2021-03-24 06:19:24.021412-05	25.1000003814697266	10
1535	2021-03-24 06:19:25.035171-05	36.4000015258789062	11
1536	2021-03-24 06:19:28.01756-05	25.1000003814697266	10
1537	2021-03-24 06:19:29.042465-05	36.4000015258789062	11
1538	2021-03-24 06:19:32.039681-05	25.1000003814697266	10
1539	2021-03-24 06:19:33.042819-05	36.4000015258789062	11
1540	2021-03-24 06:19:36.044832-05	25.1000003814697266	10
1541	2021-03-24 06:19:37.043901-05	36.4000015258789062	11
1542	2021-03-24 06:19:40.047304-05	25.1000003814697266	10
1543	2021-03-24 06:19:41.048933-05	36.4000015258789062	11
1544	2021-03-24 06:19:44.056498-05	25.1000003814697266	10
1545	2021-03-24 06:19:45.056347-05	36.4000015258789062	11
1546	2021-03-24 06:19:48.057321-05	25.1000003814697266	10
1547	2021-03-24 06:19:49.055458-05	36.4000015258789062	11
1548	2021-03-24 06:19:52.055458-05	25.1000003814697266	10
1549	2021-03-24 06:19:53.067956-05	36.4000015258789062	11
1550	2021-03-24 06:19:56.068081-05	25.1000003814697266	10
1551	2021-03-24 06:19:57.064555-05	36.4000015258789062	11
1552	2021-03-24 06:20:00.065809-05	25.1000003814697266	10
1553	2021-03-24 06:20:01.075987-05	36.4000015258789062	11
1554	2021-03-24 06:20:04.079227-05	25	10
1555	2021-03-24 06:20:05.074859-05	36.4000015258789062	11
1556	2021-03-24 06:20:08.072216-05	25.1000003814697266	10
1557	2021-03-24 06:20:09.089615-05	36.4000015258789062	11
1558	2021-03-24 06:20:12.083872-05	25.1000003814697266	10
1559	2021-03-24 06:20:13.080185-05	36.4000015258789062	11
1560	2021-03-24 06:20:16.085007-05	25.1000003814697266	10
1561	2021-03-24 06:20:17.092116-05	36.5	11
1562	2021-03-24 06:20:20.096553-05	25.1000003814697266	10
1563	2021-03-24 06:20:21.082242-05	36.5	11
1564	2021-03-24 06:20:24.097368-05	25.1000003814697266	10
1565	2021-03-24 06:20:25.097545-05	36.5	11
1566	2021-03-24 06:20:28.104789-05	25.1000003814697266	10
1567	2021-03-24 06:20:29.101398-05	36.5	11
1568	2021-03-24 06:20:32.112372-05	25.1000003814697266	10
1569	2021-03-24 06:20:33.109909-05	36.5	11
1570	2021-03-24 06:20:36.111074-05	25.1000003814697266	10
1571	2021-03-24 06:20:37.113432-05	36.4000015258789062	11
1572	2021-03-24 06:20:40.114615-05	25.1000003814697266	10
1573	2021-03-24 06:20:41.120019-05	36.5	11
1574	2021-03-24 06:20:44.121158-05	25.1000003814697266	10
1575	2021-03-24 06:20:45.123626-05	36.5	11
1576	2021-03-24 06:20:48.128777-05	25.1000003814697266	10
1577	2021-03-24 06:20:49.132846-05	36.5	11
1578	2021-03-24 06:20:52.123801-05	25.1000003814697266	10
1579	2021-03-24 06:20:53.137559-05	36.5	11
1580	2021-03-24 06:20:56.140917-05	25.1000003814697266	10
1581	2021-03-24 06:20:57.133891-05	36.4000015258789062	11
1582	2021-03-24 06:21:00.138172-05	25.1000003814697266	10
1583	2021-03-24 06:21:01.113508-05	36.4000015258789062	11
1584	2021-03-24 06:21:04.149191-05	25.1000003814697266	10
1585	2021-03-24 06:21:05.139492-05	36.5	11
1586	2021-03-24 06:21:08.147483-05	25.1000003814697266	10
1587	2021-03-24 06:21:09.14223-05	36.5	11
1588	2021-03-24 06:21:12.152037-05	25.1000003814697266	10
1589	2021-03-24 06:21:13.156956-05	36.5	11
1590	2021-03-24 06:21:16.157606-05	25.1000003814697266	10
1591	2021-03-24 06:21:17.15594-05	36.5	11
1592	2021-03-24 06:21:20.158495-05	25.1000003814697266	10
1593	2021-03-24 06:21:21.154772-05	36.4000015258789062	11
1594	2021-03-24 06:21:24.161002-05	25.1000003814697266	10
1595	2021-03-24 06:21:25.172852-05	36.4000015258789062	11
1596	2021-03-24 06:21:28.167556-05	25.1000003814697266	10
1597	2021-03-24 06:21:29.173725-05	36.4000015258789062	11
1598	2021-03-24 06:21:32.170095-05	25.1000003814697266	10
1599	2021-03-24 06:21:33.19306-05	36.4000015258789062	11
1600	2021-03-24 06:21:36.153358-05	25	10
1601	2021-03-24 06:21:37.182363-05	36.4000015258789062	11
1602	2021-03-24 06:21:40.187-05	25.1000003814697266	10
1603	2021-03-24 06:21:41.187664-05	36.4000015258789062	11
1604	2021-03-24 06:21:44.192681-05	25	10
1605	2021-03-24 06:21:45.195048-05	36.4000015258789062	11
1606	2021-03-24 06:21:48.195964-05	25.1000003814697266	10
1607	2021-03-24 06:21:49.192127-05	36.4000015258789062	11
1608	2021-03-24 06:21:52.195223-05	25.1000003814697266	10
1609	2021-03-24 06:21:53.20405-05	36.5	11
1610	2021-03-24 06:21:56.202351-05	25.1000003814697266	10
1611	2021-03-24 06:21:57.207993-05	36.4000015258789062	11
1612	2021-03-24 06:22:00.203311-05	25.1000003814697266	10
1613	2021-03-24 06:22:01.209004-05	36.4000015258789062	11
1614	2021-03-24 06:22:04.211924-05	25.1000003814697266	10
1615	2021-03-24 06:22:05.216539-05	36.4000015258789062	11
1616	2021-03-24 06:22:08.217972-05	25.1000003814697266	10
1617	2021-03-24 06:22:09.212387-05	36.4000015258789062	11
1618	2021-03-24 06:22:12.219019-05	25.1000003814697266	10
1619	2021-03-24 06:22:13.220336-05	36.4000015258789062	11
1620	2021-03-24 06:22:16.218792-05	25.1000003814697266	10
1621	2021-03-24 06:22:17.224209-05	36.4000015258789062	11
1622	2021-03-24 06:22:20.231154-05	25.1000003814697266	10
1623	2021-03-24 06:22:21.223553-05	36.4000015258789062	11
1624	2021-03-24 06:22:24.228507-05	25.1000003814697266	10
1625	2021-03-24 06:22:25.225166-05	36.4000015258789062	11
1626	2021-03-24 06:22:28.239117-05	25.1000003814697266	10
1627	2021-03-24 06:22:29.236378-05	36.4000015258789062	11
1628	2021-03-24 06:22:32.236329-05	25.1000003814697266	10
1629	2021-03-24 06:22:33.240896-05	36.4000015258789062	11
1630	2021-03-24 06:22:36.241911-05	25.1000003814697266	10
1631	2021-03-24 06:22:37.250817-05	36.4000015258789062	11
1632	2021-03-24 06:22:40.237531-05	25.1000003814697266	10
1633	2021-03-24 06:22:41.247255-05	36.4000015258789062	11
1634	2021-03-24 06:22:44.255785-05	25.1000003814697266	10
1635	2021-03-24 06:22:45.262071-05	36.4000015258789062	11
1636	2021-03-24 06:22:48.267626-05	25.1000003814697266	10
1637	2021-03-24 06:22:49.263326-05	36.4000015258789062	11
1638	2021-03-24 06:22:52.260503-05	25.1000003814697266	10
1639	2021-03-24 06:22:53.267446-05	36.4000015258789062	11
1640	2021-03-24 06:22:56.277507-05	25.1000003814697266	10
1641	2021-03-24 06:22:57.27256-05	36.4000015258789062	11
1642	2021-03-24 06:23:00.27559-05	25	10
1643	2021-03-24 06:23:01.27434-05	36.2999992370605469	11
1644	2021-03-24 06:23:04.278934-05	25.1000003814697266	10
1645	2021-03-24 06:23:05.265531-05	36.4000015258789062	11
1646	2021-03-24 06:23:08.282455-05	25	10
1647	2021-03-24 06:23:09.253255-05	36.2999992370605469	11
1648	2021-03-24 06:23:12.281967-05	25.1000003814697266	10
1649	2021-03-24 06:23:13.27817-05	36.4000015258789062	11
1650	2021-03-24 06:23:16.287131-05	25.1000003814697266	10
1651	2021-03-24 06:23:17.295422-05	36.4000015258789062	11
1652	2021-03-24 06:23:20.298587-05	25.1000003814697266	10
1653	2021-03-24 06:23:21.301339-05	36.4000015258789062	11
1654	2021-03-24 06:23:24.301359-05	25	10
1655	2021-03-24 06:23:25.302645-05	36.2999992370605469	11
1656	2021-03-24 06:23:28.30666-05	25.1000003814697266	10
1657	2021-03-24 06:23:29.298812-05	36.4000015258789062	11
1658	2021-03-24 06:23:32.302269-05	25.1000003814697266	10
1659	2021-03-24 06:23:33.303731-05	36.4000015258789062	11
1660	2021-03-24 06:23:36.295774-05	25.1000003814697266	10
1661	2021-03-24 06:23:37.310922-05	36.4000015258789062	11
1662	2021-03-24 06:23:40.288661-05	25.1000003814697266	10
1663	2021-03-24 06:23:41.313566-05	36.4000015258789062	11
1664	2021-03-24 06:23:44.320449-05	25	10
1665	2021-03-24 06:23:45.318173-05	36.2999992370605469	11
1666	2021-03-24 06:23:48.324199-05	25.1000003814697266	10
1667	2021-03-24 06:23:49.327017-05	36.4000015258789062	11
1668	2021-03-24 06:23:52.326985-05	25.1000003814697266	10
1669	2021-03-24 06:23:53.32883-05	36.4000015258789062	11
1670	2021-03-24 06:23:56.331925-05	25.1000003814697266	10
1671	2021-03-24 06:23:57.335154-05	36.4000015258789062	11
1672	2021-03-24 06:24:00.338398-05	25.1000003814697266	10
1673	2021-03-24 06:24:01.339518-05	36.4000015258789062	11
1674	2021-03-24 06:24:04.344813-05	25.1000003814697266	10
1675	2021-03-24 06:24:05.346525-05	36.4000015258789062	11
1676	2021-03-24 06:24:08.346733-05	25.1000003814697266	10
1677	2021-03-24 06:24:09.355488-05	36.4000015258789062	11
1678	2021-03-24 06:24:12.344841-05	25.1000003814697266	10
1679	2021-03-24 06:24:13.346313-05	36.4000015258789062	11
1680	2021-03-24 06:24:16.357206-05	25.1000003814697266	10
1681	2021-03-24 06:24:17.355106-05	36.4000015258789062	11
1682	2021-03-24 06:24:20.355511-05	25.1000003814697266	10
1683	2021-03-24 06:24:21.361053-05	36.4000015258789062	11
1684	2021-03-24 06:24:24.360845-05	25.1000003814697266	10
1685	2021-03-24 06:24:25.363916-05	36.4000015258789062	11
1686	2021-03-24 06:24:28.37354-05	25.1000003814697266	10
1687	2021-03-24 06:24:29.37111-05	36.4000015258789062	11
1688	2021-03-24 06:24:32.374207-05	25.1000003814697266	10
1689	2021-03-24 06:24:33.373163-05	36.4000015258789062	11
1690	2021-03-24 06:24:36.382556-05	25.1000003814697266	10
1691	2021-03-24 06:24:37.370779-05	36.4000015258789062	11
1692	2021-03-24 06:24:40.378983-05	25.1000003814697266	10
1693	2021-03-24 06:24:41.377511-05	36.4000015258789062	11
1694	2021-03-24 06:24:44.390324-05	25.1000003814697266	10
1695	2021-03-24 06:24:45.385738-05	36.4000015258789062	11
1696	2021-03-24 06:24:48.399523-05	25.1000003814697266	10
1697	2021-03-24 06:24:49.391971-05	36.4000015258789062	11
1698	2021-03-24 06:24:52.391539-05	25.1000003814697266	10
1699	2021-03-24 06:24:53.397264-05	36.4000015258789062	11
1700	2021-03-24 06:24:56.405916-05	25.1000003814697266	10
1701	2021-03-24 06:24:57.392733-05	36.4000015258789062	11
1702	2021-03-24 06:25:00.401118-05	25.1000003814697266	10
1703	2021-03-24 06:25:01.405077-05	36.4000015258789062	11
1704	2021-03-24 06:25:04.402149-05	25.1000003814697266	10
1705	2021-03-24 06:25:05.39295-05	36.4000015258789062	11
1706	2021-03-24 06:25:08.413204-05	25.1000003814697266	10
1707	2021-03-24 06:25:09.397588-05	36.4000015258789062	11
1708	2021-03-24 06:25:12.413369-05	25.1000003814697266	10
1709	2021-03-24 06:25:13.415805-05	36.4000015258789062	11
1710	2021-03-24 06:25:16.430258-05	25	10
1711	2021-03-24 06:25:17.413151-05	36.2999992370605469	11
1712	2021-03-24 06:25:20.419948-05	25.1000003814697266	10
1713	2021-03-24 06:25:21.420302-05	36.4000015258789062	11
1714	2021-03-24 06:25:24.423745-05	25.1000003814697266	10
1715	2021-03-24 06:25:25.421847-05	36.4000015258789062	11
1716	2021-03-24 06:25:28.428907-05	25.1000003814697266	10
1717	2021-03-24 06:25:29.433208-05	36.4000015258789062	11
1718	2021-03-24 06:25:32.419218-05	25.1000003814697266	10
1719	2021-03-24 06:25:33.438605-05	36.4000015258789062	11
1720	2021-03-24 06:25:36.415186-05	25	10
1721	2021-03-24 06:25:37.432798-05	36.2999992370605469	11
1722	2021-03-24 06:25:40.435928-05	25.1000003814697266	10
1723	2021-03-24 06:25:41.446668-05	36.4000015258789062	11
1724	2021-03-24 06:25:44.447857-05	25.1000003814697266	10
1725	2021-03-24 06:25:45.446139-05	36.4000015258789062	11
1726	2021-03-24 06:25:48.458842-05	25.1000003814697266	10
1727	2021-03-24 06:25:49.459796-05	36.4000015258789062	11
1728	2021-03-24 06:25:52.458801-05	25.1000003814697266	10
1729	2021-03-24 06:25:53.467201-05	36.4000015258789062	11
1730	2021-03-24 06:25:56.460306-05	25	10
1731	2021-03-24 06:25:57.464592-05	36.2999992370605469	11
1732	2021-03-24 06:26:00.457927-05	25.1000003814697266	10
1733	2021-03-24 06:26:01.470772-05	36.4000015258789062	11
1734	2021-03-24 06:26:04.46781-05	25	10
1735	2021-03-24 06:26:05.476599-05	36.4000015258789062	11
1736	2021-03-24 06:26:08.47962-05	25.1000003814697266	10
1737	2021-03-24 06:26:09.479398-05	36.4000015258789062	11
1738	2021-03-24 06:26:12.481659-05	25.1000003814697266	10
1739	2021-03-24 06:26:13.485417-05	36.4000015258789062	11
1740	2021-03-24 06:26:16.486114-05	25.1000003814697266	10
1741	2021-03-24 06:26:17.483398-05	36.4000015258789062	11
1742	2021-03-24 06:26:20.49597-05	25.1000003814697266	10
1743	2021-03-24 06:26:21.490637-05	36.4000015258789062	11
1744	2021-03-24 06:26:24.486199-05	25.1000003814697266	10
1745	2021-03-24 06:26:25.493443-05	36.4000015258789062	11
1746	2021-03-24 06:26:28.495365-05	25.1000003814697266	10
1747	2021-03-24 06:26:29.500535-05	36.4000015258789062	11
1748	2021-03-24 06:26:32.508783-05	25.1000003814697266	10
1749	2021-03-24 06:26:33.505873-05	36.4000015258789062	11
1750	2021-03-24 06:26:36.505418-05	25.1000003814697266	10
1751	2021-03-24 06:26:37.501406-05	36.4000015258789062	11
1752	2021-03-24 06:26:40.503364-05	25	10
1753	2021-03-24 06:26:41.514884-05	36.2999992370605469	11
1754	2021-03-24 06:26:44.523255-05	25.1000003814697266	10
1755	2021-03-24 06:26:45.521334-05	36.4000015258789062	11
1756	2021-03-24 06:26:48.524953-05	25	10
1757	2021-03-24 06:26:49.52763-05	36.2999992370605469	11
1758	2021-03-24 06:26:52.529131-05	25.1000003814697266	10
1759	2021-03-24 06:26:53.525241-05	36.4000015258789062	11
1760	2021-03-24 06:26:56.537154-05	25.1000003814697266	10
1761	2021-03-24 06:26:57.525892-05	36.4000015258789062	11
1762	2021-03-24 06:27:00.537098-05	25.1000003814697266	10
1763	2021-03-24 06:27:01.537855-05	36.4000015258789062	11
1764	2021-03-24 06:27:04.540215-05	25	10
1765	2021-03-24 06:27:05.516788-05	36.2999992370605469	11
1766	2021-03-24 06:27:08.547507-05	25	10
1767	2021-03-24 06:27:09.527322-05	36.4000015258789062	11
1768	2021-03-24 06:27:12.553139-05	25.1000003814697266	10
1769	2021-03-24 06:27:13.549744-05	36.4000015258789062	11
1770	2021-03-24 06:27:16.559246-05	25.1000003814697266	10
1771	2021-03-24 06:27:17.552329-05	36.4000015258789062	11
1772	2021-03-24 06:27:20.561931-05	25.1000003814697266	10
1773	2021-03-24 06:27:21.555846-05	36.4000015258789062	11
1774	2021-03-24 06:27:24.563131-05	25.1000003814697266	10
1775	2021-03-24 06:27:25.564699-05	36.4000015258789062	11
1776	2021-03-24 06:27:28.575346-05	25.1000003814697266	10
1777	2021-03-24 06:27:29.565892-05	36.4000015258789062	11
1778	2021-03-24 06:27:32.562658-05	25.1000003814697266	10
1779	2021-03-24 06:27:33.564042-05	36.4000015258789062	11
1780	2021-03-24 06:27:36.546919-05	25.1000003814697266	10
1781	2021-03-24 06:27:37.577143-05	36.4000015258789062	11
1782	2021-03-24 06:27:40.568669-05	25	10
1783	2021-03-24 06:27:41.581926-05	36.2999992370605469	11
1784	2021-03-24 06:27:44.583742-05	25	10
1785	2021-03-24 06:27:45.587554-05	36.4000015258789062	11
1786	2021-03-24 06:27:48.58422-05	25	10
1787	2021-03-24 06:27:49.59446-05	36.2999992370605469	11
1788	2021-03-24 06:27:52.577021-05	25	10
1789	2021-03-24 06:27:53.591213-05	36.4000015258789062	11
1790	2021-03-24 06:27:56.597815-05	25	10
1791	2021-03-24 06:27:57.60222-05	36.4000015258789062	11
1792	2021-03-24 06:28:00.601204-05	25	10
1793	2021-03-24 06:28:01.598771-05	36.4000015258789062	11
1794	2021-03-24 06:28:04.603957-05	25.1000003814697266	10
1795	2021-03-24 06:28:05.609291-05	36.4000015258789062	11
1796	2021-03-24 06:28:08.613185-05	25.1000003814697266	10
1797	2021-03-24 06:28:09.613914-05	36.4000015258789062	11
1798	2021-03-24 06:28:12.611142-05	25	10
1799	2021-03-24 06:28:13.620976-05	36.4000015258789062	11
1800	2021-03-24 06:28:16.61688-05	25	10
1801	2021-03-24 06:28:17.620688-05	36.4000015258789062	11
1802	2021-03-24 06:28:20.621647-05	25.1000003814697266	10
1803	2021-03-24 06:28:21.615824-05	36.4000015258789062	11
1804	2021-03-24 06:28:24.636093-05	25	10
1805	2021-03-24 06:28:25.631221-05	36.4000015258789062	11
1806	2021-03-24 06:28:28.631915-05	25	10
1807	2021-03-24 06:28:29.637035-05	36.4000015258789062	11
1808	2021-03-24 06:28:32.643392-05	25	10
1809	2021-03-24 06:28:33.636049-05	36.4000015258789062	11
1810	2021-03-24 06:28:36.639448-05	25	10
1811	2021-03-24 06:28:37.631179-05	36.2999992370605469	11
1812	2021-03-24 06:28:40.644496-05	25	10
1813	2021-03-24 06:28:41.647543-05	36.4000015258789062	11
1814	2021-03-24 06:28:44.654154-05	25	10
1815	2021-03-24 06:28:45.649535-05	36.4000015258789062	11
1816	2021-03-24 06:28:48.656355-05	25	10
1817	2021-03-24 06:28:49.657222-05	36.4000015258789062	11
1818	2021-03-24 06:28:52.667515-05	25.1000003814697266	10
1819	2021-03-24 06:28:53.651059-05	36.4000015258789062	11
1820	2021-03-24 06:28:56.667398-05	25	10
1821	2021-03-24 06:28:57.662891-05	36.2999992370605469	11
1822	2021-03-24 06:29:00.66751-05	25.1000003814697266	10
1823	2021-03-24 06:29:01.643303-05	36.4000015258789062	11
1824	2021-03-24 06:29:04.670599-05	25	10
1825	2021-03-24 06:29:05.649243-05	36.4000015258789062	11
1826	2021-03-24 06:29:08.678112-05	25.1000003814697266	10
1827	2021-03-24 06:29:09.676091-05	36.4000015258789062	11
1828	2021-03-24 06:29:12.68454-05	25	10
1829	2021-03-24 06:29:13.674856-05	36.4000015258789062	11
1830	2021-03-24 06:29:16.687722-05	25.1000003814697266	10
1831	2021-03-24 06:29:17.686987-05	36.4000015258789062	11
1832	2021-03-24 06:29:20.694033-05	25.1000003814697266	10
1833	2021-03-24 06:29:21.6816-05	36.4000015258789062	11
1834	2021-03-24 06:29:24.697664-05	25.1000003814697266	10
1835	2021-03-24 06:29:25.694331-05	36.4000015258789062	11
1836	2021-03-24 06:29:28.693593-05	25.1000003814697266	10
1837	2021-03-24 06:29:29.703419-05	36.4000015258789062	11
1838	2021-03-24 06:29:32.683848-05	25.1000003814697266	10
1839	2021-03-24 06:29:33.709409-05	36.4000015258789062	11
1840	2021-03-24 06:29:36.713208-05	25.1000003814697266	10
1841	2021-03-24 06:29:37.710904-05	36.4000015258789062	11
1842	2021-03-24 06:29:40.705951-05	25.1000003814697266	10
1843	2021-03-24 06:29:41.714978-05	36.4000015258789062	11
1844	2021-03-24 06:29:44.712769-05	25	10
1845	2021-03-24 06:29:45.719191-05	36.4000015258789062	11
1846	2021-03-24 06:29:48.713655-05	25.1000003814697266	10
1847	2021-03-24 06:29:49.72441-05	36.4000015258789062	11
1848	2021-03-24 06:29:52.725832-05	25	10
1849	2021-03-24 06:29:53.724701-05	36.4000015258789062	11
1850	2021-03-24 06:29:56.727297-05	25.1000003814697266	10
1851	2021-03-24 06:29:57.742711-05	36.4000015258789062	11
1852	2021-03-24 06:30:00.741176-05	25.1000003814697266	10
1853	2021-03-24 06:30:01.736281-05	36.4000015258789062	11
1854	2021-03-24 06:30:04.731649-05	25	10
1855	2021-03-24 06:30:05.729127-05	36.2999992370605469	11
1856	2021-03-24 06:30:08.741235-05	25.1000003814697266	10
1857	2021-03-24 06:30:09.747779-05	36.4000015258789062	11
1858	2021-03-24 06:30:12.748803-05	25	10
1859	2021-03-24 06:30:13.748494-05	36.2999992370605469	11
1860	2021-03-24 06:30:21.763575-05	25.1000003814697266	10
1861	2021-03-24 06:30:22.025909-05	36.4000015258789062	11
1862	2021-03-24 06:30:22.276869-05	25.1000003814697266	10
1863	2021-03-24 06:30:22.527139-05	36.4000015258789062	11
1864	2021-03-24 06:30:24.764236-05	25.1000003814697266	10
1865	2021-03-24 06:30:25.762174-05	36.4000015258789062	11
1866	2021-03-24 06:30:28.771232-05	25.1000003814697266	10
1867	2021-03-24 06:30:29.773335-05	36.4000015258789062	11
1868	2021-03-24 06:30:32.761867-05	25.1000003814697266	10
1869	2021-03-24 06:30:33.779681-05	36.4000015258789062	11
1870	2021-03-24 06:30:36.779656-05	25.1000003814697266	10
1871	2021-03-24 06:30:37.779198-05	36.4000015258789062	11
1872	2021-03-24 06:30:40.780969-05	25.1000003814697266	10
1873	2021-03-24 06:30:41.794587-05	36.4000015258789062	11
1874	2021-03-24 06:30:44.78871-05	25.1000003814697266	10
1875	2021-03-24 06:30:45.788287-05	36.4000015258789062	11
1876	2021-03-24 06:30:48.794355-05	25.1000003814697266	10
1877	2021-03-24 06:30:49.794487-05	36.4000015258789062	11
1878	2021-03-24 06:30:52.802507-05	25.1000003814697266	10
1879	2021-03-24 06:30:53.788193-05	36.4000015258789062	11
1880	2021-03-24 06:30:56.793592-05	25.1000003814697266	10
1881	2021-03-24 06:30:57.776254-05	36.2999992370605469	11
1882	2021-03-24 06:31:00.809723-05	25	10
1883	2021-03-24 06:31:01.793467-05	36.2999992370605469	11
1884	2021-03-24 06:31:04.815582-05	25.1000003814697266	10
1885	2021-03-24 06:31:05.808205-05	36.4000015258789062	11
1886	2021-03-24 06:31:08.819555-05	25.1000003814697266	10
1887	2021-03-24 06:31:09.807377-05	36.2999992370605469	11
1888	2021-03-24 06:31:12.815835-05	25.1000003814697266	10
1889	2021-03-24 06:31:13.818574-05	36.4000015258789062	11
1890	2021-03-24 06:31:16.819084-05	25.1000003814697266	10
1891	2021-03-24 06:31:17.822327-05	36.4000015258789062	11
1892	2021-03-24 06:31:20.821213-05	25.1000003814697266	10
1893	2021-03-24 06:31:21.830865-05	36.4000015258789062	11
1894	2021-03-24 06:31:24.796406-05	25.1000003814697266	10
1895	2021-03-24 06:31:25.83285-05	36.4000015258789062	11
1896	2021-03-24 06:31:28.839346-05	25	10
1897	2021-03-24 06:31:29.841591-05	36.2999992370605469	11
1898	2021-03-24 06:31:32.839023-05	25.1000003814697266	10
1899	2021-03-24 06:31:33.839308-05	36.4000015258789062	11
1900	2021-03-24 06:31:36.841664-05	25.1000003814697266	10
1901	2021-03-24 06:31:37.848306-05	36.4000015258789062	11
1902	2021-03-24 06:31:40.849248-05	25.1000003814697266	10
1903	2021-03-24 06:31:41.849796-05	36.2999992370605469	11
1904	2021-03-24 06:31:44.852403-05	25.1000003814697266	10
1905	2021-03-24 06:31:45.849594-05	36.2999992370605469	11
1906	2021-03-24 06:31:48.845672-05	25.1000003814697266	10
1907	2021-03-24 06:31:49.859387-05	36.4000015258789062	11
1908	2021-03-24 06:31:52.860611-05	25.1000003814697266	10
1909	2021-03-24 06:31:53.855444-05	36.4000015258789062	11
1910	2021-03-24 06:31:56.866485-05	25.1000003814697266	10
1911	2021-03-24 06:31:57.873554-05	36.4000015258789062	11
1912	2021-03-24 06:32:00.873279-05	25.1000003814697266	10
1913	2021-03-24 06:32:01.870817-05	36.4000015258789062	11
1914	2021-03-24 06:32:04.869736-05	25.1000003814697266	10
1915	2021-03-24 06:32:05.870375-05	36.4000015258789062	11
1916	2021-03-24 06:32:08.871843-05	25	10
1917	2021-03-24 06:32:09.870121-05	36.2999992370605469	11
1918	2021-03-24 06:32:12.887365-05	25.1000003814697266	10
1919	2021-03-24 06:32:13.891692-05	36.4000015258789062	11
1920	2021-03-24 06:32:16.884571-05	25.1000003814697266	10
1921	2021-03-24 06:32:17.880162-05	36.4000015258789062	11
1922	2021-03-24 06:32:20.892752-05	25.1000003814697266	10
1923	2021-03-24 06:32:21.891365-05	36.4000015258789062	11
1924	2021-03-24 06:32:24.902283-05	25.1000003814697266	10
1925	2021-03-24 06:32:25.89471-05	36.4000015258789062	11
1926	2021-03-24 06:32:28.895808-05	25.1000003814697266	10
1927	2021-03-24 06:32:29.896652-05	36.2999992370605469	11
1928	2021-03-24 06:32:32.911801-05	25	10
1929	2021-03-24 06:32:33.907371-05	36.2999992370605469	11
1930	2021-03-24 06:32:36.911529-05	25.1000003814697266	10
1931	2021-03-24 06:32:37.927111-05	36.2999992370605469	11
1932	2021-03-24 06:32:40.913216-05	25.1000003814697266	10
1933	2021-03-24 06:32:41.91549-05	36.2999992370605469	11
1934	2021-03-24 06:32:44.92496-05	25	10
1935	2021-03-24 06:32:45.897366-05	36.2999992370605469	11
1936	2021-03-24 06:32:48.926373-05	25.1000003814697266	10
1937	2021-03-24 06:32:49.925158-05	36.2999992370605469	11
1938	2021-03-24 06:32:52.928559-05	25.1000003814697266	10
1939	2021-03-24 06:32:53.917469-05	36.2999992370605469	11
1940	2021-03-24 06:32:56.935136-05	25.1000003814697266	10
1941	2021-03-24 06:32:57.932894-05	36.2999992370605469	11
1942	2021-03-24 06:33:00.935116-05	25.1000003814697266	10
1943	2021-03-24 06:33:01.936266-05	36.4000015258789062	11
1944	2021-03-24 06:33:04.951376-05	25.1000003814697266	10
1945	2021-03-24 06:33:05.938952-05	36.2999992370605469	11
1946	2021-03-24 06:33:08.944345-05	25.1000003814697266	10
1947	2021-03-24 06:33:09.948617-05	36.4000015258789062	11
1948	2021-03-24 06:33:12.929299-05	25.1000003814697266	10
1949	2021-03-24 06:33:13.954025-05	36.4000015258789062	11
1950	2021-03-24 06:33:16.946257-05	25.1000003814697266	10
1951	2021-03-24 06:33:17.959117-05	36.4000015258789062	11
1952	2021-03-24 06:33:20.956793-05	25.1000003814697266	10
1953	2021-03-24 06:33:21.962152-05	36.2999992370605469	11
1954	2021-03-24 06:33:24.967826-05	25	10
1955	2021-03-24 06:33:25.970951-05	36.2999992370605469	11
1956	2021-03-24 06:33:28.973143-05	25	10
1957	2021-03-24 06:33:29.969339-05	36.2999992370605469	11
1958	2021-03-24 06:33:32.967301-05	25.1000003814697266	10
1959	2021-03-24 06:33:33.974895-05	36.4000015258789062	11
1960	2021-03-24 06:33:36.975228-05	25.1000003814697266	10
1961	2021-03-24 06:33:37.973495-05	36.2999992370605469	11
1962	2021-03-24 06:33:40.989668-05	25.1000003814697266	10
1963	2021-03-24 06:33:41.980808-05	36.2999992370605469	11
1964	2021-03-24 06:33:44.988881-05	25	10
1965	2021-03-24 06:33:45.988141-05	36.2999992370605469	11
1966	2021-03-24 06:33:48.994783-05	25.1000003814697266	10
1967	2021-03-24 06:33:49.993881-05	36.4000015258789062	11
1968	2021-03-24 06:33:53.00252-05	25.1000003814697266	10
1969	2021-03-24 06:33:53.998456-05	36.4000015258789062	11
1970	2021-03-24 06:33:56.995219-05	25	10
1971	2021-03-24 06:33:57.994834-05	36.2999992370605469	11
1972	2021-03-24 06:34:01.006328-05	25.1000003814697266	10
1973	2021-03-24 06:34:01.999412-05	36.4000015258789062	11
1974	2021-03-24 06:34:05.009084-05	25.1000003814697266	10
1975	2021-03-24 06:34:06.011278-05	36.4000015258789062	11
1976	2021-03-24 06:34:09.012719-05	25	10
1977	2021-03-24 06:34:10.015342-05	36.2999992370605469	11
1978	2021-03-24 06:34:13.007726-05	25	10
1979	2021-03-24 06:34:14.018668-05	36.2999992370605469	11
1980	2021-03-24 06:34:17.017877-05	25.1000003814697266	10
1981	2021-03-24 06:34:18.024549-05	36.4000015258789062	11
1982	2021-03-24 06:34:21.028934-05	25.1000003814697266	10
1983	2021-03-24 06:34:22.024973-05	36.4000015258789062	11
1984	2021-03-24 06:34:25.027575-05	25	10
1985	2021-03-24 06:34:26.030884-05	36.2999992370605469	11
1986	2021-03-24 06:34:29.036674-05	25	10
1987	2021-03-24 06:34:30.029652-05	36.2999992370605469	11
1988	2021-03-24 06:34:33.035278-05	25.1000003814697266	10
1989	2021-03-24 06:34:34.015347-05	36.4000015258789062	11
1990	2021-03-24 06:34:37.038926-05	25.1000003814697266	10
1991	2021-03-24 06:34:38.033628-05	36.4000015258789062	11
1992	2021-03-24 06:34:41.048862-05	25.1000003814697266	10
1993	2021-03-24 06:34:42.044514-05	36.4000015258789062	11
1994	2021-03-24 06:34:45.048168-05	25.1000003814697266	10
1995	2021-03-24 06:34:46.049114-05	36.4000015258789062	11
1996	2021-03-24 06:34:49.055297-05	25.1000003814697266	10
1997	2021-03-24 06:34:50.05351-05	36.4000015258789062	11
1998	2021-03-24 06:34:53.062219-05	25.1000003814697266	10
1999	2021-03-24 06:34:54.06037-05	36.4000015258789062	11
2000	2021-03-24 06:34:57.066629-05	25	10
2001	2021-03-24 06:34:58.061456-05	36.2999992370605469	11
2002	2021-03-24 06:35:01.04246-05	25	10
2003	2021-03-24 06:35:02.061-05	36.2999992370605469	11
2004	2021-03-24 06:35:05.059579-05	25	10
2005	2021-03-24 06:35:06.072754-05	36.2999992370605469	11
2006	2021-03-24 06:35:09.079475-05	25.1000003814697266	10
2007	2021-03-24 06:35:10.067321-05	36.4000015258789062	11
2008	2021-03-24 06:35:13.069195-05	25	10
2009	2021-03-24 06:35:14.081277-05	36.2999992370605469	11
2010	2021-03-24 06:35:17.084594-05	25	10
2011	2021-03-24 06:35:18.093652-05	36.2999992370605469	11
2012	2021-03-24 06:35:21.089307-05	25	10
2013	2021-03-24 06:35:22.094212-05	36.2999992370605469	11
2014	2021-03-24 06:35:25.09574-05	25	10
2015	2021-03-24 06:35:26.089595-05	36.2999992370605469	11
2016	2021-03-24 06:35:29.103145-05	25	10
2017	2021-03-24 06:35:30.10479-05	36.2999992370605469	11
2018	2021-03-24 06:35:33.105047-05	25	10
2019	2021-03-24 06:35:34.098753-05	36.2999992370605469	11
2020	2021-03-24 06:35:37.10036-05	25	10
2021	2021-03-24 06:35:38.103203-05	36.2999992370605469	11
2022	2021-03-24 06:35:41.117631-05	25	10
2023	2021-03-24 06:35:42.117458-05	36.2999992370605469	11
2024	2021-03-24 06:35:45.120167-05	25	10
2025	2021-03-24 06:35:46.119155-05	36.4000015258789062	11
2026	2021-03-24 06:35:49.125676-05	25	10
2027	2021-03-24 06:35:50.126035-05	36.4000015258789062	11
2028	2021-03-24 06:35:53.123218-05	25	10
2029	2021-03-24 06:35:54.125867-05	36.4000015258789062	11
2030	2021-03-24 06:35:57.13267-05	25	10
2031	2021-03-24 06:35:58.135158-05	36.4000015258789062	11
2032	2021-03-24 06:36:01.142962-05	25	10
2033	2021-03-24 06:36:02.133163-05	36.2999992370605469	11
2034	2021-03-24 06:36:05.158763-05	25	10
2035	2021-03-24 06:36:06.13987-05	36.2999992370605469	11
2036	2021-03-24 06:36:09.148881-05	25	10
2037	2021-03-24 06:36:10.146499-05	36.4000015258789062	11
2038	2021-03-24 06:36:13.144771-05	25	10
2039	2021-03-24 06:36:14.150734-05	36.4000015258789062	11
2040	2021-03-24 06:36:17.16081-05	25	10
2041	2021-03-24 06:36:18.137341-05	36.4000015258789062	11
2042	2021-03-24 06:36:21.15694-05	25	10
2043	2021-03-24 06:36:22.148986-05	36.4000015258789062	11
2044	2021-03-24 06:36:25.163695-05	25	10
2045	2021-03-24 06:36:26.158249-05	36.4000015258789062	11
2046	2021-03-24 06:36:29.168093-05	25	10
2047	2021-03-24 06:36:30.166308-05	36.5	11
2048	2021-03-24 06:36:33.178369-05	25	10
2049	2021-03-24 06:36:34.173865-05	36.5	11
2050	2021-03-24 06:36:37.174282-05	25	10
2051	2021-03-24 06:36:38.175031-05	36.5	11
2052	2021-03-24 06:36:41.182235-05	25	10
2053	2021-03-24 06:36:42.18442-05	36.5	11
2054	2021-03-24 06:36:45.154598-05	25	10
2055	2021-03-24 06:36:46.189129-05	36.5	11
2056	2021-03-24 06:36:49.187555-05	25	10
2057	2021-03-24 06:36:50.189568-05	36.6000022888183594	11
2058	2021-03-24 06:36:53.190755-05	25	10
2059	2021-03-24 06:36:54.197418-05	36.6000022888183594	11
2060	2021-03-24 06:36:57.201123-05	25	10
2061	2021-03-24 06:36:58.192746-05	36.6000022888183594	11
2062	2021-03-24 06:37:01.201406-05	25	10
2063	2021-03-24 06:37:02.203752-05	36.7000007629394531	11
2064	2021-03-24 06:37:05.211711-05	25	10
2065	2021-03-24 06:37:06.209207-05	36.6000022888183594	11
2066	2021-03-24 06:37:09.209959-05	25	10
2067	2021-03-24 06:37:10.212374-05	36.6000022888183594	11
2068	2021-03-24 06:37:13.226236-05	25.1000003814697266	10
2069	2021-03-24 06:37:14.212126-05	36.7000007629394531	11
2070	2021-03-24 06:37:17.22244-05	25	10
2071	2021-03-24 06:37:18.224644-05	36.6000022888183594	11
2072	2021-03-24 06:37:21.226354-05	25	10
2073	2021-03-24 06:37:22.228698-05	36.6000022888183594	11
2074	2021-03-24 06:37:25.22958-05	25	10
2075	2021-03-24 06:37:26.219248-05	36.6000022888183594	11
2076	2021-03-24 06:37:29.234923-05	25	10
2077	2021-03-24 06:37:30.244435-05	36.7000007629394531	11
2078	2021-03-24 06:37:33.239648-05	25	10
2079	2021-03-24 06:37:34.241719-05	36.7999992370605469	11
2080	2021-03-24 06:37:37.239864-05	25	10
2081	2021-03-24 06:37:38.245214-05	36.7000007629394531	11
2082	2021-03-24 06:37:41.246156-05	25	10
2083	2021-03-24 06:37:42.244246-05	36.7000007629394531	11
2084	2021-03-24 06:37:45.251935-05	25	10
2085	2021-03-24 06:37:46.245128-05	36.7000007629394531	11
2086	2021-03-24 06:37:49.260852-05	25	10
2087	2021-03-24 06:37:50.259715-05	36.6000022888183594	11
2088	2021-03-24 06:37:53.25566-05	25	10
2089	2021-03-24 06:37:54.266517-05	36.6000022888183594	11
2090	2021-03-24 06:37:57.271817-05	25	10
2091	2021-03-24 06:37:58.26779-05	36.6000022888183594	11
2092	2021-03-24 06:38:01.258788-05	25	10
2093	2021-03-24 06:38:02.272432-05	36.6000022888183594	11
2094	2021-03-24 06:38:05.274289-05	25	10
2095	2021-03-24 06:38:06.263889-05	36.6000022888183594	11
2096	2021-03-24 06:38:09.278402-05	25	10
2097	2021-03-24 06:38:10.260457-05	36.6000022888183594	11
2098	2021-03-24 06:38:13.283843-05	25	10
2099	2021-03-24 06:38:14.292592-05	36.6000022888183594	11
2100	2021-03-24 06:38:17.292593-05	25	10
2101	2021-03-24 06:38:18.276522-05	36.6000022888183594	11
2102	2021-03-24 06:38:21.299593-05	25	10
2103	2021-03-24 06:38:22.293607-05	36.6000022888183594	11
2104	2021-03-24 06:38:25.295614-05	25	10
2105	2021-03-24 06:38:26.29674-05	36.6000022888183594	11
2106	2021-03-24 06:38:29.301806-05	25	10
2107	2021-03-24 06:38:30.303145-05	36.6000022888183594	11
2108	2021-03-24 06:38:33.292031-05	25	10
2109	2021-03-24 06:38:34.302869-05	36.7000007629394531	11
2110	2021-03-24 06:38:37.282976-05	25	10
2111	2021-03-24 06:38:38.314698-05	36.7000007629394531	11
2112	2021-03-24 06:38:41.300473-05	25	10
2113	2021-03-24 06:38:42.315002-05	36.7000007629394531	11
2114	2021-03-24 06:38:45.31985-05	25	10
2115	2021-03-24 06:38:46.323843-05	36.7000007629394531	11
2116	2021-03-24 06:38:49.321913-05	25	10
2117	2021-03-24 06:38:50.309605-05	36.7000007629394531	11
2118	2021-03-24 06:38:53.322328-05	25	10
2119	2021-03-24 06:38:54.320983-05	36.7000007629394531	11
2120	2021-03-24 06:38:57.318243-05	25	10
2121	2021-03-24 06:38:58.336856-05	36.7000007629394531	11
2122	2021-03-24 06:39:01.335143-05	25	10
2123	2021-03-24 06:39:02.330585-05	36.7000007629394531	11
2124	2021-03-24 06:39:05.336875-05	25	10
2125	2021-03-24 06:39:06.333397-05	36.7000007629394531	11
2126	2021-03-24 06:39:09.337065-05	25	10
2127	2021-03-24 06:39:10.329765-05	36.7000007629394531	11
2128	2021-03-24 06:39:13.350123-05	25	10
2129	2021-03-24 06:39:14.351525-05	36.7000007629394531	11
2130	2021-03-24 06:39:17.354571-05	25	10
2131	2021-03-24 06:39:18.349733-05	36.7000007629394531	11
2132	2021-03-24 06:39:21.355744-05	25	10
2133	2021-03-24 06:39:22.355381-05	36.7000007629394531	11
2134	2021-03-24 06:39:25.368481-05	25	10
2135	2021-03-24 06:39:26.360573-05	36.7000007629394531	11
2136	2021-03-24 06:39:29.362554-05	25	10
2137	2021-03-24 06:39:30.362038-05	36.7000007629394531	11
2138	2021-03-24 06:39:33.369181-05	25	10
2139	2021-03-24 06:39:34.362565-05	36.7000007629394531	11
2140	2021-03-24 06:39:37.377853-05	25	10
2141	2021-03-24 06:39:38.375073-05	36.7000007629394531	11
2142	2021-03-24 06:39:41.379985-05	25	10
2143	2021-03-24 06:39:42.374276-05	36.7000007629394531	11
2144	2021-03-24 06:39:45.381422-05	25	10
2145	2021-03-24 06:39:46.39161-05	36.7000007629394531	11
2146	2021-03-24 06:39:49.391869-05	25	10
2147	2021-03-24 06:39:50.372581-05	36.7000007629394531	11
2148	2021-03-24 06:39:53.384535-05	25	10
2149	2021-03-24 06:39:54.372059-05	36.7000007629394531	11
2150	2021-03-24 06:39:57.402903-05	25	10
2151	2021-03-24 06:39:58.390291-05	36.7000007629394531	11
2152	2021-03-24 06:40:01.402816-05	25	10
2153	2021-03-24 06:40:02.400121-05	36.7000007629394531	11
2154	2021-03-24 06:40:05.406497-05	25	10
2155	2021-03-24 06:40:06.40574-05	36.7000007629394531	11
2156	2021-03-24 06:40:09.400571-05	25	10
2157	2021-03-24 06:40:10.403178-05	36.7000007629394531	11
2158	2021-03-24 06:40:13.412897-05	25	10
2159	2021-03-24 06:40:14.41329-05	36.7000007629394531	11
2160	2021-03-24 06:40:17.398709-05	25	10
2161	2021-03-24 06:40:18.41357-05	36.7000007629394531	11
2162	2021-03-24 06:40:21.409505-05	25	10
2163	2021-03-24 06:40:22.426884-05	36.7000007629394531	11
2164	2021-03-24 06:40:25.421093-05	25	10
2165	2021-03-24 06:40:26.424512-05	36.7000007629394531	11
2166	2021-03-24 06:40:29.421726-05	25	10
2167	2021-03-24 06:40:30.425841-05	36.7000007629394531	11
2168	2021-03-24 06:40:33.432521-05	25	10
2169	2021-03-24 06:40:34.43315-05	36.7000007629394531	11
2170	2021-03-24 06:40:37.439772-05	25	10
2171	2021-03-24 06:40:38.444886-05	36.7000007629394531	11
2172	2021-03-24 06:40:41.449448-05	25	10
2173	2021-03-24 06:40:42.451174-05	36.7000007629394531	11
2174	2021-03-24 06:40:45.451689-05	25	10
2175	2021-03-24 06:40:46.44932-05	36.7000007629394531	11
2176	2021-03-24 06:40:49.459342-05	25	10
2177	2021-03-24 06:40:50.451517-05	36.7000007629394531	11
2178	2021-03-24 06:40:53.464648-05	25	10
2179	2021-03-24 06:40:54.46789-05	36.7000007629394531	11
2180	2021-03-24 06:40:57.459405-05	25	10
2181	2021-03-24 06:40:58.471363-05	36.7000007629394531	11
2182	2021-03-24 06:41:01.468177-05	25	10
2183	2021-03-24 06:41:02.477459-05	36.7000007629394531	11
2184	2021-03-24 06:41:05.481065-05	25	10
2185	2021-03-24 06:41:06.474954-05	36.7000007629394531	11
2186	2021-03-24 06:41:09.487722-05	25	10
2187	2021-03-24 06:41:10.472073-05	36.7000007629394531	11
2188	2021-03-24 06:41:13.485107-05	25	10
2189	2021-03-24 06:41:14.485085-05	36.7000007629394531	11
2190	2021-03-24 06:41:17.488463-05	25	10
2191	2021-03-24 06:41:18.49134-05	36.7000007629394531	11
2192	2021-03-24 06:41:21.495133-05	25	10
2193	2021-03-24 06:41:22.492147-05	36.6000022888183594	11
2194	2021-03-24 06:41:25.500949-05	25	10
2195	2021-03-24 06:41:26.497175-05	36.7000007629394531	11
2196	2021-03-24 06:41:29.497128-05	25	10
2197	2021-03-24 06:41:30.499055-05	36.6000022888183594	11
2198	2021-03-24 06:41:33.506713-05	25	10
2199	2021-03-24 06:41:34.49934-05	36.6000022888183594	11
2200	2021-03-24 06:41:37.515835-05	25	10
2201	2021-03-24 06:41:38.500864-05	36.6000022888183594	11
2202	2021-03-24 06:41:41.517223-05	25	10
2203	2021-03-24 06:41:42.519684-05	36.6000022888183594	11
2204	2021-03-24 06:41:45.523352-05	25	10
2205	2021-03-24 06:41:46.509798-05	36.6000022888183594	11
2206	2021-03-24 06:41:49.525068-05	25	10
2207	2021-03-24 06:41:50.517088-05	36.6000022888183594	11
2208	2021-03-24 06:41:53.53231-05	25	10
2209	2021-03-24 06:41:54.53769-05	36.6000022888183594	11
2210	2021-03-24 06:41:57.535231-05	25	10
2211	2021-03-24 06:41:58.537453-05	36.6000022888183594	11
2212	2021-03-24 06:42:01.513754-05	25	10
2213	2021-03-24 06:42:02.528887-05	36.7000007629394531	11
2214	2021-03-24 06:42:05.523501-05	25	10
2215	2021-03-24 06:42:06.54513-05	36.7000007629394531	11
2216	2021-03-24 06:42:09.547644-05	25	10
2217	2021-03-24 06:42:10.546106-05	36.6000022888183594	11
2218	2021-03-24 06:42:13.551361-05	25	10
2219	2021-03-24 06:42:14.547231-05	36.7000007629394531	11
2220	2021-03-24 06:42:17.551941-05	25	10
2221	2021-03-24 06:42:18.550625-05	36.7000007629394531	11
2222	2021-03-24 06:42:21.554914-05	25	10
2223	2021-03-24 06:42:22.567235-05	36.7000007629394531	11
2224	2021-03-24 06:42:25.562657-05	25	10
2225	2021-03-24 06:42:26.560888-05	36.7000007629394531	11
2226	2021-03-24 06:42:29.560049-05	25	10
2227	2021-03-24 06:42:30.580011-05	36.7000007629394531	11
2228	2021-03-24 06:42:33.575285-05	25	10
2229	2021-03-24 06:42:34.581017-05	36.7000007629394531	11
2230	2021-03-24 06:42:37.578684-05	25	10
2231	2021-03-24 06:42:38.575338-05	36.7000007629394531	11
2232	2021-03-24 06:42:41.585925-05	25	10
2233	2021-03-24 06:42:42.580141-05	36.6000022888183594	11
2234	2021-03-24 06:42:45.583038-05	25	10
2235	2021-03-24 06:42:46.587177-05	36.7000007629394531	11
2236	2021-03-24 06:42:49.599044-05	25	10
2237	2021-03-24 06:42:50.595643-05	36.7000007629394531	11
2238	2021-03-24 06:42:53.593926-05	25	10
2239	2021-03-24 06:42:54.600005-05	36.6000022888183594	11
2240	2021-03-24 06:42:57.61203-05	25	10
2241	2021-03-24 06:42:58.608684-05	36.7000007629394531	11
2242	2021-03-24 06:43:01.612461-05	25	10
2243	2021-03-24 06:43:02.610183-05	36.7000007629394531	11
2244	2021-03-24 06:43:05.616525-05	25	10
2245	2021-03-24 06:43:06.615181-05	36.6000022888183594	11
2246	2021-03-24 06:43:09.62086-05	25	10
2247	2021-03-24 06:43:10.616469-05	36.6000022888183594	11
2248	2021-03-24 06:43:13.625116-05	25	10
2249	2021-03-24 06:43:14.620364-05	36.6000022888183594	11
2250	2021-03-24 06:43:17.624767-05	25	10
2251	2021-03-24 06:43:18.602658-05	36.6000022888183594	11
2252	2021-03-24 06:43:21.627617-05	25	10
2253	2021-03-24 06:43:22.626146-05	36.7000007629394531	11
2254	2021-03-24 06:43:25.636936-05	25	10
2255	2021-03-24 06:43:26.635975-05	36.6000022888183594	11
2256	2021-03-24 06:43:29.644751-05	25	10
2257	2021-03-24 06:43:30.630952-05	36.7000007629394531	11
2258	2021-03-24 06:43:33.643251-05	25	10
2259	2021-03-24 06:43:34.638674-05	36.7000007629394531	11
2260	2021-03-24 06:43:37.650917-05	25	10
2261	2021-03-24 06:43:38.650329-05	36.7000007629394531	11
2262	2021-03-24 06:43:41.640661-05	25	10
2263	2021-03-24 06:43:42.660332-05	36.7000007629394531	11
2264	2021-03-24 06:43:45.626286-05	25	10
2265	2021-03-24 06:43:46.654754-05	36.7000007629394531	11
2266	2021-03-24 06:43:49.661934-05	25	10
2267	2021-03-24 06:43:50.663521-05	36.7000007629394531	11
2268	2021-03-24 06:43:53.659699-05	25	10
2269	2021-03-24 06:43:54.664699-05	36.7000007629394531	11
2270	2021-03-24 06:43:57.672569-05	25	10
2271	2021-03-24 06:43:58.669466-05	36.7000007629394531	11
2272	2021-03-24 06:44:01.661396-05	25	10
2273	2021-03-24 06:44:02.68078-05	36.7000007629394531	11
2274	2021-03-24 06:44:05.682147-05	25	10
2275	2021-03-24 06:44:06.682806-05	36.7000007629394531	11
2276	2021-03-24 06:44:09.685318-05	25	10
2277	2021-03-24 06:44:10.690238-05	36.7000007629394531	11
2278	2021-03-24 06:44:13.671975-05	25	10
2279	2021-03-24 06:44:14.68368-05	36.7000007629394531	11
2280	2021-03-24 06:44:17.678632-05	25	10
2281	2021-03-24 06:44:18.692481-05	36.7000007629394531	11
2282	2021-03-24 06:44:21.698576-05	25	10
2283	2021-03-24 06:44:22.69393-05	36.7000007629394531	11
2284	2021-03-24 06:44:25.697442-05	25	10
2285	2021-03-24 06:44:26.700136-05	36.7000007629394531	11
2286	2021-03-24 06:44:29.705582-05	25	10
2287	2021-03-24 06:44:30.712906-05	36.7000007629394531	11
2288	2021-03-24 06:44:33.70945-05	25	10
2289	2021-03-24 06:44:34.714682-05	36.7000007629394531	11
2290	2021-03-24 06:44:37.715969-05	25	10
2291	2021-03-24 06:44:38.714104-05	36.7000007629394531	11
2292	2021-03-24 06:44:41.717869-05	25	10
2293	2021-03-24 06:44:42.721761-05	36.7000007629394531	11
2294	2021-03-24 06:44:45.726679-05	25	10
2295	2021-03-24 06:44:46.725048-05	36.7000007629394531	11
2296	2021-03-24 06:44:49.732211-05	25	10
2297	2021-03-24 06:44:50.730895-05	36.7000007629394531	11
2298	2021-03-24 06:44:53.734641-05	25	10
2299	2021-03-24 06:44:54.728453-05	36.7000007629394531	11
2300	2021-03-24 06:44:57.73915-05	25	10
2301	2021-03-24 06:44:58.733419-05	36.7000007629394531	11
2302	2021-03-24 06:45:01.742221-05	25	10
2303	2021-03-24 06:45:02.71537-05	36.7000007629394531	11
2304	2021-03-24 06:45:05.747087-05	25	10
2305	2021-03-24 06:45:06.739849-05	36.7000007629394531	11
2306	2021-03-24 06:45:09.745608-05	25	10
2307	2021-03-24 06:45:10.751448-05	36.7000007629394531	11
2308	2021-03-24 06:45:13.753646-05	25	10
2309	2021-03-24 06:45:14.753953-05	36.7000007629394531	11
2310	2021-03-24 06:45:17.761722-05	25	10
2311	2021-03-24 06:45:18.761832-05	36.7000007629394531	11
2312	2021-03-24 06:45:21.762549-05	25	10
2313	2021-03-24 06:45:22.761569-05	36.7000007629394531	11
2314	2021-03-24 06:45:25.737325-05	25	10
2315	2021-03-24 06:45:26.773379-05	36.7000007629394531	11
2316	2021-03-24 06:45:29.774965-05	25	10
2317	2021-03-24 06:45:30.772996-05	36.7000007629394531	11
2318	2021-03-24 06:45:33.77593-05	25	10
2319	2021-03-24 06:45:34.774648-05	36.7000007629394531	11
2320	2021-03-24 06:45:37.777884-05	25	10
2321	2021-03-24 06:45:38.789246-05	36.7999992370605469	11
2322	2021-03-24 06:45:41.776154-05	25	10
2323	2021-03-24 06:45:42.786709-05	36.7000007629394531	11
2324	2021-03-24 06:45:45.785433-05	25	10
2325	2021-03-24 06:45:46.789454-05	36.7999992370605469	11
2326	2021-03-24 06:45:49.792043-05	25	10
2327	2021-03-24 06:45:50.797891-05	36.7999992370605469	11
2328	2021-03-24 06:45:53.796385-05	25	10
2329	2021-03-24 06:45:54.79444-05	36.7999992370605469	11
2330	2021-03-24 06:45:57.806172-05	25	10
2331	2021-03-24 06:45:58.801072-05	36.7999992370605469	11
2332	2021-03-24 06:46:01.804078-05	25	10
2333	2021-03-24 06:46:02.800643-05	36.7999992370605469	11
2334	2021-03-24 06:46:05.801888-05	25	10
2335	2021-03-24 06:46:06.806602-05	36.7999992370605469	11
2336	2021-03-24 06:46:09.819031-05	25	10
2337	2021-03-24 06:46:10.811257-05	36.7999992370605469	11
2338	2021-03-24 06:46:13.809802-05	25	10
2339	2021-03-24 06:46:14.816087-05	36.7999992370605469	11
2340	2021-03-24 06:46:17.817343-05	25	10
2341	2021-03-24 06:46:18.817659-05	36.7999992370605469	11
2342	2021-03-24 06:46:21.821267-05	25	10
2343	2021-03-24 06:46:22.822836-05	36.7999992370605469	11
2344	2021-03-24 06:46:25.83926-05	25	10
2345	2021-03-24 06:46:26.822996-05	36.7999992370605469	11
2346	2021-03-24 06:46:29.837245-05	25	10
2347	2021-03-24 06:46:30.829701-05	36.7999992370605469	11
2348	2021-03-24 06:46:33.840997-05	25	10
2349	2021-03-24 06:46:34.83914-05	36.7999992370605469	11
2350	2021-03-24 06:46:37.842171-05	25	10
2351	2021-03-24 06:46:38.83298-05	36.7999992370605469	11
2352	2021-03-24 06:46:41.842755-05	25	10
2353	2021-03-24 06:46:42.833266-05	36.7999992370605469	11
2354	2021-03-24 06:46:45.850102-05	25	10
2355	2021-03-24 06:46:46.845658-05	36.7999992370605469	11
2356	2021-03-24 06:46:49.855118-05	25	10
2357	2021-03-24 06:46:50.852144-05	36.7999992370605469	11
2358	2021-03-24 06:46:53.86315-05	25	10
2359	2021-03-24 06:46:54.868386-05	36.7999992370605469	11
2360	2021-03-24 06:46:57.87672-05	25	10
2361	2021-03-24 06:46:58.865909-05	36.7999992370605469	11
2362	2021-03-24 06:47:01.875581-05	25	10
2363	2021-03-24 06:47:02.873415-05	36.7999992370605469	11
2364	2021-03-24 06:47:05.865438-05	25	10
2365	2021-03-24 06:47:06.873564-05	36.7999992370605469	11
2366	2021-03-24 06:47:09.855151-05	25	10
2367	2021-03-24 06:47:10.873802-05	36.7999992370605469	11
2368	2021-03-24 06:47:13.878158-05	25	10
2369	2021-03-24 06:47:14.887765-05	36.7999992370605469	11
2370	2021-03-24 06:47:17.886747-05	25	10
2371	2021-03-24 06:47:18.886594-05	36.7999992370605469	11
2372	2021-03-24 06:47:21.890716-05	25	10
2373	2021-03-24 06:47:22.896426-05	36.7999992370605469	11
2374	2021-03-24 06:47:25.903525-05	25	10
2375	2021-03-24 06:47:26.889273-05	36.7999992370605469	11
2376	2021-03-24 06:47:29.893331-05	25	10
2377	2021-03-24 06:47:30.901151-05	36.7999992370605469	11
2378	2021-03-24 06:47:33.89879-05	25	10
2379	2021-03-24 06:47:34.902602-05	36.7999992370605469	11
2380	2021-03-24 06:47:37.907796-05	25	10
2381	2021-03-24 06:47:38.909935-05	36.7999992370605469	11
2382	2021-03-24 06:47:41.901395-05	25	10
2383	2021-03-24 06:47:42.908242-05	36.7000007629394531	11
2384	2021-03-24 06:47:45.911351-05	25	10
2385	2021-03-24 06:47:46.922352-05	36.7999992370605469	11
2386	2021-03-24 06:47:49.91978-05	25	10
2387	2021-03-24 06:47:50.92356-05	36.7999992370605469	11
2388	2021-03-24 06:47:53.922829-05	25	10
2389	2021-03-24 06:47:54.927692-05	36.7999992370605469	11
2390	2021-03-24 06:47:57.923763-05	25	10
2391	2021-03-24 06:47:58.928602-05	36.7999992370605469	11
2392	2021-03-24 06:48:01.931043-05	25	10
2393	2021-03-24 06:48:02.928416-05	36.7999992370605469	11
2394	2021-03-24 06:48:05.937775-05	25	10
2395	2021-03-24 06:48:06.927065-05	36.7999992370605469	11
2396	2021-03-24 06:48:09.93743-05	25	10
2397	2021-03-24 06:48:10.93988-05	36.7999992370605469	11
2398	2021-03-24 06:48:13.939156-05	25	10
2399	2021-03-24 06:48:14.94311-05	36.7999992370605469	11
2400	2021-03-24 06:48:17.945761-05	25	10
2401	2021-03-24 06:48:18.949152-05	36.7999992370605469	11
2402	2021-03-24 06:48:21.940412-05	25	10
2403	2021-03-24 06:48:22.928774-05	36.7999992370605469	11
2404	2021-03-24 06:48:25.95685-05	25	10
2405	2021-03-24 06:48:26.954394-05	36.7999992370605469	11
2406	2021-03-24 06:48:29.976677-05	25	10
2407	2021-03-24 06:48:30.958428-05	36.7999992370605469	11
2408	2021-03-24 06:48:33.975848-05	25	10
2409	2021-03-24 06:48:34.964934-05	36.7999992370605469	11
2410	2021-03-24 06:48:37.974884-05	25	10
2411	2021-03-24 06:48:38.975458-05	36.7999992370605469	11
2412	2021-03-24 06:48:41.968255-05	25	10
2413	2021-03-24 06:48:42.967592-05	36.7999992370605469	11
2414	2021-03-24 06:48:45.963002-05	25	10
2415	2021-03-24 06:48:46.974247-05	36.7999992370605469	11
2416	2021-03-24 06:48:49.969378-05	25	10
2417	2021-03-24 06:48:50.979056-05	36.7999992370605469	11
2418	2021-03-24 06:48:53.98678-05	25	10
2419	2021-03-24 06:48:54.988552-05	36.7000007629394531	11
2420	2021-03-24 06:48:57.984826-05	25	10
2421	2021-03-24 06:48:58.994964-05	36.7000007629394531	11
2422	2021-03-24 06:49:01.991177-05	25	10
2423	2021-03-24 06:49:03.000548-05	36.7000007629394531	11
2424	2021-03-24 06:49:05.993086-05	25	10
2425	2021-03-24 06:49:07.002809-05	36.7000007629394531	11
2426	2021-03-24 06:49:10.002446-05	25	10
2427	2021-03-24 06:49:11.004748-05	36.7000007629394531	11
2428	2021-03-24 06:49:14.005362-05	25	10
2429	2021-03-24 06:49:15.00278-05	36.7000007629394531	11
2430	2021-03-24 06:49:18.019352-05	25	10
2431	2021-03-24 06:49:19.026779-05	36.7000007629394531	11
2432	2021-03-24 06:49:22.017126-05	25	10
2433	2021-03-24 06:49:23.02172-05	36.7000007629394531	11
2434	2021-03-24 06:49:26.028887-05	25	10
2435	2021-03-24 06:49:27.026186-05	36.7000007629394531	11
2436	2021-03-24 06:49:30.028117-05	25	10
2437	2021-03-24 06:49:31.026861-05	36.7000007629394531	11
2438	2021-03-24 06:49:34.038613-05	25	10
2439	2021-03-24 06:49:35.029658-05	36.7000007629394531	11
2440	2021-03-24 06:49:38.032922-05	25	10
2441	2021-03-24 06:49:39.032456-05	36.7000007629394531	11
2442	2021-03-24 06:49:42.027535-05	25	10
2443	2021-03-24 06:49:43.036082-05	36.7000007629394531	11
2444	2021-03-24 06:49:46.043072-05	25	10
2445	2021-03-24 06:49:47.04155-05	36.7000007629394531	11
2446	2021-03-24 06:49:50.042433-05	25	10
2447	2021-03-24 06:49:51.044936-05	36.7000007629394531	11
2448	2021-03-24 06:49:54.047385-05	25	10
2449	2021-03-24 06:49:55.054772-05	36.7000007629394531	11
2450	2021-03-24 06:49:58.059035-05	25	10
2451	2021-03-24 06:49:59.034088-05	36.7000007629394531	11
2452	2021-03-24 06:50:02.056287-05	25	10
2453	2021-03-24 06:50:03.035197-05	36.7000007629394531	11
2454	2021-03-24 06:50:06.072214-05	25	10
2455	2021-03-24 06:50:07.062124-05	36.7000007629394531	11
2456	2021-03-24 06:50:10.073091-05	25	10
2457	2021-03-24 06:50:11.068729-05	36.7000007629394531	11
2458	2021-03-24 06:50:14.077248-05	25	10
2459	2021-03-24 06:50:15.077767-05	36.7000007629394531	11
2460	2021-03-24 06:50:18.086105-05	25	10
2461	2021-03-24 06:50:19.081953-05	36.7000007629394531	11
2462	2021-03-24 06:50:22.090503-05	25	10
2463	2021-03-24 06:50:23.080585-05	36.7000007629394531	11
2464	2021-03-24 06:50:26.058748-05	25	10
2465	2021-03-24 06:50:27.084522-05	36.7000007629394531	11
2466	2021-03-24 06:50:30.094891-05	25	10
2467	2021-03-24 06:50:31.095508-05	36.7000007629394531	11
2468	2021-03-24 06:50:34.102973-05	25	10
2469	2021-03-24 06:50:35.101744-05	36.7000007629394531	11
2470	2021-03-24 06:50:38.106821-05	25	10
2471	2021-03-24 06:50:39.110661-05	36.7000007629394531	11
2472	2021-03-24 06:50:42.101297-05	25	10
2473	2021-03-24 06:50:43.112363-05	36.7000007629394531	11
2474	2021-03-24 06:50:46.107003-05	25	10
2475	2021-03-24 06:50:47.11869-05	36.7000007629394531	11
2476	2021-03-24 06:50:50.122837-05	25	10
2477	2021-03-24 06:50:51.123815-05	36.7000007629394531	11
2478	2021-03-24 06:50:54.120199-05	25	10
2479	2021-03-24 06:50:55.125304-05	36.7000007629394531	11
2480	2021-03-24 06:50:58.119327-05	25	10
2481	2021-03-24 06:50:59.123145-05	36.7000007629394531	11
2482	2021-03-24 06:51:02.127996-05	25	10
2483	2021-03-24 06:51:03.131153-05	36.7000007629394531	11
2484	2021-03-24 06:51:06.139413-05	25	10
2485	2021-03-24 06:51:07.129798-05	36.7000007629394531	11
2486	2021-03-24 06:51:10.137557-05	25	10
2487	2021-03-24 06:51:11.146083-05	36.7000007629394531	11
2488	2021-03-24 06:51:14.141375-05	25	10
2489	2021-03-24 06:51:15.15017-05	36.7000007629394531	11
2490	2021-03-24 06:51:18.145-05	25	10
2491	2021-03-24 06:51:19.141682-05	36.7000007629394531	11
2492	2021-03-24 06:51:22.157759-05	25	10
2493	2021-03-24 06:51:23.149136-05	36.7999992370605469	11
2494	2021-03-24 06:51:26.160888-05	25	10
2495	2021-03-24 06:51:27.15693-05	36.7000007629394531	11
2496	2021-03-24 06:51:30.161402-05	25	10
2497	2021-03-24 06:51:31.166396-05	36.7000007629394531	11
2498	2021-03-24 06:51:34.17426-05	25	10
2499	2021-03-24 06:51:35.167498-05	36.7000007629394531	11
2500	2021-03-24 06:51:38.173269-05	25	10
2501	2021-03-24 06:51:39.145308-05	36.7000007629394531	11
2502	2021-03-24 06:51:42.190954-05	25	10
2503	2021-03-24 06:51:43.177811-05	36.7000007629394531	11
2504	2021-03-24 06:51:46.185658-05	25	10
2505	2021-03-24 06:51:47.175549-05	36.7000007629394531	11
2506	2021-03-24 06:51:50.19098-05	25	10
2507	2021-03-24 06:51:51.185739-05	36.7000007629394531	11
2508	2021-03-24 06:51:54.191436-05	25	10
2509	2021-03-24 06:51:55.189173-05	36.7000007629394531	11
2510	2021-03-24 06:51:58.196171-05	25	10
2511	2021-03-24 06:51:59.20226-05	36.7000007629394531	11
2512	2021-03-24 06:52:02.17099-05	25	10
2513	2021-03-24 06:52:03.201925-05	36.7000007629394531	11
2514	2021-03-24 06:52:06.203303-05	25	10
2515	2021-03-24 06:52:07.203865-05	36.7000007629394531	11
2516	2021-03-24 06:52:10.209509-05	25	10
2517	2021-03-24 06:52:11.210797-05	36.7000007629394531	11
2518	2021-03-24 06:52:14.20236-05	25	10
2519	2021-03-24 06:52:15.220436-05	36.7000007629394531	11
2520	2021-03-24 06:52:18.220748-05	25	10
2521	2021-03-24 06:52:19.218266-05	36.7000007629394531	11
2522	2021-03-24 06:52:22.219634-05	25.1000003814697266	10
2523	2021-03-24 06:52:23.21924-05	36.7000007629394531	11
2524	2021-03-24 06:52:26.229276-05	25	10
2525	2021-03-24 06:52:27.227581-05	36.7000007629394531	11
2526	2021-03-24 06:52:30.230159-05	25	10
2527	2021-03-24 06:52:31.237773-05	36.7000007629394531	11
2528	2021-03-24 06:52:34.227982-05	25.1000003814697266	10
2529	2021-03-24 06:52:35.235963-05	36.7000007629394531	11
2530	2021-03-24 06:52:38.238379-05	25	10
2531	2021-03-24 06:52:39.237033-05	36.7000007629394531	11
2532	2021-03-24 06:52:42.240559-05	25.1000003814697266	10
2533	2021-03-24 06:52:43.256154-05	36.7000007629394531	11
2534	2021-03-24 06:52:46.251805-05	25	10
2535	2021-03-24 06:52:47.259184-05	36.6000022888183594	11
2536	2021-03-24 06:52:50.250927-05	25.1000003814697266	10
2537	2021-03-24 06:52:51.25856-05	36.7000007629394531	11
2538	2021-03-24 06:52:54.253049-05	25.1000003814697266	10
2539	2021-03-24 06:52:55.261136-05	36.7000007629394531	11
2540	2021-03-24 06:52:58.262852-05	25	10
2541	2021-03-24 06:52:59.261019-05	36.6000022888183594	11
2542	2021-03-24 06:53:02.26876-05	25	10
2543	2021-03-24 06:53:03.265896-05	36.6000022888183594	11
2544	2021-03-24 06:53:06.269158-05	25	10
2545	2021-03-24 06:53:07.275602-05	36.6000022888183594	11
2546	2021-03-24 06:53:10.279309-05	25	10
2547	2021-03-24 06:53:11.281177-05	36.6000022888183594	11
2548	2021-03-24 06:53:14.285452-05	25	10
2549	2021-03-24 06:53:15.264508-05	36.6000022888183594	11
2550	2021-03-24 06:53:18.289916-05	25	10
2551	2021-03-24 06:53:19.257211-05	36.6000022888183594	11
2552	2021-03-24 06:53:22.292853-05	25	10
2553	2021-03-24 06:53:23.282651-05	36.6000022888183594	11
2554	2021-03-24 06:53:26.301021-05	25.1000003814697266	10
2555	2021-03-24 06:53:27.292682-05	36.7000007629394531	11
2556	2021-03-24 06:53:30.299193-05	25.1000003814697266	10
2557	2021-03-24 06:53:31.300594-05	36.7000007629394531	11
2558	2021-03-24 06:53:34.319763-05	25	10
2559	2021-03-24 06:53:35.306325-05	36.6000022888183594	11
2560	2021-03-24 06:53:38.308943-05	25.1000003814697266	10
2561	2021-03-24 06:53:39.313349-05	36.7000007629394531	11
2562	2021-03-24 06:53:42.289056-05	25	10
2563	2021-03-24 06:53:43.304414-05	36.6000022888183594	11
2564	2021-03-24 06:53:46.314096-05	25	10
2565	2021-03-24 06:53:47.318916-05	36.6000022888183594	11
2566	2021-03-24 06:53:50.316645-05	25	10
2567	2021-03-24 06:53:51.315244-05	36.6000022888183594	11
2568	2021-03-24 06:53:54.330307-05	25.1000003814697266	10
2569	2021-03-24 06:53:55.333807-05	36.7000007629394531	11
2570	2021-03-24 06:53:58.327691-05	25	10
2571	2021-03-24 06:53:59.329594-05	36.6000022888183594	11
2572	2021-03-24 06:54:02.33659-05	25.1000003814697266	10
2573	2021-03-24 06:54:03.338314-05	36.7000007629394531	11
2574	2021-03-24 06:54:06.346757-05	25.1000003814697266	10
2575	2021-03-24 06:54:07.339986-05	36.7000007629394531	11
2576	2021-03-24 06:54:10.350607-05	25.1000003814697266	10
2577	2021-03-24 06:54:11.343429-05	36.7000007629394531	11
2578	2021-03-24 06:54:14.358647-05	25.1000003814697266	10
2579	2021-03-24 06:54:15.3515-05	36.7000007629394531	11
2580	2021-03-24 06:54:18.352488-05	25.1000003814697266	10
2581	2021-03-24 06:54:19.355731-05	36.6000022888183594	11
2582	2021-03-24 06:54:22.369052-05	25.1000003814697266	10
2583	2021-03-24 06:54:23.357627-05	36.6000022888183594	11
2584	2021-03-24 06:54:26.366201-05	25	10
2585	2021-03-24 06:54:27.365471-05	36.6000022888183594	11
2586	2021-03-24 06:54:30.366195-05	25.1000003814697266	10
2587	2021-03-24 06:54:31.36771-05	36.6000022888183594	11
2588	2021-03-24 06:54:34.372793-05	25.1000003814697266	10
2589	2021-03-24 06:54:35.380336-05	36.6000022888183594	11
2590	2021-03-24 06:54:38.383725-05	25.1000003814697266	10
2591	2021-03-24 06:54:39.377877-05	36.6000022888183594	11
2592	2021-03-24 06:54:42.381659-05	25.1000003814697266	10
2593	2021-03-24 06:54:43.379194-05	36.6000022888183594	11
2594	2021-03-24 06:54:46.385802-05	25.1000003814697266	10
2595	2021-03-24 06:54:47.388153-05	36.6000022888183594	11
2596	2021-03-24 06:54:50.389376-05	25.1000003814697266	10
2597	2021-03-24 06:54:51.380157-05	36.6000022888183594	11
2598	2021-03-24 06:54:54.389722-05	25.1000003814697266	10
2599	2021-03-24 06:54:55.372306-05	36.6000022888183594	11
2600	2021-03-24 06:54:58.402394-05	25.1000003814697266	10
2601	2021-03-24 06:54:59.397262-05	36.6000022888183594	11
2602	2021-03-24 06:55:02.404671-05	25.1000003814697266	10
2603	2021-03-24 06:55:03.403948-05	36.6000022888183594	11
2604	2021-03-24 06:55:06.410248-05	25.1000003814697266	10
2605	2021-03-24 06:55:07.410445-05	36.6000022888183594	11
2606	2021-03-24 06:55:10.413216-05	25.1000003814697266	10
2607	2021-03-24 06:55:11.414653-05	36.6000022888183594	11
2608	2021-03-24 06:55:14.406991-05	25.1000003814697266	10
2609	2021-03-24 06:55:15.425504-05	36.6000022888183594	11
2610	2021-03-24 06:55:18.39444-05	25.1000003814697266	10
2611	2021-03-24 06:55:19.424074-05	36.6000022888183594	11
2612	2021-03-24 06:55:22.411722-05	25.1000003814697266	10
2613	2021-03-24 06:55:23.422589-05	36.6000022888183594	11
2614	2021-03-24 06:55:26.421526-05	25.1000003814697266	10
2615	2021-03-24 06:55:27.431712-05	36.6000022888183594	11
2616	2021-03-24 06:55:30.424245-05	25.1000003814697266	10
2617	2021-03-24 06:55:31.424916-05	36.6000022888183594	11
2618	2021-03-24 06:55:34.433076-05	25.1000003814697266	10
2619	2021-03-24 06:55:35.443422-05	36.6000022888183594	11
2620	2021-03-24 06:55:38.440194-05	25.1000003814697266	10
2621	2021-03-24 06:55:39.440012-05	36.6000022888183594	11
2622	2021-03-24 06:55:42.448526-05	25.1000003814697266	10
2623	2021-03-24 06:55:43.441103-05	36.6000022888183594	11
2624	2021-03-24 06:55:46.447235-05	25.1000003814697266	10
2625	2021-03-24 06:55:47.452667-05	36.6000022888183594	11
2626	2021-03-24 06:55:50.453677-05	25.1000003814697266	10
2627	2021-03-24 06:55:51.450401-05	36.6000022888183594	11
2628	2021-03-24 06:55:54.471911-05	25.1000003814697266	10
2629	2021-03-24 06:55:55.460853-05	36.6000022888183594	11
2630	2021-03-24 06:55:58.463608-05	25.1000003814697266	10
2631	2021-03-24 06:55:59.457068-05	36.6000022888183594	11
2632	2021-03-24 06:56:02.471479-05	25.1000003814697266	10
2633	2021-03-24 06:56:03.475501-05	36.6000022888183594	11
2634	2021-03-24 06:56:06.475556-05	25.1000003814697266	10
2635	2021-03-24 06:56:07.472446-05	36.6000022888183594	11
2636	2021-03-24 06:56:10.487764-05	25.1000003814697266	10
2637	2021-03-24 06:56:11.481055-05	36.6000022888183594	11
2638	2021-03-24 06:56:14.484882-05	25.1000003814697266	10
2639	2021-03-24 06:56:15.48787-05	36.6000022888183594	11
2640	2021-03-24 06:56:18.494926-05	25.1000003814697266	10
2641	2021-03-24 06:56:19.487172-05	36.6000022888183594	11
2642	2021-03-24 06:56:22.496035-05	25.1000003814697266	10
2643	2021-03-24 06:56:23.494895-05	36.6000022888183594	11
2644	2021-03-24 06:56:26.499089-05	25.1000003814697266	10
2645	2021-03-24 06:56:27.472709-05	36.6000022888183594	11
2646	2021-03-24 06:56:30.494972-05	25.1000003814697266	10
2647	2021-03-24 06:56:31.493815-05	36.6000022888183594	11
2648	2021-03-24 06:56:34.501589-05	25.1000003814697266	10
2649	2021-03-24 06:56:35.503125-05	36.6000022888183594	11
2650	2021-03-24 06:56:38.514901-05	25.1000003814697266	10
2651	2021-03-24 06:56:39.51376-05	36.6000022888183594	11
2652	2021-03-24 06:56:42.515851-05	25.1000003814697266	10
2653	2021-03-24 06:56:43.519445-05	36.6000022888183594	11
2654	2021-03-24 06:56:46.514028-05	25.1000003814697266	10
2655	2021-03-24 06:56:47.528048-05	36.6000022888183594	11
2656	2021-03-24 06:56:50.515723-05	25.1000003814697266	10
2657	2021-03-24 06:56:51.527008-05	36.6000022888183594	11
2658	2021-03-24 06:56:54.512388-05	25.1000003814697266	10
2659	2021-03-24 06:56:55.528713-05	36.6000022888183594	11
2660	2021-03-24 06:56:58.530775-05	25.1000003814697266	10
2661	2021-03-24 06:56:59.534104-05	36.6000022888183594	11
2662	2021-03-24 06:57:02.540896-05	25.1000003814697266	10
2663	2021-03-24 06:57:03.52888-05	36.6000022888183594	11
2664	2021-03-24 06:57:06.539261-05	25.1000003814697266	10
2665	2021-03-24 06:57:07.550102-05	36.6000022888183594	11
2666	2021-03-24 06:57:10.544242-05	25.1000003814697266	10
2667	2021-03-24 06:57:11.54513-05	36.6000022888183594	11
2668	2021-03-24 06:57:14.552211-05	25.1000003814697266	10
2669	2021-03-24 06:57:15.553356-05	36.5	11
2670	2021-03-24 06:57:18.556183-05	25.1000003814697266	10
2671	2021-03-24 06:57:19.556189-05	36.6000022888183594	11
2672	2021-03-24 06:57:22.555997-05	25.1000003814697266	10
2673	2021-03-24 06:57:23.565537-05	36.5	11
2674	2021-03-24 06:57:26.555442-05	25.1000003814697266	10
2675	2021-03-24 06:57:27.559394-05	36.6000022888183594	11
2676	2021-03-24 06:57:30.568859-05	25.1000003814697266	10
2677	2021-03-24 06:57:31.565083-05	36.6000022888183594	11
2678	2021-03-24 06:57:34.584314-05	25.1000003814697266	10
2679	2021-03-24 06:57:35.575295-05	36.5	11
2680	2021-03-24 06:57:38.570334-05	25.1000003814697266	10
2681	2021-03-24 06:57:39.571476-05	36.5	11
2682	2021-03-24 06:57:42.577849-05	25.1000003814697266	10
2683	2021-03-24 06:57:43.584409-05	36.6000022888183594	11
2684	2021-03-24 06:57:46.589935-05	25.1000003814697266	10
2685	2021-03-24 06:57:47.580832-05	36.6000022888183594	11
2686	2021-03-24 06:57:50.587457-05	25.1000003814697266	10
2687	2021-03-24 06:57:51.596348-05	36.5	11
2688	2021-03-24 06:57:54.595487-05	25.1000003814697266	10
2689	2021-03-24 06:57:55.592063-05	36.5	11
2690	2021-03-24 06:57:58.608139-05	25.1000003814697266	10
2691	2021-03-24 06:57:59.593809-05	36.5	11
2692	2021-03-24 06:58:02.599624-05	25.1000003814697266	10
2693	2021-03-24 06:58:03.574721-05	36.6000022888183594	11
2694	2021-03-24 06:58:06.608521-05	25.1000003814697266	10
2695	2021-03-24 06:58:07.59323-05	36.5	11
2696	2021-03-24 06:58:10.621187-05	25.1000003814697266	10
2697	2021-03-24 06:58:11.613627-05	36.6000022888183594	11
2698	2021-03-24 06:58:14.617114-05	25.1000003814697266	10
2699	2021-03-24 06:58:15.609632-05	36.6000022888183594	11
2700	2021-03-24 06:58:18.616356-05	25.1000003814697266	10
2701	2021-03-24 06:58:19.617791-05	36.6000022888183594	11
2702	2021-03-24 06:58:22.623362-05	25.1000003814697266	10
2703	2021-03-24 06:58:23.616628-05	36.5	11
2704	2021-03-24 06:58:26.61841-05	25.1000003814697266	10
2705	2021-03-24 06:58:27.642818-05	36.5	11
2706	2021-03-24 06:58:30.620599-05	25.1000003814697266	10
2707	2021-03-24 06:58:31.652766-05	36.5	11
2708	2021-03-24 06:58:34.635135-05	25.1000003814697266	10
2709	2021-03-24 06:58:35.644443-05	36.5	11
2710	2021-03-24 06:58:38.641546-05	25.1000003814697266	10
2711	2021-03-24 06:58:39.646342-05	36.5	11
2712	2021-03-24 06:58:42.652189-05	25.1000003814697266	10
2713	2021-03-24 06:58:43.650071-05	36.5	11
2714	2021-03-24 06:58:46.655273-05	25.1000003814697266	10
2715	2021-03-24 06:58:47.655878-05	36.5	11
2716	2021-03-24 06:58:50.654498-05	25.1000003814697266	10
2717	2021-03-24 06:58:51.66093-05	36.5	11
2718	2021-03-24 06:58:54.669448-05	25.1000003814697266	10
2719	2021-03-24 06:58:55.667846-05	36.5	11
2720	2021-03-24 06:58:58.665672-05	25.1000003814697266	10
2721	2021-03-24 06:58:59.674055-05	36.5	11
2722	2021-03-24 06:59:02.677299-05	25.1000003814697266	10
2723	2021-03-24 06:59:03.671541-05	36.5	11
2724	2021-03-24 06:59:06.678986-05	25.1000003814697266	10
2725	2021-03-24 06:59:07.674199-05	36.5	11
2726	2021-03-24 06:59:10.689046-05	25.1000003814697266	10
2727	2021-03-24 06:59:11.681456-05	36.5	11
2728	2021-03-24 06:59:14.679559-05	25.1000003814697266	10
2729	2021-03-24 06:59:15.685893-05	36.5	11
2730	2021-03-24 06:59:18.681026-05	25.1000003814697266	10
2731	2021-03-24 06:59:19.702179-05	36.5	11
2732	2021-03-24 06:59:22.694342-05	25.1000003814697266	10
2733	2021-03-24 06:59:23.692422-05	36.5	11
2734	2021-03-24 06:59:26.707125-05	25.1000003814697266	10
2735	2021-03-24 06:59:27.702082-05	36.5	11
2736	2021-03-24 06:59:30.717054-05	25.1000003814697266	10
2737	2021-03-24 06:59:31.705811-05	36.5	11
2738	2021-03-24 06:59:34.71774-05	25.1000003814697266	10
2739	2021-03-24 06:59:35.712212-05	36.5	11
2740	2021-03-24 06:59:38.730491-05	25.1000003814697266	10
2741	2021-03-24 06:59:39.710218-05	36.5	11
2742	2021-03-24 06:59:42.71923-05	25.1000003814697266	10
2743	2021-03-24 06:59:43.696146-05	36.5	11
2744	2021-03-24 06:59:46.728498-05	25.1000003814697266	10
2745	2021-03-24 06:59:47.718798-05	36.5	11
2746	2021-03-24 06:59:50.740397-05	25.1000003814697266	10
2747	2021-03-24 06:59:51.741111-05	36.5	11
2748	2021-03-24 06:59:54.746135-05	25.1000003814697266	10
2749	2021-03-24 06:59:55.734753-05	36.5	11
2750	2021-03-24 06:59:58.739815-05	25.1000003814697266	10
2751	2021-03-24 06:59:59.738432-05	36.5	11
2752	2021-03-24 07:00:02.739499-05	25.1000003814697266	10
2753	2021-03-24 07:00:08.746888-05	36.5	11
2754	2021-03-24 07:00:09.012732-05	25.1000003814697266	10
2755	2021-03-24 07:00:09.271186-05	36.5	11
2756	2021-03-24 07:00:15.754301-05	25.1000003814697266	10
2757	2021-03-24 07:00:16.018957-05	36.5	11
2758	2021-03-24 07:00:16.277443-05	25.1000003814697266	10
2759	2021-03-24 07:00:16.527145-05	36.5	11
2760	2021-03-24 07:00:18.769797-05	25.1000003814697266	10
2761	2021-03-24 07:00:19.764068-05	36.5	11
2762	2021-03-24 07:00:22.773694-05	25.1000003814697266	10
2763	2021-03-24 07:00:23.771856-05	36.5	11
2764	2021-03-24 07:00:26.776887-05	25.1000003814697266	10
2765	2021-03-24 07:00:27.781684-05	36.5	11
2766	2021-03-24 07:00:30.774464-05	25.1000003814697266	10
2767	2021-03-24 07:00:31.772236-05	36.5	11
2768	2021-03-24 07:00:34.772867-05	25.1000003814697266	10
2769	2021-03-24 07:00:35.779412-05	36.5	11
2770	2021-03-24 07:00:38.78482-05	25.1000003814697266	10
2771	2021-03-24 07:00:39.791787-05	36.5	11
2772	2021-03-24 07:00:42.789914-05	25.1000003814697266	10
2773	2021-03-24 07:00:43.792532-05	36.5	11
2774	2021-03-24 07:00:46.79146-05	25.1000003814697266	10
2775	2021-03-24 07:00:47.795869-05	36.5	11
2776	2021-03-24 07:00:50.800431-05	25.1000003814697266	10
2777	2021-03-24 07:00:51.799817-05	36.5	11
2778	2021-03-24 07:00:54.802234-05	25.1000003814697266	10
2779	2021-03-24 07:00:55.810332-05	36.5	11
2780	2021-03-24 07:00:58.806326-05	25.1000003814697266	10
2781	2021-03-24 07:00:59.806174-05	36.5	11
2782	2021-03-24 07:01:02.811982-05	25.1000003814697266	10
2783	2021-03-24 07:01:03.813695-05	36.5	11
2784	2021-03-24 07:01:06.823347-05	25.1000003814697266	10
2785	2021-03-24 07:01:07.817889-05	36.4000015258789062	11
2786	2021-03-24 07:01:10.823728-05	25.1000003814697266	10
2787	2021-03-24 07:01:11.834985-05	36.5	11
2788	2021-03-24 07:01:14.816273-05	25.1000003814697266	10
2789	2021-03-24 07:01:15.808463-05	36.5	11
2790	2021-03-24 07:01:18.82547-05	25.1000003814697266	10
2791	2021-03-24 07:01:19.823072-05	36.5	11
2792	2021-03-24 07:01:22.838633-05	25.1000003814697266	10
2793	2021-03-24 07:01:23.833778-05	36.4000015258789062	11
2794	2021-03-24 07:01:26.839687-05	25.1000003814697266	10
2795	2021-03-24 07:01:27.841239-05	36.4000015258789062	11
2796	2021-03-24 07:01:30.849325-05	25.1000003814697266	10
2797	2021-03-24 07:01:31.840909-05	36.4000015258789062	11
2798	2021-03-24 07:01:34.852033-05	25.1000003814697266	10
2799	2021-03-24 07:01:35.840976-05	36.4000015258789062	11
2800	2021-03-24 07:01:38.831418-05	25.1000003814697266	10
2801	2021-03-24 07:01:39.85458-05	36.4000015258789062	11
2802	2021-03-24 07:01:42.860253-05	25.1000003814697266	10
2803	2021-03-24 07:01:43.857692-05	36.4000015258789062	11
2804	2021-03-24 07:01:46.861598-05	25.1000003814697266	10
2805	2021-03-24 07:01:47.863275-05	36.4000015258789062	11
2806	2021-03-24 07:01:50.866674-05	25.1000003814697266	10
2807	2021-03-24 07:01:51.865289-05	36.4000015258789062	11
2808	2021-03-24 07:01:54.869804-05	25.1000003814697266	10
2809	2021-03-24 07:01:55.869319-05	36.4000015258789062	11
2810	2021-03-24 07:01:58.875336-05	25.1000003814697266	10
2811	2021-03-24 07:01:59.869812-05	36.4000015258789062	11
2812	2021-03-24 07:02:02.88443-05	25.1000003814697266	10
2813	2021-03-24 07:02:03.881421-05	36.4000015258789062	11
2814	2021-03-24 07:02:06.873857-05	25.1000003814697266	10
2815	2021-03-24 07:02:07.889361-05	36.4000015258789062	11
2816	2021-03-24 07:02:10.894035-05	25.1000003814697266	10
2817	2021-03-24 07:02:11.89595-05	36.4000015258789062	11
2818	2021-03-24 07:02:14.899862-05	25.1000003814697266	10
2819	2021-03-24 07:02:15.892949-05	36.4000015258789062	11
2820	2021-03-24 07:02:18.89877-05	25.1000003814697266	10
2821	2021-03-24 07:02:19.899415-05	36.4000015258789062	11
2822	2021-03-24 07:02:22.898895-05	25.1000003814697266	10
2823	2021-03-24 07:02:23.906091-05	36.4000015258789062	11
2824	2021-03-24 07:02:26.918815-05	25.1000003814697266	10
2825	2021-03-24 07:02:27.910264-05	36.4000015258789062	11
2826	2021-03-24 07:02:30.90928-05	25.1000003814697266	10
2827	2021-03-24 07:02:31.914572-05	36.4000015258789062	11
2828	2021-03-24 07:02:34.913028-05	25.1000003814697266	10
2829	2021-03-24 07:02:35.906746-05	36.4000015258789062	11
2830	2021-03-24 07:02:38.911956-05	25.1000003814697266	10
2831	2021-03-24 07:02:39.914661-05	36.4000015258789062	11
2832	2021-03-24 07:02:42.917208-05	25.1000003814697266	10
2833	2021-03-24 07:02:43.929163-05	36.4000015258789062	11
2834	2021-03-24 07:02:46.930415-05	25.1000003814697266	10
2835	2021-03-24 07:02:47.922431-05	36.4000015258789062	11
2836	2021-03-24 07:02:50.928706-05	25.1000003814697266	10
2837	2021-03-24 07:02:51.908994-05	36.4000015258789062	11
2838	2021-03-24 07:02:54.947296-05	25.1000003814697266	10
2839	2021-03-24 07:02:55.946358-05	36.4000015258789062	11
2840	2021-03-24 07:02:58.951409-05	25.1000003814697266	10
2841	2021-03-24 07:02:59.935112-05	36.4000015258789062	11
2842	2021-03-24 07:03:02.955274-05	25.1000003814697266	10
2843	2021-03-24 07:03:03.94348-05	36.4000015258789062	11
2844	2021-03-24 07:03:06.961039-05	25.1000003814697266	10
2845	2021-03-24 07:03:07.959286-05	36.4000015258789062	11
2846	2021-03-24 07:03:10.950399-05	25.1000003814697266	10
2847	2021-03-24 07:03:11.98644-05	36.4000015258789062	11
2848	2021-03-24 07:03:14.952813-05	25.1000003814697266	10
2849	2021-03-24 07:03:15.975198-05	36.4000015258789062	11
2850	2021-03-24 07:03:18.971928-05	25.1000003814697266	10
2851	2021-03-24 07:03:19.974149-05	36.4000015258789062	11
2852	2021-03-24 07:03:22.980402-05	25.1000003814697266	10
2853	2021-03-24 07:03:23.970498-05	36.4000015258789062	11
2854	2021-03-24 07:03:26.980356-05	25.1000003814697266	10
2855	2021-03-24 07:03:28.002495-05	36.4000015258789062	11
2856	2021-03-24 07:03:31.002508-05	25.1000003814697266	10
2857	2021-03-24 07:03:32.010974-05	36.4000015258789062	11
2858	2021-03-24 07:03:35.005908-05	25.1000003814697266	10
2859	2021-03-24 07:03:36.010398-05	36.4000015258789062	11
2860	2021-03-24 07:03:39.013292-05	25.1000003814697266	10
2861	2021-03-24 07:03:40.019638-05	36.4000015258789062	11
2862	2021-03-24 07:03:43.02868-05	25.1000003814697266	10
2863	2021-03-24 07:03:44.020863-05	36.4000015258789062	11
2864	2021-03-24 07:03:47.029185-05	25.1000003814697266	10
2865	2021-03-24 07:03:48.028536-05	36.4000015258789062	11
2866	2021-03-24 07:03:51.028912-05	25.1000003814697266	10
2867	2021-03-24 07:03:52.017486-05	36.4000015258789062	11
2868	2021-03-24 07:03:55.028571-05	25.1000003814697266	10
2869	2021-03-24 07:03:56.038354-05	36.4000015258789062	11
2870	2021-03-24 07:03:59.038674-05	25.1000003814697266	10
2871	2021-03-24 07:04:00.046506-05	36.4000015258789062	11
2872	2021-03-24 07:04:03.037907-05	25.1000003814697266	10
2873	2021-03-24 07:04:04.041734-05	36.4000015258789062	11
2874	2021-03-24 07:04:07.039722-05	25.1000003814697266	10
2875	2021-03-24 07:04:08.047333-05	36.4000015258789062	11
2876	2021-03-24 07:04:11.0467-05	25.2000007629394531	10
2877	2021-03-24 07:04:12.05025-05	36.4000015258789062	11
2878	2021-03-24 07:04:15.049338-05	25.1000003814697266	10
2879	2021-03-24 07:04:16.049258-05	36.4000015258789062	11
2880	2021-03-24 07:04:19.05415-05	25.2000007629394531	10
2881	2021-03-24 07:04:20.060465-05	36.4000015258789062	11
2882	2021-03-24 07:04:23.074855-05	25.1000003814697266	10
2883	2021-03-24 07:04:24.039451-05	36.4000015258789062	11
2884	2021-03-24 07:04:27.079691-05	25.1000003814697266	10
2885	2021-03-24 07:04:28.062094-05	36.4000015258789062	11
2886	2021-03-24 07:04:31.073501-05	25.2000007629394531	10
2887	2021-03-24 07:04:32.062111-05	36.4000015258789062	11
2888	2021-03-24 07:04:35.078439-05	25.1000003814697266	10
2889	2021-03-24 07:04:36.094653-05	36.4000015258789062	11
2890	2021-03-24 07:04:39.085013-05	25.1000003814697266	10
2891	2021-03-24 07:04:40.087729-05	36.4000015258789062	11
2892	2021-03-24 07:04:43.069495-05	25.1000003814697266	10
2893	2021-03-24 07:04:44.082888-05	36.4000015258789062	11
2894	2021-03-24 07:04:47.082746-05	25.2000007629394531	10
2895	2021-03-24 07:04:48.093497-05	36.4000015258789062	11
2896	2021-03-24 07:04:51.092312-05	25.2000007629394531	10
2897	2021-03-24 07:04:52.103161-05	36.4000015258789062	11
2898	2021-03-24 07:04:55.103791-05	25.2000007629394531	10
2899	2021-03-24 07:04:56.099997-05	36.4000015258789062	11
2900	2021-03-24 07:04:59.10049-05	25.2000007629394531	10
2901	2021-03-24 07:05:00.106407-05	36.4000015258789062	11
2902	2021-03-24 07:05:03.10489-05	25.1000003814697266	10
2903	2021-03-24 07:05:04.106907-05	36.4000015258789062	11
2904	2021-03-24 07:05:07.116155-05	25.2000007629394531	10
2905	2021-03-24 07:05:08.112549-05	36.4000015258789062	11
2906	2021-03-24 07:05:11.114369-05	25.2000007629394531	10
2907	2021-03-24 07:05:12.109417-05	36.4000015258789062	11
2908	2021-03-24 07:05:15.119399-05	25.2000007629394531	10
2909	2021-03-24 07:05:16.124696-05	36.4000015258789062	11
2910	2021-03-24 07:05:19.120648-05	25.2000007629394531	10
2911	2021-03-24 07:05:20.12292-05	36.4000015258789062	11
2912	2021-03-24 07:05:23.132648-05	25.2000007629394531	10
2913	2021-03-24 07:05:24.132579-05	36.4000015258789062	11
2914	2021-03-24 07:05:27.135202-05	25.1000003814697266	10
2915	2021-03-24 07:05:28.134576-05	36.4000015258789062	11
2916	2021-03-24 07:05:31.137852-05	25.2000007629394531	10
2917	2021-03-24 07:05:32.1358-05	36.4000015258789062	11
2918	2021-03-24 07:05:35.1395-05	25.1000003814697266	10
2919	2021-03-24 07:05:36.151025-05	36.4000015258789062	11
2920	2021-03-24 07:05:39.144018-05	25.2000007629394531	10
2921	2021-03-24 07:05:40.144881-05	36.4000015258789062	11
2922	2021-03-24 07:05:43.152975-05	25.2000007629394531	10
2923	2021-03-24 07:05:44.156428-05	36.4000015258789062	11
2924	2021-03-24 07:05:47.165597-05	25.1000003814697266	10
2925	2021-03-24 07:05:48.152889-05	36.4000015258789062	11
2926	2021-03-24 07:05:51.168204-05	25.2000007629394531	10
2927	2021-03-24 07:05:52.140287-05	36.4000015258789062	11
2928	2021-03-24 07:05:55.178154-05	25.2000007629394531	10
2929	2021-03-24 07:05:56.179086-05	36.4000015258789062	11
2930	2021-03-24 07:05:59.172888-05	25.1000003814697266	10
2931	2021-03-24 07:06:00.16982-05	36.4000015258789062	11
2932	2021-03-24 07:06:03.182856-05	25.2000007629394531	10
2933	2021-03-24 07:06:04.176353-05	36.4000015258789062	11
2934	2021-03-24 07:06:07.175273-05	25.2000007629394531	10
2935	2021-03-24 07:06:08.175233-05	36.4000015258789062	11
2936	2021-03-24 07:06:11.180993-05	25.1000003814697266	10
2937	2021-03-24 07:06:12.190972-05	36.4000015258789062	11
2938	2021-03-24 07:06:15.16554-05	25.2000007629394531	10
2939	2021-03-24 07:06:16.195181-05	36.4000015258789062	11
2940	2021-03-24 07:06:19.202759-05	25.2000007629394531	10
2941	2021-03-24 07:06:20.195298-05	36.4000015258789062	11
2942	2021-03-24 07:06:23.202616-05	25.2000007629394531	10
2943	2021-03-24 07:06:24.19231-05	36.4000015258789062	11
2944	2021-03-24 07:06:27.196023-05	25.2000007629394531	10
2945	2021-03-24 07:06:28.208652-05	36.4000015258789062	11
2946	2021-03-24 07:06:31.210971-05	25.2000007629394531	10
2947	2021-03-24 07:06:32.216328-05	36.4000015258789062	11
2948	2021-03-24 07:06:35.207867-05	25.2000007629394531	10
2949	2021-03-24 07:06:36.210975-05	36.4000015258789062	11
2950	2021-03-24 07:06:39.21257-05	25.1000003814697266	10
2951	2021-03-24 07:06:40.215468-05	36.4000015258789062	11
2952	2021-03-24 07:06:43.227264-05	25.2000007629394531	10
2953	2021-03-24 07:06:44.215258-05	36.4000015258789062	11
2954	2021-03-24 07:06:47.22422-05	25.2000007629394531	10
2955	2021-03-24 07:06:48.226591-05	36.4000015258789062	11
2956	2021-03-24 07:06:51.226826-05	25.2000007629394531	10
2957	2021-03-24 07:06:52.232465-05	36.4000015258789062	11
2958	2021-03-24 07:06:55.240492-05	25.2000007629394531	10
2959	2021-03-24 07:06:56.227437-05	36.4000015258789062	11
2960	2021-03-24 07:06:59.235021-05	25.2000007629394531	10
2961	2021-03-24 07:07:00.242605-05	36.4000015258789062	11
2962	2021-03-24 07:07:03.249767-05	25.2000007629394531	10
2963	2021-03-24 07:07:04.239077-05	36.4000015258789062	11
2964	2021-03-24 07:07:07.248387-05	25.2000007629394531	10
2965	2021-03-24 07:07:08.251099-05	36.4000015258789062	11
2966	2021-03-24 07:07:11.251407-05	25.2000007629394531	10
2967	2021-03-24 07:07:12.252828-05	36.4000015258789062	11
2968	2021-03-24 07:07:15.247577-05	25.2000007629394531	10
2969	2021-03-24 07:07:16.261705-05	36.4000015258789062	11
2970	2021-03-24 07:07:19.264434-05	25.2000007629394531	10
2971	2021-03-24 07:07:20.234428-05	36.4000015258789062	11
2972	2021-03-24 07:07:23.271102-05	25.2000007629394531	10
2973	2021-03-24 07:07:24.26219-05	36.4000015258789062	11
2974	2021-03-24 07:07:27.272154-05	25.2000007629394531	10
2975	2021-03-24 07:07:28.271567-05	36.4000015258789062	11
2976	2021-03-24 07:07:31.276846-05	25.2000007629394531	10
2977	2021-03-24 07:07:32.271616-05	36.4000015258789062	11
2978	2021-03-24 07:07:35.27918-05	25.2000007629394531	10
2979	2021-03-24 07:07:36.277555-05	36.4000015258789062	11
2980	2021-03-24 07:07:39.282939-05	25.2000007629394531	10
2981	2021-03-24 07:07:40.281279-05	36.4000015258789062	11
2982	2021-03-24 07:07:43.264763-05	25.2000007629394531	10
2983	2021-03-24 07:07:44.292056-05	36.4000015258789062	11
2984	2021-03-24 07:07:47.274812-05	25.2000007629394531	10
2985	2021-03-24 07:07:48.295365-05	36.4000015258789062	11
2986	2021-03-24 07:07:51.294105-05	25.2000007629394531	10
2987	2021-03-24 07:07:52.295322-05	36.4000015258789062	11
2988	2021-03-24 07:07:55.290058-05	25.2000007629394531	10
2989	2021-03-24 07:07:56.300462-05	36.4000015258789062	11
2990	2021-03-24 07:07:59.302849-05	25.2000007629394531	10
2991	2021-03-24 07:08:00.296493-05	36.4000015258789062	11
2992	2021-03-24 07:08:03.299813-05	25.2000007629394531	10
2993	2021-03-24 07:08:04.310667-05	36.4000015258789062	11
2994	2021-03-24 07:08:07.312522-05	25.2000007629394531	10
2995	2021-03-24 07:08:08.304067-05	36.4000015258789062	11
2996	2021-03-24 07:08:11.32084-05	25.2000007629394531	10
2997	2021-03-24 07:08:12.321452-05	36.4000015258789062	11
2998	2021-03-24 07:08:15.321933-05	25.2000007629394531	10
2999	2021-03-24 07:08:16.323449-05	36.4000015258789062	11
3000	2021-03-24 07:08:19.326895-05	25.2000007629394531	10
3001	2021-03-24 07:08:20.320981-05	36.4000015258789062	11
3002	2021-03-24 07:08:23.332466-05	25.2000007629394531	10
3003	2021-03-24 07:08:24.334818-05	36.4000015258789062	11
3004	2021-03-24 07:08:27.327513-05	25.2000007629394531	10
3005	2021-03-24 07:08:28.334847-05	36.4000015258789062	11
3006	2021-03-24 07:08:31.329019-05	25.2000007629394531	10
3007	2021-03-24 07:08:32.338215-05	36.4000015258789062	11
3008	2021-03-24 07:08:35.352605-05	25.2000007629394531	10
3009	2021-03-24 07:08:36.345815-05	36.4000015258789062	11
3010	2021-03-24 07:08:39.333261-05	25.2000007629394531	10
3011	2021-03-24 07:08:40.349064-05	36.4000015258789062	11
3012	2021-03-24 07:08:43.346113-05	25.2000007629394531	10
3013	2021-03-24 07:08:44.349547-05	36.4000015258789062	11
3014	2021-03-24 07:08:47.351113-05	25.2000007629394531	10
3015	2021-03-24 07:08:48.352071-05	36.4000015258789062	11
3016	2021-03-24 07:08:51.359256-05	25.2000007629394531	10
3017	2021-03-24 07:08:52.338672-05	36.4000015258789062	11
3018	2021-03-24 07:08:55.366156-05	25.2000007629394531	10
3019	2021-03-24 07:08:56.356629-05	36.4000015258789062	11
3020	2021-03-24 07:08:59.361677-05	25.2000007629394531	10
3021	2021-03-24 07:09:00.3597-05	36.4000015258789062	11
3022	2021-03-24 07:09:03.369251-05	25.2000007629394531	10
3023	2021-03-24 07:09:04.374401-05	36.4000015258789062	11
3024	2021-03-24 07:09:07.380101-05	25.2000007629394531	10
3025	2021-03-24 07:09:08.370818-05	36.4000015258789062	11
3026	2021-03-24 07:09:11.3784-05	25.2000007629394531	10
3027	2021-03-24 07:09:12.379906-05	36.4000015258789062	11
3028	2021-03-24 07:09:15.355244-05	25.2000007629394531	10
3029	2021-03-24 07:09:16.378663-05	36.4000015258789062	11
3030	2021-03-24 07:09:19.39482-05	25.2000007629394531	10
3031	2021-03-24 07:09:20.389671-05	36.4000015258789062	11
3032	2021-03-24 07:09:23.385491-05	25.2000007629394531	10
3033	2021-03-24 07:09:24.398634-05	36.4000015258789062	11
3034	2021-03-24 07:09:27.39475-05	25.2000007629394531	10
3035	2021-03-24 07:09:28.403492-05	36.4000015258789062	11
3036	2021-03-24 07:09:31.396661-05	25.2000007629394531	10
3037	2021-03-24 07:09:32.399101-05	36.4000015258789062	11
3038	2021-03-24 07:09:35.397625-05	25.2000007629394531	10
3039	2021-03-24 07:09:36.409127-05	36.5	11
3040	2021-03-24 07:09:39.407462-05	25.2000007629394531	10
3041	2021-03-24 07:09:40.411231-05	36.4000015258789062	11
3042	2021-03-24 07:09:43.421571-05	25.2000007629394531	10
3043	2021-03-24 07:09:44.418652-05	36.4000015258789062	11
3044	2021-03-24 07:09:47.407041-05	25.2000007629394531	10
3045	2021-03-24 07:09:48.422283-05	36.4000015258789062	11
3046	2021-03-24 07:09:51.418757-05	25.2000007629394531	10
3047	2021-03-24 07:09:52.416924-05	36.5	11
3048	2021-03-24 07:09:55.419926-05	25.2000007629394531	10
3049	2021-03-24 07:09:56.419172-05	36.5	11
3050	2021-03-24 07:09:59.426976-05	25.2000007629394531	10
3051	2021-03-24 07:10:00.424918-05	36.5	11
3052	2021-03-24 07:10:03.42654-05	25.2000007629394531	10
3053	2021-03-24 07:10:04.435524-05	36.5	11
3054	2021-03-24 07:10:07.43382-05	25.2000007629394531	10
3055	2021-03-24 07:10:08.443992-05	36.4000015258789062	11
3056	2021-03-24 07:10:11.443081-05	25.2000007629394531	10
3057	2021-03-24 07:10:12.441419-05	36.4000015258789062	11
3058	2021-03-24 07:10:15.449632-05	25.2000007629394531	10
3059	2021-03-24 07:10:16.443014-05	36.5	11
3060	2021-03-24 07:10:19.457024-05	25.2000007629394531	10
3061	2021-03-24 07:10:20.453533-05	36.4000015258789062	11
3062	2021-03-24 07:10:23.458033-05	25.2000007629394531	10
3063	2021-03-24 07:10:24.446018-05	36.4000015258789062	11
3064	2021-03-24 07:10:27.45587-05	25.2000007629394531	10
3065	2021-03-24 07:10:28.456148-05	36.4000015258789062	11
3066	2021-03-24 07:10:31.463055-05	25.2000007629394531	10
3067	2021-03-24 07:10:32.464346-05	36.4000015258789062	11
3068	2021-03-24 07:10:35.471954-05	25.2000007629394531	10
3069	2021-03-24 07:10:36.479223-05	36.4000015258789062	11
3070	2021-03-24 07:10:39.47235-05	25.2000007629394531	10
3071	2021-03-24 07:10:40.475402-05	36.4000015258789062	11
3072	2021-03-24 07:10:43.464168-05	25.2000007629394531	10
3073	2021-03-24 07:10:44.484845-05	36.4000015258789062	11
3074	2021-03-24 07:10:47.461684-05	25.2000007629394531	10
3075	2021-03-24 07:10:48.486642-05	36.4000015258789062	11
3076	2021-03-24 07:10:51.493585-05	25.2000007629394531	10
3077	2021-03-24 07:10:52.492589-05	36.4000015258789062	11
3078	2021-03-24 07:10:55.495097-05	25.2000007629394531	10
3079	2021-03-24 07:10:56.497004-05	36.4000015258789062	11
3080	2021-03-24 07:10:59.500501-05	25.2000007629394531	10
3081	2021-03-24 07:11:00.493709-05	36.4000015258789062	11
3082	2021-03-24 07:11:03.504363-05	25.2000007629394531	10
3083	2021-03-24 07:11:04.503678-05	36.4000015258789062	11
3084	2021-03-24 07:11:07.510248-05	25.2000007629394531	10
3085	2021-03-24 07:11:08.528005-05	36.4000015258789062	11
3086	2021-03-24 07:11:11.517014-05	25.2000007629394531	10
3087	2021-03-24 07:11:12.516172-05	36.4000015258789062	11
3088	2021-03-24 07:11:15.523758-05	25.2000007629394531	10
3089	2021-03-24 07:11:16.522902-05	36.4000015258789062	11
3090	2021-03-24 07:11:19.522672-05	25.2000007629394531	10
3091	2021-03-24 07:11:20.51703-05	36.4000015258789062	11
3092	2021-03-24 07:11:23.540294-05	25.2000007629394531	10
3093	2021-03-24 07:11:24.528448-05	36.4000015258789062	11
3094	2021-03-24 07:11:27.534108-05	25.2000007629394531	10
3095	2021-03-24 07:11:28.536161-05	36.4000015258789062	11
3096	2021-03-24 07:11:31.530502-05	25.2000007629394531	10
3097	2021-03-24 07:11:32.53313-05	36.4000015258789062	11
3098	2021-03-24 07:11:35.553633-05	25.2000007629394531	10
3099	2021-03-24 07:11:36.530208-05	36.4000015258789062	11
3100	2021-03-24 07:11:39.540881-05	25.2000007629394531	10
3101	2021-03-24 07:11:40.545635-05	36.4000015258789062	11
3102	2021-03-24 07:11:43.541118-05	25.2000007629394531	10
3103	2021-03-24 07:11:44.53926-05	36.4000015258789062	11
3104	2021-03-24 07:11:47.558338-05	25.2000007629394531	10
3105	2021-03-24 07:11:48.549584-05	36.4000015258789062	11
3106	2021-03-24 07:11:51.566233-05	25.2000007629394531	10
3107	2021-03-24 07:11:52.524944-05	36.4000015258789062	11
3108	2021-03-24 07:11:55.579819-05	25.2000007629394531	10
3109	2021-03-24 07:11:56.563757-05	36.4000015258789062	11
3110	2021-03-24 07:11:59.56864-05	25.2000007629394531	10
3111	2021-03-24 07:12:00.568991-05	36.4000015258789062	11
3112	2021-03-24 07:12:03.582404-05	25.2000007629394531	10
3113	2021-03-24 07:12:04.567021-05	36.4000015258789062	11
3114	2021-03-24 07:12:07.582763-05	25.2000007629394531	10
3115	2021-03-24 07:12:08.578319-05	36.4000015258789062	11
3116	2021-03-24 07:12:11.571994-05	25.2000007629394531	10
3117	2021-03-24 07:12:12.589519-05	36.4000015258789062	11
3118	2021-03-24 07:12:15.578081-05	25.2000007629394531	10
3119	2021-03-24 07:12:16.579084-05	36.4000015258789062	11
3120	2021-03-24 07:12:19.59944-05	25.2000007629394531	10
3121	2021-03-24 07:12:20.596278-05	36.4000015258789062	11
3122	2021-03-24 07:12:23.594592-05	25.2000007629394531	10
3123	2021-03-24 07:12:24.599308-05	36.4000015258789062	11
3124	2021-03-24 07:12:27.594196-05	25.2000007629394531	10
3125	2021-03-24 07:12:28.604642-05	36.4000015258789062	11
3126	2021-03-24 07:12:31.606697-05	25.2000007629394531	10
3127	2021-03-24 07:12:32.607749-05	36.4000015258789062	11
3128	2021-03-24 07:12:35.612231-05	25.2000007629394531	10
3129	2021-03-24 07:12:36.606561-05	36.4000015258789062	11
3130	2021-03-24 07:12:39.610511-05	25.2000007629394531	10
3131	2021-03-24 07:12:40.619023-05	36.4000015258789062	11
3132	2021-03-24 07:12:43.627514-05	25.2000007629394531	10
3133	2021-03-24 07:12:44.615494-05	36.4000015258789062	11
3134	2021-03-24 07:12:47.61657-05	25.2000007629394531	10
3135	2021-03-24 07:12:48.626794-05	36.4000015258789062	11
3136	2021-03-24 07:12:51.625246-05	25.2000007629394531	10
3137	2021-03-24 07:12:52.624648-05	36.4000015258789062	11
3138	2021-03-24 07:12:55.630228-05	25.2000007629394531	10
3139	2021-03-24 07:12:56.635051-05	36.4000015258789062	11
3140	2021-03-24 07:12:59.639405-05	25.2000007629394531	10
3141	2021-03-24 07:13:00.64314-05	36.4000015258789062	11
3142	2021-03-24 07:13:03.648515-05	25.2000007629394531	10
3143	2021-03-24 07:13:04.644435-05	36.4000015258789062	11
3144	2021-03-24 07:13:07.646744-05	25.2000007629394531	10
3145	2021-03-24 07:13:08.641107-05	36.4000015258789062	11
3146	2021-03-24 07:13:11.652115-05	25.2000007629394531	10
3147	2021-03-24 07:13:12.653871-05	36.4000015258789062	11
3148	2021-03-24 07:13:15.657764-05	25.2000007629394531	10
3149	2021-03-24 07:13:16.656285-05	36.4000015258789062	11
3150	2021-03-24 07:13:19.670655-05	25.2000007629394531	10
3151	2021-03-24 07:13:20.674761-05	36.2999992370605469	11
3152	2021-03-24 07:13:23.66889-05	25.2000007629394531	10
3153	2021-03-24 07:13:24.671903-05	36.4000015258789062	11
3154	2021-03-24 07:13:27.674204-05	25.2000007629394531	10
3155	2021-03-24 07:13:28.671553-05	36.4000015258789062	11
3156	2021-03-24 07:13:31.676493-05	25.2000007629394531	10
3157	2021-03-24 07:13:32.671936-05	36.4000015258789062	11
3158	2021-03-24 07:13:35.686289-05	25.2000007629394531	10
3159	2021-03-24 07:13:36.672898-05	36.4000015258789062	11
3160	2021-03-24 07:13:39.689948-05	25.2000007629394531	10
3161	2021-03-24 07:13:40.689095-05	36.4000015258789062	11
3162	2021-03-24 07:13:43.658865-05	25.2000007629394531	10
3163	2021-03-24 07:13:44.688403-05	36.4000015258789062	11
3164	2021-03-24 07:13:47.694973-05	25.2000007629394531	10
3165	2021-03-24 07:13:48.697232-05	36.4000015258789062	11
3166	2021-03-24 07:13:51.701823-05	25.2000007629394531	10
3167	2021-03-24 07:13:52.70078-05	36.4000015258789062	11
3168	2021-03-24 07:13:55.699021-05	25.2000007629394531	10
3169	2021-03-24 07:13:56.705631-05	36.4000015258789062	11
3170	2021-03-24 07:13:59.703206-05	25.2000007629394531	10
3171	2021-03-24 07:14:00.715256-05	36.4000015258789062	11
3172	2021-03-24 07:14:03.711733-05	25.2000007629394531	10
3173	2021-03-24 07:14:04.710942-05	36.4000015258789062	11
3174	2021-03-24 07:14:07.713699-05	25.2000007629394531	10
3175	2021-03-24 07:14:08.710816-05	36.4000015258789062	11
3176	2021-03-24 07:14:11.719026-05	25.2000007629394531	10
3177	2021-03-24 07:14:12.730252-05	36.4000015258789062	11
3178	2021-03-24 07:14:15.726333-05	25.2000007629394531	10
3179	2021-03-24 07:14:16.732348-05	36.4000015258789062	11
3180	2021-03-24 07:14:19.735227-05	25.2000007629394531	10
3181	2021-03-24 07:14:20.729931-05	36.4000015258789062	11
3182	2021-03-24 07:14:23.735676-05	25.2000007629394531	10
3183	2021-03-24 07:14:24.737578-05	36.4000015258789062	11
3184	2021-03-24 07:14:27.733339-05	25.2000007629394531	10
3185	2021-03-24 07:14:28.732351-05	36.4000015258789062	11
3186	2021-03-24 07:14:31.749625-05	25.2000007629394531	10
3187	2021-03-24 07:14:32.743958-05	36.4000015258789062	11
3188	2021-03-24 07:14:35.749406-05	25.2000007629394531	10
3189	2021-03-24 07:14:36.745604-05	36.4000015258789062	11
3190	2021-03-24 07:14:39.756078-05	25.2000007629394531	10
3191	2021-03-24 07:14:40.748873-05	36.4000015258789062	11
3192	2021-03-24 07:14:43.758488-05	25.2000007629394531	10
3193	2021-03-24 07:14:44.753575-05	36.4000015258789062	11
3194	2021-03-24 07:14:47.767053-05	25.2000007629394531	10
3195	2021-03-24 07:14:48.778679-05	36.4000015258789062	11
3196	2021-03-24 07:14:51.772258-05	25.2000007629394531	10
3197	2021-03-24 07:14:52.749256-05	36.4000015258789062	11
3198	2021-03-24 07:14:55.765154-05	25.2000007629394531	10
3199	2021-03-24 07:14:56.781634-05	36.4000015258789062	11
3200	2021-03-24 07:14:59.777972-05	25.2000007629394531	10
3201	2021-03-24 07:15:00.771689-05	36.4000015258789062	11
3202	2021-03-24 07:15:03.78186-05	25.2000007629394531	10
3203	2021-03-24 07:15:04.78025-05	36.2999992370605469	11
3204	2021-03-24 07:15:07.786346-05	25.2000007629394531	10
3205	2021-03-24 07:15:08.780192-05	36.2999992370605469	11
3206	2021-03-24 07:15:11.772102-05	25.2000007629394531	10
3207	2021-03-24 07:15:12.785341-05	36.4000015258789062	11
3208	2021-03-24 07:15:15.794234-05	25.2000007629394531	10
3209	2021-03-24 07:15:16.802463-05	36.4000015258789062	11
3210	2021-03-24 07:15:19.81136-05	25.2000007629394531	10
3211	2021-03-24 07:15:20.80759-05	36.2999992370605469	11
3212	2021-03-24 07:15:23.809137-05	25.2000007629394531	10
3213	2021-03-24 07:15:24.808571-05	36.4000015258789062	11
3214	2021-03-24 07:15:27.816436-05	25.2000007629394531	10
3215	2021-03-24 07:15:28.814108-05	36.4000015258789062	11
3216	2021-03-24 07:15:31.82028-05	25.2000007629394531	10
3217	2021-03-24 07:15:32.809161-05	36.4000015258789062	11
3218	2021-03-24 07:15:35.818023-05	25.2000007629394531	10
3219	2021-03-24 07:15:36.814992-05	36.2999992370605469	11
3220	2021-03-24 07:15:39.827793-05	25.2000007629394531	10
3221	2021-03-24 07:15:40.826104-05	36.4000015258789062	11
3222	2021-03-24 07:15:43.83717-05	25.2000007629394531	10
3223	2021-03-24 07:15:44.826322-05	36.4000015258789062	11
3224	2021-03-24 07:15:47.844822-05	25.2000007629394531	10
3225	2021-03-24 07:15:48.839001-05	36.4000015258789062	11
3226	2021-03-24 07:15:51.832874-05	25.2000007629394531	10
3227	2021-03-24 07:15:52.837365-05	36.2999992370605469	11
3228	2021-03-24 07:15:55.853564-05	25.2000007629394531	10
3229	2021-03-24 07:15:56.852196-05	36.4000015258789062	11
3230	2021-03-24 07:15:59.851317-05	25.2000007629394531	10
3231	2021-03-24 07:16:00.858092-05	36.4000015258789062	11
3232	2021-03-24 07:16:03.849847-05	25.2000007629394531	10
3233	2021-03-24 07:16:04.856645-05	36.4000015258789062	11
3234	2021-03-24 07:16:07.860078-05	25.2000007629394531	10
3235	2021-03-24 07:16:08.86784-05	36.4000015258789062	11
3236	2021-03-24 07:16:11.866735-05	25.2000007629394531	10
3237	2021-03-24 07:16:12.868101-05	36.4000015258789062	11
3238	2021-03-24 07:16:15.875083-05	25.2000007629394531	10
3239	2021-03-24 07:16:16.852338-05	36.4000015258789062	11
3240	2021-03-24 07:16:19.878732-05	25.2000007629394531	10
3241	2021-03-24 07:16:20.865722-05	36.4000015258789062	11
3242	2021-03-24 07:16:23.891241-05	25.2000007629394531	10
3243	2021-03-24 07:16:24.870463-05	36.4000015258789062	11
3244	2021-03-24 07:16:27.889684-05	25.2000007629394531	10
3245	2021-03-24 07:16:28.880371-05	36.4000015258789062	11
3246	2021-03-24 07:16:31.895097-05	25.2000007629394531	10
3247	2021-03-24 07:16:32.875603-05	36.2999992370605469	11
3248	2021-03-24 07:16:35.893853-05	25.2000007629394531	10
3249	2021-03-24 07:16:36.886605-05	36.2999992370605469	11
3250	2021-03-24 07:16:39.868134-05	25.2000007629394531	10
3251	2021-03-24 07:16:40.894166-05	36.4000015258789062	11
3252	2021-03-24 07:16:43.902613-05	25.2000007629394531	10
3253	2021-03-24 07:16:44.898428-05	36.4000015258789062	11
3254	2021-03-24 07:16:47.917103-05	25.2000007629394531	10
3255	2021-03-24 07:16:48.904373-05	36.2999992370605469	11
3256	2021-03-24 07:16:51.907255-05	25.2000007629394531	10
3257	2021-03-24 07:16:52.902656-05	36.4000015258789062	11
3258	2021-03-24 07:16:55.91206-05	25.2000007629394531	10
3259	2021-03-24 07:16:56.912924-05	36.2999992370605469	11
3260	2021-03-24 07:16:59.916964-05	25.2000007629394531	10
3261	2021-03-24 07:17:00.915897-05	36.2999992370605469	11
3262	2021-03-24 07:17:03.919893-05	25.2000007629394531	10
3263	2021-03-24 07:17:04.924199-05	36.2999992370605469	11
3264	2021-03-24 07:17:07.930409-05	25.2000007629394531	10
3265	2021-03-24 07:17:08.927688-05	36.2999992370605469	11
3266	2021-03-24 07:17:11.930906-05	25.2000007629394531	10
3267	2021-03-24 07:17:12.926366-05	36.2999992370605469	11
3268	2021-03-24 07:17:15.939268-05	25.2000007629394531	10
3269	2021-03-24 07:17:16.935213-05	36.2999992370605469	11
3270	2021-03-24 07:17:19.939688-05	25.2000007629394531	10
3271	2021-03-24 07:17:20.934788-05	36.2999992370605469	11
3272	2021-03-24 07:17:23.945107-05	25.2000007629394531	10
3273	2021-03-24 07:17:24.930094-05	36.2999992370605469	11
3274	2021-03-24 07:17:27.940142-05	25.2000007629394531	10
3275	2021-03-24 07:17:28.944067-05	36.2999992370605469	11
3276	2021-03-24 07:17:31.941776-05	25.2000007629394531	10
3277	2021-03-24 07:17:32.935165-05	36.2999992370605469	11
3278	2021-03-24 07:17:35.944713-05	25.2000007629394531	10
3279	2021-03-24 07:17:36.956274-05	36.2999992370605469	11
3280	2021-03-24 07:17:39.957787-05	25.2000007629394531	10
3281	2021-03-24 07:17:40.948855-05	36.2999992370605469	11
3282	2021-03-24 07:17:43.966629-05	25.2000007629394531	10
3283	2021-03-24 07:17:44.937104-05	36.2999992370605469	11
3284	2021-03-24 07:17:47.956083-05	25.2000007629394531	10
3285	2021-03-24 07:17:48.95836-05	36.2999992370605469	11
3286	2021-03-24 07:17:51.971127-05	25.2000007629394531	10
3287	2021-03-24 07:17:52.971414-05	36.2999992370605469	11
3288	2021-03-24 07:17:55.972743-05	25.2000007629394531	10
3289	2021-03-24 07:17:56.963721-05	36.2999992370605469	11
3290	2021-03-24 07:17:59.984299-05	25.2000007629394531	10
3291	2021-03-24 07:18:00.980656-05	36.2999992370605469	11
3292	2021-03-24 07:18:03.980354-05	25.2000007629394531	10
3293	2021-03-24 07:18:04.986847-05	36.2999992370605469	11
3294	2021-03-24 07:18:07.963747-05	25.2000007629394531	10
3295	2021-03-24 07:18:08.990907-05	36.2999992370605469	11
3296	2021-03-24 07:18:11.994031-05	25.2000007629394531	10
3297	2021-03-24 07:18:12.998219-05	36.2999992370605469	11
3298	2021-03-24 07:18:16.000601-05	25.2000007629394531	10
3299	2021-03-24 07:18:16.996551-05	36.2999992370605469	11
3300	2021-03-24 07:18:20.008912-05	25.2000007629394531	10
3301	2021-03-24 07:18:21.014953-05	36.2999992370605469	11
3302	2021-03-24 07:18:24.005694-05	25.2000007629394531	10
3303	2021-03-24 07:18:25.010708-05	36.2999992370605469	11
3304	2021-03-24 07:18:28.010448-05	25.2000007629394531	10
3305	2021-03-24 07:18:29.00583-05	36.2999992370605469	11
3306	2021-03-24 07:18:32.01767-05	25.2000007629394531	10
3307	2021-03-24 07:18:33.009788-05	36.2999992370605469	11
3308	2021-03-24 07:18:36.015229-05	25.2000007629394531	10
3309	2021-03-24 07:18:37.016872-05	36.2999992370605469	11
3310	2021-03-24 07:18:40.021884-05	25.2000007629394531	10
3311	2021-03-24 07:18:41.021432-05	36.2999992370605469	11
3312	2021-03-24 07:18:44.017857-05	25.2000007629394531	10
3313	2021-03-24 07:18:45.023729-05	36.2999992370605469	11
3314	2021-03-24 07:18:48.012855-05	25.2000007629394531	10
3315	2021-03-24 07:18:49.020226-05	36.2999992370605469	11
3316	2021-03-24 07:18:52.037776-05	25.2000007629394531	10
3317	2021-03-24 07:18:53.034895-05	36.2999992370605469	11
3318	2021-03-24 07:18:56.037941-05	25.2000007629394531	10
3319	2021-03-24 07:18:57.043565-05	36.2999992370605469	11
3320	2021-03-24 07:19:00.049114-05	25.2000007629394531	10
3321	2021-03-24 07:19:01.042358-05	36.2999992370605469	11
3322	2021-03-24 07:19:04.048502-05	25.2000007629394531	10
3323	2021-03-24 07:19:05.054183-05	36.2999992370605469	11
3324	2021-03-24 07:19:08.051535-05	25.2000007629394531	10
3325	2021-03-24 07:19:09.04304-05	36.2999992370605469	11
3326	2021-03-24 07:19:12.050263-05	25.2000007629394531	10
3327	2021-03-24 07:19:13.03215-05	36.2999992370605469	11
3328	2021-03-24 07:19:16.063251-05	25.2000007629394531	10
3329	2021-03-24 07:19:17.063788-05	36.2999992370605469	11
3330	2021-03-24 07:19:20.072642-05	25.2000007629394531	10
3331	2021-03-24 07:19:21.065488-05	36.2999992370605469	11
3332	2021-03-24 07:19:24.075306-05	25.2000007629394531	10
3333	2021-03-24 07:19:25.072537-05	36.2999992370605469	11
3334	2021-03-24 07:19:28.073247-05	25.2000007629394531	10
3335	2021-03-24 07:19:29.0724-05	36.2999992370605469	11
3336	2021-03-24 07:19:32.059054-05	25.2000007629394531	10
3337	2021-03-24 07:19:33.079451-05	36.2999992370605469	11
3338	2021-03-24 07:19:36.075371-05	25.2000007629394531	10
3339	2021-03-24 07:19:37.087855-05	36.2999992370605469	11
3340	2021-03-24 07:19:40.083449-05	25.2000007629394531	10
3341	2021-03-24 07:19:41.091591-05	36.2999992370605469	11
3342	2021-03-24 07:19:44.097681-05	25.2000007629394531	10
3343	2021-03-24 07:19:45.089555-05	36.2999992370605469	11
3344	2021-03-24 07:19:48.085161-05	25.2000007629394531	10
3345	2021-03-24 07:19:49.097745-05	36.2999992370605469	11
3346	2021-03-24 07:19:52.087386-05	25.2000007629394531	10
3347	2021-03-24 07:19:53.098569-05	36.2999992370605469	11
3348	2021-03-24 07:19:56.099299-05	25.2000007629394531	10
3349	2021-03-24 07:19:57.10515-05	36.2999992370605469	11
3350	2021-03-24 07:20:00.113806-05	25.2000007629394531	10
3351	2021-03-24 07:20:01.108269-05	36.2999992370605469	11
3352	2021-03-24 07:20:04.112237-05	25.2000007629394531	10
3353	2021-03-24 07:20:05.119795-05	36.2999992370605469	11
3354	2021-03-24 07:20:08.126863-05	25.2000007629394531	10
3355	2021-03-24 07:20:09.122235-05	36.2999992370605469	11
3356	2021-03-24 07:20:12.126601-05	25.2000007629394531	10
3357	2021-03-24 07:20:13.123015-05	36.2999992370605469	11
3358	2021-03-24 07:20:16.135995-05	25.2000007629394531	10
3359	2021-03-24 07:20:17.134497-05	36.2999992370605469	11
3360	2021-03-24 07:20:20.132515-05	25.2000007629394531	10
3361	2021-03-24 07:20:21.131468-05	36.2999992370605469	11
3362	2021-03-24 07:20:24.127276-05	25.2000007629394531	10
3363	2021-03-24 07:20:25.132935-05	36.2999992370605469	11
3364	2021-03-24 07:20:28.14933-05	25.2000007629394531	10
3365	2021-03-24 07:20:29.138219-05	36.2999992370605469	11
3366	2021-03-24 07:20:32.145807-05	25.2000007629394531	10
3367	2021-03-24 07:20:33.14308-05	36.2999992370605469	11
3368	2021-03-24 07:20:36.151095-05	25.2000007629394531	10
3369	2021-03-24 07:20:37.145017-05	36.2999992370605469	11
3370	2021-03-24 07:20:40.156107-05	25.2000007629394531	10
3371	2021-03-24 07:20:41.118754-05	36.2999992370605469	11
3372	2021-03-24 07:20:44.155009-05	25.2000007629394531	10
3373	2021-03-24 07:20:45.157345-05	36.2999992370605469	11
3374	2021-03-24 07:20:48.15849-05	25.2000007629394531	10
3375	2021-03-24 07:20:49.168998-05	36.2999992370605469	11
3376	2021-03-24 07:20:52.167176-05	25.2000007629394531	10
3377	2021-03-24 07:20:53.158636-05	36.2999992370605469	11
3378	2021-03-24 07:20:56.167092-05	25.2000007629394531	10
3379	2021-03-24 07:20:57.160892-05	36.2000007629394531	11
3380	2021-03-24 07:21:00.148688-05	25.2000007629394531	10
3381	2021-03-24 07:21:01.169523-05	36.2999992370605469	11
3382	2021-03-24 07:21:04.178143-05	25.2000007629394531	10
3383	2021-03-24 07:21:05.180766-05	36.2999992370605469	11
3384	2021-03-24 07:21:08.170354-05	25.2000007629394531	10
3385	2021-03-24 07:21:09.187453-05	36.2999992370605469	11
3386	2021-03-24 07:21:12.183391-05	25.2000007629394531	10
3387	2021-03-24 07:21:13.186536-05	36.2000007629394531	11
3388	2021-03-24 07:21:16.184142-05	25.2000007629394531	10
3389	2021-03-24 07:21:17.196848-05	36.2000007629394531	11
3390	2021-03-24 07:21:20.203708-05	25.2000007629394531	10
3391	2021-03-24 07:21:21.196168-05	36.2000007629394531	11
3392	2021-03-24 07:21:24.199695-05	25.2000007629394531	10
3393	2021-03-24 07:21:25.212074-05	36.2000007629394531	11
3394	2021-03-24 07:21:28.211663-05	25.2000007629394531	10
3395	2021-03-24 07:21:29.201414-05	36.2000007629394531	11
3396	2021-03-24 07:21:32.202777-05	25.2000007629394531	10
3397	2021-03-24 07:21:33.212895-05	36.2000007629394531	11
3398	2021-03-24 07:21:36.224874-05	25.2000007629394531	10
3399	2021-03-24 07:21:37.213444-05	36.2000007629394531	11
3400	2021-03-24 07:21:40.214221-05	25.2000007629394531	10
3401	2021-03-24 07:21:41.219165-05	36.2000007629394531	11
3402	2021-03-24 07:21:44.228355-05	25.2000007629394531	10
3403	2021-03-24 07:21:45.225-05	36.2000007629394531	11
3404	2021-03-24 07:21:48.231205-05	25.2000007629394531	10
3405	2021-03-24 07:21:49.23838-05	36.2000007629394531	11
3406	2021-03-24 07:21:52.232357-05	25.2000007629394531	10
3407	2021-03-24 07:21:53.227068-05	36.2000007629394531	11
3408	2021-03-24 07:21:56.232901-05	25.2000007629394531	10
3409	2021-03-24 07:21:57.233384-05	36.1000022888183594	11
3410	2021-03-24 07:22:00.243968-05	25.3000011444091797	10
3411	2021-03-24 07:22:01.230467-05	36.2000007629394531	11
3412	2021-03-24 07:22:04.24141-05	25.2000007629394531	10
3413	2021-03-24 07:22:05.238348-05	36.1000022888183594	11
3414	2021-03-24 07:22:08.243292-05	25.3000011444091797	10
3415	2021-03-24 07:22:09.237573-05	36.2000007629394531	11
3416	2021-03-24 07:22:12.264035-05	25.3000011444091797	10
3417	2021-03-24 07:22:13.237653-05	36.2000007629394531	11
3418	2021-03-24 07:22:16.254473-05	25.3000011444091797	10
3419	2021-03-24 07:22:17.258638-05	36.2000007629394531	11
3420	2021-03-24 07:22:20.266667-05	25.3000011444091797	10
3421	2021-03-24 07:22:21.263323-05	36.2000007629394531	11
3422	2021-03-24 07:22:24.254376-05	25.3000011444091797	10
3423	2021-03-24 07:22:25.269063-05	36.2000007629394531	11
3424	2021-03-24 07:22:28.241905-05	25.3000011444091797	10
3425	2021-03-24 07:22:29.275074-05	36.2000007629394531	11
3426	2021-03-24 07:22:32.279657-05	25.2000007629394531	10
3427	2021-03-24 07:22:33.276514-05	36.1000022888183594	11
3428	2021-03-24 07:22:36.27292-05	25.3000011444091797	10
3429	2021-03-24 07:22:37.275305-05	36.2000007629394531	11
3430	2021-03-24 07:22:40.285427-05	25.3000011444091797	10
3431	2021-03-24 07:22:41.287808-05	36.2000007629394531	11
3432	2021-03-24 07:22:44.291559-05	25.3000011444091797	10
3433	2021-03-24 07:22:45.28436-05	36.2000007629394531	11
3434	2021-03-24 07:22:48.291243-05	25.3000011444091797	10
3435	2021-03-24 07:22:49.296252-05	36.2000007629394531	11
3436	2021-03-24 07:22:52.293959-05	25.2000007629394531	10
3437	2021-03-24 07:22:53.291794-05	36.1000022888183594	11
3438	2021-03-24 07:22:56.302018-05	25.2000007629394531	10
3439	2021-03-24 07:22:57.299225-05	36.1000022888183594	11
3440	2021-03-24 07:23:00.301782-05	25.3000011444091797	10
3441	2021-03-24 07:23:01.30279-05	36.2000007629394531	11
3442	2021-03-24 07:23:04.311363-05	25.3000011444091797	10
3443	2021-03-24 07:23:05.309596-05	36.2000007629394531	11
3444	2021-03-24 07:23:08.31312-05	25.2000007629394531	10
3445	2021-03-24 07:23:09.309216-05	36.1000022888183594	11
3446	2021-03-24 07:23:12.314528-05	25.3000011444091797	10
3447	2021-03-24 07:23:13.317546-05	36.2000007629394531	11
3448	2021-03-24 07:23:16.323116-05	25.3000011444091797	10
3449	2021-03-24 07:23:17.325848-05	36.2000007629394531	11
3450	2021-03-24 07:23:20.324393-05	25.2000007629394531	10
3451	2021-03-24 07:23:21.32141-05	36.1000022888183594	11
3452	2021-03-24 07:23:24.329969-05	25.2000007629394531	10
3453	2021-03-24 07:23:25.322077-05	36.1000022888183594	11
3454	2021-03-24 07:23:28.340252-05	25.3000011444091797	10
3455	2021-03-24 07:23:29.318621-05	36.1000022888183594	11
3456	2021-03-24 07:23:32.343298-05	25.2000007629394531	10
3457	2021-03-24 07:23:33.335924-05	36.1000022888183594	11
3458	2021-03-24 07:23:36.335762-05	25.3000011444091797	10
3459	2021-03-24 07:23:37.347-05	36.1000022888183594	11
3460	2021-03-24 07:23:40.349228-05	25.3000011444091797	10
3461	2021-03-24 07:23:41.348471-05	36.1000022888183594	11
3462	2021-03-24 07:23:44.345903-05	25.3000011444091797	10
3463	2021-03-24 07:23:45.348559-05	36.1000022888183594	11
3464	2021-03-24 07:23:48.341822-05	25.2000007629394531	10
3465	2021-03-24 07:23:49.349554-05	36.1000022888183594	11
3466	2021-03-24 07:23:52.355207-05	25.3000011444091797	10
3467	2021-03-24 07:23:53.363828-05	36.1000022888183594	11
3468	2021-03-24 07:23:56.358476-05	25.3000011444091797	10
3469	2021-03-24 07:23:57.367516-05	36.1000022888183594	11
3470	2021-03-24 07:24:00.357051-05	25.3000011444091797	10
3471	2021-03-24 07:24:01.372469-05	36.1000022888183594	11
3472	2021-03-24 07:24:04.372474-05	25.3000011444091797	10
3473	2021-03-24 07:24:05.374692-05	36.1000022888183594	11
3474	2021-03-24 07:24:08.374844-05	25.3000011444091797	10
3475	2021-03-24 07:24:09.377617-05	36.1000022888183594	11
3476	2021-03-24 07:24:12.380515-05	25.3000011444091797	10
3477	2021-03-24 07:24:13.374506-05	36.1000022888183594	11
3478	2021-03-24 07:24:16.388039-05	25.3000011444091797	10
3479	2021-03-24 07:24:17.387391-05	36.1000022888183594	11
3480	2021-03-24 07:24:20.396403-05	25.3000011444091797	10
3481	2021-03-24 07:24:21.39632-05	36.1000022888183594	11
3482	2021-03-24 07:24:24.394691-05	25.3000011444091797	10
3483	2021-03-24 07:24:25.396774-05	36.1000022888183594	11
3484	2021-03-24 07:24:28.401845-05	25.3000011444091797	10
3485	2021-03-24 07:24:29.400318-05	36.1000022888183594	11
3486	2021-03-24 07:24:32.413278-05	25.3000011444091797	10
3487	2021-03-24 07:24:33.406607-05	36.1000022888183594	11
3488	2021-03-24 07:24:36.413849-05	25.3000011444091797	10
3489	2021-03-24 07:24:37.411434-05	36.1000022888183594	11
3490	2021-03-24 07:24:40.417448-05	25.3000011444091797	10
3491	2021-03-24 07:24:41.4187-05	36.1000022888183594	11
3492	2021-03-24 07:24:44.418981-05	25.2000007629394531	10
3493	2021-03-24 07:24:45.423215-05	36	11
3494	2021-03-24 07:24:48.41903-05	25.3000011444091797	10
3495	2021-03-24 07:24:49.430621-05	36.1000022888183594	11
3496	2021-03-24 07:24:52.425929-05	25.3000011444091797	10
3497	2021-03-24 07:24:53.41626-05	36.1000022888183594	11
3498	2021-03-24 07:24:56.440516-05	25.3000011444091797	10
3499	2021-03-24 07:24:57.433588-05	36.1000022888183594	11
3500	2021-03-24 07:25:00.448292-05	25.3000011444091797	10
3501	2021-03-24 07:25:01.439912-05	36.1000022888183594	11
3502	2021-03-24 07:25:04.44081-05	25.3000011444091797	10
3503	2021-03-24 07:25:05.438764-05	36.1000022888183594	11
3504	2021-03-24 07:25:08.451407-05	25.3000011444091797	10
3505	2021-03-24 07:25:09.447026-05	36.1000022888183594	11
3506	2021-03-24 07:25:12.441755-05	25.3000011444091797	10
3507	2021-03-24 07:25:13.45474-05	36.1000022888183594	11
3508	2021-03-24 07:25:16.426948-05	25.3000011444091797	10
3509	2021-03-24 07:25:17.453594-05	36.1000022888183594	11
3510	2021-03-24 07:25:20.452451-05	25.2000007629394531	10
3511	2021-03-24 07:25:21.464655-05	36	11
3512	2021-03-24 07:25:24.46929-05	25.3000011444091797	10
3513	2021-03-24 07:25:25.460804-05	36.1000022888183594	11
3514	2021-03-24 07:25:28.468382-05	25.3000011444091797	10
3515	2021-03-24 07:25:29.473213-05	36.1000022888183594	11
3516	2021-03-24 07:25:32.476481-05	25.3000011444091797	10
3517	2021-03-24 07:25:33.489125-05	36.1000022888183594	11
3518	2021-03-24 07:25:36.481825-05	25.3000011444091797	10
3519	2021-03-24 07:25:37.473238-05	36.1000022888183594	11
3520	2021-03-24 07:25:40.485432-05	25.2000007629394531	10
3521	2021-03-24 07:25:41.479491-05	36	11
3522	2021-03-24 07:25:44.489018-05	25.3000011444091797	10
3523	2021-03-24 07:25:45.489421-05	36.1000022888183594	11
3524	2021-03-24 07:25:48.497599-05	25.3000011444091797	10
3525	2021-03-24 07:25:49.489804-05	36.1000022888183594	11
3526	2021-03-24 07:25:52.49587-05	25.2000007629394531	10
3527	2021-03-24 07:25:53.498827-05	36	11
3528	2021-03-24 07:25:56.501567-05	25.3000011444091797	10
3529	2021-03-24 07:25:57.499853-05	36.1000022888183594	11
3530	2021-03-24 07:26:00.502734-05	25.2000007629394531	10
3531	2021-03-24 07:26:01.505747-05	36	11
3532	2021-03-24 07:26:04.49862-05	25.3000011444091797	10
3533	2021-03-24 07:26:05.50532-05	36.1000022888183594	11
3534	2021-03-24 07:26:08.519295-05	25.3000011444091797	10
3535	2021-03-24 07:26:09.518997-05	36.1000022888183594	11
3536	2021-03-24 07:26:12.512347-05	25.3000011444091797	10
3537	2021-03-24 07:26:13.51769-05	36.1000022888183594	11
3538	2021-03-24 07:26:16.528406-05	25.3000011444091797	10
3539	2021-03-24 07:26:17.511992-05	36.1000022888183594	11
3540	2021-03-24 07:26:20.531815-05	25.3000011444091797	10
3541	2021-03-24 07:26:21.522627-05	36.1000022888183594	11
3542	2021-03-24 07:26:24.535807-05	25.3000011444091797	10
3543	2021-03-24 07:26:25.525479-05	36.1000022888183594	11
3544	2021-03-24 07:26:28.531849-05	25.3000011444091797	10
3545	2021-03-24 07:26:29.525027-05	36.1000022888183594	11
3546	2021-03-24 07:26:32.543001-05	25.3000011444091797	10
3547	2021-03-24 07:26:33.53894-05	36.1000022888183594	11
3548	2021-03-24 07:26:36.54768-05	25.3000011444091797	10
3549	2021-03-24 07:26:37.547741-05	36.1000022888183594	11
3550	2021-03-24 07:26:40.521342-05	25.3000011444091797	10
3551	2021-03-24 07:26:41.552621-05	36	11
3552	2021-03-24 07:26:44.560088-05	25.3000011444091797	10
3553	2021-03-24 07:26:45.558353-05	36	11
3554	2021-03-24 07:26:48.562246-05	25.3000011444091797	10
3555	2021-03-24 07:26:49.565821-05	36.1000022888183594	11
3556	2021-03-24 07:26:52.55891-05	25.3000011444091797	10
3557	2021-03-24 07:26:53.566322-05	36	11
3558	2021-03-24 07:26:56.574373-05	25.3000011444091797	10
3559	2021-03-24 07:26:57.570677-05	36	11
3560	2021-03-24 07:27:00.569028-05	25.3000011444091797	10
3561	2021-03-24 07:27:01.569384-05	36	11
3562	2021-03-24 07:27:04.580371-05	25.3000011444091797	10
3563	2021-03-24 07:27:05.575366-05	36.1000022888183594	11
3564	2021-03-24 07:27:08.583906-05	25.3000011444091797	10
3565	2021-03-24 07:27:09.584866-05	36	11
3566	2021-03-24 07:27:12.582172-05	25.3000011444091797	10
3567	2021-03-24 07:27:13.586619-05	36	11
3568	2021-03-24 07:27:16.586299-05	25.3000011444091797	10
3569	2021-03-24 07:27:17.589129-05	36	11
3570	2021-03-24 07:27:20.599136-05	25.3000011444091797	10
3571	2021-03-24 07:27:21.59454-05	36	11
3572	2021-03-24 07:27:24.604282-05	25.3000011444091797	10
3573	2021-03-24 07:27:25.598218-05	36	11
3574	2021-03-24 07:27:28.606131-05	25.3000011444091797	10
3575	2021-03-24 07:27:29.603558-05	36	11
3576	2021-03-24 07:27:32.604896-05	25.3000011444091797	10
3577	2021-03-24 07:27:33.609147-05	36.1000022888183594	11
3578	2021-03-24 07:27:36.617364-05	25.3000011444091797	10
3579	2021-03-24 07:27:37.614938-05	36	11
3580	2021-03-24 07:27:40.616772-05	25.3000011444091797	10
3581	2021-03-24 07:27:41.587457-05	36	11
3582	2021-03-24 07:27:44.61299-05	25.3000011444091797	10
3583	2021-03-24 07:27:45.613688-05	36	11
3584	2021-03-24 07:27:48.628142-05	25.3000011444091797	10
3585	2021-03-24 07:27:49.629569-05	36	11
3586	2021-03-24 07:27:52.629867-05	25.3000011444091797	10
3587	2021-03-24 07:27:53.63141-05	36	11
3588	2021-03-24 07:27:56.625797-05	25.3000011444091797	10
3589	2021-03-24 07:27:57.628608-05	36	11
3590	2021-03-24 07:28:00.620702-05	25.3000011444091797	10
3591	2021-03-24 07:28:01.641373-05	36.1000022888183594	11
3592	2021-03-24 07:28:04.631422-05	25.3000011444091797	10
3593	2021-03-24 07:28:05.642237-05	36.1000022888183594	11
3594	2021-03-24 07:28:08.631778-05	25.3000011444091797	10
3595	2021-03-24 07:28:09.645577-05	36.1000022888183594	11
3596	2021-03-24 07:28:12.645636-05	25.3000011444091797	10
3597	2021-03-24 07:28:13.647116-05	36.1000022888183594	11
3598	2021-03-24 07:28:16.655253-05	25.3000011444091797	10
3599	2021-03-24 07:28:17.659884-05	36.1000022888183594	11
3600	2021-03-24 07:28:20.659425-05	25.3000011444091797	10
3601	2021-03-24 07:28:21.65612-05	36.1000022888183594	11
3602	2021-03-24 07:28:24.677139-05	25.3000011444091797	10
3603	2021-03-24 07:28:25.667858-05	36.1000022888183594	11
3604	2021-03-24 07:28:28.66888-05	25.3000011444091797	10
3605	2021-03-24 07:28:29.663856-05	36.1000022888183594	11
3606	2021-03-24 07:28:32.67394-05	25.3000011444091797	10
3607	2021-03-24 07:28:33.66631-05	36.1000022888183594	11
3608	2021-03-24 07:28:36.678698-05	25.3000011444091797	10
3609	2021-03-24 07:28:37.680838-05	36	11
3610	2021-03-24 07:28:40.678148-05	25.3000011444091797	10
3611	2021-03-24 07:28:41.67283-05	36.1000022888183594	11
3612	2021-03-24 07:28:44.701059-05	25.3000011444091797	10
3613	2021-03-24 07:28:45.687133-05	36.1000022888183594	11
3614	2021-03-24 07:28:48.695629-05	25.3000011444091797	10
3615	2021-03-24 07:28:49.689966-05	36	11
3616	2021-03-24 07:28:52.689053-05	25.3000011444091797	10
3617	2021-03-24 07:28:53.696343-05	36	11
3618	2021-03-24 07:28:56.702321-05	25.3000011444091797	10
3619	2021-03-24 07:28:57.700731-05	36.1000022888183594	11
3620	2021-03-24 07:29:00.704094-05	25.3000011444091797	10
3621	2021-03-24 07:29:01.704934-05	36.1000022888183594	11
3622	2021-03-24 07:29:04.706968-05	25.3000011444091797	10
3623	2021-03-24 07:29:05.69521-05	36	11
3624	2021-03-24 07:29:08.714922-05	25.3000011444091797	10
3625	2021-03-24 07:29:09.707371-05	36.1000022888183594	11
3626	2021-03-24 07:29:12.720733-05	25.3000011444091797	10
3627	2021-03-24 07:29:13.714027-05	36	11
3628	2021-03-24 07:29:16.724245-05	25.3000011444091797	10
3629	2021-03-24 07:29:17.723004-05	36.1000022888183594	11
3630	2021-03-24 07:29:20.727292-05	25.3000011444091797	10
3631	2021-03-24 07:29:21.723592-05	36.1000022888183594	11
3632	2021-03-24 07:29:24.725035-05	25.3000011444091797	10
3633	2021-03-24 07:29:25.734767-05	36.1000022888183594	11
3634	2021-03-24 07:29:28.721623-05	25.3000011444091797	10
3635	2021-03-24 07:29:29.737038-05	36.1000022888183594	11
3636	2021-03-24 07:29:32.740728-05	25.3000011444091797	10
3637	2021-03-24 07:29:33.741429-05	36.1000022888183594	11
3638	2021-03-24 07:29:36.745783-05	25.3000011444091797	10
3639	2021-03-24 07:29:37.75029-05	36.1000022888183594	11
3640	2021-03-24 07:29:40.751285-05	25.3000011444091797	10
3641	2021-03-24 07:29:41.755981-05	36.1000022888183594	11
3642	2021-03-24 07:29:44.740727-05	25.3000011444091797	10
3643	2021-03-24 07:29:45.755778-05	36.1000022888183594	11
3644	2021-03-24 07:29:48.755079-05	25.3000011444091797	10
3645	2021-03-24 07:29:49.762091-05	36.1000022888183594	11
3646	2021-03-24 07:29:52.770854-05	25.3000011444091797	10
3647	2021-03-24 07:29:53.763052-05	36.1000022888183594	11
3648	2021-03-24 07:29:56.772826-05	25.3000011444091797	10
3649	2021-03-24 07:29:57.774611-05	36.1000022888183594	11
3650	2021-03-24 07:30:00.770409-05	25.3000011444091797	10
3651	2021-03-24 07:30:01.76829-05	36.1000022888183594	11
3652	2021-03-24 07:30:09.780431-05	25.3000011444091797	10
3653	2021-03-24 07:30:10.053827-05	36.1000022888183594	11
3654	2021-03-24 07:30:10.315739-05	25.3000011444091797	10
3655	2021-03-24 07:30:10.557908-05	36.1000022888183594	11
3656	2021-03-24 07:30:12.780252-05	25.3000011444091797	10
3657	2021-03-24 07:30:13.800377-05	36.1000022888183594	11
3658	2021-03-24 07:30:16.798807-05	25.3000011444091797	10
3659	2021-03-24 07:30:17.794465-05	36.1000022888183594	11
3660	2021-03-24 07:30:20.795027-05	25.3000011444091797	10
3661	2021-03-24 07:30:21.801819-05	36.1000022888183594	11
3662	2021-03-24 07:30:24.79112-05	25.3000011444091797	10
3663	2021-03-24 07:30:25.795897-05	36.1000022888183594	11
3664	2021-03-24 07:30:28.813133-05	25.3000011444091797	10
3665	2021-03-24 07:30:29.782623-05	36.1000022888183594	11
3666	2021-03-24 07:30:32.813739-05	25.3000011444091797	10
3667	2021-03-24 07:30:33.80197-05	36.1000022888183594	11
3668	2021-03-24 07:30:36.8134-05	25.3000011444091797	10
3669	2021-03-24 07:30:37.802804-05	36.1000022888183594	11
3670	2021-03-24 07:30:40.821061-05	25.3000011444091797	10
3671	2021-03-24 07:30:41.812961-05	36.1000022888183594	11
3672	2021-03-24 07:30:44.823345-05	25.3000011444091797	10
3673	2021-03-24 07:30:45.817338-05	36.1000022888183594	11
3674	2021-03-24 07:30:48.805276-05	25.3000011444091797	10
3675	2021-03-24 07:30:49.825446-05	36	11
3676	2021-03-24 07:30:52.834823-05	25.3000011444091797	10
3677	2021-03-24 07:30:53.838359-05	36.1000022888183594	11
3678	2021-03-24 07:30:56.833236-05	25.3000011444091797	10
3679	2021-03-24 07:30:57.838321-05	36	11
3680	2021-03-24 07:31:00.839933-05	25.3000011444091797	10
3681	2021-03-24 07:31:01.842663-05	36	11
3682	2021-03-24 07:31:04.838428-05	25.3000011444091797	10
3683	2021-03-24 07:31:05.844565-05	36	11
3684	2021-03-24 07:31:08.846446-05	25.3000011444091797	10
3685	2021-03-24 07:31:09.840058-05	36	11
3686	2021-03-24 07:31:12.857613-05	25.3000011444091797	10
3687	2021-03-24 07:31:13.85192-05	36	11
3688	2021-03-24 07:31:16.858775-05	25.3000011444091797	10
3689	2021-03-24 07:31:17.858505-05	36	11
3690	2021-03-24 07:31:20.862362-05	25.3000011444091797	10
3691	2021-03-24 07:31:21.860789-05	36	11
3692	2021-03-24 07:31:24.860572-05	25.3000011444091797	10
3693	2021-03-24 07:31:25.860402-05	36	11
3694	2021-03-24 07:31:28.867172-05	25.3000011444091797	10
3695	2021-03-24 07:31:29.878995-05	36	11
3696	2021-03-24 07:31:32.878245-05	25.3000011444091797	10
3697	2021-03-24 07:31:33.873642-05	36	11
3698	2021-03-24 07:31:36.878559-05	25.3000011444091797	10
3699	2021-03-24 07:31:37.881466-05	36	11
3700	2021-03-24 07:31:40.879211-05	25.3000011444091797	10
3701	2021-03-24 07:31:41.88346-05	36	11
3702	2021-03-24 07:31:44.886408-05	25.3000011444091797	10
3703	2021-03-24 07:31:45.891388-05	36	11
3704	2021-03-24 07:31:48.900919-05	25.3000011444091797	10
3705	2021-03-24 07:31:49.861364-05	36	11
3706	2021-03-24 07:31:52.898812-05	25.3000011444091797	10
3707	2021-03-24 07:31:53.893947-05	36	11
3708	2021-03-24 07:31:56.904044-05	25.3000011444091797	10
3709	2021-03-24 07:31:57.899301-05	36	11
3710	2021-03-24 07:32:00.911404-05	25.3000011444091797	10
3711	2021-03-24 07:32:01.910604-05	36	11
3712	2021-03-24 07:32:04.915044-05	25.3000011444091797	10
3713	2021-03-24 07:32:05.900012-05	36	11
3714	2021-03-24 07:32:08.893301-05	25.3000011444091797	10
3715	2021-03-24 07:32:09.918415-05	36	11
3716	2021-03-24 07:32:12.914684-05	25.3000011444091797	10
3717	2021-03-24 07:32:13.926411-05	36	11
3718	2021-03-24 07:32:16.92026-05	25.3000011444091797	10
3719	2021-03-24 07:32:17.92986-05	36	11
3720	2021-03-24 07:32:20.918495-05	25.3000011444091797	10
3721	2021-03-24 07:32:21.926355-05	36	11
3722	2021-03-24 07:32:24.928429-05	25.3000011444091797	10
3723	2021-03-24 07:32:25.935108-05	36	11
3724	2021-03-24 07:32:28.934632-05	25.3000011444091797	10
3725	2021-03-24 07:32:29.936485-05	36	11
3726	2021-03-24 07:32:32.938765-05	25.3000011444091797	10
3727	2021-03-24 07:32:33.948224-05	36	11
3728	2021-03-24 07:32:36.95717-05	25.3000011444091797	10
3729	2021-03-24 07:32:37.943981-05	36	11
3730	2021-03-24 07:32:40.955362-05	25.3000011444091797	10
3731	2021-03-24 07:32:41.946059-05	36	11
3732	2021-03-24 07:32:44.968446-05	25.3000011444091797	10
3733	2021-03-24 07:32:45.953493-05	36	11
3734	2021-03-24 07:32:48.959831-05	25.3000011444091797	10
3735	2021-03-24 07:32:49.955717-05	36	11
3736	2021-03-24 07:32:52.958597-05	25.3000011444091797	10
3737	2021-03-24 07:32:53.963191-05	36	11
3738	2021-03-24 07:32:56.965366-05	25.3000011444091797	10
3739	2021-03-24 07:32:57.971234-05	36	11
3740	2021-03-24 07:33:00.975414-05	25.3000011444091797	10
3741	2021-03-24 07:33:01.974677-05	36	11
3742	2021-03-24 07:33:04.975922-05	25.3000011444091797	10
3743	2021-03-24 07:33:05.964777-05	36	11
3744	2021-03-24 07:33:08.977337-05	25.3000011444091797	10
3745	2021-03-24 07:33:09.974056-05	36	11
3746	2021-03-24 07:33:12.98337-05	25.3000011444091797	10
3747	2021-03-24 07:33:13.957737-05	36	11
3748	2021-03-24 07:33:16.988974-05	25.3000011444091797	10
3749	2021-03-24 07:33:17.992178-05	36	11
3750	2021-03-24 07:33:20.97985-05	25.3000011444091797	10
3751	2021-03-24 07:33:21.987759-05	36	11
3752	2021-03-24 07:33:24.991586-05	25.3000011444091797	10
3753	2021-03-24 07:33:25.987162-05	36	11
3754	2021-03-24 07:33:28.998131-05	25.3000011444091797	10
3755	2021-03-24 07:33:30.004297-05	36	11
3756	2021-03-24 07:33:32.979651-05	25.3000011444091797	10
3757	2021-03-24 07:33:33.998156-05	36	11
3758	2021-03-24 07:33:37.00687-05	25.3000011444091797	10
3759	2021-03-24 07:33:38.007823-05	36	11
3760	2021-03-24 07:33:41.022159-05	25.3000011444091797	10
3761	2021-03-24 07:33:42.015138-05	36	11
3762	2021-03-24 07:33:45.016755-05	25.3000011444091797	10
3763	2021-03-24 07:33:46.015715-05	36	11
3764	2021-03-24 07:33:49.027042-05	25.3000011444091797	10
3765	2021-03-24 07:33:50.029128-05	36	11
3766	2021-03-24 07:33:53.026405-05	25.3000011444091797	10
3767	2021-03-24 07:33:54.02238-05	36	11
3768	2021-03-24 07:33:57.033356-05	25.3000011444091797	10
3769	2021-03-24 07:33:58.030698-05	36	11
3770	2021-03-24 07:34:01.038863-05	25.3000011444091797	10
3771	2021-03-24 07:34:02.033551-05	36	11
3772	2021-03-24 07:34:05.041937-05	25.3000011444091797	10
3773	2021-03-24 07:34:06.042964-05	36	11
3774	2021-03-24 07:34:09.042686-05	25.3000011444091797	10
3775	2021-03-24 07:34:10.047953-05	36	11
3776	2021-03-24 07:34:13.039966-05	25.3000011444091797	10
3777	2021-03-24 07:34:14.044759-05	36	11
3778	2021-03-24 07:34:17.058951-05	25.3000011444091797	10
3779	2021-03-24 07:34:18.05425-05	36	11
3780	2021-03-24 07:34:21.052982-05	25.3000011444091797	10
3781	2021-03-24 07:34:22.068728-05	36	11
3782	2021-03-24 07:34:25.06475-05	25.3000011444091797	10
3783	2021-03-24 07:34:26.061927-05	36	11
3784	2021-03-24 07:34:29.0674-05	25.3000011444091797	10
3785	2021-03-24 07:34:30.064793-05	36	11
3786	2021-03-24 07:34:33.060301-05	25.3000011444091797	10
3787	2021-03-24 07:34:34.053539-05	36	11
3788	2021-03-24 07:34:37.066686-05	25.3000011444091797	10
3789	2021-03-24 07:34:38.059236-05	36	11
3790	2021-03-24 07:34:41.084629-05	25.3000011444091797	10
3791	2021-03-24 07:34:42.079419-05	36	11
3792	2021-03-24 07:34:45.083035-05	25.3000011444091797	10
3793	2021-03-24 07:34:46.081433-05	36	11
3794	2021-03-24 07:34:49.085767-05	25.3000011444091797	10
3795	2021-03-24 07:34:50.087489-05	36	11
3796	2021-03-24 07:34:53.091022-05	25.3000011444091797	10
3797	2021-03-24 07:34:54.093535-05	36	11
3798	2021-03-24 07:34:57.087361-05	25.3000011444091797	10
3799	2021-03-24 07:34:58.096018-05	36	11
3800	2021-03-24 07:35:01.10037-05	25.3000011444091797	10
3801	2021-03-24 07:35:02.088009-05	36	11
3802	2021-03-24 07:35:05.097763-05	25.3000011444091797	10
3803	2021-03-24 07:35:06.102251-05	36	11
3804	2021-03-24 07:35:09.107252-05	25.3000011444091797	10
3805	2021-03-24 07:35:10.103811-05	36	11
3806	2021-03-24 07:35:13.109763-05	25.3000011444091797	10
3807	2021-03-24 07:35:14.119431-05	36	11
3808	2021-03-24 07:35:17.116994-05	25.3000011444091797	10
3809	2021-03-24 07:35:18.107828-05	36	11
3810	2021-03-24 07:35:21.121356-05	25.3000011444091797	10
3811	2021-03-24 07:35:22.125366-05	36	11
3812	2021-03-24 07:35:25.127726-05	25.3000011444091797	10
3813	2021-03-24 07:35:26.125684-05	36	11
3814	2021-03-24 07:35:29.130476-05	25.3000011444091797	10
3815	2021-03-24 07:35:30.125677-05	36	11
3816	2021-03-24 07:35:33.140232-05	25.3000011444091797	10
3817	2021-03-24 07:35:34.141199-05	36	11
3818	2021-03-24 07:35:37.137762-05	25.3000011444091797	10
3819	2021-03-24 07:35:38.137953-05	36	11
3820	2021-03-24 07:35:41.153469-05	25.3000011444091797	10
3821	2021-03-24 07:35:42.146165-05	36	11
3822	2021-03-24 07:35:45.151294-05	25.3000011444091797	10
3823	2021-03-24 07:35:46.154761-05	36	11
3824	2021-03-24 07:35:49.158767-05	25.3000011444091797	10
3825	2021-03-24 07:35:50.154344-05	36	11
3826	2021-03-24 07:35:53.162766-05	25.3000011444091797	10
3827	2021-03-24 07:35:54.145523-05	36	11
3828	2021-03-24 07:35:57.16121-05	25.3000011444091797	10
3829	2021-03-24 07:35:58.150506-05	36	11
3830	2021-03-24 07:36:01.168503-05	25.3000011444091797	10
3831	2021-03-24 07:36:02.170757-05	36	11
3832	2021-03-24 07:36:05.172027-05	25.3000011444091797	10
3833	2021-03-24 07:36:06.17079-05	36	11
3834	2021-03-24 07:36:09.188145-05	25.3000011444091797	10
3835	2021-03-24 07:36:10.185584-05	36	11
3836	2021-03-24 07:36:13.183126-05	25.3000011444091797	10
3837	2021-03-24 07:36:14.172444-05	36	11
3838	2021-03-24 07:36:17.166418-05	25.3000011444091797	10
3839	2021-03-24 07:36:18.188887-05	36	11
3840	2021-03-24 07:36:21.178073-05	25.3000011444091797	10
3841	2021-03-24 07:36:22.181708-05	36	11
3842	2021-03-24 07:36:25.195483-05	25.3000011444091797	10
3843	2021-03-24 07:36:26.203017-05	36	11
3844	2021-03-24 07:36:29.200947-05	25.3000011444091797	10
3845	2021-03-24 07:36:30.206715-05	36	11
3846	2021-03-24 07:36:33.204878-05	25.3000011444091797	10
3847	2021-03-24 07:36:34.203415-05	36	11
3848	2021-03-24 07:36:37.20788-05	25.3000011444091797	10
3849	2021-03-24 07:36:38.204346-05	36	11
3850	2021-03-24 07:36:41.20257-05	25.3000011444091797	10
3851	2021-03-24 07:36:42.209777-05	36	11
3852	2021-03-24 07:36:45.20799-05	25.3000011444091797	10
3853	2021-03-24 07:36:46.212855-05	36	11
3854	2021-03-24 07:36:49.217294-05	25.3000011444091797	10
3855	2021-03-24 07:36:50.220559-05	36	11
3856	2021-03-24 07:36:53.221006-05	25.3000011444091797	10
3857	2021-03-24 07:36:54.231703-05	36	11
3858	2021-03-24 07:36:57.228163-05	25.3000011444091797	10
3859	2021-03-24 07:36:58.224895-05	36	11
3860	2021-03-24 07:37:01.230428-05	25.3000011444091797	10
3861	2021-03-24 07:37:02.22577-05	36	11
3862	2021-03-24 07:37:05.24127-05	25.3000011444091797	10
3863	2021-03-24 07:37:06.241088-05	36	11
3864	2021-03-24 07:37:09.234902-05	25.3000011444091797	10
3865	2021-03-24 07:37:10.243371-05	35.9000015258789062	11
3866	2021-03-24 07:37:13.254668-05	25.3000011444091797	10
3867	2021-03-24 07:37:14.243047-05	36	11
3868	2021-03-24 07:37:17.244618-05	25.3000011444091797	10
3869	2021-03-24 07:37:18.224481-05	36	11
3870	2021-03-24 07:37:21.257953-05	25.3000011444091797	10
3871	2021-03-24 07:37:22.259552-05	36	11
3872	2021-03-24 07:37:25.255191-05	25.3000011444091797	10
3873	2021-03-24 07:37:26.251984-05	36	11
3874	2021-03-24 07:37:29.261569-05	25.3000011444091797	10
3875	2021-03-24 07:37:30.250845-05	36	11
3876	2021-03-24 07:37:33.256726-05	25.3000011444091797	10
3877	2021-03-24 07:37:34.261272-05	36	11
3878	2021-03-24 07:37:37.258649-05	25.3000011444091797	10
3879	2021-03-24 07:37:38.264365-05	36	11
3880	2021-03-24 07:37:41.269893-05	25.3000011444091797	10
3881	2021-03-24 07:37:42.269659-05	36	11
3882	2021-03-24 07:37:45.278643-05	25.3000011444091797	10
3883	2021-03-24 07:37:46.297089-05	36	11
3884	2021-03-24 07:37:49.281859-05	25.3000011444091797	10
3885	2021-03-24 07:37:50.293247-05	36	11
3886	2021-03-24 07:37:53.292299-05	25.3000011444091797	10
3887	2021-03-24 07:37:54.286696-05	36	11
3888	2021-03-24 07:37:57.28969-05	25.3000011444091797	10
3889	2021-03-24 07:37:58.294497-05	36	11
3890	2021-03-24 07:38:01.289884-05	25.3000011444091797	10
3891	2021-03-24 07:38:02.294863-05	36	11
3892	2021-03-24 07:38:05.304252-05	25.3000011444091797	10
3893	2021-03-24 07:38:06.298454-05	36	11
3894	2021-03-24 07:38:09.296683-05	25.3000011444091797	10
3895	2021-03-24 07:38:10.300469-05	36	11
3896	2021-03-24 07:38:13.312042-05	25.3000011444091797	10
3897	2021-03-24 07:38:14.309713-05	36	11
3898	2021-03-24 07:38:17.311141-05	25.3000011444091797	10
3899	2021-03-24 07:38:18.309304-05	36	11
3900	2021-03-24 07:38:21.319144-05	25.3000011444091797	10
3901	2021-03-24 07:38:22.318535-05	36	11
3902	2021-03-24 07:38:25.32979-05	25.3000011444091797	10
3903	2021-03-24 07:38:26.326211-05	36	11
3904	2021-03-24 07:38:29.316351-05	25.3000011444091797	10
3905	2021-03-24 07:38:30.327098-05	36	11
3906	2021-03-24 07:38:33.329343-05	25.3000011444091797	10
3907	2021-03-24 07:38:34.329621-05	36	11
3908	2021-03-24 07:38:37.342436-05	25.3000011444091797	10
3909	2021-03-24 07:38:38.304539-05	36	11
3910	2021-03-24 07:38:41.342108-05	25.3000011444091797	10
3911	2021-03-24 07:38:42.325253-05	36	11
3912	2021-03-24 07:38:45.34303-05	25.3000011444091797	10
3913	2021-03-24 07:38:46.347206-05	36	11
3914	2021-03-24 07:38:49.351033-05	25.3000011444091797	10
3915	2021-03-24 07:38:50.352724-05	36	11
3916	2021-03-24 07:38:53.358716-05	25.3000011444091797	10
3917	2021-03-24 07:38:54.349649-05	36	11
3918	2021-03-24 07:38:57.360779-05	25.3000011444091797	10
3919	2021-03-24 07:38:58.358016-05	36	11
3920	2021-03-24 07:39:01.333478-05	25.3000011444091797	10
3921	2021-03-24 07:39:02.363461-05	35.9000015258789062	11
3922	2021-03-24 07:39:05.381043-05	25.3000011444091797	10
3923	2021-03-24 07:39:06.369238-05	35.9000015258789062	11
3924	2021-03-24 07:39:09.379367-05	25.3000011444091797	10
3925	2021-03-24 07:39:10.369861-05	35.9000015258789062	11
3926	2021-03-24 07:39:13.380954-05	25.3000011444091797	10
3927	2021-03-24 07:39:14.379944-05	35.9000015258789062	11
3928	2021-03-24 07:39:17.394589-05	25.3000011444091797	10
3929	2021-03-24 07:39:18.382931-05	35.9000015258789062	11
3930	2021-03-24 07:39:21.392498-05	25.3000011444091797	10
3931	2021-03-24 07:39:22.393705-05	36	11
3932	2021-03-24 07:39:25.392883-05	25.3000011444091797	10
3933	2021-03-24 07:39:26.393826-05	35.9000015258789062	11
3934	2021-03-24 07:39:29.40195-05	25.3000011444091797	10
3935	2021-03-24 07:39:30.402257-05	36	11
3936	2021-03-24 07:39:33.388368-05	25.3000011444091797	10
3937	2021-03-24 07:39:34.398157-05	35.9000015258789062	11
3938	2021-03-24 07:39:37.394997-05	25.3000011444091797	10
3939	2021-03-24 07:39:38.410108-05	35.9000015258789062	11
3940	2021-03-24 07:39:41.411111-05	25.3000011444091797	10
3941	2021-03-24 07:39:42.405111-05	35.9000015258789062	11
3942	2021-03-24 07:39:45.410994-05	25.3000011444091797	10
3943	2021-03-24 07:39:46.411905-05	36	11
3944	2021-03-24 07:39:49.416357-05	25.3000011444091797	10
3945	2021-03-24 07:39:50.420763-05	35.9000015258789062	11
3946	2021-03-24 07:39:53.425338-05	25.3000011444091797	10
3947	2021-03-24 07:39:54.427368-05	36	11
3948	2021-03-24 07:39:57.431329-05	25.3000011444091797	10
3949	2021-03-24 07:39:58.417003-05	36	11
3950	2021-03-24 07:40:01.439594-05	25.3000011444091797	10
3951	2021-03-24 07:40:02.406672-05	35.9000015258789062	11
3952	2021-03-24 07:40:05.436962-05	25.3000011444091797	10
3953	2021-03-24 07:40:06.422484-05	35.9000015258789062	11
3954	2021-03-24 07:40:09.439083-05	25.3000011444091797	10
3955	2021-03-24 07:40:10.44016-05	35.9000015258789062	11
3956	2021-03-24 07:40:13.444404-05	25.3000011444091797	10
3957	2021-03-24 07:40:14.438062-05	35.9000015258789062	11
3958	2021-03-24 07:40:17.448239-05	25.3000011444091797	10
3959	2021-03-24 07:40:18.44085-05	35.9000015258789062	11
3960	2021-03-24 07:40:21.426365-05	25.3000011444091797	10
3961	2021-03-24 07:40:22.436477-05	35.9000015258789062	11
3962	2021-03-24 07:40:25.454323-05	25.3000011444091797	10
3963	2021-03-24 07:40:26.45232-05	35.9000015258789062	11
3964	2021-03-24 07:40:29.453597-05	25.3000011444091797	10
3965	2021-03-24 07:40:30.461612-05	35.9000015258789062	11
3966	2021-03-24 07:40:33.459201-05	25.3000011444091797	10
3967	2021-03-24 07:40:34.461687-05	35.9000015258789062	11
3968	2021-03-24 07:40:37.462584-05	25.3000011444091797	10
3969	2021-03-24 07:40:38.466082-05	35.9000015258789062	11
3970	2021-03-24 07:40:41.469961-05	25.3000011444091797	10
3971	2021-03-24 07:40:42.469165-05	35.9000015258789062	11
3972	2021-03-24 07:40:45.482321-05	25.3000011444091797	10
3973	2021-03-24 07:40:46.46836-05	35.9000015258789062	11
3974	2021-03-24 07:40:49.474766-05	25.3000011444091797	10
3975	2021-03-24 07:40:50.479711-05	35.9000015258789062	11
3976	2021-03-24 07:40:53.493312-05	25.3000011444091797	10
3977	2021-03-24 07:40:54.499782-05	35.9000015258789062	11
3978	2021-03-24 07:40:57.494787-05	25.3000011444091797	10
3979	2021-03-24 07:40:58.496957-05	35.9000015258789062	11
3980	2021-03-24 07:41:01.500102-05	25.3000011444091797	10
3981	2021-03-24 07:41:02.50222-05	35.9000015258789062	11
3982	2021-03-24 07:41:05.506446-05	25.3000011444091797	10
3983	2021-03-24 07:41:06.505479-05	35.9000015258789062	11
3984	2021-03-24 07:41:09.504675-05	25.3000011444091797	10
3985	2021-03-24 07:41:10.528741-05	35.9000015258789062	11
3986	2021-03-24 07:41:13.518592-05	25.3000011444091797	10
3987	2021-03-24 07:41:14.51719-05	35.9000015258789062	11
3988	2021-03-24 07:41:17.520054-05	25.3000011444091797	10
3989	2021-03-24 07:41:18.519058-05	35.9000015258789062	11
3990	2021-03-24 07:41:21.522084-05	25.3000011444091797	10
3991	2021-03-24 07:41:22.494569-05	35.9000015258789062	11
3992	2021-03-24 07:41:25.518157-05	25.3000011444091797	10
3993	2021-03-24 07:41:26.518938-05	35.9000015258789062	11
3994	2021-03-24 07:41:29.539282-05	25.3000011444091797	10
3995	2021-03-24 07:41:30.53283-05	35.9000015258789062	11
3996	2021-03-24 07:41:33.533096-05	25.3000011444091797	10
3997	2021-03-24 07:41:34.529147-05	35.9000015258789062	11
3998	2021-03-24 07:41:37.542752-05	25.3000011444091797	10
3999	2021-03-24 07:41:38.558419-05	35.9000015258789062	11
4000	2021-03-24 07:41:41.519767-05	25.3000011444091797	10
4001	2021-03-24 07:41:42.536196-05	35.9000015258789062	11
4002	2021-03-24 07:41:45.548234-05	25.3999996185302734	10
4003	2021-03-24 07:41:46.547056-05	35.9000015258789062	11
4004	2021-03-24 07:41:49.546748-05	25.3000011444091797	10
4005	2021-03-24 07:41:50.546592-05	35.9000015258789062	11
4006	2021-03-24 07:41:53.563266-05	25.3000011444091797	10
4007	2021-03-24 07:41:54.555732-05	35.9000015258789062	11
4008	2021-03-24 07:41:57.56272-05	25.3000011444091797	10
4009	2021-03-24 07:41:58.557375-05	35.9000015258789062	11
4010	2021-03-24 07:42:01.563008-05	25.3000011444091797	10
4011	2021-03-24 07:42:02.572742-05	35.7999992370605469	11
4012	2021-03-24 07:42:05.577575-05	25.3000011444091797	10
4013	2021-03-24 07:42:06.570833-05	35.7999992370605469	11
4014	2021-03-24 07:42:09.580614-05	25.3999996185302734	10
4015	2021-03-24 07:42:10.574019-05	35.9000015258789062	11
4016	2021-03-24 07:42:13.578809-05	25.3000011444091797	10
4017	2021-03-24 07:42:14.581728-05	35.7999992370605469	11
4018	2021-03-24 07:42:17.58754-05	25.3000011444091797	10
4019	2021-03-24 07:42:18.585296-05	35.7999992370605469	11
4020	2021-03-24 07:42:21.592647-05	25.3000011444091797	10
4021	2021-03-24 07:42:22.604718-05	35.7999992370605469	11
4022	2021-03-24 07:42:25.591102-05	25.3999996185302734	10
4023	2021-03-24 07:42:26.599151-05	35.9000015258789062	11
4024	2021-03-24 07:42:29.597672-05	25.3000011444091797	10
4025	2021-03-24 07:42:30.600954-05	35.7999992370605469	11
4026	2021-03-24 07:42:33.600026-05	25.3000011444091797	10
4027	2021-03-24 07:42:34.601926-05	35.7999992370605469	11
4028	2021-03-24 07:42:37.605637-05	25.3000011444091797	10
4029	2021-03-24 07:42:38.597229-05	35.7999992370605469	11
4030	2021-03-24 07:42:41.617308-05	25.3999996185302734	10
4031	2021-03-24 07:42:42.576019-05	35.9000015258789062	11
4032	2021-03-24 07:42:45.615122-05	25.3000011444091797	10
4033	2021-03-24 07:42:46.602291-05	35.7999992370605469	11
4034	2021-03-24 07:42:49.621025-05	25.3999996185302734	10
4035	2021-03-24 07:42:50.618788-05	35.9000015258789062	11
4036	2021-03-24 07:42:53.626988-05	25.3999996185302734	10
4037	2021-03-24 07:42:54.629373-05	35.9000015258789062	11
4038	2021-03-24 07:42:57.626446-05	25.3999996185302734	10
4039	2021-03-24 07:42:58.637357-05	35.9000015258789062	11
4040	2021-03-24 07:43:01.60148-05	25.3000011444091797	10
4041	2021-03-24 07:43:02.629438-05	35.7999992370605469	11
4042	2021-03-24 07:43:05.624998-05	25.3000011444091797	10
4043	2021-03-24 07:43:06.63588-05	35.7999992370605469	11
4044	2021-03-24 07:43:09.635642-05	25.3999996185302734	10
4045	2021-03-24 07:43:10.642698-05	35.9000015258789062	11
4046	2021-03-24 07:43:13.640625-05	25.3000011444091797	10
4047	2021-03-24 07:43:14.652638-05	35.7999992370605469	11
4048	2021-03-24 07:43:17.636802-05	25.3999996185302734	10
4049	2021-03-24 07:43:18.649492-05	35.7999992370605469	11
4050	2021-03-24 07:43:21.649182-05	25.3999996185302734	10
4051	2021-03-24 07:43:22.652366-05	35.7999992370605469	11
4052	2021-03-24 07:43:25.655552-05	25.3000011444091797	10
4053	2021-03-24 07:43:26.666137-05	35.7999992370605469	11
4054	2021-03-24 07:43:29.666202-05	25.3999996185302734	10
4055	2021-03-24 07:43:30.666393-05	35.7999992370605469	11
4056	2021-03-24 07:43:33.667584-05	25.3999996185302734	10
4057	2021-03-24 07:43:34.668452-05	35.7999992370605469	11
4058	2021-03-24 07:43:37.671782-05	25.3000011444091797	10
4059	2021-03-24 07:43:38.668844-05	35.7999992370605469	11
4060	2021-03-24 07:43:41.673454-05	25.3000011444091797	10
4061	2021-03-24 07:43:42.679243-05	35.7999992370605469	11
4062	2021-03-24 07:43:45.684108-05	25.3999996185302734	10
4063	2021-03-24 07:43:46.681741-05	35.7999992370605469	11
4064	2021-03-24 07:43:49.683698-05	25.3999996185302734	10
4065	2021-03-24 07:43:50.684981-05	35.7999992370605469	11
4066	2021-03-24 07:43:53.685463-05	25.3000011444091797	10
4067	2021-03-24 07:43:54.687997-05	35.7999992370605469	11
4068	2021-03-24 07:43:57.686867-05	25.3000011444091797	10
4069	2021-03-24 07:43:58.699-05	35.7999992370605469	11
4070	2021-03-24 07:44:01.693908-05	25.3000011444091797	10
4071	2021-03-24 07:44:02.669366-05	35.7999992370605469	11
4072	2021-03-24 07:44:05.704284-05	25.3000011444091797	10
4073	2021-03-24 07:44:06.693044-05	35.7999992370605469	11
4074	2021-03-24 07:44:09.711077-05	25.3999996185302734	10
4075	2021-03-24 07:44:10.709184-05	35.7999992370605469	11
4076	2021-03-24 07:44:13.713171-05	25.3000011444091797	10
4077	2021-03-24 07:44:14.712255-05	35.7999992370605469	11
4078	2021-03-24 07:44:17.717915-05	25.3999996185302734	10
4079	2021-03-24 07:44:18.707453-05	35.7999992370605469	11
4080	2021-03-24 07:44:21.699599-05	25.3000011444091797	10
4081	2021-03-24 07:44:22.718131-05	35.7999992370605469	11
4082	2021-03-24 07:44:25.724058-05	25.3999996185302734	10
4083	2021-03-24 07:44:26.731273-05	35.7999992370605469	11
4084	2021-03-24 07:44:29.72921-05	25.3000011444091797	10
4085	2021-03-24 07:44:30.72874-05	35.7999992370605469	11
4086	2021-03-24 07:44:33.731657-05	25.3999996185302734	10
4087	2021-03-24 07:44:34.738056-05	35.7999992370605469	11
4088	2021-03-24 07:44:37.738744-05	25.3999996185302734	10
4089	2021-03-24 07:44:38.737326-05	35.7999992370605469	11
4090	2021-03-24 07:44:41.738816-05	25.3000011444091797	10
4091	2021-03-24 07:44:42.736995-05	35.7999992370605469	11
4092	2021-03-24 07:44:45.742347-05	25.3999996185302734	10
4093	2021-03-24 07:44:46.743194-05	35.7999992370605469	11
4094	2021-03-24 07:44:49.755541-05	25.3999996185302734	10
4095	2021-03-24 07:44:50.753159-05	35.7999992370605469	11
4096	2021-03-24 07:44:53.752899-05	25.3000011444091797	10
4097	2021-03-24 07:44:54.757641-05	35.7000007629394531	11
4098	2021-03-24 07:44:57.758414-05	25.3999996185302734	10
4099	2021-03-24 07:44:58.761813-05	35.7999992370605469	11
4100	2021-03-24 07:45:01.769287-05	25.3999996185302734	10
4101	2021-03-24 07:45:02.764858-05	35.7999992370605469	11
4102	2021-03-24 07:45:05.777449-05	25.3999996185302734	10
4103	2021-03-24 07:45:06.771745-05	35.7999992370605469	11
4104	2021-03-24 07:45:09.77874-05	25.3999996185302734	10
4105	2021-03-24 07:45:10.778887-05	35.7999992370605469	11
4106	2021-03-24 07:45:13.777801-05	25.3999996185302734	10
4107	2021-03-24 07:45:14.781353-05	35.7999992370605469	11
4108	2021-03-24 07:45:17.782066-05	25.3000011444091797	10
4109	2021-03-24 07:45:18.770578-05	35.7999992370605469	11
4110	2021-03-24 07:45:21.782729-05	25.3999996185302734	10
4111	2021-03-24 07:45:22.787365-05	35.7999992370605469	11
4112	2021-03-24 07:45:25.795134-05	25.3999996185302734	10
4113	2021-03-24 07:45:26.78271-05	35.7999992370605469	11
4114	2021-03-24 07:45:29.797116-05	25.3999996185302734	10
4115	2021-03-24 07:45:30.798265-05	35.7999992370605469	11
4116	2021-03-24 07:45:33.805762-05	25.3999996185302734	10
4117	2021-03-24 07:45:34.788227-05	35.7999992370605469	11
4118	2021-03-24 07:45:37.802411-05	25.3999996185302734	10
4119	2021-03-24 07:45:38.794828-05	35.7999992370605469	11
4120	2021-03-24 07:45:41.790838-05	25.3999996185302734	10
4121	2021-03-24 07:45:42.812177-05	35.7999992370605469	11
4122	2021-03-24 07:45:45.814644-05	25.3999996185302734	10
4123	2021-03-24 07:45:46.813648-05	35.7999992370605469	11
4124	2021-03-24 07:45:49.818745-05	25.3999996185302734	10
4125	2021-03-24 07:45:50.822783-05	35.7999992370605469	11
4126	2021-03-24 07:45:53.813413-05	25.3999996185302734	10
4127	2021-03-24 07:45:54.827025-05	35.7999992370605469	11
4128	2021-03-24 07:45:57.824702-05	25.3999996185302734	10
4129	2021-03-24 07:45:58.833579-05	35.7999992370605469	11
4130	2021-03-24 07:46:01.829502-05	25.3000011444091797	10
4131	2021-03-24 07:46:02.825634-05	35.7999992370605469	11
4132	2021-03-24 07:46:05.834757-05	25.3999996185302734	10
4133	2021-03-24 07:46:06.835283-05	35.7999992370605469	11
4134	2021-03-24 07:46:09.84211-05	25.3999996185302734	10
4135	2021-03-24 07:46:10.83167-05	35.7999992370605469	11
4136	2021-03-24 07:46:13.841553-05	25.3999996185302734	10
4137	2021-03-24 07:46:14.842779-05	35.7999992370605469	11
4138	2021-03-24 07:46:17.854227-05	25.3999996185302734	10
4139	2021-03-24 07:46:18.844599-05	35.7999992370605469	11
4140	2021-03-24 07:46:21.84613-05	25.3999996185302734	10
4141	2021-03-24 07:46:22.858309-05	35.7999992370605469	11
4142	2021-03-24 07:46:25.853873-05	25.3999996185302734	10
4143	2021-03-24 07:46:26.860579-05	35.7999992370605469	11
4144	2021-03-24 07:46:29.873264-05	25.3999996185302734	10
4145	2021-03-24 07:46:30.866592-05	35.7999992370605469	11
4146	2021-03-24 07:46:33.868503-05	25.3999996185302734	10
4147	2021-03-24 07:46:34.869255-05	35.7999992370605469	11
4148	2021-03-24 07:46:37.871705-05	25.3000011444091797	10
4149	2021-03-24 07:46:38.859168-05	35.7999992370605469	11
4150	2021-03-24 07:46:41.869234-05	25.3999996185302734	10
4151	2021-03-24 07:46:42.867324-05	35.7999992370605469	11
4152	2021-03-24 07:46:45.880299-05	25.3999996185302734	10
4153	2021-03-24 07:46:46.881547-05	35.7999992370605469	11
4154	2021-03-24 07:46:49.887994-05	25.3000011444091797	10
4155	2021-03-24 07:46:50.873189-05	35.7999992370605469	11
4156	2021-03-24 07:46:53.889958-05	25.3999996185302734	10
4157	2021-03-24 07:46:54.894497-05	35.7999992370605469	11
4158	2021-03-24 07:46:57.890532-05	25.3999996185302734	10
4159	2021-03-24 07:46:58.895358-05	35.7999992370605469	11
4160	2021-03-24 07:47:01.886404-05	25.3999996185302734	10
4161	2021-03-24 07:47:02.903523-05	35.7999992370605469	11
4162	2021-03-24 07:47:05.90222-05	25.3999996185302734	10
4163	2021-03-24 07:47:06.908653-05	35.7999992370605469	11
4164	2021-03-24 07:47:09.910209-05	25.3999996185302734	10
4165	2021-03-24 07:47:10.907871-05	35.7999992370605469	11
4166	2021-03-24 07:47:13.900259-05	25.3000011444091797	10
4167	2021-03-24 07:47:14.904515-05	35.7999992370605469	11
4168	2021-03-24 07:47:17.908611-05	25.3999996185302734	10
4169	2021-03-24 07:47:18.9089-05	35.7999992370605469	11
4170	2021-03-24 07:47:21.923154-05	25.3000011444091797	10
4171	2021-03-24 07:47:22.92527-05	35.7999992370605469	11
4172	2021-03-24 07:47:25.931693-05	25.3999996185302734	10
4173	2021-03-24 07:47:26.931062-05	35.7999992370605469	11
4174	2021-03-24 07:47:29.933572-05	25.3000011444091797	10
4175	2021-03-24 07:47:30.930114-05	35.7999992370605469	11
4176	2021-03-24 07:47:33.932987-05	25.3000011444091797	10
4177	2021-03-24 07:47:34.935252-05	35.7999992370605469	11
4178	2021-03-24 07:47:37.934914-05	25.3999996185302734	10
4179	2021-03-24 07:47:38.919189-05	35.7999992370605469	11
4180	2021-03-24 07:47:41.943532-05	25.3999996185302734	10
4181	2021-03-24 07:47:42.941186-05	35.7999992370605469	11
4182	2021-03-24 07:47:45.947982-05	25.3999996185302734	10
4183	2021-03-24 07:47:46.943923-05	35.7999992370605469	11
4184	2021-03-24 07:47:49.950933-05	25.3999996185302734	10
4185	2021-03-24 07:47:50.948733-05	35.7999992370605469	11
4186	2021-03-24 07:47:53.942554-05	25.3999996185302734	10
4187	2021-03-24 07:47:54.947802-05	35.7999992370605469	11
4188	2021-03-24 07:47:57.964347-05	25.3999996185302734	10
4189	2021-03-24 07:47:58.928761-05	35.7999992370605469	11
4190	2021-03-24 07:48:01.967454-05	25.3999996185302734	10
4191	2021-03-24 07:48:02.963202-05	35.7999992370605469	11
4192	2021-03-24 07:48:05.967286-05	25.3999996185302734	10
4193	2021-03-24 07:48:06.973965-05	35.7999992370605469	11
4194	2021-03-24 07:48:09.977771-05	25.3999996185302734	10
4195	2021-03-24 07:48:10.969272-05	35.7999992370605469	11
4196	2021-03-24 07:48:13.978775-05	25.3000011444091797	10
4197	2021-03-24 07:48:14.975349-05	35.7999992370605469	11
4198	2021-03-24 07:48:17.956254-05	25.3000011444091797	10
4199	2021-03-24 07:48:18.977626-05	35.7999992370605469	11
4200	2021-03-24 07:48:21.982389-05	25.3999996185302734	10
4201	2021-03-24 07:48:22.985801-05	35.7999992370605469	11
4202	2021-03-24 07:48:25.99093-05	25.3999996185302734	10
4203	2021-03-24 07:48:26.992561-05	35.7999992370605469	11
4204	2021-03-24 07:48:29.98883-05	25.3999996185302734	10
4205	2021-03-24 07:48:31.008955-05	35.7999992370605469	11
4206	2021-03-24 07:48:34.002149-05	25.3999996185302734	10
4207	2021-03-24 07:48:34.994045-05	35.7999992370605469	11
4208	2021-03-24 07:48:38.016678-05	25.3999996185302734	10
4209	2021-03-24 07:48:39.004213-05	35.7999992370605469	11
4210	2021-03-24 07:48:42.01347-05	25.3000011444091797	10
4211	2021-03-24 07:48:43.01402-05	35.7999992370605469	11
4212	2021-03-24 07:48:46.011897-05	25.3000011444091797	10
4213	2021-03-24 07:48:47.023146-05	35.7999992370605469	11
4214	2021-03-24 07:48:50.016337-05	25.3000011444091797	10
4215	2021-03-24 07:48:51.0237-05	35.7999992370605469	11
4216	2021-03-24 07:48:54.017856-05	25.3000011444091797	10
4217	2021-03-24 07:48:55.02279-05	35.7999992370605469	11
4218	2021-03-24 07:48:58.032433-05	25.3999996185302734	10
4219	2021-03-24 07:48:59.029551-05	35.9000015258789062	11
4220	2021-03-24 07:49:02.023938-05	25.3000011444091797	10
4221	2021-03-24 07:49:03.035551-05	35.7999992370605469	11
4222	2021-03-24 07:49:06.035385-05	25.3999996185302734	10
4223	2021-03-24 07:49:07.034081-05	35.9000015258789062	11
4224	2021-03-24 07:49:10.039473-05	25.3000011444091797	10
4225	2021-03-24 07:49:11.046468-05	35.7999992370605469	11
4226	2021-03-24 07:49:14.048517-05	25.3999996185302734	10
4227	2021-03-24 07:49:15.039112-05	35.9000015258789062	11
4228	2021-03-24 07:49:18.058455-05	25.3000011444091797	10
4229	2021-03-24 07:49:19.039828-05	35.7999992370605469	11
4230	2021-03-24 07:49:22.054725-05	25.3999996185302734	10
4231	2021-03-24 07:49:23.062923-05	35.9000015258789062	11
4232	2021-03-24 07:49:26.063028-05	25.3000011444091797	10
4233	2021-03-24 07:49:27.05008-05	35.7999992370605469	11
4234	2021-03-24 07:49:30.066911-05	25.3999996185302734	10
4235	2021-03-24 07:49:31.064873-05	35.9000015258789062	11
4236	2021-03-24 07:49:34.065223-05	25.3999996185302734	10
4237	2021-03-24 07:49:35.067473-05	35.9000015258789062	11
4238	2021-03-24 07:49:38.043352-05	25.3999996185302734	10
4239	2021-03-24 07:49:39.076444-05	35.9000015258789062	11
4240	2021-03-24 07:49:42.063506-05	25.3999996185302734	10
4241	2021-03-24 07:49:43.070103-05	35.9000015258789062	11
4242	2021-03-24 07:49:46.076185-05	25.3999996185302734	10
4243	2021-03-24 07:49:47.081176-05	35.9000015258789062	11
4244	2021-03-24 07:49:50.081884-05	25.3999996185302734	10
4245	2021-03-24 07:49:51.085818-05	35.9000015258789062	11
4246	2021-03-24 07:49:54.086929-05	25.3999996185302734	10
4247	2021-03-24 07:49:55.100686-05	35.9000015258789062	11
4248	2021-03-24 07:49:58.09234-05	25.3000011444091797	10
4249	2021-03-24 07:49:59.08935-05	35.7999992370605469	11
4250	2021-03-24 07:50:02.107003-05	25.3000011444091797	10
4251	2021-03-24 07:50:03.096176-05	35.7999992370605469	11
4252	2021-03-24 07:50:06.10239-05	25.3999996185302734	10
4253	2021-03-24 07:50:07.105526-05	35.7999992370605469	11
4254	2021-03-24 07:50:10.101666-05	25.3999996185302734	10
4255	2021-03-24 07:50:11.107153-05	35.7999992370605469	11
4256	2021-03-24 07:50:14.115121-05	25.3000011444091797	10
4257	2021-03-24 07:50:15.116469-05	35.7999992370605469	11
4258	2021-03-24 07:50:18.10613-05	25.3999996185302734	10
4259	2021-03-24 07:50:19.115739-05	35.7999992370605469	11
4260	2021-03-24 07:50:22.11938-05	25.3000011444091797	10
4261	2021-03-24 07:50:23.129219-05	35.7999992370605469	11
4262	2021-03-24 07:50:26.128564-05	25.3999996185302734	10
4263	2021-03-24 07:50:27.116085-05	35.7999992370605469	11
4264	2021-03-24 07:50:30.138612-05	25.3999996185302734	10
4265	2021-03-24 07:50:31.136061-05	35.7999992370605469	11
4266	2021-03-24 07:50:34.128185-05	25.3999996185302734	10
4267	2021-03-24 07:50:35.122478-05	35.7999992370605469	11
4268	2021-03-24 07:50:38.137791-05	25.3999996185302734	10
4269	2021-03-24 07:50:39.13467-05	35.9000015258789062	11
4270	2021-03-24 07:50:42.13856-05	25.3999996185302734	10
4271	2021-03-24 07:50:43.133775-05	35.7999992370605469	11
4272	2021-03-24 07:50:46.149493-05	25.3999996185302734	10
4273	2021-03-24 07:50:47.142831-05	35.7999992370605469	11
4274	2021-03-24 07:50:50.153922-05	25.3000011444091797	10
4275	2021-03-24 07:50:51.153568-05	35.7999992370605469	11
4276	2021-03-24 07:50:54.151067-05	25.3000011444091797	10
4277	2021-03-24 07:50:55.149269-05	35.7999992370605469	11
4278	2021-03-24 07:50:58.14952-05	25.3999996185302734	10
4279	2021-03-24 07:50:59.164402-05	35.7999992370605469	11
4280	2021-03-24 07:51:02.169361-05	25.3999996185302734	10
4281	2021-03-24 07:51:03.172106-05	35.7999992370605469	11
4282	2021-03-24 07:51:06.160606-05	25.3999996185302734	10
4283	2021-03-24 07:51:07.16656-05	35.7999992370605469	11
4284	2021-03-24 07:51:10.18065-05	25.3999996185302734	10
4285	2021-03-24 07:51:11.167237-05	35.7999992370605469	11
4286	2021-03-24 07:51:14.174427-05	25.3999996185302734	10
4287	2021-03-24 07:51:15.175205-05	35.7999992370605469	11
4288	2021-03-24 07:51:18.182749-05	25.3999996185302734	10
4289	2021-03-24 07:51:19.17875-05	35.7999992370605469	11
4290	2021-03-24 07:51:22.194065-05	25.3000011444091797	10
4291	2021-03-24 07:51:23.186497-05	35.7999992370605469	11
4292	2021-03-24 07:51:26.197446-05	25.3999996185302734	10
4293	2021-03-24 07:51:27.194513-05	35.7999992370605469	11
4294	2021-03-24 07:51:30.202427-05	25.3999996185302734	10
4295	2021-03-24 07:51:31.208049-05	35.7999992370605469	11
4296	2021-03-24 07:51:34.202093-05	25.3999996185302734	10
4297	2021-03-24 07:51:35.187853-05	35.7999992370605469	11
4298	2021-03-24 07:51:38.209671-05	25.3000011444091797	10
4299	2021-03-24 07:51:39.201244-05	35.7000007629394531	11
4300	2021-03-24 07:51:42.215162-05	25.3999996185302734	10
4301	2021-03-24 07:51:43.202653-05	35.7999992370605469	11
4302	2021-03-24 07:51:46.218934-05	25.3999996185302734	10
4303	2021-03-24 07:51:47.211858-05	35.7999992370605469	11
4304	2021-03-24 07:51:50.22016-05	25.3999996185302734	10
4305	2021-03-24 07:51:51.219029-05	35.7999992370605469	11
4306	2021-03-24 07:51:54.22554-05	25.3999996185302734	10
4307	2021-03-24 07:51:55.197077-05	35.7999992370605469	11
4308	2021-03-24 07:51:58.233364-05	25.3999996185302734	10
4309	2021-03-24 07:51:59.222575-05	35.7999992370605469	11
4310	2021-03-24 07:52:02.236102-05	25.3999996185302734	10
4311	2021-03-24 07:52:03.241327-05	35.7999992370605469	11
4312	2021-03-24 07:52:06.246082-05	25.3999996185302734	10
4313	2021-03-24 07:52:07.240896-05	35.7999992370605469	11
4314	2021-03-24 07:52:10.237785-05	25.3999996185302734	10
4315	2021-03-24 07:52:11.237389-05	35.7999992370605469	11
4316	2021-03-24 07:52:14.226184-05	25.3000011444091797	10
4317	2021-03-24 07:52:15.241693-05	35.7000007629394531	11
4318	2021-03-24 07:52:18.248805-05	25.3999996185302734	10
4319	2021-03-24 07:52:19.253705-05	35.7999992370605469	11
4320	2021-03-24 07:52:22.26738-05	25.3000011444091797	10
4321	2021-03-24 07:52:23.256723-05	35.7000007629394531	11
4322	2021-03-24 07:52:26.26128-05	25.3000011444091797	10
4323	2021-03-24 07:52:27.26279-05	35.7000007629394531	11
4324	2021-03-24 07:52:30.273442-05	25.3999996185302734	10
4325	2021-03-24 07:52:31.272014-05	35.7999992370605469	11
4326	2021-03-24 07:52:34.273426-05	25.3000011444091797	10
4327	2021-03-24 07:52:35.273755-05	35.7000007629394531	11
4328	2021-03-24 07:52:38.274852-05	25.3999996185302734	10
4329	2021-03-24 07:52:39.27289-05	35.7999992370605469	11
4330	2021-03-24 07:52:42.284266-05	25.3999996185302734	10
4331	2021-03-24 07:52:43.275722-05	35.7999992370605469	11
4332	2021-03-24 07:52:46.287175-05	25.3000011444091797	10
4333	2021-03-24 07:52:47.282917-05	35.7000007629394531	11
4334	2021-03-24 07:52:50.28653-05	25.3999996185302734	10
4335	2021-03-24 07:52:51.293347-05	35.7999992370605469	11
4336	2021-03-24 07:52:54.291434-05	25.3999996185302734	10
4337	2021-03-24 07:52:55.292641-05	35.7999992370605469	11
4338	2021-03-24 07:52:58.303779-05	25.3999996185302734	10
4339	2021-03-24 07:52:59.291623-05	35.7999992370605469	11
4340	2021-03-24 07:53:02.306528-05	25.3999996185302734	10
4341	2021-03-24 07:53:03.296768-05	35.7999992370605469	11
4342	2021-03-24 07:53:06.307489-05	25.3000011444091797	10
4343	2021-03-24 07:53:07.304021-05	35.7000007629394531	11
4344	2021-03-24 07:53:10.315189-05	25.3000011444091797	10
4345	2021-03-24 07:53:11.304852-05	35.7000007629394531	11
4346	2021-03-24 07:53:14.317125-05	25.3999996185302734	10
4347	2021-03-24 07:53:15.288243-05	35.7999992370605469	11
4348	2021-03-24 07:53:18.317865-05	25.3999996185302734	10
4349	2021-03-24 07:53:19.309107-05	35.7999992370605469	11
4350	2021-03-24 07:53:22.327599-05	25.3999996185302734	10
4351	2021-03-24 07:53:23.320801-05	35.7999992370605469	11
4352	2021-03-24 07:53:26.337572-05	25.3999996185302734	10
4353	2021-03-24 07:53:27.326241-05	35.7999992370605469	11
4354	2021-03-24 07:53:30.324035-05	25.3000011444091797	10
4355	2021-03-24 07:53:31.335345-05	35.7000007629394531	11
4356	2021-03-24 07:53:34.313923-05	25.3999996185302734	10
4357	2021-03-24 07:53:35.331769-05	35.7000007629394531	11
4358	2021-03-24 07:53:38.326792-05	25.3000011444091797	10
4359	2021-03-24 07:53:39.339333-05	35.7000007629394531	11
4360	2021-03-24 07:53:42.3356-05	25.3999996185302734	10
4361	2021-03-24 07:53:43.34792-05	35.7000007629394531	11
4362	2021-03-24 07:53:46.3469-05	25.3999996185302734	10
4363	2021-03-24 07:53:47.347467-05	35.7000007629394531	11
4364	2021-03-24 07:53:50.347229-05	25.3999996185302734	10
4365	2021-03-24 07:53:51.357866-05	35.7000007629394531	11
4366	2021-03-24 07:53:54.356883-05	25.3000011444091797	10
4367	2021-03-24 07:53:55.350593-05	35.7000007629394531	11
4368	2021-03-24 07:53:58.362362-05	25.3999996185302734	10
4369	2021-03-24 07:53:59.361315-05	35.7000007629394531	11
4370	2021-03-24 07:54:02.36796-05	25.3999996185302734	10
4371	2021-03-24 07:54:03.36995-05	35.7000007629394531	11
4372	2021-03-24 07:54:06.369441-05	25.3999996185302734	10
4373	2021-03-24 07:54:07.360216-05	35.7999992370605469	11
4374	2021-03-24 07:54:10.367981-05	25.3999996185302734	10
4375	2021-03-24 07:54:11.375418-05	35.7999992370605469	11
4376	2021-03-24 07:54:14.379233-05	25.3999996185302734	10
4377	2021-03-24 07:54:15.372992-05	35.7999992370605469	11
4378	2021-03-24 07:54:18.38604-05	25.3999996185302734	10
4379	2021-03-24 07:54:19.386961-05	35.7999992370605469	11
4380	2021-03-24 07:54:22.3923-05	25.3999996185302734	10
4381	2021-03-24 07:54:23.395145-05	35.7999992370605469	11
4382	2021-03-24 07:54:26.392569-05	25.3999996185302734	10
4383	2021-03-24 07:54:27.38722-05	35.7000007629394531	11
4384	2021-03-24 07:54:30.403745-05	25.3999996185302734	10
4385	2021-03-24 07:54:31.375538-05	35.7999992370605469	11
4386	2021-03-24 07:54:34.404733-05	25.3999996185302734	10
4387	2021-03-24 07:54:35.402629-05	35.7999992370605469	11
4388	2021-03-24 07:54:38.403204-05	25.3999996185302734	10
4389	2021-03-24 07:54:39.413913-05	35.7999992370605469	11
4390	2021-03-24 07:54:42.417655-05	25.3000011444091797	10
4391	2021-03-24 07:54:43.407159-05	35.7000007629394531	11
4392	2021-03-24 07:54:46.411139-05	25.3999996185302734	10
4393	2021-03-24 07:54:47.413151-05	35.7999992370605469	11
4394	2021-03-24 07:54:50.419324-05	25.3000011444091797	10
4395	2021-03-24 07:54:51.432995-05	35.7000007629394531	11
4396	2021-03-24 07:54:54.417398-05	25.3999996185302734	10
4397	2021-03-24 07:54:55.419801-05	35.7999992370605469	11
4398	2021-03-24 07:54:58.43108-05	25.3999996185302734	10
4399	2021-03-24 07:54:59.430298-05	35.7000007629394531	11
4400	2021-03-24 07:55:02.435061-05	25.3999996185302734	10
4401	2021-03-24 07:55:03.436279-05	35.7999992370605469	11
4402	2021-03-24 07:55:06.442153-05	25.3999996185302734	10
4403	2021-03-24 07:55:07.435624-05	35.7999992370605469	11
4404	2021-03-24 07:55:10.432101-05	25.3999996185302734	10
4405	2021-03-24 07:55:11.429537-05	35.7999992370605469	11
4406	2021-03-24 07:55:14.461432-05	25.3000011444091797	10
4407	2021-03-24 07:55:15.447578-05	35.7000007629394531	11
4408	2021-03-24 07:55:18.450345-05	25.3000011444091797	10
4409	2021-03-24 07:55:19.441853-05	35.7000007629394531	11
4410	2021-03-24 07:55:22.465432-05	25.3999996185302734	10
4411	2021-03-24 07:55:23.453558-05	35.7999992370605469	11
4412	2021-03-24 07:55:26.467116-05	25.3000011444091797	10
4413	2021-03-24 07:55:27.46877-05	35.7000007629394531	11
4414	2021-03-24 07:55:30.476169-05	25.3999996185302734	10
4415	2021-03-24 07:55:31.479001-05	35.7999992370605469	11
4416	2021-03-24 07:55:34.471917-05	25.3000011444091797	10
4417	2021-03-24 07:55:35.478587-05	35.7000007629394531	11
4418	2021-03-24 07:55:38.492026-05	25.3999996185302734	10
4419	2021-03-24 07:55:39.47947-05	35.7999992370605469	11
4420	2021-03-24 07:55:42.494567-05	25.3000011444091797	10
4421	2021-03-24 07:55:43.486782-05	35.7000007629394531	11
4422	2021-03-24 07:55:46.489699-05	25.3999996185302734	10
4423	2021-03-24 07:55:47.481813-05	35.7999992370605469	11
4424	2021-03-24 07:55:50.485328-05	25.3999996185302734	10
4425	2021-03-24 07:55:51.482456-05	35.7999992370605469	11
4426	2021-03-24 07:55:54.50431-05	25.3999996185302734	10
4427	2021-03-24 07:55:55.496414-05	35.7999992370605469	11
4428	2021-03-24 07:55:58.507768-05	25.3999996185302734	10
4429	2021-03-24 07:55:59.507335-05	35.7999992370605469	11
4430	2021-03-24 07:56:02.514436-05	25.3999996185302734	10
4431	2021-03-24 07:56:03.510597-05	35.7999992370605469	11
4432	2021-03-24 07:56:06.495574-05	25.3000011444091797	10
4433	2021-03-24 07:56:07.516211-05	35.7000007629394531	11
4434	2021-03-24 07:56:10.519189-05	25.3000011444091797	10
4435	2021-03-24 07:56:11.523017-05	35.7000007629394531	11
4436	2021-03-24 07:56:14.531192-05	25.3999996185302734	10
4437	2021-03-24 07:56:15.526702-05	35.7999992370605469	11
4438	2021-03-24 07:56:18.527209-05	25.3999996185302734	10
4439	2021-03-24 07:56:19.530842-05	35.7999992370605469	11
4440	2021-03-24 07:56:22.533225-05	25.3999996185302734	10
4441	2021-03-24 07:56:23.533951-05	35.7999992370605469	11
4442	2021-03-24 07:56:26.542179-05	25.3999996185302734	10
4443	2021-03-24 07:56:27.53785-05	35.7999992370605469	11
4444	2021-03-24 07:56:30.551224-05	25.3999996185302734	10
4445	2021-03-24 07:56:31.547517-05	35.7999992370605469	11
4446	2021-03-24 07:56:34.54318-05	25.3000011444091797	10
4447	2021-03-24 07:56:35.55253-05	35.7999992370605469	11
4448	2021-03-24 07:56:38.552674-05	25.3000011444091797	10
4449	2021-03-24 07:56:39.552272-05	35.7999992370605469	11
4450	2021-03-24 07:56:42.551546-05	25.3999996185302734	10
4451	2021-03-24 07:56:43.560905-05	35.7999992370605469	11
4452	2021-03-24 07:56:46.553723-05	25.3999996185302734	10
4453	2021-03-24 07:56:47.5541-05	35.7999992370605469	11
4454	2021-03-24 07:56:50.565712-05	25.3999996185302734	10
4455	2021-03-24 07:56:51.561429-05	35.7999992370605469	11
4456	2021-03-24 07:56:54.567193-05	25.3000011444091797	10
4457	2021-03-24 07:56:55.569744-05	35.7999992370605469	11
4458	2021-03-24 07:56:58.570054-05	25.3000011444091797	10
4459	2021-03-24 07:56:59.577388-05	35.7999992370605469	11
4460	2021-03-24 07:57:02.57846-05	25.3000011444091797	10
4461	2021-03-24 07:57:03.576467-05	35.7999992370605469	11
4462	2021-03-24 07:57:06.585545-05	25.3999996185302734	10
4463	2021-03-24 07:57:07.559048-05	35.7999992370605469	11
4464	2021-03-24 07:57:10.596911-05	25.3000011444091797	10
4465	2021-03-24 07:57:11.583067-05	35.7999992370605469	11
4466	2021-03-24 07:57:14.592621-05	25.3999996185302734	10
4467	2021-03-24 07:57:15.59879-05	35.7999992370605469	11
4468	2021-03-24 07:57:18.611417-05	25.3999996185302734	10
4469	2021-03-24 07:57:19.592755-05	35.7999992370605469	11
4470	2021-03-24 07:57:22.601355-05	25.3000011444091797	10
4471	2021-03-24 07:57:23.5992-05	35.7999992370605469	11
4472	2021-03-24 07:57:26.580239-05	25.3000011444091797	10
4473	2021-03-24 07:57:27.608095-05	35.7999992370605469	11
4474	2021-03-24 07:57:30.614241-05	25.3999996185302734	10
4475	2021-03-24 07:57:31.615202-05	35.7999992370605469	11
4476	2021-03-24 07:57:34.610985-05	25.3000011444091797	10
4477	2021-03-24 07:57:35.608354-05	35.7999992370605469	11
4478	2021-03-24 07:57:38.621908-05	25.3000011444091797	10
4479	2021-03-24 07:57:39.632349-05	35.7999992370605469	11
4480	2021-03-24 07:57:42.625346-05	25.3999996185302734	10
4481	2021-03-24 07:57:43.623209-05	35.7999992370605469	11
4482	2021-03-24 07:57:46.622165-05	25.3000011444091797	10
4483	2021-03-24 07:57:47.633093-05	35.7999992370605469	11
4484	2021-03-24 07:57:50.637584-05	25.3000011444091797	10
4485	2021-03-24 07:57:51.632423-05	35.7999992370605469	11
4486	2021-03-24 07:57:54.63202-05	25.3999996185302734	10
4487	2021-03-24 07:57:55.634407-05	35.7999992370605469	11
4488	2021-03-24 07:57:58.652009-05	25.3000011444091797	10
4489	2021-03-24 07:57:59.641739-05	35.7999992370605469	11
4490	2021-03-24 07:58:02.65611-05	25.3000011444091797	10
4491	2021-03-24 07:58:03.646899-05	35.7999992370605469	11
4492	2021-03-24 07:58:06.656986-05	25.3999996185302734	10
4493	2021-03-24 07:58:07.656852-05	35.7999992370605469	11
4494	2021-03-24 07:58:10.664964-05	25.3000011444091797	10
4495	2021-03-24 07:58:11.662665-05	35.7999992370605469	11
4496	2021-03-24 07:58:14.668642-05	25.3999996185302734	10
4497	2021-03-24 07:58:15.669508-05	35.7999992370605469	11
4498	2021-03-24 07:58:18.663917-05	25.3999996185302734	10
4499	2021-03-24 07:58:19.675718-05	35.7999992370605469	11
4500	2021-03-24 07:58:22.673521-05	25.3999996185302734	10
4501	2021-03-24 07:58:23.651241-05	35.7999992370605469	11
4502	2021-03-24 07:58:26.686625-05	25.3000011444091797	10
4503	2021-03-24 07:58:27.675162-05	35.7999992370605469	11
4504	2021-03-24 07:58:30.685877-05	25.3000011444091797	10
4505	2021-03-24 07:58:31.681722-05	35.7999992370605469	11
4506	2021-03-24 07:58:34.689221-05	25.3000011444091797	10
4507	2021-03-24 07:58:35.694832-05	35.7999992370605469	11
4508	2021-03-24 07:58:38.693922-05	25.3999996185302734	10
4509	2021-03-24 07:58:39.695911-05	35.7999992370605469	11
4510	2021-03-24 07:58:42.680059-05	25.3000011444091797	10
4511	2021-03-24 07:58:43.693026-05	35.7999992370605469	11
4512	2021-03-24 07:58:46.693924-05	25.3999996185302734	10
4513	2021-03-24 07:58:47.703867-05	35.7999992370605469	11
4514	2021-03-24 07:58:50.706475-05	25.3999996185302734	10
4515	2021-03-24 07:58:51.707505-05	35.7999992370605469	11
4516	2021-03-24 07:58:54.710591-05	25.3999996185302734	10
4517	2021-03-24 07:58:55.709229-05	35.7999992370605469	11
4518	2021-03-24 07:58:58.716266-05	25.3999996185302734	10
4519	2021-03-24 07:58:59.715496-05	35.7999992370605469	11
4520	2021-03-24 07:59:02.717209-05	25.3999996185302734	10
4521	2021-03-24 07:59:03.717446-05	35.7999992370605469	11
4522	2021-03-24 07:59:06.71955-05	25.3000011444091797	10
4523	2021-03-24 07:59:07.723238-05	35.7000007629394531	11
4524	2021-03-24 07:59:10.725519-05	25.3000011444091797	10
4525	2021-03-24 07:59:11.726823-05	35.7000007629394531	11
4526	2021-03-24 07:59:14.730057-05	25.3000011444091797	10
4527	2021-03-24 07:59:15.734306-05	35.7000007629394531	11
4528	2021-03-24 07:59:18.73715-05	25.3999996185302734	10
4529	2021-03-24 07:59:19.732963-05	35.7999992370605469	11
4530	2021-03-24 07:59:22.739581-05	25.3000011444091797	10
4531	2021-03-24 07:59:23.738036-05	35.7000007629394531	11
4532	2021-03-24 07:59:26.744465-05	25.3999996185302734	10
4533	2021-03-24 07:59:27.73814-05	35.7999992370605469	11
4534	2021-03-24 07:59:30.752584-05	25.3000011444091797	10
4535	2021-03-24 07:59:31.752457-05	35.7000007629394531	11
4536	2021-03-24 07:59:34.753273-05	25.3999996185302734	10
4537	2021-03-24 07:59:35.755799-05	35.7999992370605469	11
4538	2021-03-24 07:59:38.762615-05	25.3000011444091797	10
4539	2021-03-24 07:59:39.758483-05	35.7000007629394531	11
4540	2021-03-24 07:59:42.751413-05	25.3999996185302734	10
4541	2021-03-24 07:59:43.740922-05	35.7999992370605469	11
4542	2021-03-24 07:59:46.76425-05	25.3999996185302734	10
4543	2021-03-24 07:59:47.77539-05	35.7999992370605469	11
4544	2021-03-24 07:59:50.775464-05	25.3999996185302734	10
4545	2021-03-24 07:59:51.765935-05	35.7999992370605469	11
4546	2021-03-24 07:59:54.775794-05	25.3999996185302734	10
4547	2021-03-24 07:59:55.779371-05	35.7999992370605469	11
4548	2021-03-24 07:59:58.77976-05	25.3000011444091797	10
4549	2021-03-24 07:59:59.780595-05	35.7000007629394531	11
4550	2021-03-24 08:00:02.75911-05	25.3999996185302734	10
4551	2021-03-24 08:00:03.792429-05	35.7999992370605469	11
4552	2021-03-24 08:00:06.780493-05	25.3000011444091797	10
4553	2021-03-24 08:00:07.796404-05	35.7000007629394531	11
4554	2021-03-24 08:00:10.79085-05	25.3999996185302734	10
4555	2021-03-24 08:00:11.799195-05	35.7999992370605469	11
4556	2021-03-24 08:00:19.789064-05	25.3000011444091797	10
4557	2021-03-24 08:00:20.043522-05	35.7000007629394531	11
4558	2021-03-24 08:00:20.309721-05	25.3000011444091797	10
4559	2021-03-24 08:00:20.571407-05	35.7000007629394531	11
4560	2021-03-24 08:00:22.805666-05	25.3999996185302734	10
4561	2021-03-24 08:00:23.809425-05	35.7999992370605469	11
4562	2021-03-24 08:00:26.815067-05	25.3999996185302734	10
4563	2021-03-24 08:00:27.815164-05	35.7999992370605469	11
4564	2021-03-24 08:00:30.81788-05	25.3000011444091797	10
4565	2021-03-24 08:00:31.818416-05	35.7000007629394531	11
4566	2021-03-24 08:00:34.819515-05	25.3000011444091797	10
4567	2021-03-24 08:00:35.820495-05	35.7000007629394531	11
4568	2021-03-24 08:00:38.826793-05	25.3999996185302734	10
4569	2021-03-24 08:00:39.826597-05	35.7999992370605469	11
4570	2021-03-24 08:00:42.825565-05	25.3000011444091797	10
4571	2021-03-24 08:00:43.824619-05	35.7000007629394531	11
4572	2021-03-24 08:00:46.82919-05	25.3000011444091797	10
4573	2021-03-24 08:00:47.833861-05	35.7000007629394531	11
4574	2021-03-24 08:00:50.841915-05	25.3999996185302734	10
4575	2021-03-24 08:00:51.830568-05	35.7999992370605469	11
4576	2021-03-24 08:00:54.830692-05	25.3999996185302734	10
4577	2021-03-24 08:00:55.835891-05	35.7999992370605469	11
4578	2021-03-24 08:00:58.835241-05	25.3999996185302734	10
4579	2021-03-24 08:00:59.836262-05	35.7999992370605469	11
4580	2021-03-24 08:01:02.852415-05	25.3999996185302734	10
4581	2021-03-24 08:01:03.846301-05	35.7999992370605469	11
4582	2021-03-24 08:01:06.862995-05	25.3999996185302734	10
4583	2021-03-24 08:01:07.853241-05	35.7999992370605469	11
4584	2021-03-24 08:01:10.859884-05	25.3999996185302734	10
4585	2021-03-24 08:01:11.850515-05	35.7999992370605469	11
4586	2021-03-24 08:01:14.869671-05	25.3000011444091797	10
4587	2021-03-24 08:01:15.859755-05	35.7999992370605469	11
4588	2021-03-24 08:01:18.841819-05	25.3000011444091797	10
4589	2021-03-24 08:01:19.863242-05	35.7999992370605469	11
4590	2021-03-24 08:01:22.874282-05	25.3000011444091797	10
4591	2021-03-24 08:01:23.876932-05	35.7999992370605469	11
4592	2021-03-24 08:01:26.867582-05	25.3999996185302734	10
4593	2021-03-24 08:01:27.875366-05	35.7999992370605469	11
4594	2021-03-24 08:01:30.880068-05	25.3999996185302734	10
4595	2021-03-24 08:01:31.877579-05	35.7999992370605469	11
4596	2021-03-24 08:01:34.884842-05	25.3000011444091797	10
4597	2021-03-24 08:01:35.886212-05	35.7999992370605469	11
4598	2021-03-24 08:01:38.903705-05	25.3999996185302734	10
4599	2021-03-24 08:01:39.893508-05	35.7999992370605469	11
4600	2021-03-24 08:01:42.884655-05	25.3000011444091797	10
4601	2021-03-24 08:01:43.897958-05	35.7999992370605469	11
4602	2021-03-24 08:01:46.888477-05	25.3999996185302734	10
4603	2021-03-24 08:01:47.899934-05	35.7999992370605469	11
4604	2021-03-24 08:01:50.897711-05	25.3999996185302734	10
4605	2021-03-24 08:01:51.912112-05	35.7999992370605469	11
4606	2021-03-24 08:01:54.911523-05	25.3000011444091797	10
4607	2021-03-24 08:01:55.909344-05	35.7999992370605469	11
4608	2021-03-24 08:01:58.918771-05	25.3999996185302734	10
4609	2021-03-24 08:01:59.909388-05	35.7999992370605469	11
4610	2021-03-24 08:02:02.914601-05	25.3000011444091797	10
4611	2021-03-24 08:02:03.919368-05	35.7999992370605469	11
4612	2021-03-24 08:02:06.921399-05	25.3000011444091797	10
4613	2021-03-24 08:02:07.930611-05	35.7999992370605469	11
4614	2021-03-24 08:02:10.925799-05	25.3000011444091797	10
4615	2021-03-24 08:02:11.919984-05	35.7999992370605469	11
4616	2021-03-24 08:02:14.937137-05	25.3000011444091797	10
4617	2021-03-24 08:02:15.907605-05	35.7999992370605469	11
4618	2021-03-24 08:02:18.930385-05	25.3000011444091797	10
4619	2021-03-24 08:02:19.934458-05	35.7999992370605469	11
4620	2021-03-24 08:02:22.944911-05	25.3000011444091797	10
4621	2021-03-24 08:02:23.946187-05	35.7999992370605469	11
4622	2021-03-24 08:02:26.947357-05	25.3000011444091797	10
4623	2021-03-24 08:02:27.942756-05	35.7999992370605469	11
4624	2021-03-24 08:02:30.952927-05	25.3999996185302734	10
4625	2021-03-24 08:02:31.943845-05	35.7999992370605469	11
4626	2021-03-24 08:02:34.934799-05	25.3999996185302734	10
4627	2021-03-24 08:02:35.960467-05	35.7999992370605469	11
4628	2021-03-24 08:02:38.955986-05	25.3999996185302734	10
4629	2021-03-24 08:02:39.96413-05	35.7999992370605469	11
4630	2021-03-24 08:02:42.974875-05	25.3999996185302734	10
4631	2021-03-24 08:02:43.969491-05	35.7999992370605469	11
4632	2021-03-24 08:02:46.966938-05	25.3999996185302734	10
4633	2021-03-24 08:02:47.966976-05	35.9000015258789062	11
4634	2021-03-24 08:02:50.969665-05	25.3000011444091797	10
4635	2021-03-24 08:02:51.969356-05	35.7999992370605469	11
4636	2021-03-24 08:02:54.982451-05	25.3999996185302734	10
4637	2021-03-24 08:02:55.988811-05	35.7999992370605469	11
4638	2021-03-24 08:02:58.986715-05	25.3000011444091797	10
4639	2021-03-24 08:02:59.981158-05	35.7999992370605469	11
4640	2021-03-24 08:03:02.981445-05	25.3000011444091797	10
4641	2021-03-24 08:03:03.994879-05	35.7999992370605469	11
4642	2021-03-24 08:03:06.992074-05	25.3999996185302734	10
4643	2021-03-24 08:03:07.98533-05	35.7999992370605469	11
4644	2021-03-24 08:03:10.99881-05	25.3000011444091797	10
4645	2021-03-24 08:03:11.989475-05	35.7999992370605469	11
4646	2021-03-24 08:03:14.998685-05	25.3000011444091797	10
4647	2021-03-24 08:03:15.998812-05	35.7999992370605469	11
4648	2021-03-24 08:03:19.003642-05	25.3999996185302734	10
4649	2021-03-24 08:03:19.998296-05	35.7999992370605469	11
4650	2021-03-24 08:03:23.007244-05	25.3000011444091797	10
4651	2021-03-24 08:03:24.004903-05	35.7999992370605469	11
4652	2021-03-24 08:03:27.007476-05	25.3999996185302734	10
4653	2021-03-24 08:03:28.013927-05	35.7999992370605469	11
4654	2021-03-24 08:03:31.013189-05	25.3999996185302734	10
4655	2021-03-24 08:03:31.996886-05	35.7999992370605469	11
4656	2021-03-24 08:03:35.017493-05	25.3999996185302734	10
4657	2021-03-24 08:03:36.018297-05	35.7999992370605469	11
4658	2021-03-24 08:03:39.02207-05	25.3999996185302734	10
4659	2021-03-24 08:03:40.019315-05	35.7999992370605469	11
4660	2021-03-24 08:03:48.03369-05	25.3999996185302734	10
4661	2021-03-24 08:03:48.309814-05	35.7999992370605469	11
4662	2021-03-24 08:03:48.581275-05	25.3999996185302734	10
4663	2021-03-24 08:03:48.854769-05	35.7999992370605469	11
4664	2021-03-24 08:03:51.00942-05	25.3000011444091797	10
4665	2021-03-24 08:03:52.037347-05	35.7999992370605469	11
4666	2021-03-24 08:03:55.036166-05	25.3000011444091797	10
4667	2021-03-24 08:03:56.039699-05	35.7999992370605469	11
4668	2021-03-24 08:03:59.051595-05	25.3999996185302734	10
4669	2021-03-24 08:04:00.050896-05	35.7999992370605469	11
4670	2021-03-24 08:04:03.053469-05	25.3000011444091797	10
4671	2021-03-24 08:04:04.046063-05	35.7999992370605469	11
4672	2021-03-24 08:04:07.051216-05	25.3999996185302734	10
4673	2021-03-24 08:04:08.054519-05	35.7999992370605469	11
4674	2021-03-24 08:04:11.051944-05	25.3999996185302734	10
4675	2021-03-24 08:04:12.05937-05	35.7999992370605469	11
4676	2021-03-24 08:04:15.065636-05	25.3999996185302734	10
4677	2021-03-24 08:04:16.059008-05	35.7999992370605469	11
4678	2021-03-24 08:04:19.068791-05	25.3999996185302734	10
4679	2021-03-24 08:04:20.076195-05	35.7999992370605469	11
4680	2021-03-24 08:04:23.072178-05	25.3999996185302734	10
4681	2021-03-24 08:04:24.070404-05	35.7999992370605469	11
4682	2021-03-24 08:04:27.080289-05	25.3999996185302734	10
4683	2021-03-24 08:04:28.071991-05	35.7999992370605469	11
4684	2021-03-24 08:04:31.088321-05	25.3999996185302734	10
4685	2021-03-24 08:04:32.071242-05	35.7999992370605469	11
4686	2021-03-24 08:04:35.086225-05	25.3000011444091797	10
4687	2021-03-24 08:04:36.083419-05	35.7999992370605469	11
4688	2021-03-24 08:04:39.100872-05	25.3999996185302734	10
4689	2021-03-24 08:04:40.092475-05	35.7999992370605469	11
4690	2021-03-24 08:04:43.099502-05	25.3999996185302734	10
4691	2021-03-24 08:04:44.099275-05	35.7999992370605469	11
4692	2021-03-24 08:04:47.111192-05	25.3000011444091797	10
4693	2021-03-24 08:04:48.073352-05	35.7999992370605469	11
4694	2021-03-24 08:04:51.109138-05	25.3999996185302734	10
4695	2021-03-24 08:04:52.110802-05	35.7999992370605469	11
4696	2021-03-24 08:04:55.116687-05	25.3999996185302734	10
4697	2021-03-24 08:04:56.118819-05	35.7999992370605469	11
4698	2021-03-24 08:04:59.12357-05	25.3999996185302734	10
4699	2021-03-24 08:05:00.114981-05	35.7999992370605469	11
4700	2021-03-24 08:05:03.13069-05	25.3000011444091797	10
4701	2021-03-24 08:05:04.124615-05	35.7999992370605469	11
4702	2021-03-24 08:05:07.114676-05	25.3999996185302734	10
4703	2021-03-24 08:05:08.121107-05	35.7999992370605469	11
4704	2021-03-24 08:05:11.132842-05	25.3000011444091797	10
4705	2021-03-24 08:05:12.133858-05	35.7999992370605469	11
4706	2021-03-24 08:05:15.143175-05	25.3999996185302734	10
4707	2021-03-24 08:05:16.134512-05	35.7999992370605469	11
4708	2021-03-24 08:05:19.136715-05	25.3999996185302734	10
4709	2021-03-24 08:05:20.140898-05	35.7999992370605469	11
4710	2021-03-24 08:05:23.133475-05	25.3000011444091797	10
4711	2021-03-24 08:05:24.151058-05	35.7999992370605469	11
4712	2021-03-24 08:05:27.142528-05	25.3999996185302734	10
4713	2021-03-24 08:05:28.148828-05	35.7999992370605469	11
4714	2021-03-24 08:05:31.153017-05	25.3999996185302734	10
4715	2021-03-24 08:05:32.164187-05	35.7999992370605469	11
4716	2021-03-24 08:05:35.163636-05	25.3000011444091797	10
4717	2021-03-24 08:05:36.15298-05	35.7999992370605469	11
4718	2021-03-24 08:05:39.169795-05	25.3999996185302734	10
4719	2021-03-24 08:05:40.171753-05	35.7999992370605469	11
4720	2021-03-24 08:05:43.169916-05	25.3999996185302734	10
4721	2021-03-24 08:05:44.17614-05	35.7999992370605469	11
4722	2021-03-24 08:05:47.176244-05	25.3000011444091797	10
4723	2021-03-24 08:05:48.178517-05	35.7999992370605469	11
4724	2021-03-24 08:05:51.183035-05	25.3999996185302734	10
4725	2021-03-24 08:05:52.17702-05	35.7999992370605469	11
4726	2021-03-24 08:05:55.183521-05	25.3999996185302734	10
4727	2021-03-24 08:05:56.17641-05	35.7999992370605469	11
4728	2021-03-24 08:05:59.187233-05	25.3999996185302734	10
4729	2021-03-24 08:06:00.17595-05	35.7999992370605469	11
4730	2021-03-24 08:06:03.18969-05	25.3000011444091797	10
4731	2021-03-24 08:06:04.164699-05	35.7999992370605469	11
4732	2021-03-24 08:06:07.200071-05	25.3999996185302734	10
4733	2021-03-24 08:06:08.192319-05	35.7999992370605469	11
4734	2021-03-24 08:06:11.195444-05	25.3999996185302734	10
4735	2021-03-24 08:06:12.200431-05	35.7999992370605469	11
4736	2021-03-24 08:06:15.209905-05	25.3000011444091797	10
4737	2021-03-24 08:06:16.195957-05	35.7999992370605469	11
4738	2021-03-24 08:06:19.204287-05	25.3999996185302734	10
4739	2021-03-24 08:06:20.211134-05	35.7999992370605469	11
4740	2021-03-24 08:06:23.183657-05	25.3000011444091797	10
4741	2021-03-24 08:06:24.209716-05	35.7999992370605469	11
4742	2021-03-24 08:06:27.210787-05	25.3999996185302734	10
4743	2021-03-24 08:06:28.214995-05	35.7999992370605469	11
4744	2021-03-24 08:06:31.223468-05	25.3999996185302734	10
4745	2021-03-24 08:06:32.223345-05	35.7999992370605469	11
4746	2021-03-24 08:06:35.22457-05	25.3999996185302734	10
4747	2021-03-24 08:06:36.227699-05	35.7999992370605469	11
4748	2021-03-24 08:06:39.229606-05	25.3999996185302734	10
4749	2021-03-24 08:06:40.244454-05	35.7999992370605469	11
4750	2021-03-24 08:06:43.23397-05	25.3999996185302734	10
4751	2021-03-24 08:06:44.232714-05	35.7999992370605469	11
4752	2021-03-24 08:06:47.234141-05	25.3999996185302734	10
4753	2021-03-24 08:06:48.240738-05	35.7999992370605469	11
4754	2021-03-24 08:06:51.245776-05	25.3999996185302734	10
4755	2021-03-24 08:06:52.230986-05	35.7999992370605469	11
4756	2021-03-24 08:06:55.243608-05	25.3999996185302734	10
4757	2021-03-24 08:06:56.238764-05	35.7999992370605469	11
4758	2021-03-24 08:06:59.248256-05	25.3999996185302734	10
4759	2021-03-24 08:07:00.257148-05	35.7999992370605469	11
4760	2021-03-24 08:07:03.254986-05	25.3999996185302734	10
4761	2021-03-24 08:07:04.250981-05	35.7999992370605469	11
4762	2021-03-24 08:07:07.255414-05	25.3999996185302734	10
4763	2021-03-24 08:07:08.255091-05	35.7999992370605469	11
4764	2021-03-24 08:07:11.27246-05	25.3999996185302734	10
4765	2021-03-24 08:07:12.259505-05	35.7999992370605469	11
4766	2021-03-24 08:07:15.272059-05	25.3000011444091797	10
4767	2021-03-24 08:07:16.271364-05	35.7999992370605469	11
4768	2021-03-24 08:07:19.273196-05	25.3999996185302734	10
4769	2021-03-24 08:07:20.23821-05	35.9000015258789062	11
4770	2021-03-24 08:07:23.277052-05	25.3999996185302734	10
4771	2021-03-24 08:07:24.273003-05	35.7999992370605469	11
4772	2021-03-24 08:07:27.282603-05	25.3000011444091797	10
4773	2021-03-24 08:07:28.284261-05	35.7999992370605469	11
4774	2021-03-24 08:07:31.287012-05	25.3999996185302734	10
4775	2021-03-24 08:07:32.277043-05	35.9000015258789062	11
4776	2021-03-24 08:07:35.294161-05	25.3000011444091797	10
4777	2021-03-24 08:07:36.290215-05	35.7999992370605469	11
4778	2021-03-24 08:07:39.272629-05	25.3999996185302734	10
4779	2021-03-24 08:07:40.298323-05	35.7999992370605469	11
4780	2021-03-24 08:07:43.299888-05	25.3999996185302734	10
4781	2021-03-24 08:07:44.289539-05	35.7999992370605469	11
4782	2021-03-24 08:07:47.306797-05	25.3999996185302734	10
4783	2021-03-24 08:07:48.30327-05	35.9000015258789062	11
4784	2021-03-24 08:07:51.300571-05	25.3999996185302734	10
4785	2021-03-24 08:07:52.320357-05	35.7999992370605469	11
4786	2021-03-24 08:07:55.307997-05	25.3000011444091797	10
4787	2021-03-24 08:07:56.318401-05	35.7999992370605469	11
4788	2021-03-24 08:07:59.32066-05	25.3000011444091797	10
4789	2021-03-24 08:08:00.318159-05	35.7999992370605469	11
4790	2021-03-24 08:08:03.319317-05	25.3999996185302734	10
4791	2021-03-24 08:08:04.327345-05	35.7999992370605469	11
4792	2021-03-24 08:08:07.32013-05	25.3999996185302734	10
4793	2021-03-24 08:08:08.320588-05	35.7999992370605469	11
4794	2021-03-24 08:08:11.332978-05	25.3000011444091797	10
4795	2021-03-24 08:08:12.331635-05	35.7999992370605469	11
4796	2021-03-24 08:08:15.336926-05	25.3999996185302734	10
4797	2021-03-24 08:08:16.338606-05	35.7999992370605469	11
4798	2021-03-24 08:08:19.337073-05	25.3999996185302734	10
4799	2021-03-24 08:08:20.337415-05	35.7999992370605469	11
4800	2021-03-24 08:08:23.346333-05	25.3000011444091797	10
4801	2021-03-24 08:08:24.348643-05	35.7999992370605469	11
4802	2021-03-24 08:08:27.34922-05	25.3999996185302734	10
4803	2021-03-24 08:08:28.351356-05	35.7999992370605469	11
4804	2021-03-24 08:08:31.355978-05	25.3999996185302734	10
4805	2021-03-24 08:08:32.36026-05	35.7999992370605469	11
4806	2021-03-24 08:08:35.358939-05	25.3000011444091797	10
4807	2021-03-24 08:08:36.328553-05	35.7999992370605469	11
4808	2021-03-24 08:08:39.362864-05	25.3000011444091797	10
4809	2021-03-24 08:08:40.355916-05	35.7999992370605469	11
4810	2021-03-24 08:08:43.359423-05	25.3999996185302734	10
4811	2021-03-24 08:08:44.367115-05	35.7999992370605469	11
4812	2021-03-24 08:08:47.36579-05	25.3999996185302734	10
4813	2021-03-24 08:08:48.361318-05	35.7999992370605469	11
4814	2021-03-24 08:08:51.367498-05	25.3000011444091797	10
4815	2021-03-24 08:08:52.373815-05	35.7999992370605469	11
4816	2021-03-24 08:08:55.365397-05	25.3999996185302734	10
4817	2021-03-24 08:08:56.379828-05	35.7999992370605469	11
4818	2021-03-24 08:08:59.377095-05	25.3000011444091797	10
4819	2021-03-24 08:09:00.385227-05	35.7999992370605469	11
4820	2021-03-24 08:09:03.392079-05	25.3999996185302734	10
4821	2021-03-24 08:09:04.392554-05	35.7999992370605469	11
4822	2021-03-24 08:09:07.387325-05	25.3999996185302734	10
4823	2021-03-24 08:09:08.40214-05	35.7999992370605469	11
4824	2021-03-24 08:09:11.392948-05	25.3000011444091797	10
4825	2021-03-24 08:09:12.397248-05	35.7999992370605469	11
4826	2021-03-24 08:09:15.39985-05	25.3999996185302734	10
4827	2021-03-24 08:09:16.397484-05	35.7999992370605469	11
4828	2021-03-24 08:09:19.410295-05	25.3999996185302734	10
4829	2021-03-24 08:09:20.403111-05	35.7999992370605469	11
4830	2021-03-24 08:09:23.411622-05	25.3999996185302734	10
4831	2021-03-24 08:09:24.404254-05	35.7999992370605469	11
4832	2021-03-24 08:09:27.407037-05	25.3999996185302734	10
4833	2021-03-24 08:09:28.396899-05	35.7999992370605469	11
4834	2021-03-24 08:09:31.414943-05	25.3000011444091797	10
4835	2021-03-24 08:09:32.415703-05	35.7999992370605469	11
4836	2021-03-24 08:09:35.4206-05	25.3999996185302734	10
4837	2021-03-24 08:09:36.425345-05	35.7999992370605469	11
4838	2021-03-24 08:09:39.432228-05	25.3999996185302734	10
4839	2021-03-24 08:09:40.427587-05	35.7999992370605469	11
4840	2021-03-24 08:09:43.42141-05	25.3000011444091797	10
4841	2021-03-24 08:09:44.43375-05	35.7999992370605469	11
4842	2021-03-24 08:09:47.434834-05	25.3000011444091797	10
4843	2021-03-24 08:09:48.426936-05	35.7999992370605469	11
4844	2021-03-24 08:09:51.445073-05	25.3999996185302734	10
4845	2021-03-24 08:09:52.412395-05	35.7999992370605469	11
4846	2021-03-24 08:09:55.444747-05	25.3000011444091797	10
4847	2021-03-24 08:09:56.43685-05	35.7999992370605469	11
4848	2021-03-24 08:09:59.452891-05	25.3000011444091797	10
4849	2021-03-24 08:10:00.447752-05	35.7999992370605469	11
4850	2021-03-24 08:10:03.459833-05	25.3999996185302734	10
4851	2021-03-24 08:10:04.450712-05	35.7999992370605469	11
4852	2021-03-24 08:10:07.464541-05	25.3999996185302734	10
4853	2021-03-24 08:10:08.456408-05	35.7999992370605469	11
4854	2021-03-24 08:10:11.440681-05	25.3999996185302734	10
4855	2021-03-24 08:10:12.474878-05	35.7999992370605469	11
4856	2021-03-24 08:10:15.464884-05	25.3999996185302734	10
4857	2021-03-24 08:10:16.468361-05	35.7999992370605469	11
4858	2021-03-24 08:10:19.479177-05	25.3000011444091797	10
4859	2021-03-24 08:10:20.477713-05	35.7999992370605469	11
4860	2021-03-24 08:10:23.488528-05	25.3999996185302734	10
4861	2021-03-24 08:10:24.487421-05	35.7999992370605469	11
4862	2021-03-24 08:10:27.482722-05	25.3999996185302734	10
4863	2021-03-24 08:10:28.473351-05	35.7999992370605469	11
4864	2021-03-24 08:10:31.493495-05	25.3999996185302734	10
4865	2021-03-24 08:10:32.489497-05	35.7999992370605469	11
4866	2021-03-24 08:10:35.499148-05	25.3000011444091797	10
4867	2021-03-24 08:10:36.497-05	35.7999992370605469	11
4868	2021-03-24 08:10:39.5003-05	25.3000011444091797	10
4869	2021-03-24 08:10:40.50135-05	35.7999992370605469	11
4870	2021-03-24 08:10:43.499113-05	25.3999996185302734	10
4871	2021-03-24 08:10:44.506556-05	35.7999992370605469	11
4872	2021-03-24 08:10:47.49735-05	25.3000011444091797	10
4873	2021-03-24 08:10:48.512074-05	35.7999992370605469	11
4874	2021-03-24 08:10:51.507514-05	25.3999996185302734	10
4875	2021-03-24 08:10:52.508846-05	35.7999992370605469	11
4876	2021-03-24 08:10:55.512974-05	25.3000011444091797	10
4877	2021-03-24 08:10:56.530399-05	35.7999992370605469	11
4878	2021-03-24 08:10:59.521799-05	25.3999996185302734	10
4879	2021-03-24 08:11:00.519593-05	35.7999992370605469	11
4880	2021-03-24 08:11:03.534061-05	25.3999996185302734	10
4881	2021-03-24 08:11:04.517917-05	35.7999992370605469	11
4882	2021-03-24 08:11:07.526386-05	25.3000011444091797	10
4883	2021-03-24 08:11:08.500544-05	35.7999992370605469	11
4884	2021-03-24 08:11:11.534499-05	25.3999996185302734	10
4885	2021-03-24 08:11:12.532653-05	35.7999992370605469	11
4886	2021-03-24 08:11:15.544572-05	25.3999996185302734	10
4887	2021-03-24 08:11:16.534203-05	35.7999992370605469	11
4888	2021-03-24 08:11:19.539867-05	25.3999996185302734	10
4889	2021-03-24 08:11:20.539728-05	35.7999992370605469	11
4890	2021-03-24 08:11:23.545015-05	25.3999996185302734	10
4891	2021-03-24 08:11:24.546106-05	35.7999992370605469	11
4892	2021-03-24 08:11:27.518272-05	25.3000011444091797	10
4893	2021-03-24 08:11:28.543441-05	35.7999992370605469	11
4894	2021-03-24 08:11:31.548161-05	25.3999996185302734	10
4895	2021-03-24 08:11:32.553383-05	35.7999992370605469	11
4896	2021-03-24 08:11:35.550394-05	25.3999996185302734	10
4897	2021-03-24 08:11:36.554112-05	35.7999992370605469	11
4898	2021-03-24 08:11:39.550335-05	25.3999996185302734	10
4899	2021-03-24 08:11:40.563479-05	35.7999992370605469	11
4900	2021-03-24 08:11:43.569946-05	25.3999996185302734	10
4901	2021-03-24 08:11:44.56881-05	35.7999992370605469	11
4902	2021-03-24 08:11:47.562383-05	25.3999996185302734	10
4903	2021-03-24 08:11:48.570569-05	35.7999992370605469	11
4904	2021-03-24 08:11:51.57844-05	25.3999996185302734	10
4905	2021-03-24 08:11:52.569796-05	35.7999992370605469	11
4906	2021-03-24 08:11:55.575562-05	25.3999996185302734	10
4907	2021-03-24 08:11:56.581221-05	35.7999992370605469	11
4908	2021-03-24 08:11:59.575876-05	25.3999996185302734	10
4909	2021-03-24 08:12:00.580545-05	35.7999992370605469	11
4910	2021-03-24 08:12:03.593093-05	25.3999996185302734	10
4911	2021-03-24 08:12:04.592262-05	35.7999992370605469	11
4912	2021-03-24 08:12:07.594569-05	25.3999996185302734	10
4913	2021-03-24 08:12:08.596798-05	35.7999992370605469	11
4914	2021-03-24 08:12:11.597099-05	25.3999996185302734	10
4915	2021-03-24 08:12:12.597774-05	35.7999992370605469	11
4916	2021-03-24 08:12:15.602841-05	25.3999996185302734	10
4917	2021-03-24 08:12:16.603355-05	35.7999992370605469	11
4918	2021-03-24 08:12:19.603277-05	25.3999996185302734	10
4919	2021-03-24 08:12:20.600156-05	35.7999992370605469	11
4920	2021-03-24 08:12:23.607031-05	25.3999996185302734	10
4921	2021-03-24 08:12:24.587131-05	35.7999992370605469	11
4922	2021-03-24 08:12:27.620332-05	25.3999996185302734	10
4923	2021-03-24 08:12:28.606748-05	35.7999992370605469	11
4924	2021-03-24 08:12:31.617117-05	25.3999996185302734	10
4925	2021-03-24 08:12:32.617086-05	35.7999992370605469	11
4926	2021-03-24 08:12:35.625607-05	25.3999996185302734	10
4927	2021-03-24 08:12:36.619335-05	35.7999992370605469	11
4928	2021-03-24 08:12:39.632414-05	25.3999996185302734	10
4929	2021-03-24 08:12:40.623023-05	35.7999992370605469	11
4930	2021-03-24 08:12:43.606322-05	25.3999996185302734	10
4931	2021-03-24 08:12:44.636306-05	35.7999992370605469	11
4932	2021-03-24 08:12:47.637386-05	25.3999996185302734	10
4933	2021-03-24 08:12:48.634277-05	35.7999992370605469	11
4934	2021-03-24 08:12:51.641668-05	25.3999996185302734	10
4935	2021-03-24 08:12:52.641209-05	35.7999992370605469	11
4936	2021-03-24 08:12:55.647921-05	25.3999996185302734	10
4937	2021-03-24 08:12:56.647132-05	35.7999992370605469	11
4938	2021-03-24 08:12:59.654406-05	25.3999996185302734	10
4939	2021-03-24 08:13:00.657901-05	35.7999992370605469	11
4940	2021-03-24 08:13:03.661192-05	25.3999996185302734	10
4941	2021-03-24 08:13:04.654977-05	35.7999992370605469	11
4942	2021-03-24 08:13:07.666787-05	25.3999996185302734	10
4943	2021-03-24 08:13:08.662079-05	35.7999992370605469	11
4944	2021-03-24 08:13:11.678381-05	25.3999996185302734	10
4945	2021-03-24 08:13:12.666857-05	35.7999992370605469	11
4946	2021-03-24 08:13:15.665631-05	25.3999996185302734	10
4947	2021-03-24 08:13:16.66848-05	35.7999992370605469	11
4948	2021-03-24 08:13:19.67407-05	25.3999996185302734	10
4949	2021-03-24 08:13:20.675297-05	35.7999992370605469	11
4950	2021-03-24 08:13:23.676379-05	25.3999996185302734	10
4951	2021-03-24 08:13:24.6837-05	35.7999992370605469	11
4952	2021-03-24 08:13:27.675302-05	25.3999996185302734	10
4953	2021-03-24 08:13:28.689791-05	35.7999992370605469	11
4954	2021-03-24 08:13:31.688071-05	25.3999996185302734	10
4955	2021-03-24 08:13:32.690565-05	35.7999992370605469	11
4956	2021-03-24 08:13:35.693346-05	25.3999996185302734	10
4957	2021-03-24 08:13:36.69728-05	35.7999992370605469	11
4958	2021-03-24 08:13:39.695772-05	25.3999996185302734	10
4959	2021-03-24 08:13:40.67364-05	35.7999992370605469	11
4960	2021-03-24 08:13:43.701409-05	25.3999996185302734	10
4961	2021-03-24 08:13:44.704833-05	35.7999992370605469	11
4962	2021-03-24 08:13:47.711939-05	25.3999996185302734	10
4963	2021-03-24 08:13:48.708514-05	35.7999992370605469	11
4964	2021-03-24 08:13:51.715851-05	25.3999996185302734	10
4965	2021-03-24 08:13:52.707217-05	35.7999992370605469	11
4966	2021-03-24 08:13:55.71436-05	25.3999996185302734	10
4967	2021-03-24 08:13:56.714904-05	35.7999992370605469	11
4968	2021-03-24 08:13:59.689599-05	25.3999996185302734	10
4969	2021-03-24 08:14:00.72073-05	35.7999992370605469	11
4970	2021-03-24 08:14:03.713421-05	25.3999996185302734	10
4971	2021-03-24 08:14:04.723418-05	35.7999992370605469	11
4972	2021-03-24 08:14:07.720889-05	25.3999996185302734	10
4973	2021-03-24 08:14:08.729629-05	35.7999992370605469	11
4974	2021-03-24 08:14:11.741663-05	25.3999996185302734	10
4975	2021-03-24 08:14:12.73535-05	35.7999992370605469	11
4976	2021-03-24 08:14:15.733126-05	25.3999996185302734	10
4977	2021-03-24 08:14:16.728675-05	35.7999992370605469	11
4978	2021-03-24 08:14:19.733048-05	25.3999996185302734	10
4979	2021-03-24 08:14:20.743705-05	35.7999992370605469	11
4980	2021-03-24 08:14:23.74731-05	25.3999996185302734	10
4981	2021-03-24 08:14:24.739597-05	35.7999992370605469	11
4982	2021-03-24 08:14:27.753961-05	25.3999996185302734	10
4983	2021-03-24 08:14:28.752213-05	35.7999992370605469	11
4984	2021-03-24 08:14:31.754218-05	25.3999996185302734	10
4985	2021-03-24 08:14:32.763051-05	35.7999992370605469	11
4986	2021-03-24 08:14:35.758859-05	25.3999996185302734	10
4987	2021-03-24 08:14:36.762162-05	35.7999992370605469	11
4988	2021-03-24 08:14:39.763984-05	25.3999996185302734	10
4989	2021-03-24 08:14:40.759409-05	35.7999992370605469	11
4990	2021-03-24 08:14:43.759811-05	25.3999996185302734	10
4991	2021-03-24 08:14:44.767157-05	35.7999992370605469	11
4992	2021-03-24 08:14:47.77405-05	25.3999996185302734	10
4993	2021-03-24 08:14:48.768866-05	35.7999992370605469	11
4994	2021-03-24 08:14:51.775845-05	25.3999996185302734	10
4995	2021-03-24 08:14:52.771532-05	35.7999992370605469	11
4996	2021-03-24 08:14:55.779125-05	25.3999996185302734	10
4997	2021-03-24 08:14:56.759436-05	35.7999992370605469	11
4998	2021-03-24 08:14:59.789485-05	25.3999996185302734	10
4999	2021-03-24 08:15:00.78448-05	35.7999992370605469	11
5000	2021-03-24 08:15:03.790302-05	25.3999996185302734	10
5001	2021-03-24 08:15:04.795144-05	35.7999992370605469	11
5002	2021-03-24 08:15:07.820775-05	25.3999996185302734	10
5003	2021-03-24 08:15:08.807405-05	35.7999992370605469	11
5004	2021-03-24 08:15:11.790133-05	25.3999996185302734	10
5005	2021-03-24 08:15:12.807648-05	35.7999992370605469	11
5006	2021-03-24 08:15:15.797365-05	25.3999996185302734	10
5007	2021-03-24 08:15:16.800824-05	35.7999992370605469	11
5008	2021-03-24 08:15:19.81851-05	25.3999996185302734	10
5009	2021-03-24 08:15:20.809298-05	35.7999992370605469	11
5010	2021-03-24 08:15:23.814828-05	25.3999996185302734	10
5011	2021-03-24 08:15:24.815382-05	35.7999992370605469	11
5012	2021-03-24 08:15:27.820773-05	25.3999996185302734	10
5013	2021-03-24 08:15:28.810665-05	35.7999992370605469	11
5014	2021-03-24 08:15:31.822215-05	25.3999996185302734	10
5015	2021-03-24 08:15:32.817577-05	35.7999992370605469	11
5016	2021-03-24 08:15:35.832119-05	25.3999996185302734	10
5017	2021-03-24 08:15:36.824521-05	35.7999992370605469	11
5018	2021-03-24 08:15:39.836364-05	25.3999996185302734	10
5019	2021-03-24 08:15:40.81895-05	35.7999992370605469	11
5020	2021-03-24 08:15:43.839381-05	25.3999996185302734	10
5021	2021-03-24 08:15:44.826825-05	35.7999992370605469	11
5022	2021-03-24 08:15:47.844283-05	25.3999996185302734	10
5023	2021-03-24 08:15:48.835297-05	35.7999992370605469	11
5024	2021-03-24 08:15:51.848365-05	25.3999996185302734	10
5025	2021-03-24 08:15:52.845888-05	35.7999992370605469	11
5026	2021-03-24 08:15:55.841562-05	25.3999996185302734	10
5027	2021-03-24 08:15:56.84347-05	35.7999992370605469	11
5028	2021-03-24 08:15:59.836728-05	25.3999996185302734	10
5029	2021-03-24 08:16:00.867368-05	35.7999992370605469	11
5030	2021-03-24 08:16:03.866812-05	25.3999996185302734	10
5031	2021-03-24 08:16:04.861011-05	35.7999992370605469	11
5032	2021-03-24 08:16:07.871236-05	25.3999996185302734	10
5033	2021-03-24 08:16:08.832381-05	35.7999992370605469	11
5034	2021-03-24 08:16:11.883206-05	25.3999996185302734	10
5035	2021-03-24 08:16:12.855787-05	35.7999992370605469	11
5036	2021-03-24 08:16:15.896021-05	25.3999996185302734	10
5037	2021-03-24 08:16:16.876661-05	35.7999992370605469	11
5038	2021-03-24 08:16:19.876736-05	25.3999996185302734	10
5039	2021-03-24 08:16:20.879319-05	35.7999992370605469	11
5040	2021-03-24 08:16:23.877418-05	25.3999996185302734	10
5041	2021-03-24 08:16:24.878639-05	35.7999992370605469	11
5042	2021-03-24 08:16:27.854158-05	25.3999996185302734	10
5043	2021-03-24 08:16:28.880729-05	35.7999992370605469	11
5044	2021-03-24 08:16:31.890135-05	25.3999996185302734	10
5045	2021-03-24 08:16:32.890375-05	35.7999992370605469	11
5046	2021-03-24 08:16:35.883175-05	25.3999996185302734	10
5047	2021-03-24 08:16:36.891977-05	35.7999992370605469	11
5048	2021-03-24 08:16:39.894992-05	25.3999996185302734	10
5049	2021-03-24 08:16:40.897491-05	35.7999992370605469	11
5050	2021-03-24 08:16:43.900018-05	25.3999996185302734	10
5051	2021-03-24 08:16:44.89975-05	35.7999992370605469	11
5052	2021-03-24 08:16:47.910969-05	25.3999996185302734	10
5053	2021-03-24 08:16:48.901319-05	35.7000007629394531	11
5054	2021-03-24 08:16:51.907652-05	25.3999996185302734	10
5055	2021-03-24 08:16:52.915784-05	35.7999992370605469	11
5056	2021-03-24 08:16:55.907518-05	25.3999996185302734	10
5057	2021-03-24 08:16:56.912482-05	35.7999992370605469	11
5058	2021-03-24 08:16:59.909679-05	25.3999996185302734	10
5059	2021-03-24 08:17:00.907351-05	35.7999992370605469	11
5060	2021-03-24 08:17:03.917406-05	25.3999996185302734	10
5061	2021-03-24 08:17:04.917943-05	35.7000007629394531	11
5062	2021-03-24 08:17:07.930691-05	25.3999996185302734	10
5063	2021-03-24 08:17:08.92265-05	35.7999992370605469	11
5064	2021-03-24 08:17:11.928933-05	25.3999996185302734	10
5065	2021-03-24 08:17:12.928262-05	35.7999992370605469	11
5066	2021-03-24 08:17:15.939325-05	25.3999996185302734	10
5067	2021-03-24 08:17:16.938945-05	35.7999992370605469	11
5068	2021-03-24 08:17:19.940981-05	25.3999996185302734	10
5069	2021-03-24 08:17:20.941428-05	35.7999992370605469	11
5070	2021-03-24 08:17:23.941446-05	25.3999996185302734	10
5071	2021-03-24 08:17:24.908672-05	35.7000007629394531	11
5072	2021-03-24 08:17:27.945401-05	25.3999996185302734	10
5073	2021-03-24 08:17:28.94493-05	35.7000007629394531	11
5074	2021-03-24 08:17:31.958552-05	25.3999996185302734	10
5075	2021-03-24 08:17:32.947436-05	35.7000007629394531	11
5076	2021-03-24 08:17:35.951202-05	25.3999996185302734	10
5077	2021-03-24 08:17:36.955362-05	35.7999992370605469	11
5078	2021-03-24 08:17:39.965169-05	25.3999996185302734	10
5079	2021-03-24 08:17:40.96024-05	35.7000007629394531	11
5080	2021-03-24 08:17:43.940451-05	25.3999996185302734	10
5081	2021-03-24 08:17:44.967084-05	35.7000007629394531	11
5082	2021-03-24 08:17:47.963893-05	25.3999996185302734	10
5083	2021-03-24 08:17:48.969245-05	35.7000007629394531	11
5084	2021-03-24 08:17:51.975628-05	25.3999996185302734	10
5085	2021-03-24 08:17:52.976906-05	35.7000007629394531	11
5086	2021-03-24 08:17:55.980747-05	25.3999996185302734	10
5087	2021-03-24 08:17:56.968507-05	35.7999992370605469	11
5088	2021-03-24 08:17:59.970905-05	25.3999996185302734	10
5089	2021-03-24 08:18:00.984672-05	35.7000007629394531	11
5090	2021-03-24 08:18:03.990653-05	25.3999996185302734	10
5091	2021-03-24 08:18:04.995132-05	35.7999992370605469	11
5092	2021-03-24 08:18:07.992453-05	25.3999996185302734	10
5093	2021-03-24 08:18:08.991454-05	35.7000007629394531	11
5094	2021-03-24 08:18:11.999535-05	25.3999996185302734	10
5095	2021-03-24 08:18:12.990399-05	35.7000007629394531	11
5096	2021-03-24 08:18:16.002782-05	25.3999996185302734	10
5097	2021-03-24 08:18:17.001729-05	35.7000007629394531	11
5098	2021-03-24 08:18:20.012826-05	25.3999996185302734	10
5099	2021-03-24 08:18:21.010483-05	35.7000007629394531	11
5100	2021-03-24 08:18:23.995511-05	25.3999996185302734	10
5101	2021-03-24 08:18:25.00677-05	35.7000007629394531	11
5102	2021-03-24 08:18:28.020048-05	25.3999996185302734	10
5103	2021-03-24 08:18:29.018922-05	35.7000007629394531	11
5104	2021-03-24 08:18:32.013713-05	25.3999996185302734	10
5105	2021-03-24 08:18:33.019842-05	35.7000007629394531	11
5106	2021-03-24 08:18:36.03154-05	25.3999996185302734	10
5107	2021-03-24 08:18:37.017765-05	35.7000007629394531	11
5108	2021-03-24 08:18:40.039374-05	25.3999996185302734	10
5109	2021-03-24 08:18:41.032663-05	35.7000007629394531	11
5110	2021-03-24 08:18:44.034963-05	25.3999996185302734	10
5111	2021-03-24 08:18:45.040756-05	35.7000007629394531	11
5112	2021-03-24 08:18:48.044906-05	25.3999996185302734	10
5113	2021-03-24 08:18:49.048265-05	35.7000007629394531	11
5114	2021-03-24 08:18:52.051204-05	25.3999996185302734	10
5115	2021-03-24 08:18:53.042719-05	35.7000007629394531	11
5116	2021-03-24 08:18:56.018494-05	25.3999996185302734	10
5117	2021-03-24 08:18:57.048931-05	35.7000007629394531	11
5118	2021-03-24 08:19:00.051955-05	25.3999996185302734	10
5119	2021-03-24 08:19:01.059186-05	35.7000007629394531	11
5120	2021-03-24 08:19:04.051201-05	25.3999996185302734	10
5121	2021-03-24 08:19:05.056423-05	35.7000007629394531	11
5122	2021-03-24 08:19:08.06326-05	25.3999996185302734	10
5123	2021-03-24 08:19:09.06628-05	35.7000007629394531	11
5124	2021-03-24 08:19:12.065488-05	25.3999996185302734	10
5125	2021-03-24 08:19:13.069107-05	35.7000007629394531	11
5126	2021-03-24 08:19:16.08515-05	25.3999996185302734	10
5127	2021-03-24 08:19:17.085493-05	35.7000007629394531	11
5128	2021-03-24 08:19:20.079871-05	25.3999996185302734	10
5129	2021-03-24 08:19:21.074073-05	35.7000007629394531	11
5130	2021-03-24 08:19:24.079863-05	25.3999996185302734	10
5131	2021-03-24 08:19:25.085564-05	35.7000007629394531	11
5132	2021-03-24 08:19:28.08517-05	25.3999996185302734	10
5133	2021-03-24 08:19:29.086423-05	35.7000007629394531	11
5134	2021-03-24 08:19:32.094215-05	25.3999996185302734	10
5135	2021-03-24 08:19:33.095226-05	35.7000007629394531	11
5136	2021-03-24 08:19:36.094069-05	25.3999996185302734	10
5137	2021-03-24 08:19:37.097135-05	35.7000007629394531	11
5138	2021-03-24 08:19:40.098438-05	25.3999996185302734	10
5139	2021-03-24 08:19:41.100597-05	35.7000007629394531	11
5140	2021-03-24 08:19:44.103405-05	25.3999996185302734	10
5141	2021-03-24 08:19:45.103974-05	35.7000007629394531	11
5142	2021-03-24 08:19:48.111129-05	25.3999996185302734	10
5143	2021-03-24 08:19:49.112637-05	35.7000007629394531	11
5144	2021-03-24 08:19:52.103532-05	25.3999996185302734	10
5145	2021-03-24 08:19:53.082811-05	35.7000007629394531	11
5146	2021-03-24 08:19:56.119948-05	25.3999996185302734	10
5147	2021-03-24 08:19:57.118185-05	35.7000007629394531	11
5148	2021-03-24 08:20:00.12234-05	25.3999996185302734	10
5149	2021-03-24 08:20:01.118467-05	35.7000007629394531	11
5150	2021-03-24 08:20:04.122954-05	25.3999996185302734	10
5151	2021-03-24 08:20:05.110534-05	35.7000007629394531	11
5152	2021-03-24 08:20:08.134837-05	25.3999996185302734	10
5153	2021-03-24 08:20:09.133212-05	35.7000007629394531	11
5154	2021-03-24 08:20:12.106196-05	25.3999996185302734	10
5155	2021-03-24 08:20:13.136779-05	35.7000007629394531	11
5156	2021-03-24 08:20:16.142421-05	25.3999996185302734	10
5157	2021-03-24 08:20:17.146082-05	35.7000007629394531	11
5158	2021-03-24 08:20:20.135484-05	25.3999996185302734	10
5159	2021-03-24 08:20:21.13464-05	35.7000007629394531	11
5160	2021-03-24 08:20:24.139668-05	25.3999996185302734	10
5161	2021-03-24 08:20:25.150337-05	35.6000022888183594	11
5162	2021-03-24 08:20:28.148386-05	25.3999996185302734	10
5163	2021-03-24 08:20:29.155731-05	35.6000022888183594	11
5164	2021-03-24 08:20:32.160252-05	25.3999996185302734	10
5165	2021-03-24 08:20:33.149313-05	35.6000022888183594	11
5166	2021-03-24 08:20:36.166893-05	25.3999996185302734	10
5167	2021-03-24 08:20:37.17223-05	35.6000022888183594	11
5168	2021-03-24 08:20:40.163108-05	25.3999996185302734	10
5169	2021-03-24 08:20:41.166549-05	35.6000022888183594	11
5170	2021-03-24 08:20:44.166459-05	25.3999996185302734	10
5171	2021-03-24 08:20:45.175197-05	35.6000022888183594	11
5172	2021-03-24 08:20:48.165304-05	25.3999996185302734	10
5173	2021-03-24 08:20:49.176542-05	35.6000022888183594	11
5174	2021-03-24 08:20:52.178681-05	25.3999996185302734	10
5175	2021-03-24 08:20:53.175303-05	35.6000022888183594	11
5176	2021-03-24 08:20:56.178127-05	25.3999996185302734	10
5177	2021-03-24 08:20:57.184433-05	35.5	11
5178	2021-03-24 08:21:00.190589-05	25.3999996185302734	10
5179	2021-03-24 08:21:01.186547-05	35.5	11
5180	2021-03-24 08:21:04.206628-05	25.3999996185302734	10
5181	2021-03-24 08:21:05.18057-05	35.5	11
5182	2021-03-24 08:21:08.200369-05	25.3999996185302734	10
5183	2021-03-24 08:21:09.191653-05	35.6000022888183594	11
5184	2021-03-24 08:21:12.198115-05	25.3999996185302734	10
5185	2021-03-24 08:21:13.196959-05	35.6000022888183594	11
5186	2021-03-24 08:21:16.191849-05	25.3999996185302734	10
5187	2021-03-24 08:21:17.202625-05	35.5	11
5188	2021-03-24 08:21:20.214181-05	25.3999996185302734	10
5189	2021-03-24 08:21:21.213581-05	35.5	11
5190	2021-03-24 08:21:24.196671-05	25.3999996185302734	10
5191	2021-03-24 08:21:25.222406-05	35.5	11
5192	2021-03-24 08:21:28.204865-05	25.3999996185302734	10
5193	2021-03-24 08:21:29.22047-05	35.6000022888183594	11
5194	2021-03-24 08:21:32.212206-05	25.3999996185302734	10
5195	2021-03-24 08:21:33.224372-05	35.6000022888183594	11
5196	2021-03-24 08:21:36.238621-05	25.3999996185302734	10
5197	2021-03-24 08:21:37.222241-05	35.6000022888183594	11
5198	2021-03-24 08:21:40.227359-05	25.3999996185302734	10
5199	2021-03-24 08:21:41.234505-05	35.6000022888183594	11
5200	2021-03-24 08:21:44.232428-05	25.5	10
5201	2021-03-24 08:21:45.231131-05	35.6000022888183594	11
5202	2021-03-24 08:21:48.249115-05	25.3999996185302734	10
5203	2021-03-24 08:21:49.231302-05	35.5	11
5204	2021-03-24 08:21:52.245149-05	25.3999996185302734	10
5205	2021-03-24 08:21:53.22814-05	35.5	11
5206	2021-03-24 08:21:56.248425-05	25.3999996185302734	10
5207	2021-03-24 08:21:57.243264-05	35.5	11
5208	2021-03-24 08:22:00.259926-05	25.3999996185302734	10
5209	2021-03-24 08:22:01.249956-05	35.5	11
5210	2021-03-24 08:22:04.253072-05	25.3999996185302734	10
5211	2021-03-24 08:22:05.266473-05	35.5	11
5212	2021-03-24 08:22:08.26107-05	25.3999996185302734	10
5213	2021-03-24 08:22:09.262737-05	35.5	11
5214	2021-03-24 08:22:12.264447-05	25.3999996185302734	10
5215	2021-03-24 08:22:13.255071-05	35.5	11
5216	2021-03-24 08:22:16.271656-05	25.3999996185302734	10
5217	2021-03-24 08:22:17.249938-05	35.5	11
5218	2021-03-24 08:22:20.26555-05	25.3999996185302734	10
5219	2021-03-24 08:22:21.263967-05	35.5	11
5220	2021-03-24 08:22:24.269233-05	25.3999996185302734	10
5221	2021-03-24 08:22:25.273469-05	35.5	11
5222	2021-03-24 08:22:28.283319-05	25.3999996185302734	10
5223	2021-03-24 08:22:29.276806-05	35.5	11
5224	2021-03-24 08:22:32.287402-05	25.3999996185302734	10
5225	2021-03-24 08:22:33.283441-05	35.5	11
5226	2021-03-24 08:22:36.269138-05	25.3999996185302734	10
5227	2021-03-24 08:22:37.289131-05	35.5	11
5228	2021-03-24 08:22:40.287498-05	25.3999996185302734	10
5229	2021-03-24 08:22:41.290157-05	35.5	11
5230	2021-03-24 08:22:44.298647-05	25.3999996185302734	10
5231	2021-03-24 08:22:45.291535-05	35.5	11
5232	2021-03-24 08:22:48.305339-05	25.3999996185302734	10
5233	2021-03-24 08:22:49.294791-05	35.5	11
5234	2021-03-24 08:22:52.30153-05	25.3999996185302734	10
5235	2021-03-24 08:22:53.298816-05	35.5	11
5236	2021-03-24 08:22:56.301931-05	25.3999996185302734	10
5237	2021-03-24 08:22:57.320634-05	35.5	11
5238	2021-03-24 08:23:00.306522-05	25.3999996185302734	10
5239	2021-03-24 08:23:01.310633-05	35.5	11
5240	2021-03-24 08:23:04.315563-05	25.3999996185302734	10
5241	2021-03-24 08:23:05.31796-05	35.5	11
5242	2021-03-24 08:23:08.328289-05	25.3999996185302734	10
5243	2021-03-24 08:23:09.319706-05	35.5	11
5244	2021-03-24 08:23:12.328595-05	25.3999996185302734	10
5245	2021-03-24 08:23:13.32857-05	35.5	11
5246	2021-03-24 08:23:16.32971-05	25.3999996185302734	10
5247	2021-03-24 08:23:17.328695-05	35.5	11
5248	2021-03-24 08:23:20.338983-05	25.3999996185302734	10
5249	2021-03-24 08:23:21.337414-05	35.5	11
5250	2021-03-24 08:23:24.343896-05	25.3999996185302734	10
5251	2021-03-24 08:23:25.340594-05	35.5	11
5252	2021-03-24 08:23:28.347148-05	25.3999996185302734	10
5253	2021-03-24 08:23:29.339769-05	35.5	11
5254	2021-03-24 08:23:32.347811-05	25.3999996185302734	10
5255	2021-03-24 08:23:33.32446-05	35.5	11
5256	2021-03-24 08:23:36.358629-05	25.3999996185302734	10
5257	2021-03-24 08:23:37.356656-05	35.5	11
5258	2021-03-24 08:23:40.357949-05	25.3999996185302734	10
5259	2021-03-24 08:23:41.363568-05	35.5	11
5260	2021-03-24 08:23:44.375494-05	25.3999996185302734	10
5261	2021-03-24 08:23:45.361378-05	35.5	11
5262	2021-03-24 08:23:48.354992-05	25.3999996185302734	10
5263	2021-03-24 08:23:49.366407-05	35.5	11
5264	2021-03-24 08:23:52.372893-05	25.3999996185302734	10
5265	2021-03-24 08:23:53.368051-05	35.5	11
5266	2021-03-24 08:23:56.373927-05	25.3999996185302734	10
5267	2021-03-24 08:23:57.378302-05	35.5	11
5268	2021-03-24 08:24:00.373421-05	25.3999996185302734	10
5269	2021-03-24 08:24:01.369478-05	35.5	11
5270	2021-03-24 08:24:04.379345-05	25.3999996185302734	10
5271	2021-03-24 08:24:05.376042-05	35.5	11
5272	2021-03-24 08:24:08.385196-05	25.3999996185302734	10
5273	2021-03-24 08:24:09.390952-05	35.5	11
5274	2021-03-24 08:24:12.3855-05	25.3999996185302734	10
5275	2021-03-24 08:24:13.392747-05	35.5	11
5276	2021-03-24 08:24:16.38042-05	25.3999996185302734	10
5277	2021-03-24 08:24:17.397058-05	35.5	11
5278	2021-03-24 08:24:20.397168-05	25.3999996185302734	10
5279	2021-03-24 08:24:21.407572-05	35.5	11
5280	2021-03-24 08:24:24.398865-05	25.3999996185302734	10
5281	2021-03-24 08:24:25.409827-05	35.6000022888183594	11
5282	2021-03-24 08:24:28.405394-05	25.5	10
5283	2021-03-24 08:24:29.415817-05	35.6000022888183594	11
5284	2021-03-24 08:24:32.417042-05	25.3999996185302734	10
5285	2021-03-24 08:24:33.413677-05	35.6000022888183594	11
5286	2021-03-24 08:24:36.420072-05	25.3999996185302734	10
5287	2021-03-24 08:24:37.424397-05	35.6000022888183594	11
5288	2021-03-24 08:24:40.427106-05	25.3999996185302734	10
5289	2021-03-24 08:24:41.425104-05	35.6000022888183594	11
5290	2021-03-24 08:24:44.423815-05	25.3999996185302734	10
5291	2021-03-24 08:24:45.397387-05	35.5	11
5292	2021-03-24 08:24:48.434055-05	25.3999996185302734	10
5293	2021-03-24 08:24:49.440384-05	35.5	11
5294	2021-03-24 08:24:52.432929-05	25.5	10
5295	2021-03-24 08:24:53.435031-05	35.6000022888183594	11
5296	2021-03-24 08:24:56.440366-05	25.3999996185302734	10
5297	2021-03-24 08:24:57.441715-05	35.6000022888183594	11
5298	2021-03-24 08:25:00.43992-05	25.5	10
5299	2021-03-24 08:25:01.43897-05	35.6000022888183594	11
5300	2021-03-24 08:25:04.426075-05	25.3999996185302734	10
5301	2021-03-24 08:25:05.447409-05	35.6000022888183594	11
5302	2021-03-24 08:25:08.452375-05	25.3999996185302734	10
5303	2021-03-24 08:25:09.449671-05	35.6000022888183594	11
5304	2021-03-24 08:25:12.451022-05	25.5	10
5305	2021-03-24 08:25:13.442956-05	35.6000022888183594	11
5306	2021-03-24 08:25:16.446126-05	25.3999996185302734	10
5307	2021-03-24 08:25:17.460226-05	35.5	11
5308	2021-03-24 08:25:20.463124-05	25.3999996185302734	10
5309	2021-03-24 08:25:21.461299-05	35.5	11
5310	2021-03-24 08:25:24.465098-05	25.5	10
5311	2021-03-24 08:25:25.474729-05	35.6000022888183594	11
5312	2021-03-24 08:25:28.47237-05	25.3999996185302734	10
5313	2021-03-24 08:25:29.465673-05	35.5	11
5314	2021-03-24 08:25:32.479142-05	25.5	10
5315	2021-03-24 08:25:33.479903-05	35.6000022888183594	11
5316	2021-03-24 08:25:36.481832-05	25.5	10
5317	2021-03-24 08:25:37.488062-05	35.6000022888183594	11
5318	2021-03-24 08:25:40.49196-05	25.3999996185302734	10
5319	2021-03-24 08:25:41.48368-05	35.6000022888183594	11
5320	2021-03-24 08:25:44.49462-05	25.3999996185302734	10
5321	2021-03-24 08:25:45.492269-05	35.6000022888183594	11
5322	2021-03-24 08:25:48.492684-05	25.5	10
5323	2021-03-24 08:25:49.50302-05	35.6000022888183594	11
5324	2021-03-24 08:25:52.494928-05	25.3999996185302734	10
5325	2021-03-24 08:25:53.499789-05	35.6000022888183594	11
5326	2021-03-24 08:25:56.507324-05	25.5	10
5327	2021-03-24 08:25:57.491791-05	35.7000007629394531	11
5328	2021-03-24 08:26:00.504501-05	25.3999996185302734	10
5329	2021-03-24 08:26:01.498992-05	35.6000022888183594	11
5330	2021-03-24 08:26:04.510813-05	25.3999996185302734	10
5331	2021-03-24 08:26:05.505886-05	35.6000022888183594	11
5332	2021-03-24 08:26:08.52006-05	25.3999996185302734	10
5333	2021-03-24 08:26:09.519901-05	35.6000022888183594	11
5334	2021-03-24 08:26:12.511342-05	25.5	10
5335	2021-03-24 08:26:13.508612-05	35.7000007629394531	11
5336	2021-03-24 08:26:16.495127-05	25.3999996185302734	10
5337	2021-03-24 08:26:17.525518-05	35.6000022888183594	11
5338	2021-03-24 08:26:20.52578-05	25.5	10
5339	2021-03-24 08:26:21.530403-05	35.7000007629394531	11
5340	2021-03-24 08:26:24.543466-05	25.5	10
5341	2021-03-24 08:26:25.546522-05	35.7000007629394531	11
5342	2021-03-24 08:26:28.538779-05	25.5	10
5343	2021-03-24 08:26:29.54594-05	35.7000007629394531	11
5344	2021-03-24 08:26:32.552052-05	25.5	10
5345	2021-03-24 08:26:33.5469-05	35.7000007629394531	11
5346	2021-03-24 08:26:36.553973-05	25.5	10
5347	2021-03-24 08:26:37.55921-05	35.7000007629394531	11
5348	2021-03-24 08:26:40.554172-05	25.3999996185302734	10
5349	2021-03-24 08:26:41.56035-05	35.6000022888183594	11
5350	2021-03-24 08:26:44.569168-05	25.5	10
5351	2021-03-24 08:26:45.568772-05	35.7000007629394531	11
5352	2021-03-24 08:26:48.560194-05	25.5	10
5353	2021-03-24 08:26:49.564991-05	35.7000007629394531	11
5354	2021-03-24 08:26:52.562497-05	25.3999996185302734	10
5355	2021-03-24 08:26:53.571032-05	35.6000022888183594	11
5356	2021-03-24 08:26:56.57536-05	25.5	10
5357	2021-03-24 08:26:57.572609-05	35.7000007629394531	11
5358	2021-03-24 08:27:00.565303-05	25.3999996185302734	10
5359	2021-03-24 08:27:01.583047-05	35.6000022888183594	11
5360	2021-03-24 08:27:04.584661-05	25.5	10
5361	2021-03-24 08:27:05.576778-05	35.7000007629394531	11
5362	2021-03-24 08:27:08.584545-05	25.5	10
5363	2021-03-24 08:27:09.577594-05	35.7000007629394531	11
5364	2021-03-24 08:27:12.589894-05	25.3999996185302734	10
5365	2021-03-24 08:27:13.575655-05	35.6000022888183594	11
5366	2021-03-24 08:27:16.604225-05	25.5	10
5367	2021-03-24 08:27:17.587797-05	35.7000007629394531	11
5368	2021-03-24 08:27:20.610861-05	25.5	10
5369	2021-03-24 08:27:21.613306-05	35.7000007629394531	11
5370	2021-03-24 08:27:24.604432-05	25.3999996185302734	10
5371	2021-03-24 08:27:25.603952-05	35.6000022888183594	11
5372	2021-03-24 08:27:28.581956-05	25.3999996185302734	10
5373	2021-03-24 08:27:29.607306-05	35.6000022888183594	11
5374	2021-03-24 08:27:32.614048-05	25.5	10
5375	2021-03-24 08:27:33.610378-05	35.7000007629394531	11
5376	2021-03-24 08:27:36.610766-05	25.5	10
5377	2021-03-24 08:27:37.609854-05	35.7000007629394531	11
5378	2021-03-24 08:27:40.620151-05	25.3999996185302734	10
5379	2021-03-24 08:27:41.619116-05	35.6000022888183594	11
5380	2021-03-24 08:27:44.626252-05	25.5	10
5381	2021-03-24 08:27:45.62468-05	35.7000007629394531	11
5382	2021-03-24 08:27:48.632303-05	25.5	10
5383	2021-03-24 08:27:49.629735-05	35.7000007629394531	11
5384	2021-03-24 08:27:52.632626-05	25.3999996185302734	10
5385	2021-03-24 08:27:53.633901-05	35.6000022888183594	11
5386	2021-03-24 08:27:56.645405-05	25.3999996185302734	10
5387	2021-03-24 08:27:57.642302-05	35.6000022888183594	11
5388	2021-03-24 08:28:00.647679-05	25.3999996185302734	10
5389	2021-03-24 08:28:01.659542-05	35.6000022888183594	11
5390	2021-03-24 08:28:04.650247-05	25.3999996185302734	10
5391	2021-03-24 08:28:05.652729-05	35.6000022888183594	11
5392	2021-03-24 08:28:08.647457-05	25.3999996185302734	10
5393	2021-03-24 08:28:09.653818-05	35.7000007629394531	11
5394	2021-03-24 08:28:12.660789-05	25.5	10
5395	2021-03-24 08:28:13.663883-05	35.7000007629394531	11
5396	2021-03-24 08:28:16.661857-05	25.3999996185302734	10
5397	2021-03-24 08:28:17.659498-05	35.7000007629394531	11
5398	2021-03-24 08:28:20.673244-05	25.3999996185302734	10
5399	2021-03-24 08:28:21.642029-05	35.7000007629394531	11
5400	2021-03-24 08:28:24.677172-05	25.3999996185302734	10
5401	2021-03-24 08:28:25.672794-05	35.7000007629394531	11
5402	2021-03-24 08:28:28.674158-05	25.5	10
5403	2021-03-24 08:28:29.66852-05	35.7000007629394531	11
5404	2021-03-24 08:28:32.680351-05	25.5	10
5405	2021-03-24 08:28:33.687387-05	35.7000007629394531	11
5406	2021-03-24 08:28:36.697708-05	25.5	10
5407	2021-03-24 08:28:37.68666-05	35.7000007629394531	11
5408	2021-03-24 08:28:40.671541-05	25.5	10
5409	2021-03-24 08:28:41.690716-05	35.7000007629394531	11
5410	2021-03-24 08:28:44.694661-05	25.5	10
5411	2021-03-24 08:28:45.699555-05	35.7999992370605469	11
5412	2021-03-24 08:28:48.696696-05	25.5	10
5413	2021-03-24 08:28:49.700389-05	35.7999992370605469	11
5414	2021-03-24 08:28:52.706481-05	25.5	10
5415	2021-03-24 08:28:53.711515-05	35.7999992370605469	11
5416	2021-03-24 08:28:56.709366-05	25.5	10
5417	2021-03-24 08:28:57.715245-05	35.7000007629394531	11
5418	2021-03-24 08:29:00.719137-05	25.3999996185302734	10
5419	2021-03-24 08:29:01.718303-05	35.7000007629394531	11
5420	2021-03-24 08:29:04.733856-05	25.3999996185302734	10
5421	2021-03-24 08:29:05.725262-05	35.7000007629394531	11
5422	2021-03-24 08:29:08.721779-05	25.5	10
5423	2021-03-24 08:29:09.725069-05	35.7000007629394531	11
5424	2021-03-24 08:29:12.732379-05	25.5	10
5425	2021-03-24 08:29:13.728526-05	35.7000007629394531	11
5426	2021-03-24 08:29:16.737387-05	25.5	10
5427	2021-03-24 08:29:17.738924-05	35.7999992370605469	11
5428	2021-03-24 08:29:20.743666-05	25.3999996185302734	10
5429	2021-03-24 08:29:21.736116-05	35.7000007629394531	11
5430	2021-03-24 08:29:24.742692-05	25.3999996185302734	10
5431	2021-03-24 08:29:25.746164-05	35.7000007629394531	11
5432	2021-03-24 08:29:28.75258-05	25.5	10
5433	2021-03-24 08:29:29.752288-05	35.7999992370605469	11
5434	2021-03-24 08:29:32.760027-05	25.3999996185302734	10
5435	2021-03-24 08:29:33.741716-05	35.7000007629394531	11
5436	2021-03-24 08:29:36.758226-05	25.5	10
5437	2021-03-24 08:29:37.748626-05	35.7999992370605469	11
5438	2021-03-24 08:29:40.757849-05	25.5	10
5439	2021-03-24 08:29:41.762631-05	35.7999992370605469	11
5440	2021-03-24 08:29:44.762819-05	25.3999996185302734	10
5441	2021-03-24 08:29:45.767138-05	35.7000007629394531	11
5442	2021-03-24 08:29:48.767538-05	25.5	10
5443	2021-03-24 08:29:49.767491-05	35.7999992370605469	11
5444	2021-03-24 08:29:52.747804-05	25.5	10
5445	2021-03-24 08:29:53.777932-05	35.7999992370605469	11
5446	2021-03-24 08:29:56.772712-05	25.5	10
5447	2021-03-24 08:29:57.793616-05	35.7999992370605469	11
5448	2021-03-24 08:30:00.776357-05	25.3999996185302734	10
5449	2021-03-24 08:30:01.785305-05	35.7000007629394531	11
5450	2021-03-24 08:30:04.78027-05	25.5	10
5451	2021-03-24 08:30:10.782361-05	35.7999992370605469	11
5452	2021-03-24 08:30:11.038512-05	25.3999996185302734	10
5453	2021-03-24 08:30:11.29863-05	35.7000007629394531	11
5454	2021-03-24 08:30:12.791033-05	25.5	10
5455	2021-03-24 08:30:13.802782-05	35.7999992370605469	11
5456	2021-03-24 08:30:16.797892-05	25.3999996185302734	10
5457	2021-03-24 08:30:17.802771-05	35.7000007629394531	11
5458	2021-03-24 08:30:20.811101-05	25.3999996185302734	10
5459	2021-03-24 08:30:21.805188-05	35.7000007629394531	11
5460	2021-03-24 08:30:24.82046-05	25.5	10
5461	2021-03-24 08:30:25.816335-05	35.7999992370605469	11
5462	2021-03-24 08:30:28.822321-05	25.5	10
5463	2021-03-24 08:30:29.819076-05	35.7999992370605469	11
5464	2021-03-24 08:30:32.820664-05	25.3999996185302734	10
5465	2021-03-24 08:30:33.824154-05	35.7000007629394531	11
5466	2021-03-24 08:30:36.820922-05	25.3999996185302734	10
5467	2021-03-24 08:30:37.820969-05	35.7000007629394531	11
5468	2021-03-24 08:30:40.824012-05	25.5	10
5469	2021-03-24 08:30:41.822067-05	35.7999992370605469	11
5470	2021-03-24 08:30:44.827568-05	25.5	10
5471	2021-03-24 08:30:45.813647-05	35.7999992370605469	11
5472	2021-03-24 08:30:48.839516-05	25.5	10
5473	2021-03-24 08:30:49.838968-05	35.7999992370605469	11
5474	2021-03-24 08:30:52.830955-05	25.5	10
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_group (id, name) FROM stdin;
1	farmUserGroup
2	gatewayGroup
3	RFIDDeviceGroup
4	objectGroup
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	2	55
2	2	76
3	3	83
4	3	103
5	3	100
6	1	40
7	1	99
8	1	87
9	1	72
10	1	54
11	1	51
12	1	52
13	1	53
14	1	76
15	1	73
16	1	74
17	1	75
18	1	107
19	1	104
20	1	105
21	1	106
22	1	36
23	1	33
24	1	34
25	1	35
26	1	91
27	1	88
28	1	89
29	1	90
30	1	41
31	1	44
32	1	43
33	1	92
34	1	95
35	1	94
36	1	60
37	1	63
38	1	62
39	1	80
40	1	83
41	1	82
42	1	100
43	1	103
44	1	102
45	1	64
46	1	65
47	1	67
48	1	66
49	1	46
50	1	47
51	1	49
52	1	48
53	1	55
54	1	45
55	1	68
56	1	50
57	4	84
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add Token	7	add_token
26	Can change Token	7	change_token
27	Can delete Token	7	delete_token
28	Can view Token	7	view_token
29	Can add token	8	add_tokenproxy
30	Can change token	8	change_tokenproxy
31	Can delete token	8	delete_tokenproxy
32	Can view token	8	view_tokenproxy
33	Can add alarm trigger	9	add_alarmtrigger
34	Can change alarm trigger	9	change_alarmtrigger
35	Can delete alarm trigger	9	delete_alarmtrigger
36	Can view alarm trigger	9	view_alarmtrigger
37	Can add farm	10	add_farm
38	Can change farm	10	change_farm
39	Can delete farm	10	delete_farm
40	Can view farm	10	view_farm
41	Can add foreign farm user	11	add_foreignfarmuser
42	Can change foreign farm user	11	change_foreignfarmuser
43	Can delete foreign farm user	11	delete_foreignfarmuser
44	Can view foreign farm user	11	view_foreignfarmuser
45	Can access auth token	11	access_foreign_user_token
46	Can add gateway user	12	add_gatewayuser
47	Can change gateway user	12	change_gatewayuser
48	Can delete gateway user	12	delete_gatewayuser
49	Can view gateway user	12	view_gatewayuser
50	Can access auth token	12	access_gateway_user_token
51	Can add object	13	add_object
52	Can change object	13	change_object
53	Can delete object	13	delete_object
54	Can view object	13	view_object
55	Can access auth token	13	access_object_token
56	Can add user	14	add_objectuser
57	Can change user	14	change_objectuser
58	Can delete user	14	delete_objectuser
59	Can view user	14	view_objectuser
60	Can add remote instance	15	add_remoteinstance
61	Can change remote instance	15	change_remoteinstance
62	Can delete remote instance	15	delete_remoteinstance
63	Can view remote instance	15	view_remoteinstance
64	Can add rfid device user	16	add_rfiddeviceuser
65	Can change rfid device user	16	change_rfiddeviceuser
66	Can delete rfid device user	16	delete_rfiddeviceuser
67	Can view rfid device user	16	view_rfiddeviceuser
68	Can access auth token	16	access_rfiddevice_user_token
69	Can add sensor	17	add_sensor
70	Can change sensor	17	change_sensor
71	Can delete sensor	17	delete_sensor
72	Can view sensor	17	view_sensor
73	Can add sensor object	18	add_sensorobject
74	Can change sensor object	18	change_sensorobject
75	Can delete sensor object	18	delete_sensorobject
76	Can view sensor object	18	view_sensorobject
77	View last associated value/operation	18	view_last_value
78	View all associated value/operation	18	view_all_value
79	View all associated value/operation in date range	18	view_date_range_value
80	Can add task	19	add_task
81	Can change task	19	change_task
82	Can delete task	19	delete_task
83	Can view task	19	view_task
84	Can add value	20	add_value
85	Can change value	20	change_value
86	Can delete value	20	delete_value
87	Can view value	20	view_value
88	Can add rised alarm	21	add_risedalarm
89	Can change rised alarm	21	change_risedalarm
90	Can delete rised alarm	21	delete_risedalarm
91	Can view rised alarm	21	view_risedalarm
92	Can add remote access	22	add_remoteaccess
93	Can change remote access	22	change_remoteaccess
94	Can delete remote access	22	delete_remoteaccess
95	Can view remote access	22	view_remoteaccess
96	Can add parcel	23	add_parcel
97	Can change parcel	23	change_parcel
98	Can delete parcel	23	delete_parcel
99	Can view parcel	23	view_parcel
100	Can add operation	24	add_operation
101	Can change operation	24	change_operation
102	Can delete operation	24	delete_operation
103	Can view operation	24	view_operation
104	Can add alarm element	25	add_alarmelement
105	Can change alarm element	25	change_alarmelement
106	Can delete alarm element	25	delete_alarmelement
107	Can view alarm element	25	view_alarmelement
108	Can add group object permission	26	add_groupobjectpermission
109	Can change group object permission	26	change_groupobjectpermission
110	Can delete group object permission	26	delete_groupobjectpermission
111	Can view group object permission	26	view_groupobjectpermission
112	Can add user object permission	27	add_userobjectpermission
113	Can change user object permission	27	change_userobjectpermission
114	Can delete user object permission	27	delete_userobjectpermission
115	Can view user object permission	27	view_userobjectpermission
116	Can add annotation template	19	add_annotationtemplate
117	Can change annotation template	19	change_annotationtemplate
118	Can delete annotation template	19	delete_annotationtemplate
119	Can view annotation template	19	view_annotationtemplate
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	!cQuSW2sAXjdsIVeD72AcfSIo3GDYk6rk0wDcKhdq	\N	f	AnonymousUser				f	t	2021-02-08 10:46:46.891942-06
4		\N	f	gateway			normal@gateway.fr	f	t	2021-02-08 10:46:52.382601-06
5		\N	f	rfidDevice1			normal@rfidDevice1.fr	f	t	2021-02-08 10:46:52.438167-06
2	pbkdf2_sha256$216000$Lb8Y7fqRuERL$VmGfrO4v4n4rlVxeF4dhLij1PF5Yjejl5AKDINb+jq8=	2021-02-10 07:03:05.985582-06	t	root			root@root.fr	t	t	2021-02-08 10:46:51.154818-06
8	pbkdf2_sha256$216000$sE7zCSpQXlmY$ZV4d5GkcjHxkD8RJYFAHUey1Hs4zrFUrpK9RFmlI9ps=	\N	f	E2003412012C0100				f	t	2021-02-10 07:32:20.516099-06
9	pbkdf2_sha256$216000$0dypXL18zCyK$I63px9J58aqkLZfMCCZ52v7iUjNiL0sXRZGrYGmUWTI=	\N	f	E2003412012A0100				f	t	2021-02-10 07:32:21.347877-06
10	pbkdf2_sha256$216000$zsu4DViqjrBi$GeTIPy701pDhP5u+fMMRdXezwGbvBOR60rXHR3Cfx+M=	\N	f	E2003412012E0100				f	t	2021-02-10 07:32:22.001391-06
11	pbkdf2_sha256$216000$XA4CVk6lIdSZ$1G0lrudfGdaTmle9enD0/Ub+qUUQOqsPhs4mRL0QE3I=	\N	f	E2003412012F0100				f	t	2021-02-10 07:32:22.651576-06
12	pbkdf2_sha256$216000$edDI7Sfbfqe5$y+sA6Vdc6HJayed6l1ONFLYsd0Axpx4d4c+9ExbsgzA=	\N	f	E200341201381700				f	t	2021-02-10 07:32:23.207256-06
13	pbkdf2_sha256$216000$xGCCcwQA5Zjq$DiQsl5Y07jAdX0RCIQ1Cpy7/SymIPvZbl8cXPSfMZmA=	\N	f	E2003412012C1700				f	t	2021-03-03 07:39:38.291981-06
14	pbkdf2_sha256$216000$1IaflvRhACye$jlywPz4QsloPJmA2pVlWR4MLkOL+ZsJ7g8DlgFZVB44=	\N	f	E200341201300100				f	t	2021-03-03 07:39:39.021613-06
3	pbkdf2_sha256$216000$wsNBopgFP9Ao$Np4samdDMlxJXRuUwscsYj2hWxi0JwKYRuzHB6gVznw=	2021-03-24 05:06:00.799627-05	f	agribiot			normal@user.fr	f	t	2021-02-08 10:46:51.793819-06
15	pbkdf2_sha256$216000$Ue5PzWBw8jCY$8Yrr5GjrlaThSz9T4ViKE6ayIS0g2Fj6OdCETrG8xnE=	\N	f	0001				f	t	2021-03-24 05:20:57.203855-05
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
1	3	1
2	4	2
3	5	3
8	8	4
10	9	4
12	10	4
14	11	4
15	12	4
18	13	4
20	14	4
22	15	4
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
eac38ce677ac816d9e6064d2e713f7019ca51342	2021-02-08 10:48:20.462099-06	5
2bc99102b4e533e9d9fdd24f0a19cd6ed33b9a01	2021-03-24 05:21:29.10184-05	4
228ccbfe98ce25e8f81db743718eb392f941e272	2021-03-24 05:21:38.548699-05	9
081fdf0d604f64d385b1a8311f3f11eeedcc881d	2021-03-24 05:28:12.731703-05	15
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2021-02-08 10:47:56.660306-06	3	agribiot	2	[{"changed": {"fields": ["password"]}}]	4	2
2	2021-02-10 07:03:27.048239-06	2	Object object (2)	3		13	2
3	2021-02-10 07:03:27.086398-06	1	Object object (1)	3		13	2
4	2021-02-10 07:03:37.277549-06	6	E2003412012B0100	3		4	2
5	2021-02-10 07:03:37.311887-06	7	E200341201301700	3		4	2
6	2021-02-10 07:03:45.649648-06	2	Task object (2)	3		19	2
7	2021-02-10 07:03:45.683017-06	1	Task object (1)	3		19	2
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	authtoken	token
8	authtoken	tokenproxy
9	agribiot	alarmtrigger
10	agribiot	farm
11	agribiot	foreignfarmuser
12	agribiot	gatewayuser
13	agribiot	object
14	agribiot	objectuser
15	agribiot	remoteinstance
16	agribiot	rfiddeviceuser
17	agribiot	sensor
18	agribiot	sensorobject
20	agribiot	value
21	agribiot	risedalarm
22	agribiot	remoteaccess
23	agribiot	parcel
24	agribiot	operation
25	agribiot	alarmelement
26	guardian	groupobjectpermission
27	guardian	userobjectpermission
19	agribiot	annotationtemplate
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-02-08 10:46:43.089844-06
2	auth	0001_initial	2021-02-08 10:46:43.233005-06
3	admin	0001_initial	2021-02-08 10:46:43.388253-06
4	admin	0002_logentry_remove_auto_add	2021-02-08 10:46:43.44933-06
5	admin	0003_logentry_add_action_flag_choices	2021-02-08 10:46:43.482311-06
6	contenttypes	0002_remove_content_type_name	2021-02-08 10:46:43.558505-06
7	auth	0002_alter_permission_name_max_length	2021-02-08 10:46:43.624639-06
8	auth	0003_alter_user_email_max_length	2021-02-08 10:46:43.659744-06
9	auth	0004_alter_user_username_opts	2021-02-08 10:46:43.693059-06
10	auth	0005_alter_user_last_login_null	2021-02-08 10:46:43.735781-06
11	auth	0006_require_contenttypes_0002	2021-02-08 10:46:43.744649-06
12	auth	0007_alter_validators_add_error_messages	2021-02-08 10:46:43.780893-06
13	auth	0008_alter_user_username_max_length	2021-02-08 10:46:43.827193-06
14	auth	0009_alter_user_last_name_max_length	2021-02-08 10:46:43.867174-06
15	auth	0010_alter_group_name_max_length	2021-02-08 10:46:43.908445-06
16	auth	0011_update_proxy_permissions	2021-02-08 10:46:43.948864-06
17	auth	0012_alter_user_first_name_max_length	2021-02-08 10:46:43.98349-06
18	agribiot	0001_initial	2021-02-08 10:46:45.061134-06
19	authtoken	0001_initial	2021-02-08 10:46:45.464141-06
20	authtoken	0002_auto_20160226_1747	2021-02-08 10:46:45.732102-06
21	authtoken	0003_tokenproxy	2021-02-08 10:46:45.776902-06
22	guardian	0001_initial	2021-02-08 10:46:46.138379-06
23	guardian	0002_generic_permissions_index	2021-02-08 10:46:46.3416-06
24	sessions	0001_initial	2021-02-08 10:46:46.370462-06
25	agribiot	0002_v02tomaster	2021-03-24 05:06:40.951256-05
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
pzvprih70miwqy6xcl5bfgi621o151ns	.eJxVjEEOgjAQRe_StWk6M1CoS_eegcy0g0VNSSisjHdXEha6_e-9_zIDb2setqrLMCVzNmROv5twfGjZQbpzuc02zmVdJrG7Yg9a7XVO-rwc7t9B5pq_NTCFvtVRkHGktsMeVEJC8ty4AMkHDwKkERshAA3dGBFRlFyMznnz_gDTLDdZ:1l9SZ9:wnxNFNli7XYNgGw4vg6z4rQRwsValJMximWMZvAvIxY	2021-02-23 06:56:27.225987-06
hhrnych9nkzpa8e6wah5fmufdy7zzsii	.eJxVjEEOgjAQRe_StWk6M1CoS_eegcy0g0VNSSisjHdXEha6_e-9_zIDb2setqrLMCVzNmROv5twfGjZQbpzuc02zmVdJrG7Yg9a7XVO-rwc7t9B5pq_NTCFvtVRkHGktsMeVEJC8ty4AMkHDwKkERshAA3dGBFRlFyMznnz_gDTLDdZ:1l9p6E:TMCKdUqTLXZa8ARcVvrgQhikufeFlQGjjcKN4asYdrc	2021-02-24 07:00:06.857086-06
9vn503nfy3jm2uzha7wp88hmps6zekfu	.eJxVjEsOwiAUAO_C2hBoHz-X7nsG8niAVA1NSrsy3t2SdKHbmcm8mcd9K35vafVzZFc2sMsvC0jPVLuID6z3hdNSt3UOvCf8tI1PS0yv29n-DQq20rcBAGPSGR2RIBttUnJ0BgJlCyRcADyUsRqssHmQUioySoijSqPW7PMF96k3dQ:1l9p98:2-dczOmbXbZxa1-Ci4dfxkkR1HUoq_aGmoG6KxwU4sU	2021-02-24 07:03:06.009367-06
o17c410s28s36emp20xl6wd8kh533kfx	.eJxVjEEOgjAQRe_StWk6M1CoS_eegcy0g0VNSSisjHdXEha6_e-9_zIDb2setqrLMCVzNmROv5twfGjZQbpzuc02zmVdJrG7Yg9a7XVO-rwc7t9B5pq_NTCFvtVRkHGktsMeVEJC8ty4AMkHDwKkERshAA3dGBFRlFyMznnz_gDTLDdZ:1lHMtB:uQCz5WxzbHAxIbW4KBCe5vhQThIfrD-WCTjztetJ7a8	2021-03-17 03:29:49.272036-05
27a1rj8kfwiw4gzswzp49gikyddkzlft	.eJxVjEEOgjAQRe_StWk6M1CoS_eegcy0g0VNSSisjHdXEha6_e-9_zIDb2setqrLMCVzNmROv5twfGjZQbpzuc02zmVdJrG7Yg9a7XVO-rwc7t9B5pq_NTCFvtVRkHGktsMeVEJC8ty4AMkHDwKkERshAA3dGBFRlFyMznnz_gDTLDdZ:1lHR9V:sbcBL8bvomSIF4ttIQEd9IGuzVG_P_mHMMTtreXeY2k	2021-03-17 08:02:57.11958-05
h0iqo7hbczi5jj9759fugcl1hp90ephu	.eJxVjEEOgjAQRe_StWk6M1CoS_eegcy0g0VNSSisjHdXEha6_e-9_zIDb2setqrLMCVzNmROv5twfGjZQbpzuc02zmVdJrG7Yg9a7XVO-rwc7t9B5pq_NTCFvtVRkHGktsMeVEJC8ty4AMkHDwKkERshAA3dGBFRlFyMznnz_gDTLDdZ:1lP0Om:p36P-IA0OHbtN1EsvUFJ9vgnulsty7Ev1sO4WSBxUDU	2021-04-07 05:06:00.818488-05
\.


--
-- Data for Name: guardian_groupobjectpermission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.guardian_groupobjectpermission (id, object_pk, content_type_id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: guardian_userobjectpermission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.guardian_userobjectpermission (id, object_pk, content_type_id, permission_id, user_id) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Name: agribiot_alarmelement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_alarmelement_id_seq', 1, false);


--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_alarmtrigger_id_seq', 1, false);


--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_farm_id_seq', 1, false);


--
-- Name: agribiot_object_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_object_id_seq', 11, true);


--
-- Name: agribiot_operation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_operation_id_seq', 19, true);


--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_parcel_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_selected_objects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_selected_objects_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_sensor_objects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_sensor_objects_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_sensors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_sensors_id_seq', 1, false);


--
-- Name: agribiot_remoteinstance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteinstance_id_seq', 1, false);


--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_risedalarm_id_seq', 1, false);


--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_sensor_id_seq', 3, true);


--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_sensorobject_id_seq', 11, true);


--
-- Name: agribiot_task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_task_id_seq', 8, true);


--
-- Name: agribiot_value_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_value_id_seq', 5474, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 4, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 57, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 119, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 23, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 15, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 7, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 27, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 25, true);


--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.guardian_groupobjectpermission_id_seq', 1, false);


--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.guardian_userobjectpermission_id_seq', 1, false);


--
-- Name: agribiot_alarmelement agribiot_alarmelement_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelement_pkey PRIMARY KEY (id);


--
-- Name: agribiot_alarmtrigger agribiot_alarmtrigger_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger
    ADD CONSTRAINT agribiot_alarmtrigger_name_key UNIQUE (name);


--
-- Name: agribiot_alarmtrigger agribiot_alarmtrigger_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger
    ADD CONSTRAINT agribiot_alarmtrigger_pkey PRIMARY KEY (id);


--
-- Name: agribiot_farm agribiot_farm_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_farm
    ADD CONSTRAINT agribiot_farm_pkey PRIMARY KEY (id);


--
-- Name: agribiot_foreignfarmuser agribiot_foreignfarmuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_foreignfarmuser
    ADD CONSTRAINT agribiot_foreignfarmuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_gatewayuser agribiot_gatewayuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_gatewayuser
    ADD CONSTRAINT agribiot_gatewayuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_object agribiot_object_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_pkey PRIMARY KEY (id);


--
-- Name: agribiot_object agribiot_object_uuid_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_uuid_key UNIQUE (uuid);


--
-- Name: agribiot_objectuser agribiot_objectuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_objectuser
    ADD CONSTRAINT agribiot_objectuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_operation agribiot_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation
    ADD CONSTRAINT agribiot_operation_pkey PRIMARY KEY (id);


--
-- Name: agribiot_parcel agribiot_parcel_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel
    ADD CONSTRAINT agribiot_parcel_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess agribiot_remoteaccess_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess
    ADD CONSTRAINT agribiot_remoteaccess_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteaccess_se_remoteaccess_id_object_i_6a4c0d2b_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_object_i_6a4c0d2b_uniq UNIQUE (remoteaccess_id, object_id);


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteaccess_se_remoteaccess_id_sensor_i_0da2fc55_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensor_i_0da2fc55_uniq UNIQUE (remoteaccess_id, sensor_id);


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteaccess_se_remoteaccess_id_sensorob_81219c57_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensorob_81219c57_uniq UNIQUE (remoteaccess_id, sensorobject_id);


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteaccess_selected_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteaccess_selected_objects_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteaccess_sensor_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteaccess_sensor_objects_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteaccess_sensors_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteaccess_sensors_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteinstance agribiot_remoteinstance_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteinstance
    ADD CONSTRAINT agribiot_remoteinstance_pkey PRIMARY KEY (id);


--
-- Name: agribiot_rfiddeviceuser agribiot_rfiddeviceuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_rfiddeviceuser
    ADD CONSTRAINT agribiot_rfiddeviceuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_risedalarm agribiot_risedalarm_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_pkey PRIMARY KEY (id);


--
-- Name: agribiot_sensor agribiot_sensor_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor
    ADD CONSTRAINT agribiot_sensor_name_key UNIQUE (name);


--
-- Name: agribiot_sensor agribiot_sensor_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor
    ADD CONSTRAINT agribiot_sensor_pkey PRIMARY KEY (id);


--
-- Name: agribiot_sensorobject agribiot_sensorobject_object_id_sensor_id_a7d20fea_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_object_id_sensor_id_a7d20fea_uniq UNIQUE (object_id, sensor_id);


--
-- Name: agribiot_sensorobject agribiot_sensorobject_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_pkey PRIMARY KEY (id);


--
-- Name: agribiot_annotationtemplate agribiot_task_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_annotationtemplate
    ADD CONSTRAINT agribiot_task_pkey PRIMARY KEY (id);


--
-- Name: agribiot_value agribiot_value_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value
    ADD CONSTRAINT agribiot_value_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: guardian_groupobjectpermission guardian_groupobjectperm_group_id_permission_id_o_3f189f7c_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobjectperm_group_id_permission_id_o_3f189f7c_uniq UNIQUE (group_id, permission_id, object_pk);


--
-- Name: guardian_groupobjectpermission guardian_groupobjectpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobjectpermission_pkey PRIMARY KEY (id);


--
-- Name: guardian_userobjectpermission guardian_userobjectpermi_user_id_permission_id_ob_b0b3d2fc_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermi_user_id_permission_id_ob_b0b3d2fc_uniq UNIQUE (user_id, permission_id, object_pk);


--
-- Name: guardian_userobjectpermission guardian_userobjectpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermission_pkey PRIMARY KEY (id);


--
-- Name: agribiot_alarmelement_alarm_trigger_id_9ba193db; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_alarm_trigger_id_9ba193db ON public.agribiot_alarmelement USING btree (alarm_trigger_id);


--
-- Name: agribiot_alarmelement_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_geometry_id ON public.agribiot_alarmelement USING gist (geometry);


--
-- Name: agribiot_alarmelement_object_id_c292df7b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_object_id_c292df7b ON public.agribiot_alarmelement USING btree (object_id);


--
-- Name: agribiot_alarmelement_sensor_id_ab7c0bfe; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_sensor_id_ab7c0bfe ON public.agribiot_alarmelement USING btree (sensor_id);


--
-- Name: agribiot_alarmtrigger_name_2a404727_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmtrigger_name_2a404727_like ON public.agribiot_alarmtrigger USING btree (name varchar_pattern_ops);


--
-- Name: agribiot_object_position_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_position_id ON public.agribiot_object USING gist ("position");


--
-- Name: agribiot_object_user_id_69163bcb; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_user_id_69163bcb ON public.agribiot_object USING btree (user_id);


--
-- Name: agribiot_object_uuid_a3a0d421_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_uuid_a3a0d421_like ON public.agribiot_object USING btree (uuid varchar_pattern_ops);


--
-- Name: agribiot_operation_position_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_position_id ON public.agribiot_operation USING gist ("position");


--
-- Name: agribiot_operation_sensor_object_id_cb2c3e2f; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_sensor_object_id_cb2c3e2f ON public.agribiot_operation USING btree (sensor_object_id);


--
-- Name: agribiot_operation_task_id_13228df0; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_task_id_13228df0 ON public.agribiot_operation USING btree (annotation_template_id);


--
-- Name: agribiot_parcel_farm_id_63c02727; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_parcel_farm_id_63c02727 ON public.agribiot_parcel USING btree (farm_id);


--
-- Name: agribiot_parcel_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_parcel_geometry_id ON public.agribiot_parcel USING gist (geometry);


--
-- Name: agribiot_remoteaccess_foreign_users_207df1dc; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_foreign_users_207df1dc ON public.agribiot_remoteaccess USING btree (foreign_users);


--
-- Name: agribiot_remoteaccess_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_geometry_id ON public.agribiot_remoteaccess USING gist (geometry);


--
-- Name: agribiot_remoteaccess_selected_objects_object_id_b8286ddc; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_selected_objects_object_id_b8286ddc ON public.agribiot_remoteaccess_selected_objects USING btree (object_id);


--
-- Name: agribiot_remoteaccess_selected_objects_remoteaccess_id_382e8a77; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_selected_objects_remoteaccess_id_382e8a77 ON public.agribiot_remoteaccess_selected_objects USING btree (remoteaccess_id);


--
-- Name: agribiot_remoteaccess_sensor_objects_remoteaccess_id_b12c6c90; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensor_objects_remoteaccess_id_b12c6c90 ON public.agribiot_remoteaccess_sensor_objects USING btree (remoteaccess_id);


--
-- Name: agribiot_remoteaccess_sensor_objects_sensorobject_id_3a831118; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensor_objects_sensorobject_id_3a831118 ON public.agribiot_remoteaccess_sensor_objects USING btree (sensorobject_id);


--
-- Name: agribiot_remoteaccess_sensors_remoteaccess_id_a33d91e7; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensors_remoteaccess_id_a33d91e7 ON public.agribiot_remoteaccess_sensors USING btree (remoteaccess_id);


--
-- Name: agribiot_remoteaccess_sensors_sensor_id_a56784c0; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensors_sensor_id_a56784c0 ON public.agribiot_remoteaccess_sensors USING btree (sensor_id);


--
-- Name: agribiot_risedalarm_alarm_trigger_id_d6008608; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_risedalarm_alarm_trigger_id_d6008608 ON public.agribiot_risedalarm USING btree (alarm_trigger_id);


--
-- Name: agribiot_risedalarm_value_id_49e5444e; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_risedalarm_value_id_49e5444e ON public.agribiot_risedalarm USING btree (value_id);


--
-- Name: agribiot_sensor_name_56d3fb1c_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensor_name_56d3fb1c_like ON public.agribiot_sensor USING btree (name varchar_pattern_ops);


--
-- Name: agribiot_sensorobject_object_id_fcf9bc39; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensorobject_object_id_fcf9bc39 ON public.agribiot_sensorobject USING btree (object_id);


--
-- Name: agribiot_sensorobject_sensor_id_4a836e2c; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensorobject_sensor_id_4a836e2c ON public.agribiot_sensorobject USING btree (sensor_id);


--
-- Name: agribiot_value_sensor_object_id_97097252; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_value_sensor_object_id_97097252 ON public.agribiot_value USING btree (sensor_object_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: guardian_gr_content_ae6aec_idx; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_gr_content_ae6aec_idx ON public.guardian_groupobjectpermission USING btree (content_type_id, object_pk);


--
-- Name: guardian_groupobjectpermission_content_type_id_7ade36b8; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_groupobjectpermission_content_type_id_7ade36b8 ON public.guardian_groupobjectpermission USING btree (content_type_id);


--
-- Name: guardian_groupobjectpermission_group_id_4bbbfb62; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_groupobjectpermission_group_id_4bbbfb62 ON public.guardian_groupobjectpermission USING btree (group_id);


--
-- Name: guardian_groupobjectpermission_permission_id_36572738; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_groupobjectpermission_permission_id_36572738 ON public.guardian_groupobjectpermission USING btree (permission_id);


--
-- Name: guardian_us_content_179ed2_idx; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_us_content_179ed2_idx ON public.guardian_userobjectpermission USING btree (content_type_id, object_pk);


--
-- Name: guardian_userobjectpermission_content_type_id_2e892405; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_userobjectpermission_content_type_id_2e892405 ON public.guardian_userobjectpermission USING btree (content_type_id);


--
-- Name: guardian_userobjectpermission_permission_id_71807bfc; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_userobjectpermission_permission_id_71807bfc ON public.guardian_userobjectpermission USING btree (permission_id);


--
-- Name: guardian_userobjectpermission_user_id_d5c1e964; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_userobjectpermission_user_id_d5c1e964 ON public.guardian_userobjectpermission USING btree (user_id);


--
-- Name: agribiot_alarmelement agribiot_alarmelemen_alarm_trigger_id_9ba193db_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelemen_alarm_trigger_id_9ba193db_fk_agribiot_ FOREIGN KEY (alarm_trigger_id) REFERENCES public.agribiot_alarmtrigger(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_alarmelement agribiot_alarmelement_object_id_c292df7b_fk_agribiot_object_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelement_object_id_c292df7b_fk_agribiot_object_id FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_alarmelement agribiot_alarmelement_sensor_id_ab7c0bfe_fk_agribiot_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelement_sensor_id_ab7c0bfe_fk_agribiot_sensor_id FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_foreignfarmuser agribiot_foreignfarmuser_user_ptr_id_d62330a8_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_foreignfarmuser
    ADD CONSTRAINT agribiot_foreignfarmuser_user_ptr_id_d62330a8_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_gatewayuser agribiot_gatewayuser_user_ptr_id_c1b9d969_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_gatewayuser
    ADD CONSTRAINT agribiot_gatewayuser_user_ptr_id_c1b9d969_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_object agribiot_object_user_id_69163bcb_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_user_id_69163bcb_fk_agribiot_ FOREIGN KEY (user_id) REFERENCES public.agribiot_objectuser(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_objectuser agribiot_objectuser_user_ptr_id_d9b15f29_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_objectuser
    ADD CONSTRAINT agribiot_objectuser_user_ptr_id_d9b15f29_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_operation agribiot_operation_annotation_template__af355ffe_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation
    ADD CONSTRAINT agribiot_operation_annotation_template__af355ffe_fk_agribiot_ FOREIGN KEY (annotation_template_id) REFERENCES public.agribiot_annotationtemplate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_operation agribiot_operation_sensor_object_id_cb2c3e2f_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation
    ADD CONSTRAINT agribiot_operation_sensor_object_id_cb2c3e2f_fk_agribiot_ FOREIGN KEY (sensor_object_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_parcel agribiot_parcel_farm_id_63c02727_fk_agribiot_farm_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel
    ADD CONSTRAINT agribiot_parcel_farm_id_63c02727_fk_agribiot_farm_id FOREIGN KEY (farm_id) REFERENCES public.agribiot_farm(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess agribiot_remoteacces_foreign_users_207df1dc_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess
    ADD CONSTRAINT agribiot_remoteacces_foreign_users_207df1dc_fk_agribiot_ FOREIGN KEY (foreign_users) REFERENCES public.agribiot_foreignfarmuser(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteacces_object_id_b8286ddc_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteacces_object_id_b8286ddc_fk_agribiot_ FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteacces_remoteaccess_id_382e8a77_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteacces_remoteaccess_id_382e8a77_fk_agribiot_ FOREIGN KEY (remoteaccess_id) REFERENCES public.agribiot_remoteaccess(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteacces_remoteaccess_id_a33d91e7_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteacces_remoteaccess_id_a33d91e7_fk_agribiot_ FOREIGN KEY (remoteaccess_id) REFERENCES public.agribiot_remoteaccess(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteacces_remoteaccess_id_b12c6c90_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteacces_remoteaccess_id_b12c6c90_fk_agribiot_ FOREIGN KEY (remoteaccess_id) REFERENCES public.agribiot_remoteaccess(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteacces_sensor_id_a56784c0_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteacces_sensor_id_a56784c0_fk_agribiot_ FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteacces_sensorobject_id_3a831118_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteacces_sensorobject_id_3a831118_fk_agribiot_ FOREIGN KEY (sensorobject_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_rfiddeviceuser agribiot_rfiddeviceuser_user_ptr_id_6733a39c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_rfiddeviceuser
    ADD CONSTRAINT agribiot_rfiddeviceuser_user_ptr_id_6733a39c_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_risedalarm agribiot_risedalarm_alarm_trigger_id_d6008608_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_alarm_trigger_id_d6008608_fk_agribiot_ FOREIGN KEY (alarm_trigger_id) REFERENCES public.agribiot_alarmtrigger(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_risedalarm agribiot_risedalarm_value_id_49e5444e_fk_agribiot_value_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_value_id_49e5444e_fk_agribiot_value_id FOREIGN KEY (value_id) REFERENCES public.agribiot_value(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_sensorobject agribiot_sensorobject_object_id_fcf9bc39_fk_agribiot_object_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_object_id_fcf9bc39_fk_agribiot_object_id FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_sensorobject agribiot_sensorobject_sensor_id_4a836e2c_fk_agribiot_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_sensor_id_4a836e2c_fk_agribiot_sensor_id FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_value agribiot_value_sensor_object_id_97097252_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value
    ADD CONSTRAINT agribiot_value_sensor_object_id_97097252_fk_agribiot_ FOREIGN KEY (sensor_object_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobjectpermission guardian_groupobject_content_type_id_7ade36b8_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobject_content_type_id_7ade36b8_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobjectpermission guardian_groupobject_group_id_4bbbfb62_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobject_group_id_4bbbfb62_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobjectpermission guardian_groupobject_permission_id_36572738_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobject_permission_id_36572738_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjectpermission guardian_userobjectp_content_type_id_2e892405_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectp_content_type_id_2e892405_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjectpermission guardian_userobjectp_permission_id_71807bfc_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectp_permission_id_71807bfc_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjectpermission guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

