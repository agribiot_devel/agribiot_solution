--
-- PostgreSQL database dump
--

-- Dumped from database version 11.9 (Debian 11.9-0+deb10u1)
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.agribiot_value DROP CONSTRAINT agribiot_value_sensor_object_id_a7beb118_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_sensor_id_36d39b3c_fk_agribiot_sensor_id;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_object_id_0d702adb_fk_agribiot_object_id;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_value_71960421_fk_agribiot_value_id;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_trigger_65bf5420_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_parcel DROP CONSTRAINT agribiot_parcel_farm_id_b5346027_fk_agribiot_farm_id;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_parcel_id_adb1022e_fk_agribiot_parcel_id;
ALTER TABLE ONLY public.agribiot_alarmtrigger DROP CONSTRAINT agribiot_alarmtrigge_probe_id_854b602b_fk_agribiot_;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
DROP INDEX public.agribiot_value_sensor_object_id_a7beb118;
DROP INDEX public.agribiot_sensorobject_sensor_id_36d39b3c;
DROP INDEX public.agribiot_sensorobject_object_id_0d702adb;
DROP INDEX public.agribiot_risedalarm_value_71960421;
DROP INDEX public.agribiot_risedalarm_trigger_65bf5420;
DROP INDEX public.agribiot_parcel_geometry_id;
DROP INDEX public.agribiot_parcel_farm_id_b5346027;
DROP INDEX public.agribiot_object_position_id;
DROP INDEX public.agribiot_object_parcel_id_adb1022e;
DROP INDEX public.agribiot_alarmtrigger_probe_id_854b602b;
DROP INDEX public.django_session_session_key_c0390e0f_like;
DROP INDEX public.django_session_expire_date_a5c62663;
DROP INDEX public.django_admin_log_user_id_c564eba6;
DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
DROP INDEX public.auth_user_username_6821ab7c_like;
DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
DROP INDEX public.auth_user_groups_group_id_97559544;
DROP INDEX public.auth_permission_content_type_id_2f476e4b;
DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
DROP INDEX public.auth_group_name_a6ea08ec_like;
ALTER TABLE ONLY public.agribiot_value DROP CONSTRAINT agribiot_value_pkey;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_pkey;
ALTER TABLE ONLY public.agribiot_sensor DROP CONSTRAINT agribiot_sensor_pkey;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_pkey;
ALTER TABLE ONLY public.agribiot_parcel DROP CONSTRAINT agribiot_parcel_pkey;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_pkey;
ALTER TABLE ONLY public.agribiot_farm DROP CONSTRAINT agribiot_farm_pkey;
ALTER TABLE ONLY public.agribiot_alarmtrigger DROP CONSTRAINT agribiot_alarmtrigger_pkey;
ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
ALTER TABLE public.agribiot_value ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_sensorobject ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_sensor ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_risedalarm ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_parcel ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_object ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_farm ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_alarmtrigger ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.agribiot_value_id_seq;
DROP TABLE public.agribiot_value;
DROP SEQUENCE public.agribiot_sensorobject_id_seq;
DROP TABLE public.agribiot_sensorobject;
DROP SEQUENCE public.agribiot_sensor_id_seq;
DROP TABLE public.agribiot_sensor;
DROP SEQUENCE public.agribiot_risedalarm_id_seq;
DROP TABLE public.agribiot_risedalarm;
DROP SEQUENCE public.agribiot_parcel_id_seq;
DROP TABLE public.agribiot_parcel;
DROP SEQUENCE public.agribiot_object_id_seq;
DROP TABLE public.agribiot_object;
DROP SEQUENCE public.agribiot_farm_id_seq;
DROP TABLE public.agribiot_farm;
DROP SEQUENCE public.agribiot_alarmtrigger_id_seq;
DROP TABLE public.agribiot_alarmtrigger;
DROP TABLE public.django_session;
DROP SEQUENCE public.django_migrations_id_seq;
DROP TABLE public.django_migrations;
DROP SEQUENCE public.django_content_type_id_seq;
DROP TABLE public.django_content_type;
DROP SEQUENCE public.django_admin_log_id_seq;
DROP TABLE public.django_admin_log;
DROP SEQUENCE public.auth_user_user_permissions_id_seq;
DROP TABLE public.auth_user_user_permissions;
DROP SEQUENCE public.auth_user_id_seq;
DROP SEQUENCE public.auth_user_groups_id_seq;
DROP TABLE public.auth_user_groups;
DROP TABLE public.auth_user;
DROP SEQUENCE public.auth_permission_id_seq;
DROP TABLE public.auth_permission;
DROP SEQUENCE public.auth_group_permissions_id_seq;
DROP TABLE public.auth_group_permissions;
DROP SEQUENCE public.auth_group_id_seq;
DROP TABLE public.auth_group;
DROP EXTENSION postgis;
--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO agribiot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO agribiot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO agribiot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO agribiot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO agribiot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO agribiot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO agribiot;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO agribiot;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO agribiot;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO agribiot;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO agribiot;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO agribiot;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO agribiot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO agribiot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO agribiot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO agribiot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO agribiot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO agribiot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_alarmtrigger (
    id integer NOT NULL,
    trigger_type character varying(100) NOT NULL,
    trigger_value double precision NOT NULL,
    probe_id integer
);


ALTER TABLE public.agribiot_alarmtrigger OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_alarmtrigger_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_alarmtrigger_id_seq OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_alarmtrigger_id_seq OWNED BY public.agribiot_alarmtrigger.id;


--
-- Name: agribiot_farm; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_farm (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_farm OWNER TO agribiot;

--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_farm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_farm_id_seq OWNER TO agribiot;

--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_farm_id_seq OWNED BY public.agribiot_farm.id;


--
-- Name: agribiot_object; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_object (
    id integer NOT NULL,
    "position" public.geography(Point,4326) NOT NULL,
    parcel_id integer NOT NULL
);


ALTER TABLE public.agribiot_object OWNER TO agribiot;

--
-- Name: agribiot_object_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_object_id_seq OWNER TO agribiot;

--
-- Name: agribiot_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_object_id_seq OWNED BY public.agribiot_object.id;


--
-- Name: agribiot_parcel; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_parcel (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    geometry public.geography(Polygon,4326) NOT NULL,
    farm_id integer NOT NULL
);


ALTER TABLE public.agribiot_parcel OWNER TO agribiot;

--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_parcel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_parcel_id_seq OWNER TO agribiot;

--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_parcel_id_seq OWNED BY public.agribiot_parcel.id;


--
-- Name: agribiot_risedalarm; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_risedalarm (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    trigger integer NOT NULL,
    value integer NOT NULL
);


ALTER TABLE public.agribiot_risedalarm OWNER TO agribiot;

--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_risedalarm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_risedalarm_id_seq OWNER TO agribiot;

--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_risedalarm_id_seq OWNED BY public.agribiot_risedalarm.id;


--
-- Name: agribiot_sensor; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_sensor (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    unit character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_sensor OWNER TO agribiot;

--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_sensor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_sensor_id_seq OWNER TO agribiot;

--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_sensor_id_seq OWNED BY public.agribiot_sensor.id;


--
-- Name: agribiot_sensorobject; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_sensorobject (
    id integer NOT NULL,
    object_id integer NOT NULL,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_sensorobject OWNER TO agribiot;

--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_sensorobject_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_sensorobject_id_seq OWNER TO agribiot;

--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_sensorobject_id_seq OWNED BY public.agribiot_sensorobject.id;


--
-- Name: agribiot_value; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_value (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    value double precision NOT NULL,
    sensor_object_id integer NOT NULL
);


ALTER TABLE public.agribiot_value OWNER TO agribiot;

--
-- Name: agribiot_value_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_value_id_seq OWNER TO agribiot;

--
-- Name: agribiot_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_value_id_seq OWNED BY public.agribiot_value.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: agribiot_alarmtrigger id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger ALTER COLUMN id SET DEFAULT nextval('public.agribiot_alarmtrigger_id_seq'::regclass);


--
-- Name: agribiot_farm id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_farm ALTER COLUMN id SET DEFAULT nextval('public.agribiot_farm_id_seq'::regclass);


--
-- Name: agribiot_object id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object ALTER COLUMN id SET DEFAULT nextval('public.agribiot_object_id_seq'::regclass);


--
-- Name: agribiot_parcel id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel ALTER COLUMN id SET DEFAULT nextval('public.agribiot_parcel_id_seq'::regclass);


--
-- Name: agribiot_risedalarm id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm ALTER COLUMN id SET DEFAULT nextval('public.agribiot_risedalarm_id_seq'::regclass);


--
-- Name: agribiot_sensor id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor ALTER COLUMN id SET DEFAULT nextval('public.agribiot_sensor_id_seq'::regclass);


--
-- Name: agribiot_sensorobject id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject ALTER COLUMN id SET DEFAULT nextval('public.agribiot_sensorobject_id_seq'::regclass);


--
-- Name: agribiot_value id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value ALTER COLUMN id SET DEFAULT nextval('public.agribiot_value_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add alarm trigger	7	add_alarmtrigger
26	Can change alarm trigger	7	change_alarmtrigger
27	Can delete alarm trigger	7	delete_alarmtrigger
28	Can view alarm trigger	7	view_alarmtrigger
29	Can add farm	8	add_farm
30	Can change farm	8	change_farm
31	Can delete farm	8	delete_farm
32	Can view farm	8	view_farm
33	Can add object	9	add_object
34	Can change object	9	change_object
35	Can delete object	9	delete_object
36	Can view object	9	view_object
37	Can add sensor	10	add_sensor
38	Can change sensor	10	change_sensor
39	Can delete sensor	10	delete_sensor
40	Can view sensor	10	view_sensor
41	Can add sensor object	11	add_sensorobject
42	Can change sensor object	11	change_sensorobject
43	Can delete sensor object	11	delete_sensorobject
44	Can view sensor object	11	view_sensorobject
45	Can add value	12	add_value
46	Can change value	12	change_value
47	Can delete value	12	delete_value
48	Can view value	12	view_value
49	Can add rised alarm	13	add_risedalarm
50	Can change rised alarm	13	change_risedalarm
51	Can delete rised alarm	13	delete_risedalarm
52	Can view rised alarm	13	view_risedalarm
53	Can add parcel	14	add_parcel
54	Can change parcel	14	change_parcel
55	Can delete parcel	14	delete_parcel
56	Can view parcel	14	view_parcel
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	agribiot	alarmtrigger
8	agribiot	farm
9	agribiot	object
10	agribiot	sensor
11	agribiot	sensorobject
12	agribiot	value
13	agribiot	risedalarm
14	agribiot	parcel
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2020-10-08 12:44:22.105891+00
2	auth	0001_initial	2020-10-08 12:44:22.141778+00
3	admin	0001_initial	2020-10-08 12:44:22.172896+00
4	admin	0002_logentry_remove_auto_add	2020-10-08 12:44:22.183221+00
5	admin	0003_logentry_add_action_flag_choices	2020-10-08 12:44:22.195651+00
6	contenttypes	0002_remove_content_type_name	2020-10-08 12:44:22.217967+00
7	auth	0002_alter_permission_name_max_length	2020-10-08 12:44:22.225895+00
8	auth	0003_alter_user_email_max_length	2020-10-08 12:44:22.239482+00
9	auth	0004_alter_user_username_opts	2020-10-08 12:44:22.248648+00
10	auth	0005_alter_user_last_login_null	2020-10-08 12:44:22.262217+00
11	auth	0006_require_contenttypes_0002	2020-10-08 12:44:22.264867+00
12	auth	0007_alter_validators_add_error_messages	2020-10-08 12:44:22.278172+00
13	auth	0008_alter_user_username_max_length	2020-10-08 12:44:22.295305+00
14	auth	0009_alter_user_last_name_max_length	2020-10-08 12:44:22.305095+00
15	auth	0010_alter_group_name_max_length	2020-10-08 12:44:22.315463+00
16	auth	0011_update_proxy_permissions	2020-10-08 12:44:22.32937+00
17	auth	0012_alter_user_first_name_max_length	2020-10-08 12:44:22.338121+00
18	sessions	0001_initial	2020-10-08 12:44:22.343605+00
19	agribiot	0001_initial	2020-10-08 12:44:22.437037+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: agribiot_alarmtrigger; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_alarmtrigger (id, trigger_type, trigger_value, probe_id) FROM stdin;
\.


--
-- Data for Name: agribiot_farm; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_farm (id, name) FROM stdin;
1	Château Guiraud
\.


--
-- Data for Name: agribiot_object; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_object (id, "position", parcel_id) FROM stdin;
1	0101000020E6100000FFFFFFFF831BD5BFB2AB88B383444640
2	0101000020E61000000000000076C0D4BF80657B1F63444640
3	0101000020E6100000000000009519D5BF509B56331C444640
4	0101000020E61000000000000094CFD4BFD90F6F56EE434640
5	0101000020E6100000000000008CF5D4BFF96AE24940444640
\.


--
-- Data for Name: agribiot_parcel; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_parcel (id, name, geometry, farm_id) FROM stdin;
1	335040000D0474	0103000020E610000001000000100000008C42DCE6D720D5BFFFBDCA9019444640A085A9D2BB1DD5BFD6BA1E9617444640475C5BD3171DD5BFE1BEC40E194446400F8EA3833F0DD5BFCFBAEB110F4446405E9AC706770CD5BF52FB63100E444640AD2DE175A2C9D4BF449957BFE3434640C4341D4B69C7D4BF03D8DBC1E343464040FCFCF7E0B5D4BFFF04172B6A4446408C570F4EFA31D5BF8FDDAA9097444640512BF125F936D5BFD08140C28B44464031DB04CE0825D5BFCF166B1382444640FCD3FCE71F33D5BF2A7E422C51444640DF5740FC5731D5BFBFFB9930504446409CF232D47231D5BF8AE8D7D64F444640A10040040B16D5BFA47B8CA83F4446408C42DCE6D720D5BFFFBDCA9019444640	1
2	335040000D0458	0103000020E6100000010000000A000000512BF125F936D5BFD08140C28B4446406F788BD18437D5BF952710768A44464044FAEDEBC039D5BFCD04C3B986444640C1B6346DB53DD5BF6B99B10D814446407C7C4276DE46D5BFD2DD1A7D71444640E855F88E754CD5BF6A50340F60444640F4CDECA98B4AD5BF291F27F15E444640FCD3FCE71F33D5BF2A7E422C5144464031DB04CE0825D5BFCF166B1382444640512BF125F936D5BFD08140C28B444640	1
3	335040000D0486	0103000020E61000000100000009000000CAD875148CEFD4BF16BD5301F743464031BF89D7ABFED4BF2FA52E19C7434640E384AEE9E607D5BFC7287403AA434640424937781508D5BFD49AE61DA7434640D71AA5A6B805D5BFFE8F5DB3A6434640C4D6C79E98D0D4BF9E78735D9D43464098816F3F85C8D4BFE395DA41DB4346406F92301D84CAD4BF88C89F5EDF434640CAD875148CEFD4BF16BD5301F7434640	1
4	335040000D0478	0103000020E61000000100000008000000A5373701E11ED5BF59709A99F44346401BB39190ED21D5BF5557F43DD94346406D7D47437C16D5BFA9B7ABFCD743464031BF89D7ABFED4BF2FA52E19C7434640CAD875148CEFD4BF16BD5301F74346407AAA436E861BD5BF1ABC541113444640BC8564B7851DD5BF833A408B00444640A5373701E11ED5BF59709A99F4434640	1
5	335040000D0453	0103000020E6100000010000000C0000001BB39190ED21D5BF5557F43DD9434640C72DE6E78626D5BF71EA5E82AE434640C7D8092FC129D5BF7A3DF3CD914346401B9A571A9D18D5BFEC408CC6904346402EBF78AB530AD5BFD5D6E3198F43464037E911FEA009D5BFF8C66BB98E4346408AA251CB2008D5BF28B3E66CA6434640424937781508D5BFD49AE61DA7434640E384AEE9E607D5BFC7287403AA43464031BF89D7ABFED4BF2FA52E19C74346406D7D47437C16D5BFA9B7ABFCD74346401BB39190ED21D5BF5557F43DD9434640	1
6	335040000D0491	0103000020E610000001000000210000003242D36DE495D5BF3B0CF7DBE84346409A006839758AD5BFA31122CFE44346403311DB824A81D5BF4A141049E3434640ADF13E332D56D5BFFDAA121BE24346401BB39190ED21D5BF5557F43DD9434640A5373701E11ED5BF59709A99F44346400A48FB1F602DD5BF344690EFF74346403F840200112CD5BF0D2ABBAAFD434640780E65A88A29D5BF6ED113E0054446403440B3356C25D5BF043BFE0B044446407A64BD625724D5BFB8C60CF90C4446401DED139BEA24D5BFE62ACC310F444640371CF1BFF025D5BFF747CE78114446403C7F7FEFDB35D5BF9934A1A41B44464064F72EEFBB33D5BF0C4D237722444640EB803518343ED5BF326C393C2944464089997D1EA33CD5BFC7B546A92944464022646A5C493CD5BF319413ED2A44464055CF937C363CD5BF6DACC43C2B444640B421FFCC203ED5BF7E5935632C4446403E271829EF3ED5BF80C1C99129444640CEA1B19BBE43D5BF2556EB692C444640FF52509B8246D5BF7E50BC6F21444640EDC1FFB16B56D5BFA9DE1AD82A444640B8DB3F602F5ED5BF2F54596F2F44464052C8DF073161D5BFE1B0D936314446404B5CC7B8E262D5BF34F1B336324446406F0ED76A0F7BD5BFEDC8A2FA184446404C09771D609CD5BF33EDAC27044446402AD725F444A1D5BF1EBD8685FF43464063B7CF2A33A5D5BF7DEA58A5F4434640EC8FE67E3D95D5BFF175638BEE4346403242D36DE495D5BF3B0CF7DBE8434640	1
7	335040000D0452	0103000020E61000000100000009000000ADF13E332D56D5BFFDAA121BE2434640DF23511ECC5CD5BF0278B06AB54346408DB3E908E066D5BF1F155A31B7434640C3BCC799266CD5BFD7F1046795434640C7ECD1C03431D5BFB583B64192434640C7D8092FC129D5BF7A3DF3CD91434640C72DE6E78626D5BF71EA5E82AE4346401BB39190ED21D5BF5557F43DD9434640ADF13E332D56D5BFFDAA121BE2434640	1
8	335040000D0482	0103000020E61000000100000016000000A30392B06FA7D5BFD3009475EE4346400850F81164A9D5BF95B31C8DE843464085668D30FBABD5BFDFBF7971E24346405F888B9246BBD5BF543FCAE3C4434640FAD51C2098A3D5BF20DC5328C14346404573AE72B29ED5BFD9017C5CC04346407BB9F4D48F72D5BF30D63730B9434640707A17EFC76DD5BF66796869B84346408DB3E908E066D5BF1F155A31B7434640DF23511ECC5CD5BF0278B06AB5434640ADF13E332D56D5BFFDAA121BE24346403311DB824A81D5BF4A141049E34346409A006839758AD5BFA31122CFE44346403242D36DE495D5BF3B0CF7DBE8434640939A2CA4B297D5BF7FA88F1BD9434640DD674B0CB89DD5BF8A50114CDA4346401521D0F46D9CD5BF216D8896E143464007CE1951DA9BD5BF85B6F704E4434640B01B5B74579BD5BF90FC1C7AE643464079D4F3C9E59AD5BF6BBEA5F7E8434640D8B04BF9B59AD5BF5F24592CEA434640A30392B06FA7D5BFD3009475EE434640	1
9	335040000D0475	0103000020E610000001000000050000008C42DCE6D720D5BFFFBDCA901944464022646A5C493CD5BF319413ED2A44464089997D1EA33CD5BFC7B546A92944464071B9B0242F21D5BFCA969B5E184446408C42DCE6D720D5BFFFBDCA9019444640	1
10	335040000D0484	0103000020E6100000010000000E000000D19A7A38DCA2D5BFEBF18CC756444640D7D6F445E7A1D5BFD5EDEC2B0F4446401CCAF55BE097D5BFD41BFF4F0F444640F3D544550298D5BF422C51AC094446402E5ADB6F487ED5BF35BD1F121944464029441BDBC67AD5BF4BF37D271C4446403D128A085A5CD5BFA0408E9C3B444640932694748357D5BFEA91ABFD424446409AC1CE030E57D5BFFB9A406C44444640726C3D433866D5BF380A5A924D444640A83D80A03770D5BFB9944E7F51444640DF933DF83F76D5BF53C9A596524446408D11E4FB3D8CD5BFBEC1172653444640D19A7A38DCA2D5BFEBF18CC756444640	1
11	335040000D0455	0103000020E610000001000000150000000AFA66F6D445D5BFE53EDE509D44464068E503A7E356D5BF65C39ACAA2444640AA66D652405AD5BF0B163DA6A44446405BC4C1EFF068D5BFC619C39CA04446409E6CB9E98571D5BF987384679D4446400562235A3C86D5BF1DA386808D444640CAE42F88A386D5BF1AF5B5D37B4446403C6876DD5B91D5BF77A9C76C7F4446407284B12A6794D5BFD7016B3068444640D982948E28A3D5BF2D211FF46C444640D19A7A38DCA2D5BFEBF18CC7564446408D11E4FB3D8CD5BFBEC1172653444640DF933DF83F76D5BF53C9A59652444640A83D80A03770D5BFB9944E7F51444640726C3D433866D5BF380A5A924D4446409AC1CE030E57D5BFFB9A406C444446409187742E7B48D5BFBA53951172444640B93C31467F43D5BF2761F07A7A444640AC0F351B753ED5BF5B1E108D93444640A23A67559547D5BFCBE1EE07974446400AFA66F6D445D5BFE53EDE509D444640	1
12	335040000D0472	0103000020E610000001000000240000007E6830575062D5BF6B48DC63E9444640F94E2734A465D5BFB1A371A8DF444640E7A90EB9196ED5BF02D2A34EE54446401480DA03087AD5BF3599F1B6D244464064DA4823B083D5BFDD22D51CC5444640E7CE02A3818ED5BFB2F6D26FBA4446406E5D7BB0D69AD5BF675F79909E4446407F1E59AFD895D5BF377980DD9F4446409662A29CC390D5BF16C330BB824446407B7F283D7892D5BF9BADBCE47F444640B7DC4F7C6B96D5BF3D55CF937C4446402B4EB51666A1D5BF24D0059A74444640D982948E28A3D5BF2D211FF46C4446407284B12A6794D5BFD7016B30684446403C6876DD5B91D5BF77A9C76C7F444640CAE42F88A386D5BF1AF5B5D37B4446400562235A3C86D5BF1DA386808D4446409E6CB9E98571D5BF987384679D4446405BC4C1EFF068D5BFC619C39CA0444640AA66D652405AD5BF0B163DA6A444464068E503A7E356D5BF65C39ACAA24446400AFA66F6D445D5BFE53EDE509D444640FF0B5FBAA445D5BF0943D3C89D444640954FEA268C41D5BF10C4C3C59B4446403C1CB85E2E3DD5BFC95FB58D9A444640351D4B69473BD5BFD58338C599444640657330F6B935D5BF0628B27B9744464055BBCBEAC234D5BF363CBD5296444640F8B53F619A33D5BF70BEC74F99444640FC7DB559AB2CD5BF48E17A14AE4446408EB2D9ECA32ED5BF3C61D394AE4446404A3A6F08FA30D5BF0C957F2DAF444640E08849134A3AD5BFCFE6278BB144464090977F3EDB34D5BFFC1C1F2DCE4446405262D7F6764BD5BF8473FC06DC4446407E6830575062D5BF6B48DC63E9444640	1
13	335040000D0494	0103000020E61000000100000016000000FCD3FCE71F33D5BF2A7E422C51444640F4CDECA98B4AD5BF291F27F15E44464041DE0610994DD5BFDABAE53455444640BD1E4C8A8F4FD5BF2724E362564446400E40B4A04154D5BF9AB4A9BA474446407DA4D299C555D5BFA8D02F0043444640E47F97BE7C57D5BF39CBD1883E4446400EA0DFF76F5ED5BFAE8A2606374446404B5CC7B8E262D5BF34F1B3363244464052C8DF073161D5BFE1B0D93631444640B8DB3F602F5ED5BF2F54596F2F444640EDC1FFB16B56D5BFA9DE1AD82A444640FF52509B8246D5BF7E50BC6F21444640CEA1B19BBE43D5BF2556EB692C4446403E271829EF3ED5BF80C1C99129444640B421FFCC203ED5BF7E5935632C44464055CF937C363CD5BF6DACC43C2B44464003DD4DA6C038D5BFC6CCF401374446408048BF7D1D38D5BF3CF71E2E394446409CF232D47231D5BF8AE8D7D64F444640DF5740FC5731D5BFBFFB993050444640FCD3FCE71F33D5BF2A7E422C51444640	1
14	335040000D0476	0103000020E610000001000000080000008C42DCE6D720D5BFFFBDCA9019444640A10040040B16D5BFA47B8CA83F4446409CF232D47231D5BF8AE8D7D64F4446408048BF7D1D38D5BF3CF71E2E3944464003DD4DA6C038D5BFC6CCF4013744464055CF937C363CD5BF6DACC43C2B44464022646A5C493CD5BF319413ED2A4446408C42DCE6D720D5BFFFBDCA9019444640	1
\.


--
-- Data for Name: agribiot_risedalarm; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_risedalarm (id, created, trigger, value) FROM stdin;
\.



COPY public.agribiot_sensor (id, name, unit) FROM stdin;
1	Temperature	°C
2	Humidité	%
\.


--
-- Data for Name: agribiot_sensorobject; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_sensorobject (id, object_id, sensor_id) FROM stdin;
1	1	1
2	1	2
3	2	1
4	2	2
5	3	1
6	3	2
7	4	1
8	4	2
9	5	1
10	5	2
\.


--
-- Data for Name: agribiot_value; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_value (id, created, value, sensor_object_id) FROM stdin;
1	2020-10-06 13:32:05.494729+00	21.1000003814697266	1
2	2020-10-06 13:32:06.494648+00	58.2999992370605469	2
3	2020-10-06 13:32:09.52145+00	21.1000003814697266	1
4	2020-10-06 13:32:10.49293+00	58.2000007629394531	2
5	2020-10-06 13:32:13.526733+00	21.1000003814697266	1
6	2020-10-06 13:32:14.504382+00	58.1000022888183594	2
7	2020-10-06 13:32:17.505259+00	21.1000003814697266	1
8	2020-10-06 13:32:18.518224+00	58.1000022888183594	2
9	2020-10-06 13:32:21.52185+00	21.1000003814697266	1
10	2020-10-06 13:32:22.508546+00	58	2
11	2020-10-06 13:32:25.515466+00	21.1000003814697266	1
12	2020-10-06 13:32:26.516158+00	58	2
13	2020-10-06 13:32:29.519732+00	21.1000003814697266	1
14	2020-10-06 13:32:30.519687+00	58	2
15	2020-10-06 13:32:33.526779+00	21.1000003814697266	1
16	2020-10-06 13:32:34.537422+00	58	2
17	2020-10-06 13:32:37.522477+00	21.1000003814697266	1
18	2020-10-06 13:32:38.545572+00	58	2
19	2020-10-06 13:32:41.53213+00	21.1000003814697266	1
20	2020-10-06 13:32:42.533681+00	58	2
21	2020-10-06 13:32:45.536282+00	21	1
22	2020-10-06 13:32:46.53625+00	57.9000015258789062	2
23	2020-10-06 13:32:49.561067+00	21	1
24	2020-10-06 13:32:50.540019+00	58	2
25	2020-10-06 13:32:53.544771+00	21.1000003814697266	1
26	2020-10-06 13:32:54.545299+00	58.2000007629394531	2
27	2020-10-06 13:32:57.570698+00	21	1
28	2020-10-06 13:32:58.575757+00	58.2000007629394531	2
29	2020-10-06 13:33:01.566606+00	21	1
30	2020-10-06 13:33:02.558+00	58.2000007629394531	2
31	2020-10-06 13:33:05.586996+00	21	1
32	2020-10-06 13:33:06.577051+00	58.2000007629394531	2
33	2020-10-06 13:33:09.565233+00	21	1
34	2020-10-06 13:33:10.558473+00	58.2000007629394531	2
35	2020-10-06 13:34:29.659383+00	21	1
36	2020-10-06 13:34:30.659576+00	58.1000022888183594	2
37	2020-10-06 13:34:33.636637+00	21	1
38	2020-10-06 13:34:34.662741+00	58.1000022888183594	2
39	2020-10-06 13:34:37.669132+00	21	1
40	2020-10-06 13:34:38.667864+00	58.1000022888183594	2
41	2020-10-06 13:39:13.940805+00	21	1
42	2020-10-06 13:39:14.949624+00	58.1000022888183594	2
43	2020-10-06 13:39:17.923547+00	21	1
44	2020-10-06 13:39:18.949148+00	58.1000022888183594	2
45	2020-10-06 13:39:21.930045+00	21	1
46	2020-10-06 13:39:22.926852+00	58.1000022888183594	2
47	2020-10-06 13:39:25.932096+00	21	1
48	2020-10-06 13:39:26.929922+00	58.1000022888183594	2
49	2020-10-06 13:39:29.962536+00	21	1
50	2020-10-06 13:39:30.928787+00	58.1000022888183594	2
51	2020-10-06 15:03:52.477501+00	56.7999992370605469	2
52	2020-10-06 15:03:55.482437+00	21.2000007629394531	1
53	2020-10-06 15:03:56.482127+00	56.7999992370605469	2
54	2020-10-06 15:03:59.498782+00	21.2000007629394531	1
55	2020-10-06 15:04:00.497835+00	56.7999992370605469	2
56	2020-10-06 15:04:03.51722+00	21.2000007629394531	1
57	2020-10-06 15:04:04.489712+00	56.7999992370605469	2
58	2020-10-06 15:04:07.486528+00	21.2000007629394531	1
59	2020-10-06 15:04:08.488579+00	56.7999992370605469	2
60	2020-10-06 15:04:11.509618+00	21.2000007629394531	1
61	2020-10-06 15:04:12.497085+00	56.7000007629394531	2
62	2020-10-06 15:04:15.501982+00	21.2000007629394531	1
63	2020-10-06 15:04:16.512661+00	56.7999992370605469	2
64	2020-10-06 15:04:19.504439+00	21.2000007629394531	1
65	2020-10-06 15:04:20.505533+00	56.7999992370605469	2
66	2020-10-06 15:04:23.508403+00	21.2000007629394531	1
67	2020-10-06 15:04:24.508519+00	56.7000007629394531	2
68	2020-10-06 15:04:27.5099+00	21.2000007629394531	1
69	2020-10-06 15:04:28.512757+00	56.7000007629394531	2
70	2020-10-06 15:04:31.516729+00	21.2000007629394531	1
71	2020-10-06 15:04:32.517162+00	56.7999992370605469	2
72	2020-10-06 15:04:35.520449+00	21.2000007629394531	1
73	2020-10-06 15:04:36.520099+00	56.7999992370605469	2
74	2020-10-06 15:04:39.548698+00	21.2000007629394531	1
75	2020-10-06 15:04:40.523904+00	56.7999992370605469	2
76	2020-10-06 15:04:43.527568+00	21.1000003814697266	1
77	2020-10-06 15:04:44.527788+00	56.7000007629394531	2
78	2020-10-06 15:04:47.532014+00	21.2000007629394531	1
79	2020-10-06 15:04:48.531319+00	56.7999992370605469	2
80	2020-10-06 15:04:51.536807+00	21.1000003814697266	1
81	2020-10-06 15:04:52.535593+00	56.7000007629394531	2
82	2020-10-06 15:04:55.536722+00	21.1000003814697266	1
83	2020-10-06 15:04:56.539913+00	56.7999992370605469	2
84	2020-10-06 15:04:59.543881+00	21.1000003814697266	1
85	2020-10-06 15:05:00.544075+00	56.7999992370605469	2
86	2020-10-06 15:05:03.547093+00	21.2000007629394531	1
87	2020-10-06 15:05:04.547768+00	56.9000015258789062	2
88	2020-10-06 15:05:07.551673+00	21.1000003814697266	1
89	2020-10-06 15:05:08.548657+00	56.7999992370605469	2
90	2020-10-06 15:05:11.55619+00	21.1000003814697266	1
91	2020-10-06 15:05:12.568095+00	56.9000015258789062	2
92	2020-10-06 15:05:15.558841+00	21.2000007629394531	1
93	2020-10-06 15:05:16.559079+00	56.9000015258789062	2
94	2020-10-06 15:05:19.58561+00	21.1000003814697266	1
95	2020-10-06 15:05:20.562971+00	56.9000015258789062	2
96	2020-10-06 15:05:23.567796+00	21.1000003814697266	1
97	2020-10-06 15:05:24.567478+00	56.9000015258789062	2
98	2020-10-06 15:05:27.571368+00	21.1000003814697266	1
99	2020-10-06 15:05:28.571089+00	56.9000015258789062	2
100	2020-10-06 15:05:31.575062+00	21.1000003814697266	1
101	2020-10-06 15:05:32.574015+00	57	2
102	2020-10-06 15:05:35.580467+00	21.1000003814697266	1
103	2020-10-06 15:05:36.578931+00	57	2
104	2020-10-06 15:05:39.58292+00	21.2000007629394531	1
105	2020-10-06 15:05:40.604429+00	57	2
106	2020-10-06 15:05:43.598973+00	21.2000007629394531	1
107	2020-10-06 15:05:44.587094+00	57	2
108	2020-10-06 15:05:47.591174+00	21.2000007629394531	1
109	2020-10-06 15:05:48.590818+00	56.9000015258789062	2
110	2020-10-06 15:05:51.616161+00	21.2000007629394531	1
111	2020-10-06 15:05:52.594855+00	56.9000015258789062	2
112	2020-10-06 15:05:55.610696+00	21.2000007629394531	1
113	2020-10-06 15:05:56.599164+00	56.9000015258789062	2
114	2020-10-06 15:05:59.60291+00	21.2000007629394531	1
115	2020-10-06 15:06:00.602846+00	56.9000015258789062	2
116	2020-10-06 15:06:03.605963+00	21.2000007629394531	1
117	2020-10-06 15:06:04.603323+00	56.9000015258789062	2
118	2020-10-06 15:06:07.609675+00	21.2000007629394531	1
119	2020-10-06 15:06:08.610389+00	56.9000015258789062	2
120	2020-10-06 15:06:11.611451+00	21.2000007629394531	1
121	2020-10-06 15:06:12.614426+00	56.9000015258789062	2
122	2020-10-06 15:06:15.618993+00	21.2000007629394531	1
123	2020-10-06 15:06:16.618302+00	56.9000015258789062	2
124	2020-10-06 15:06:19.622354+00	21.2000007629394531	1
125	2020-10-06 15:06:20.63253+00	56.9000015258789062	2
126	2020-10-06 15:06:23.625324+00	21.2000007629394531	1
127	2020-10-06 15:06:24.626408+00	56.9000015258789062	2
128	2020-10-06 15:06:27.651655+00	21.2000007629394531	1
129	2020-10-06 15:06:28.629663+00	56.9000015258789062	2
130	2020-10-06 15:06:31.635056+00	21.2000007629394531	1
131	2020-10-06 15:06:32.633959+00	56.7999992370605469	2
132	2020-10-06 15:06:35.637888+00	21.2000007629394531	1
133	2020-10-06 15:06:36.637904+00	56.7999992370605469	2
134	2020-10-06 15:06:39.641647+00	21.2000007629394531	1
135	2020-10-06 15:06:40.662035+00	56.7999992370605469	2
136	2020-10-06 15:06:43.646578+00	21.2000007629394531	1
137	2020-10-06 15:06:44.646102+00	56.7999992370605469	2
138	2020-10-06 15:06:47.648771+00	21.2000007629394531	1
139	2020-10-06 15:06:48.648662+00	56.7999992370605469	2
140	2020-10-06 15:06:51.652765+00	21.2000007629394531	1
141	2020-10-06 15:06:52.652738+00	56.7999992370605469	2
142	2020-10-06 15:06:55.657495+00	21.2000007629394531	1
143	2020-10-06 15:06:56.658472+00	56.7999992370605469	2
144	2020-10-06 15:06:59.662084+00	21.2000007629394531	1
145	2020-10-06 15:07:00.660804+00	56.7999992370605469	2
146	2020-10-06 15:07:03.690428+00	21.2000007629394531	1
147	2020-10-06 15:07:04.662338+00	56.7999992370605469	2
148	2020-10-06 15:07:07.669746+00	21.2000007629394531	1
149	2020-10-06 15:07:08.66436+00	56.7000007629394531	2
150	2020-10-06 15:07:11.693181+00	21.2000007629394531	1
151	2020-10-06 15:07:12.672435+00	56.7000007629394531	2
152	2020-10-06 15:07:15.676587+00	21.2000007629394531	1
153	2020-10-06 15:07:16.677426+00	56.7000007629394531	2
154	2020-10-06 15:07:19.680442+00	21.2000007629394531	1
155	2020-10-06 15:07:20.680446+00	56.7000007629394531	2
156	2020-10-06 15:07:23.684197+00	21.2000007629394531	1
157	2020-10-06 15:07:24.684317+00	56.7000007629394531	2
158	2020-10-06 15:07:27.688257+00	21.2000007629394531	1
159	2020-10-06 15:07:28.689274+00	56.7000007629394531	2
160	2020-10-06 15:07:31.69234+00	21.2000007629394531	1
161	2020-10-06 15:07:32.69285+00	56.7000007629394531	2
162	2020-10-06 15:07:35.718541+00	21.2000007629394531	1
163	2020-10-06 15:07:36.695851+00	56.7000007629394531	2
164	2020-10-06 15:07:39.699258+00	21.2000007629394531	1
165	2020-10-06 15:07:40.700502+00	56.7000007629394531	2
166	2020-10-06 15:07:43.703926+00	21.2000007629394531	1
167	2020-10-06 15:07:44.70362+00	56.7000007629394531	2
168	2020-10-06 15:07:47.7088+00	21.2000007629394531	1
169	2020-10-06 15:07:48.707536+00	56.7000007629394531	2
170	2020-10-06 15:07:51.732957+00	21.2000007629394531	1
171	2020-10-06 15:07:52.712661+00	56.7000007629394531	2
172	2020-10-06 15:07:55.716149+00	21.2000007629394531	1
173	2020-10-06 15:07:56.713762+00	56.7000007629394531	2
174	2020-10-06 15:07:59.719932+00	21.2000007629394531	1
175	2020-10-06 15:08:00.719762+00	56.7000007629394531	2
176	2020-10-06 15:08:03.723211+00	21.2000007629394531	1
177	2020-10-06 15:08:04.745767+00	56.6000022888183594	2
178	2020-10-06 15:08:07.727938+00	21.2000007629394531	1
179	2020-10-06 15:08:08.735955+00	56.6000022888183594	2
180	2020-10-06 15:08:11.753595+00	21.2000007629394531	1
181	2020-10-06 15:08:12.732697+00	56.7000007629394531	2
182	2020-10-06 15:08:15.735686+00	21.2000007629394531	1
183	2020-10-06 15:08:16.73472+00	56.6000022888183594	2
184	2020-10-06 15:08:19.739217+00	21.2000007629394531	1
185	2020-10-06 15:08:20.739594+00	56.7000007629394531	2
186	2020-10-06 15:08:23.755884+00	21.2000007629394531	1
187	2020-10-06 15:08:24.743297+00	56.6000022888183594	2
188	2020-10-06 15:08:27.759536+00	21.2000007629394531	1
189	2020-10-06 15:08:28.747609+00	56.6000022888183594	2
190	2020-10-06 15:08:31.751541+00	21.3000011444091797	1
191	2020-10-06 15:08:32.751822+00	56.7000007629394531	2
192	2020-10-06 15:08:35.749325+00	21.3000011444091797	1
193	2020-10-06 15:08:36.755538+00	56.7000007629394531	2
194	2020-10-06 15:08:39.758256+00	21.2000007629394531	1
195	2020-10-06 15:08:40.758994+00	56.6000022888183594	2
196	2020-10-06 15:08:43.759918+00	21.2000007629394531	1
197	2020-10-06 15:08:44.762726+00	56.6000022888183594	2
198	2020-10-06 15:08:47.766787+00	21.2000007629394531	1
199	2020-10-06 15:08:48.763898+00	56.6000022888183594	2
200	2020-10-06 15:08:51.806722+00	21.2000007629394531	1
201	2020-10-06 15:08:52.77026+00	56.6000022888183594	2
202	2020-10-06 15:08:55.775797+00	21.3000011444091797	1
203	2020-10-06 15:08:56.771536+00	56.7000007629394531	2
204	2020-10-06 15:08:59.779091+00	21.2000007629394531	1
205	2020-10-06 15:09:00.779018+00	56.6000022888183594	2
206	2020-10-06 15:09:03.783412+00	21.3000011444091797	1
207	2020-10-06 15:09:04.782881+00	56.7000007629394531	2
208	2020-10-06 15:09:07.78717+00	21.3000011444091797	1
209	2020-10-06 15:09:08.787117+00	56.7000007629394531	2
210	2020-10-06 15:09:11.790808+00	21.2000007629394531	1
211	2020-10-06 15:09:12.791202+00	56.7000007629394531	2
212	2020-10-06 15:09:15.795001+00	21.3000011444091797	1
213	2020-10-06 15:09:16.791897+00	56.7000007629394531	2
214	2020-10-06 15:09:19.799398+00	21.3000011444091797	1
215	2020-10-06 15:09:20.810167+00	56.7000007629394531	2
216	2020-10-06 15:09:23.802397+00	21.3000011444091797	1
217	2020-10-06 15:09:24.803495+00	56.7000007629394531	2
218	2020-10-06 15:09:27.818527+00	21.3000011444091797	1
219	2020-10-06 15:09:28.807324+00	56.7000007629394531	2
220	2020-10-06 15:09:31.80976+00	21.3000011444091797	1
221	2020-10-06 15:09:32.811153+00	56.7000007629394531	2
222	2020-10-06 15:09:35.815213+00	21.3000011444091797	1
223	2020-10-06 15:09:36.825885+00	56.7000007629394531	2
224	2020-10-06 15:09:39.818849+00	21.3000011444091797	1
225	2020-10-06 15:09:40.840497+00	56.7000007629394531	2
226	2020-10-06 15:09:43.823448+00	21.3000011444091797	1
227	2020-10-06 15:09:44.823276+00	56.7000007629394531	2
228	2020-10-06 15:09:47.8274+00	21.3000011444091797	1
229	2020-10-06 15:09:48.837661+00	56.7000007629394531	2
230	2020-10-06 15:09:51.831466+00	21.3000011444091797	1
231	2020-10-06 15:09:52.830049+00	56.7000007629394531	2
232	2020-10-06 15:09:55.835107+00	21.3000011444091797	1
233	2020-10-06 15:09:56.835149+00	56.7000007629394531	2
234	2020-10-06 15:09:59.839623+00	21.3000011444091797	1
235	2020-10-06 15:10:00.839417+00	56.7000007629394531	2
236	2020-10-06 15:10:03.842686+00	21.3000011444091797	1
237	2020-10-06 15:10:04.84251+00	56.7000007629394531	2
238	2020-10-06 15:10:07.847345+00	21.3000011444091797	1
239	2020-10-06 15:10:08.847345+00	56.7000007629394531	2
240	2020-10-06 15:10:11.850551+00	21.3000011444091797	1
241	2020-10-06 15:10:12.873069+00	56.7000007629394531	2
242	2020-10-06 15:10:15.855374+00	21.3000011444091797	1
243	2020-10-06 15:10:16.854461+00	56.7000007629394531	2
244	2020-10-06 15:10:19.858716+00	21.3000011444091797	1
245	2020-10-06 15:10:20.869737+00	56.7000007629394531	2
246	2020-10-06 15:10:23.863662+00	21.3000011444091797	1
247	2020-10-06 15:10:24.863418+00	56.7000007629394531	2
248	2020-10-06 15:10:27.891769+00	21.3000011444091797	1
249	2020-10-06 15:10:28.889188+00	56.7000007629394531	2
250	2020-10-06 15:10:31.870461+00	21.3000011444091797	1
251	2020-10-06 15:10:32.881753+00	56.7000007629394531	2
252	2020-10-06 15:10:35.874758+00	21.3000011444091797	1
253	2020-10-06 15:10:36.874824+00	56.7000007629394531	2
254	2020-10-06 15:10:39.879483+00	21.3000011444091797	1
255	2020-10-06 15:10:40.894043+00	56.7000007629394531	2
256	2020-10-06 15:10:43.885002+00	21.3000011444091797	1
257	2020-10-06 15:10:44.883927+00	56.7000007629394531	2
258	2020-10-06 15:10:47.900425+00	21.3000011444091797	1
259	2020-10-06 15:10:48.890206+00	56.7000007629394531	2
260	2020-10-06 15:10:51.893284+00	21.3000011444091797	1
261	2020-10-06 15:10:52.893749+00	56.7000007629394531	2
262	2020-10-06 15:10:55.900253+00	21.3000011444091797	1
263	2020-10-06 15:10:56.897389+00	56.7000007629394531	2
264	2020-10-06 15:10:59.900348+00	21.3000011444091797	1
265	2020-10-06 15:11:00.925082+00	56.7000007629394531	2
266	2020-10-06 15:11:03.916339+00	21.3000011444091797	1
267	2020-10-06 15:11:04.905722+00	56.7000007629394531	2
268	2020-10-06 15:11:07.910713+00	21.3000011444091797	1
269	2020-10-06 15:11:08.917259+00	56.7000007629394531	2
270	2020-10-06 15:11:11.917375+00	21.3000011444091797	1
271	2020-10-06 15:11:12.933673+00	56.7000007629394531	2
272	2020-10-06 15:11:15.921722+00	21.3000011444091797	1
273	2020-10-06 15:11:16.921753+00	56.7000007629394531	2
274	2020-10-06 15:11:19.92097+00	21.3000011444091797	1
275	2020-10-06 15:11:20.922275+00	56.7000007629394531	2
276	2020-10-06 15:11:23.948647+00	21.3000011444091797	1
277	2020-10-06 15:11:24.926701+00	56.6000022888183594	2
278	2020-10-06 15:11:27.945695+00	21.3000011444091797	1
279	2020-10-06 15:11:28.938212+00	56.6000022888183594	2
280	2020-10-06 15:11:31.932624+00	21.3999996185302734	1
281	2020-10-06 15:11:32.931886+00	56.6000022888183594	2
282	2020-10-06 15:11:35.942285+00	21.3000011444091797	1
283	2020-10-06 15:11:36.937128+00	56.5	2
284	2020-10-06 15:11:39.939492+00	21.3000011444091797	1
285	2020-10-06 15:11:40.950009+00	56.5	2
286	2020-10-06 15:11:43.940259+00	21.3000011444091797	1
287	2020-10-06 15:11:44.942474+00	56.5	2
288	2020-10-06 15:11:47.968272+00	21.3000011444091797	1
289	2020-10-06 15:11:48.957828+00	56.4000015258789062	2
290	2020-10-06 15:11:52.002905+00	21.3000011444091797	1
291	2020-10-06 15:11:52.950762+00	56.4000015258789062	2
292	2020-10-06 15:11:55.979807+00	21.3000011444091797	1
293	2020-10-06 15:11:56.947921+00	56.4000015258789062	2
294	2020-10-06 15:11:59.958807+00	21.3000011444091797	1
295	2020-10-06 15:12:00.993882+00	56.4000015258789062	2
296	2020-10-06 15:12:03.995383+00	21.3000011444091797	1
297	2020-10-06 15:12:04.983555+00	56.4000015258789062	2
298	2020-10-06 15:12:07.965744+00	21.3000011444091797	1
299	2020-10-06 15:12:08.986632+00	56.4000015258789062	2
300	2020-10-06 15:12:11.962909+00	21.3000011444091797	1
301	2020-10-06 15:12:12.971943+00	56.4000015258789062	2
302	2020-10-06 15:12:15.973949+00	21.3000011444091797	1
303	2020-10-06 15:12:16.973446+00	56.4000015258789062	2
304	2020-10-06 15:12:19.977798+00	21.3000011444091797	1
305	2020-10-06 15:12:20.998526+00	56.4000015258789062	2
306	2020-10-06 15:12:23.985796+00	21.3000011444091797	1
307	2020-10-06 15:12:24.975165+00	56.4000015258789062	2
308	2020-10-06 15:12:27.986456+00	21.3000011444091797	1
309	2020-10-06 15:12:28.981913+00	56.4000015258789062	2
310	2020-10-06 15:12:31.982286+00	21.3000011444091797	1
311	2020-10-06 15:12:33.012635+00	56.4000015258789062	2
312	2020-10-06 15:12:35.992847+00	21.3000011444091797	1
313	2020-10-06 15:12:36.992978+00	56.4000015258789062	2
314	2020-10-06 15:12:40.020181+00	21.3000011444091797	1
315	2020-10-06 15:12:40.995793+00	56.4000015258789062	2
316	2020-10-06 15:12:44.020972+00	21.3000011444091797	1
317	2020-10-06 15:12:45.000193+00	56.4000015258789062	2
318	2020-10-06 15:12:48.005436+00	21.3000011444091797	1
319	2020-10-06 15:12:49.005032+00	56.4000015258789062	2
320	2020-10-06 15:12:52.00868+00	21.3000011444091797	1
321	2020-10-06 15:12:53.00747+00	56.4000015258789062	2
322	2020-10-06 15:12:56.012304+00	21.3000011444091797	1
323	2020-10-06 15:12:57.013158+00	56.5	2
324	2020-10-06 15:13:00.016573+00	21.3000011444091797	1
325	2020-10-06 15:13:01.016411+00	56.5	2
326	2020-10-06 15:13:04.02018+00	21.3000011444091797	1
327	2020-10-06 15:13:05.020466+00	56.4000015258789062	2
328	2020-10-06 15:13:08.024184+00	21.3000011444091797	1
329	2020-10-06 15:13:09.023307+00	56.4000015258789062	2
330	2020-10-06 15:13:12.028462+00	21.3000011444091797	1
331	2020-10-06 15:13:13.027274+00	56.5	2
332	2020-10-06 15:13:16.032331+00	21.3000011444091797	1
333	2020-10-06 15:13:17.031511+00	56.5	2
334	2020-10-06 15:13:20.034213+00	21.3000011444091797	1
335	2020-10-06 15:13:21.032395+00	56.5	2
336	2020-10-06 15:13:24.051839+00	21.3000011444091797	1
337	2020-10-06 15:13:25.039374+00	56.5	2
338	2020-10-06 15:13:28.064524+00	21.3000011444091797	1
339	2020-10-06 15:13:29.044313+00	56.5	2
340	2020-10-06 15:13:32.047334+00	21.3000011444091797	1
341	2020-10-06 15:13:33.047441+00	56.5	2
342	2020-10-06 15:13:36.051813+00	21.3000011444091797	1
343	2020-10-06 15:13:37.051814+00	56.5	2
344	2020-10-06 15:13:40.056605+00	21.2000007629394531	1
345	2020-10-06 15:13:41.055384+00	56.5	2
346	2020-10-06 15:13:44.070752+00	21.3000011444091797	1
347	2020-10-06 15:13:45.057879+00	56.5	2
348	2020-10-06 15:13:48.082907+00	21.3000011444091797	1
349	2020-10-06 15:13:49.06253+00	56.5	2
350	2020-10-06 15:13:52.087924+00	21.3000011444091797	1
351	2020-10-06 15:13:53.080904+00	56.4000015258789062	2
352	2020-10-06 15:13:56.070643+00	21.3000011444091797	1
353	2020-10-06 15:13:57.074954+00	56.5	2
354	2020-10-06 15:14:00.07481+00	21.3000011444091797	1
355	2020-10-06 15:14:01.074186+00	56.5	2
356	2020-10-06 15:14:04.083066+00	21.3000011444091797	1
357	2020-10-06 15:14:05.078382+00	56.5	2
358	2020-10-06 15:14:08.102848+00	21.3000011444091797	1
359	2020-10-06 15:14:09.082405+00	56.5	2
360	2020-10-06 15:14:12.109398+00	21.3000011444091797	1
361	2020-10-06 15:14:13.086719+00	56.5	2
362	2020-10-06 15:14:16.110258+00	21.3000011444091797	1
363	2020-10-06 15:14:17.103549+00	56.4000015258789062	2
364	2020-10-06 15:14:20.11552+00	21.3000011444091797	1
365	2020-10-06 15:14:21.116273+00	56.4000015258789062	2
366	2020-10-06 15:14:24.09235+00	21.3000011444091797	1
367	2020-10-06 15:14:25.092526+00	56.4000015258789062	2
368	2020-10-06 15:14:28.100111+00	21.3000011444091797	1
369	2020-10-06 15:14:29.121127+00	56.4000015258789062	2
370	2020-10-06 15:14:32.11165+00	21.3000011444091797	1
371	2020-10-06 15:14:33.103755+00	56.4000015258789062	2
372	2020-10-06 15:14:36.123733+00	21.3000011444091797	1
373	2020-10-06 15:14:37.134503+00	56.4000015258789062	2
374	2020-10-06 15:14:40.132619+00	21.3000011444091797	1
375	2020-10-06 15:14:41.115828+00	56.4000015258789062	2
376	2020-10-06 15:14:44.119528+00	21.3000011444091797	1
377	2020-10-06 15:14:45.125319+00	56.4000015258789062	2
378	2020-10-06 15:14:48.128052+00	21.3000011444091797	1
379	2020-10-06 15:14:49.144751+00	56.4000015258789062	2
380	2020-10-06 15:14:52.157278+00	21.3000011444091797	1
381	2020-10-06 15:14:53.127756+00	56.4000015258789062	2
382	2020-10-06 15:14:56.124204+00	21.3000011444091797	1
383	2020-10-06 15:14:57.123983+00	56.4000015258789062	2
384	2020-10-06 15:15:00.151876+00	21.3000011444091797	1
385	2020-10-06 15:15:01.134405+00	56.4000015258789062	2
386	2020-10-06 15:15:04.145729+00	21.3000011444091797	1
387	2020-10-06 15:15:05.139731+00	56.4000015258789062	2
388	2020-10-06 15:15:08.159729+00	21.3000011444091797	1
389	2020-10-06 15:15:09.163541+00	56.4000015258789062	2
390	2020-10-06 15:15:12.140903+00	21.3000011444091797	1
391	2020-10-06 15:15:13.147144+00	56.4000015258789062	2
392	2020-10-06 15:15:16.143934+00	21.3000011444091797	1
393	2020-10-06 15:15:17.147644+00	56.4000015258789062	2
394	2020-10-06 15:15:20.149447+00	21.3000011444091797	1
395	2020-10-06 15:15:21.151691+00	56.4000015258789062	2
396	2020-10-06 15:15:24.159013+00	21.3000011444091797	1
397	2020-10-06 15:15:25.161147+00	56.4000015258789062	2
398	2020-10-06 15:15:28.156003+00	21.3000011444091797	1
399	2020-10-06 15:15:29.163909+00	56.4000015258789062	2
400	2020-10-06 15:15:32.163694+00	21.3000011444091797	1
401	2020-10-06 15:15:33.174526+00	56.4000015258789062	2
402	2020-10-06 15:15:36.193466+00	21.3000011444091797	1
403	2020-10-06 15:15:37.174408+00	56.4000015258789062	2
404	2020-10-06 15:15:40.201612+00	21.3000011444091797	1
405	2020-10-06 15:15:41.174067+00	56.4000015258789062	2
406	2020-10-06 15:15:44.200152+00	21.3000011444091797	1
407	2020-10-06 15:15:45.18438+00	56.4000015258789062	2
408	2020-10-06 15:15:48.207994+00	21.3000011444091797	1
409	2020-10-06 15:15:49.196641+00	56.4000015258789062	2
410	2020-10-06 15:15:52.183723+00	21.3000011444091797	1
411	2020-10-06 15:15:53.182271+00	56.4000015258789062	2
412	2020-10-06 15:15:56.188011+00	21.3000011444091797	1
413	2020-10-06 15:15:57.188835+00	56.4000015258789062	2
414	2020-10-06 15:16:00.189941+00	21.3000011444091797	1
415	2020-10-06 15:16:01.187723+00	56.4000015258789062	2
416	2020-10-06 15:16:04.190983+00	21.3000011444091797	1
417	2020-10-06 15:16:05.219986+00	56.4000015258789062	2
418	2020-10-06 15:16:08.201792+00	21.2000007629394531	1
419	2020-10-06 15:16:09.19465+00	56.4000015258789062	2
420	2020-10-06 15:16:12.199509+00	21.2000007629394531	1
421	2020-10-06 15:16:13.205482+00	56.4000015258789062	2
422	2020-10-06 15:16:16.233157+00	21.2000007629394531	1
423	2020-10-06 15:16:17.231901+00	56.4000015258789062	2
424	2020-10-06 15:16:20.211945+00	21.2000007629394531	1
425	2020-10-06 15:16:21.213571+00	56.4000015258789062	2
426	2020-10-06 15:16:24.209265+00	21.2000007629394531	1
427	2020-10-06 15:16:25.211014+00	56.4000015258789062	2
428	2020-10-06 15:16:28.221419+00	21.2000007629394531	1
429	2020-10-06 15:16:29.220419+00	56.4000015258789062	2
430	2020-10-06 15:16:32.230525+00	21.2000007629394531	1
431	2020-10-06 15:16:33.23518+00	56.5	2
432	2020-10-06 15:16:36.223294+00	21.2000007629394531	1
433	2020-10-06 15:16:37.221077+00	56.5	2
434	2020-10-06 15:16:40.227062+00	21.2000007629394531	1
435	2020-10-06 15:16:41.225311+00	56.5	2
436	2020-10-06 15:16:44.236484+00	21.2000007629394531	1
437	2020-10-06 15:16:45.236041+00	56.5	2
438	2020-10-06 15:16:48.233797+00	21.2000007629394531	1
439	2020-10-06 15:16:49.233884+00	56.5	2
440	2020-10-06 15:16:52.243955+00	21.2000007629394531	1
441	2020-10-06 15:16:53.243686+00	56.5	2
442	2020-10-06 15:16:56.242846+00	21.2000007629394531	1
443	2020-10-06 15:16:57.242417+00	56.5	2
444	2020-10-06 15:17:00.251744+00	21.2000007629394531	1
445	2020-10-06 15:17:01.272264+00	56.5	2
446	2020-10-06 15:17:04.265374+00	21.2000007629394531	1
447	2020-10-06 15:17:05.255761+00	56.5	2
448	2020-10-06 15:17:08.260203+00	21.2000007629394531	1
449	2020-10-06 15:17:09.260162+00	56.5	2
450	2020-10-06 15:17:12.264219+00	21.2000007629394531	1
451	2020-10-06 15:17:13.263906+00	56.6000022888183594	2
452	2020-10-06 15:17:16.266826+00	21.2000007629394531	1
453	2020-10-06 15:17:17.268055+00	56.6000022888183594	2
454	2020-10-06 15:17:20.293824+00	21.2000007629394531	1
455	2020-10-06 15:17:21.271694+00	56.6000022888183594	2
456	2020-10-06 15:17:24.274796+00	21.2000007629394531	1
457	2020-10-06 15:17:25.277428+00	56.6000022888183594	2
458	2020-10-06 15:17:28.274712+00	21.2000007629394531	1
459	2020-10-06 15:17:29.282513+00	56.6000022888183594	2
460	2020-10-06 15:17:32.286437+00	21.2000007629394531	1
461	2020-10-06 15:17:33.286713+00	56.6000022888183594	2
462	2020-10-06 15:17:36.285064+00	21.2000007629394531	1
463	2020-10-06 15:17:37.29467+00	56.6000022888183594	2
464	2020-10-06 15:17:40.315107+00	21.2000007629394531	1
465	2020-10-06 15:17:41.288007+00	56.6000022888183594	2
466	2020-10-06 15:17:44.28774+00	21.2000007629394531	1
467	2020-10-06 15:17:45.31476+00	56.6000022888183594	2
468	2020-10-06 15:17:48.298162+00	21.2000007629394531	1
469	2020-10-06 15:17:49.298197+00	56.6000022888183594	2
470	2020-10-06 15:17:52.302005+00	21.2000007629394531	1
471	2020-10-06 15:17:53.302418+00	56.6000022888183594	2
472	2020-10-06 15:17:56.308988+00	21.2000007629394531	1
473	2020-10-06 15:17:57.305655+00	56.6000022888183594	2
474	2020-10-06 15:18:00.330685+00	21.2000007629394531	1
475	2020-10-06 15:18:01.312956+00	56.6000022888183594	2
476	2020-10-06 15:18:04.319523+00	21.2000007629394531	1
477	2020-10-06 15:18:05.312773+00	56.6000022888183594	2
478	2020-10-06 15:18:08.322876+00	21.2000007629394531	1
479	2020-10-06 15:18:09.317406+00	56.6000022888183594	2
480	2020-10-06 15:18:12.340421+00	21.2000007629394531	1
481	2020-10-06 15:18:13.321731+00	56.6000022888183594	2
482	2020-10-06 15:18:16.334476+00	21.2000007629394531	1
483	2020-10-06 15:18:17.325226+00	56.6000022888183594	2
484	2020-10-06 15:18:20.323146+00	21.2000007629394531	1
485	2020-10-06 15:18:21.32138+00	56.6000022888183594	2
486	2020-10-06 15:18:24.329895+00	21.2000007629394531	1
487	2020-10-06 15:18:25.325261+00	56.6000022888183594	2
488	2020-10-06 15:18:28.334007+00	21.2000007629394531	1
489	2020-10-06 15:18:29.357052+00	56.6000022888183594	2
490	2020-10-06 15:18:32.333565+00	21.2000007629394531	1
491	2020-10-06 15:18:33.3602+00	56.6000022888183594	2
492	2020-10-06 15:18:36.337529+00	21.2000007629394531	1
493	2020-10-06 15:18:37.344221+00	56.6000022888183594	2
494	2020-10-06 15:18:40.36876+00	21.2000007629394531	1
495	2020-10-06 15:18:41.368743+00	56.6000022888183594	2
496	2020-10-06 15:18:44.344921+00	21.2000007629394531	1
497	2020-10-06 15:18:45.346054+00	56.6000022888183594	2
498	2020-10-06 15:18:48.34988+00	21.2000007629394531	1
499	2020-10-06 15:18:49.355522+00	56.6000022888183594	2
500	2020-10-06 15:18:52.352967+00	21.2000007629394531	1
501	2020-10-06 15:18:53.36279+00	56.6000022888183594	2
502	2020-10-06 15:18:56.374323+00	21.2000007629394531	1
503	2020-10-06 15:18:57.38105+00	56.6000022888183594	2
504	2020-10-06 15:19:00.360317+00	21.2000007629394531	1
505	2020-10-06 15:19:01.360214+00	56.6000022888183594	2
506	2020-10-06 15:19:04.38771+00	21.2000007629394531	1
507	2020-10-06 15:19:05.367837+00	56.6000022888183594	2
508	2020-10-06 15:19:08.399273+00	21.2000007629394531	1
509	2020-10-06 15:19:09.385161+00	56.6000022888183594	2
510	2020-10-06 15:19:12.371444+00	21.2000007629394531	1
511	2020-10-06 15:19:13.37805+00	56.6000022888183594	2
512	2020-10-06 15:19:16.383662+00	21.2000007629394531	1
513	2020-10-06 15:19:17.406159+00	56.6000022888183594	2
514	2020-10-06 15:19:20.412758+00	21.2000007629394531	1
515	2020-10-06 15:19:21.409025+00	56.6000022888183594	2
516	2020-10-06 15:19:24.393882+00	21.2000007629394531	1
517	2020-10-06 15:19:25.386837+00	56.7000007629394531	2
518	2020-10-06 15:19:28.41814+00	21.2000007629394531	1
519	2020-10-06 15:19:29.396623+00	56.6000022888183594	2
520	2020-10-06 15:19:32.401582+00	21.2000007629394531	1
521	2020-10-06 15:19:33.398165+00	56.6000022888183594	2
522	2020-10-06 15:19:36.418494+00	21.2000007629394531	1
523	2020-10-06 15:19:37.407518+00	56.6000022888183594	2
524	2020-10-06 15:19:40.403354+00	21.2000007629394531	1
525	2020-10-06 15:19:41.401688+00	56.6000022888183594	2
526	2020-10-06 15:19:44.410595+00	21.2000007629394531	1
527	2020-10-06 15:19:45.408082+00	56.6000022888183594	2
528	2020-10-06 15:19:48.44053+00	21.2000007629394531	1
529	2020-10-06 15:19:49.439774+00	56.7000007629394531	2
530	2020-10-06 15:19:52.420364+00	21.2000007629394531	1
531	2020-10-06 15:19:53.421808+00	56.7000007629394531	2
532	2020-10-06 15:19:56.424345+00	21.2000007629394531	1
533	2020-10-06 15:19:57.423704+00	56.7000007629394531	2
534	2020-10-06 15:20:00.421438+00	21.2000007629394531	1
535	2020-10-06 15:20:01.451858+00	56.7000007629394531	2
536	2020-10-06 15:20:04.424971+00	21.2000007629394531	1
537	2020-10-06 15:20:05.426791+00	56.7000007629394531	2
538	2020-10-06 15:20:08.462667+00	21.2000007629394531	1
539	2020-10-06 15:20:09.459956+00	56.7000007629394531	2
540	2020-10-06 15:20:12.442062+00	21.2000007629394531	1
541	2020-10-06 15:20:13.4419+00	56.7000007629394531	2
542	2020-10-06 15:20:16.445832+00	21.2000007629394531	1
543	2020-10-06 15:20:17.446858+00	56.7000007629394531	2
544	2020-10-06 15:20:20.469747+00	21.2000007629394531	1
545	2020-10-06 15:20:21.466521+00	56.7000007629394531	2
546	2020-10-06 15:20:24.45388+00	21.2000007629394531	1
547	2020-10-06 15:20:25.453154+00	56.7000007629394531	2
548	2020-10-06 15:20:28.479366+00	21.2000007629394531	1
549	2020-10-06 15:20:29.456872+00	56.7000007629394531	2
550	2020-10-06 15:20:32.460933+00	21.1000003814697266	1
551	2020-10-06 15:20:33.485167+00	56.6000022888183594	2
552	2020-10-06 15:20:36.485003+00	21.1000003814697266	1
553	2020-10-06 15:20:37.465008+00	56.7000007629394531	2
554	2020-10-06 15:20:40.468384+00	21.2000007629394531	1
555	2020-10-06 15:20:41.484931+00	56.7000007629394531	2
556	2020-10-06 15:20:44.472966+00	21.1000003814697266	1
557	2020-10-06 15:20:45.472913+00	56.7000007629394531	2
558	2020-10-06 15:20:48.478217+00	21.1000003814697266	1
559	2020-10-06 15:20:49.477055+00	56.7000007629394531	2
560	2020-10-06 15:20:52.481154+00	21.1000003814697266	1
561	2020-10-06 15:20:53.480947+00	56.7000007629394531	2
562	2020-10-06 15:20:56.50832+00	21.2000007629394531	1
563	2020-10-06 15:20:57.487082+00	56.7000007629394531	2
564	2020-10-06 15:21:00.512456+00	21.1000003814697266	1
565	2020-10-06 15:21:01.491178+00	56.7999992370605469	2
566	2020-10-06 15:21:04.506568+00	21.1000003814697266	1
567	2020-10-06 15:21:05.518661+00	56.7999992370605469	2
568	2020-10-06 15:21:08.498755+00	21.1000003814697266	1
569	2020-10-06 15:21:09.497445+00	56.7999992370605469	2
570	2020-10-06 15:21:12.496817+00	21.1000003814697266	1
571	2020-10-06 15:21:13.524674+00	56.7999992370605469	2
572	2020-10-06 15:21:16.533381+00	21.2000007629394531	1
573	2020-10-06 15:21:17.518929+00	56.9000015258789062	2
574	2020-10-06 15:21:20.513928+00	21.2000007629394531	1
575	2020-10-06 15:21:21.53174+00	56.9000015258789062	2
576	2020-10-06 15:21:24.513261+00	21.2000007629394531	1
577	2020-10-06 15:21:25.541994+00	56.9000015258789062	2
578	2020-10-06 15:21:28.545636+00	21.1000003814697266	1
579	2020-10-06 15:21:29.520193+00	56.7999992370605469	2
580	2020-10-06 15:21:32.521032+00	21.1000003814697266	1
581	2020-10-06 15:21:33.540601+00	56.9000015258789062	2
582	2020-10-06 15:21:36.553461+00	21.2000007629394531	1
583	2020-10-06 15:21:37.527271+00	56.9000015258789062	2
584	2020-10-06 15:21:40.549511+00	21.2000007629394531	1
585	2020-10-06 15:21:41.531293+00	56.9000015258789062	2
586	2020-10-06 15:21:44.557538+00	21.2000007629394531	1
587	2020-10-06 15:21:45.554478+00	56.9000015258789062	2
588	2020-10-06 15:21:48.560796+00	21.2000007629394531	1
589	2020-10-06 15:21:49.541454+00	56.7999992370605469	2
590	2020-10-06 15:21:52.571519+00	21.2000007629394531	1
591	2020-10-06 15:21:53.552404+00	56.9000015258789062	2
592	2020-10-06 15:21:56.54263+00	21.2000007629394531	1
593	2020-10-06 15:21:57.577491+00	56.9000015258789062	2
594	2020-10-06 15:22:00.564496+00	21.2000007629394531	1
595	2020-10-06 15:22:01.553616+00	56.9000015258789062	2
596	2020-10-06 15:22:04.585911+00	21.2000007629394531	1
597	2020-10-06 15:22:05.578566+00	56.9000015258789062	2
598	2020-10-06 15:22:08.578842+00	21.2000007629394531	1
599	2020-10-06 15:22:09.586191+00	56.7999992370605469	2
600	2020-10-06 15:22:12.563412+00	21.2000007629394531	1
601	2020-10-06 15:22:13.581631+00	56.7999992370605469	2
602	2020-10-06 15:22:16.583744+00	21.2000007629394531	1
603	2020-10-06 15:22:17.561509+00	56.7999992370605469	2
604	2020-10-06 15:22:20.57941+00	21.2000007629394531	1
605	2020-10-06 15:22:21.564577+00	56.7999992370605469	2
606	2020-10-06 15:22:24.587977+00	21.2000007629394531	1
607	2020-10-06 15:22:25.581293+00	56.7999992370605469	2
608	2020-10-06 15:22:28.600199+00	21.2000007629394531	1
609	2020-10-06 15:22:29.579298+00	56.7000007629394531	2
610	2020-10-06 15:22:32.587671+00	21.2000007629394531	1
611	2020-10-06 15:22:33.604116+00	56.7000007629394531	2
612	2020-10-06 15:22:36.590781+00	21.2000007629394531	1
613	2020-10-06 15:22:37.585659+00	56.7000007629394531	2
614	2020-10-06 15:22:40.611066+00	21.2000007629394531	1
615	2020-10-06 15:22:41.608152+00	56.7000007629394531	2
616	2020-10-06 15:22:44.594848+00	21.2000007629394531	1
617	2020-10-06 15:22:45.611541+00	56.7000007629394531	2
618	2020-10-06 15:22:48.617551+00	21.2000007629394531	1
619	2020-10-06 15:22:49.617398+00	56.7000007629394531	2
620	2020-10-06 15:22:52.625337+00	21.2000007629394531	1
621	2020-10-06 15:22:53.624671+00	56.6000022888183594	2
622	2020-10-06 15:22:56.631733+00	21.2000007629394531	1
623	2020-10-06 15:22:57.599545+00	56.6000022888183594	2
624	2020-10-06 15:23:00.609607+00	21.2000007629394531	1
625	2020-10-06 15:23:01.629871+00	56.6000022888183594	2
626	2020-10-06 15:23:04.611493+00	21.2000007629394531	1
627	2020-10-06 15:23:05.63574+00	56.6000022888183594	2
628	2020-10-06 15:23:08.625928+00	21.2000007629394531	1
629	2020-10-06 15:23:09.617452+00	56.6000022888183594	2
630	2020-10-06 15:23:12.619699+00	21.2000007629394531	1
631	2020-10-06 15:23:13.621549+00	56.6000022888183594	2
632	2020-10-06 15:23:16.646399+00	21.2000007629394531	1
633	2020-10-06 15:23:17.645063+00	56.6000022888183594	2
634	2020-10-06 15:23:20.635689+00	21.2000007629394531	1
635	2020-10-06 15:23:21.626383+00	56.6000022888183594	2
636	2020-10-06 15:23:24.650806+00	21.2000007629394531	1
637	2020-10-06 15:23:25.653979+00	56.6000022888183594	2
638	2020-10-06 15:23:28.63669+00	21.2000007629394531	1
639	2020-10-06 15:23:29.655763+00	56.6000022888183594	2
640	2020-10-06 15:23:32.635176+00	21.2000007629394531	1
641	2020-10-06 15:23:33.662351+00	56.6000022888183594	2
642	2020-10-06 15:23:36.663097+00	21.2000007629394531	1
643	2020-10-06 15:23:37.67019+00	56.6000022888183594	2
644	2020-10-06 15:23:40.659936+00	21.2000007629394531	1
645	2020-10-06 15:23:41.650719+00	56.6000022888183594	2
646	2020-10-06 15:23:44.673231+00	21.2000007629394531	1
647	2020-10-06 15:23:45.647952+00	56.6000022888183594	2
648	2020-10-06 15:23:48.665525+00	21.2000007629394531	1
649	2020-10-06 15:23:49.659187+00	56.5	2
650	2020-10-06 15:23:52.660133+00	21.3000011444091797	1
651	2020-10-06 15:23:53.659928+00	56.6000022888183594	2
652	2020-10-06 15:23:56.686296+00	21.3000011444091797	1
653	2020-10-06 15:23:57.68342+00	56.6000022888183594	2
654	2020-10-06 15:24:00.688385+00	21.2000007629394531	1
655	2020-10-06 15:24:01.667687+00	56.5	2
656	2020-10-06 15:24:04.671717+00	21.2000007629394531	1
657	2020-10-06 15:24:05.685154+00	56.6000022888183594	2
658	2020-10-06 15:24:08.701302+00	21.2000007629394531	1
659	2020-10-06 15:24:09.694943+00	56.5	2
660	2020-10-06 15:24:12.703037+00	21.2000007629394531	1
661	2020-10-06 15:24:13.679145+00	56.6000022888183594	2
662	2020-10-06 15:24:16.681779+00	21.2000007629394531	1
663	2020-10-06 15:24:17.701807+00	56.6000022888183594	2
664	2020-10-06 15:24:20.708883+00	21.2000007629394531	1
665	2020-10-06 15:24:21.687553+00	56.6000022888183594	2
666	2020-10-06 15:24:24.690862+00	21.2000007629394531	1
667	2020-10-06 15:24:25.71217+00	56.6000022888183594	2
668	2020-10-06 15:24:28.714923+00	21.2000007629394531	1
669	2020-10-06 15:24:29.716516+00	56.6000022888183594	2
670	2020-10-06 15:24:32.710056+00	21.2000007629394531	1
671	2020-10-06 15:24:33.69491+00	56.6000022888183594	2
672	2020-10-06 15:24:36.707728+00	21.2000007629394531	1
673	2020-10-06 15:24:37.699519+00	56.6000022888183594	2
674	2020-10-06 15:24:40.70184+00	21.2000007629394531	1
675	2020-10-06 15:24:41.707648+00	56.6000022888183594	2
676	2020-10-06 15:24:44.711095+00	21.2000007629394531	1
677	2020-10-06 15:24:45.706819+00	56.7000007629394531	2
678	2020-10-06 15:24:48.715233+00	21.2000007629394531	1
679	2020-10-06 15:24:49.707254+00	56.7000007629394531	2
680	2020-10-06 15:24:52.719118+00	21.2000007629394531	1
681	2020-10-06 15:24:53.716267+00	56.7000007629394531	2
682	2020-10-06 15:24:56.717747+00	21.1000003814697266	1
683	2020-10-06 15:24:57.725277+00	56.6000022888183594	2
684	2020-10-06 15:25:00.727443+00	21.2000007629394531	1
685	2020-10-06 15:25:01.721641+00	56.7000007629394531	2
686	2020-10-06 15:25:04.758721+00	21.1000003814697266	1
687	2020-10-06 15:25:05.730828+00	56.7000007629394531	2
688	2020-10-06 15:25:08.73329+00	21.2000007629394531	1
689	2020-10-06 15:25:09.735416+00	56.7000007629394531	2
690	2020-10-06 15:25:12.739481+00	21.1000003814697266	1
691	2020-10-06 15:25:13.737153+00	56.7000007629394531	2
692	2020-10-06 15:25:16.748071+00	21.1000003814697266	1
693	2020-10-06 15:25:17.764486+00	56.7000007629394531	2
694	2020-10-06 15:25:20.774408+00	21.1000003814697266	1
695	2020-10-06 15:25:21.768448+00	56.7000007629394531	2
696	2020-10-06 15:25:24.776797+00	21.2000007629394531	1
697	2020-10-06 15:25:25.753146+00	56.7999992370605469	2
698	2020-10-06 15:25:28.759342+00	21.1000003814697266	1
699	2020-10-06 15:25:29.75723+00	56.7999992370605469	2
700	2020-10-06 15:25:32.782303+00	21.2000007629394531	1
701	2020-10-06 15:25:33.766218+00	56.7999992370605469	2
702	2020-10-06 15:25:36.758352+00	21.1000003814697266	1
703	2020-10-06 15:25:37.78852+00	56.7999992370605469	2
704	2020-10-06 15:25:40.773159+00	21.1000003814697266	1
705	2020-10-06 15:25:41.792354+00	56.7999992370605469	2
706	2020-10-06 15:25:44.795289+00	21.1000003814697266	1
707	2020-10-06 15:25:45.777755+00	56.7999992370605469	2
708	2020-10-06 15:25:48.775831+00	21.1000003814697266	1
709	2020-10-06 15:25:49.769832+00	56.7999992370605469	2
710	2020-10-06 15:25:52.774088+00	21.1000003814697266	1
711	2020-10-06 15:25:53.80313+00	56.7999992370605469	2
712	2020-10-06 15:25:56.807558+00	21.2000007629394531	1
713	2020-10-06 15:25:57.779401+00	56.7999992370605469	2
714	2020-10-06 15:26:00.810755+00	21.2000007629394531	1
715	2020-10-06 15:26:01.788707+00	56.7999992370605469	2
716	2020-10-06 15:26:04.793887+00	21.2000007629394531	1
717	2020-10-06 15:26:05.788299+00	56.7999992370605469	2
718	2020-10-06 15:26:08.798449+00	21.2000007629394531	1
719	2020-10-06 15:26:09.791577+00	56.7999992370605469	2
720	2020-10-06 15:26:12.797046+00	21.1000003814697266	1
721	2020-10-06 15:26:13.801692+00	56.7000007629394531	2
722	2020-10-06 15:26:16.831718+00	21.2000007629394531	1
723	2020-10-06 15:26:17.799493+00	56.7999992370605469	2
724	2020-10-06 15:26:20.836804+00	21.1000003814697266	1
725	2020-10-06 15:26:21.810814+00	56.7999992370605469	2
726	2020-10-06 15:26:24.80847+00	21.1000003814697266	1
727	2020-10-06 15:26:25.806572+00	56.7999992370605469	2
728	2020-10-06 15:26:28.813785+00	21.2000007629394531	1
729	2020-10-06 15:26:29.842149+00	56.7999992370605469	2
730	2020-10-06 15:26:32.817129+00	21.1000003814697266	1
731	2020-10-06 15:26:33.82173+00	56.7999992370605469	2
732	2020-10-06 15:26:36.824494+00	21.1000003814697266	1
733	2020-10-06 15:26:37.848123+00	56.7999992370605469	2
734	2020-10-06 15:26:40.82909+00	21.1000003814697266	1
735	2020-10-06 15:26:41.854208+00	56.7999992370605469	2
736	2020-10-06 15:26:44.856007+00	21.1000003814697266	1
737	2020-10-06 15:26:45.852564+00	56.7999992370605469	2
738	2020-10-06 15:26:48.835639+00	21.1000003814697266	1
739	2020-10-06 15:26:49.832281+00	56.7999992370605469	2
740	2020-10-06 15:26:52.864367+00	21.1000003814697266	1
741	2020-10-06 15:26:53.86367+00	56.7999992370605469	2
742	2020-10-06 15:26:56.845837+00	21.1000003814697266	1
743	2020-10-06 15:26:57.842137+00	56.7999992370605469	2
744	2020-10-06 15:27:00.84864+00	21.1000003814697266	1
745	2020-10-06 15:27:01.84701+00	56.7999992370605469	2
746	2020-10-06 15:27:04.850291+00	21.1000003814697266	1
747	2020-10-06 15:27:05.849942+00	56.7999992370605469	2
748	2020-10-06 15:27:08.883913+00	21.1000003814697266	1
749	2020-10-06 15:27:09.857208+00	56.7999992370605469	2
750	2020-10-06 15:27:12.862397+00	21.1000003814697266	1
751	2020-10-06 15:27:13.85998+00	56.7999992370605469	2
752	2020-10-06 15:27:16.86921+00	21.1000003814697266	1
753	2020-10-06 15:27:17.869478+00	56.7999992370605469	2
754	2020-10-06 15:27:20.872569+00	21.1000003814697266	1
755	2020-10-06 15:27:21.872491+00	56.7999992370605469	2
756	2020-10-06 15:27:24.891929+00	21.1000003814697266	1
757	2020-10-06 15:27:25.875534+00	56.9000015258789062	2
758	2020-10-06 15:27:28.876618+00	21.1000003814697266	1
759	2020-10-06 15:27:29.876017+00	56.9000015258789062	2
760	2020-10-06 15:27:32.879923+00	21.1000003814697266	1
761	2020-10-06 15:27:33.881039+00	56.9000015258789062	2
762	2020-10-06 15:27:36.907489+00	21.1000003814697266	1
763	2020-10-06 15:27:37.893954+00	56.9000015258789062	2
764	2020-10-06 15:27:40.907698+00	21.1000003814697266	1
765	2020-10-06 15:27:41.9102+00	56.9000015258789062	2
766	2020-10-06 15:27:44.904419+00	21.1000003814697266	1
767	2020-10-06 15:27:45.910244+00	56.9000015258789062	2
768	2020-10-06 15:27:48.896143+00	21.1000003814697266	1
769	2020-10-06 15:27:49.898923+00	56.7999992370605469	2
770	2020-10-06 15:27:52.904551+00	21.1000003814697266	1
771	2020-10-06 15:27:53.901369+00	56.7999992370605469	2
772	2020-10-06 15:27:56.899935+00	21.1000003814697266	1
773	2020-10-06 15:27:57.903147+00	56.7999992370605469	2
774	2020-10-06 15:28:00.930779+00	21.1000003814697266	1
775	2020-10-06 15:28:01.910101+00	56.7999992370605469	2
776	2020-10-06 15:28:04.905549+00	21.1000003814697266	1
777	2020-10-06 15:28:05.914631+00	56.7999992370605469	2
778	2020-10-06 15:28:08.915255+00	21.1000003814697266	1
779	2020-10-06 15:28:09.91095+00	56.7999992370605469	2
780	2020-10-06 15:28:12.912646+00	21.1000003814697266	1
781	2020-10-06 15:28:13.914333+00	56.7999992370605469	2
782	2020-10-06 15:28:16.919884+00	21.1000003814697266	1
783	2020-10-06 15:28:17.929949+00	56.7999992370605469	2
784	2020-10-06 15:28:20.933651+00	21.2000007629394531	1
785	2020-10-06 15:28:21.930141+00	56.9000015258789062	2
786	2020-10-06 15:28:24.938462+00	21.2000007629394531	1
787	2020-10-06 15:28:25.939731+00	56.7999992370605469	2
788	2020-10-06 15:28:28.962928+00	21.2000007629394531	1
789	2020-10-06 15:28:29.9625+00	56.7999992370605469	2
790	2020-10-06 15:28:32.943341+00	21.2000007629394531	1
791	2020-10-06 15:28:33.934912+00	56.7999992370605469	2
792	2020-10-06 15:28:36.94543+00	21.2000007629394531	1
793	2020-10-06 15:28:37.943534+00	56.7999992370605469	2
794	2020-10-06 15:28:40.949315+00	21.2000007629394531	1
795	2020-10-06 15:28:41.953421+00	56.7000007629394531	2
796	2020-10-06 15:28:44.960354+00	21.2000007629394531	1
797	2020-10-06 15:28:45.971287+00	56.7000007629394531	2
798	2020-10-06 15:28:48.956319+00	21.2000007629394531	1
799	2020-10-06 15:28:49.958385+00	56.7000007629394531	2
800	2020-10-06 15:28:52.961683+00	21.2000007629394531	1
801	2020-10-06 15:28:53.959568+00	56.7000007629394531	2
802	2020-10-06 15:28:56.964143+00	21.2000007629394531	1
803	2020-10-06 15:28:57.968792+00	56.7000007629394531	2
804	2020-10-06 15:29:00.97324+00	21.2000007629394531	1
805	2020-10-06 15:29:01.991714+00	56.7000007629394531	2
806	2020-10-06 15:29:04.969647+00	21.2000007629394531	1
807	2020-10-06 15:29:05.994247+00	56.7000007629394531	2
808	2020-10-06 15:29:08.975963+00	21.2000007629394531	1
809	2020-10-06 15:29:10.008027+00	56.7000007629394531	2
810	2020-10-06 15:29:12.98161+00	21.2000007629394531	1
811	2020-10-06 15:29:13.986451+00	56.7000007629394531	2
812	2020-10-06 15:29:17.003516+00	21.2000007629394531	1
813	2020-10-06 15:29:17.978857+00	56.7000007629394531	2
814	2020-10-06 15:29:20.98201+00	21.2000007629394531	1
815	2020-10-06 15:29:21.985046+00	56.7000007629394531	2
816	2020-10-06 15:29:24.996223+00	21.2000007629394531	1
817	2020-10-06 15:29:25.994122+00	56.7000007629394531	2
818	2020-10-06 15:29:29.01017+00	21.2000007629394531	1
819	2020-10-06 15:29:29.995321+00	56.7999992370605469	2
820	2020-10-06 15:29:32.999205+00	21.2000007629394531	1
821	2020-10-06 15:29:33.999799+00	56.7999992370605469	2
822	2020-10-06 15:29:37.022076+00	21.2000007629394531	1
823	2020-10-06 15:29:38.002793+00	56.7999992370605469	2
824	2020-10-06 15:29:41.0044+00	21.2000007629394531	1
825	2020-10-06 15:29:42.000158+00	56.7000007629394531	2
826	2020-10-06 15:29:45.034176+00	21.2000007629394531	1
827	2020-10-06 15:29:46.004271+00	56.7000007629394531	2
828	2020-10-06 15:29:49.025299+00	21.2000007629394531	1
829	2020-10-06 15:29:50.028277+00	56.7000007629394531	2
830	2020-10-06 15:29:53.019636+00	21.2000007629394531	1
831	2020-10-06 15:29:54.023911+00	56.7000007629394531	2
832	2020-10-06 15:29:57.049499+00	21.2000007629394531	1
833	2020-10-06 15:29:58.048533+00	56.7000007629394531	2
834	2020-10-06 15:30:01.03374+00	21.2000007629394531	1
835	2020-10-06 15:30:02.028341+00	56.7000007629394531	2
836	2020-10-06 15:30:05.055429+00	21.2000007629394531	1
837	2020-10-06 15:30:06.031765+00	56.7000007629394531	2
838	2020-10-06 15:30:09.035868+00	21.2000007629394531	1
839	2020-10-06 15:30:10.035852+00	56.7000007629394531	2
840	2020-10-06 15:30:13.04085+00	21.2000007629394531	1
841	2020-10-06 15:30:14.058276+00	56.7000007629394531	2
842	2020-10-06 15:30:17.044054+00	21.2000007629394531	1
843	2020-10-06 15:30:18.043715+00	56.7000007629394531	2
844	2020-10-06 15:30:21.069342+00	21.2000007629394531	1
845	2020-10-06 15:30:22.04795+00	56.7000007629394531	2
846	2020-10-06 15:30:25.07329+00	21.2000007629394531	1
847	2020-10-06 15:30:26.052006+00	56.7999992370605469	2
848	2020-10-06 15:30:29.05599+00	21.2000007629394531	1
849	2020-10-06 15:30:30.078892+00	56.7000007629394531	2
850	2020-10-06 15:30:33.080739+00	21.2000007629394531	1
851	2020-10-06 15:30:34.079574+00	56.7000007629394531	2
852	2020-10-06 15:30:37.057299+00	21.2000007629394531	1
853	2020-10-06 15:30:38.056296+00	56.7000007629394531	2
854	2020-10-06 15:30:41.088284+00	21.2000007629394531	1
855	2020-10-06 15:30:42.064653+00	56.7000007629394531	2
856	2020-10-06 15:30:45.09098+00	21.2000007629394531	1
857	2020-10-06 15:30:46.078729+00	56.7000007629394531	2
858	2020-10-06 15:30:49.09609+00	21.2000007629394531	1
859	2020-10-06 15:30:50.100907+00	56.7000007629394531	2
860	2020-10-06 15:30:53.071998+00	21.2000007629394531	1
861	2020-10-06 15:30:54.071526+00	56.7000007629394531	2
862	2020-10-06 15:30:57.092755+00	21.2000007629394531	1
863	2020-10-06 15:30:58.075946+00	56.7000007629394531	2
864	2020-10-06 15:31:01.087271+00	21.2000007629394531	1
865	2020-10-06 15:31:02.111386+00	56.7000007629394531	2
866	2020-10-06 15:31:05.090378+00	21.2000007629394531	1
867	2020-10-06 15:31:06.091174+00	56.6000022888183594	2
868	2020-10-06 15:31:09.09553+00	21.2000007629394531	1
869	2020-10-06 15:31:10.091325+00	56.6000022888183594	2
870	2020-10-06 15:31:13.098542+00	21.2000007629394531	1
871	2020-10-06 15:31:14.092286+00	56.6000022888183594	2
872	2020-10-06 15:31:17.100253+00	21.2000007629394531	1
873	2020-10-06 15:31:18.098236+00	56.6000022888183594	2
874	2020-10-06 15:31:21.106435+00	21.2000007629394531	1
875	2020-10-06 15:31:22.105982+00	56.6000022888183594	2
876	2020-10-06 15:31:25.109931+00	21.2000007629394531	1
877	2020-10-06 15:31:26.13215+00	56.6000022888183594	2
878	2020-10-06 15:31:29.137626+00	21.2000007629394531	1
879	2020-10-06 15:31:30.136865+00	56.6000022888183594	2
880	2020-10-06 15:31:33.115203+00	21.2000007629394531	1
881	2020-10-06 15:31:34.143126+00	56.6000022888183594	2
882	2020-10-06 15:31:37.144366+00	21.2000007629394531	1
883	2020-10-06 15:31:38.116996+00	56.6000022888183594	2
884	2020-10-06 15:31:41.125229+00	21.2000007629394531	1
885	2020-10-06 15:31:42.124232+00	56.6000022888183594	2
886	2020-10-06 15:31:45.153746+00	21.2000007629394531	1
887	2020-10-06 15:31:46.122249+00	56.6000022888183594	2
888	2020-10-06 15:31:49.132619+00	21.2000007629394531	1
889	2020-10-06 15:31:50.13489+00	56.6000022888183594	2
890	2020-10-06 15:31:53.136159+00	21.2000007629394531	1
891	2020-10-06 15:31:54.129685+00	56.6000022888183594	2
892	2020-10-06 15:31:57.140765+00	21.2000007629394531	1
893	2020-10-06 15:31:58.161388+00	56.6000022888183594	2
894	2020-10-06 15:32:01.144315+00	21.2000007629394531	1
895	2020-10-06 15:32:02.145386+00	56.6000022888183594	2
896	2020-10-06 15:32:05.166299+00	21.2000007629394531	1
897	2020-10-06 15:32:06.149158+00	56.6000022888183594	2
898	2020-10-06 15:32:09.153832+00	21.3000011444091797	1
899	2020-10-06 15:32:10.152032+00	56.7000007629394531	2
900	2020-10-06 15:32:13.157412+00	21.2000007629394531	1
901	2020-10-06 15:32:14.156515+00	56.6000022888183594	2
902	2020-10-06 15:32:17.170061+00	21.2000007629394531	1
903	2020-10-06 15:32:18.161053+00	56.6000022888183594	2
904	2020-10-06 15:32:21.164492+00	21.2000007629394531	1
905	2020-10-06 15:32:22.164386+00	56.6000022888183594	2
906	2020-10-06 15:32:25.168923+00	21.2000007629394531	1
907	2020-10-06 15:32:26.179057+00	56.6000022888183594	2
908	2020-10-06 15:32:29.172588+00	21.2000007629394531	1
909	2020-10-06 15:32:30.172642+00	56.6000022888183594	2
910	2020-10-06 15:32:33.176562+00	21.2000007629394531	1
911	2020-10-06 15:32:34.175518+00	56.6000022888183594	2
912	2020-10-06 15:32:37.180535+00	21.2000007629394531	1
913	2020-10-06 15:32:38.179683+00	56.7000007629394531	2
914	2020-10-06 15:32:41.206309+00	21.2000007629394531	1
915	2020-10-06 15:32:42.183996+00	56.7000007629394531	2
916	2020-10-06 15:32:45.188796+00	21.2000007629394531	1
917	2020-10-06 15:32:46.209399+00	56.7000007629394531	2
918	2020-10-06 15:32:49.191483+00	21.2000007629394531	1
919	2020-10-06 15:32:50.191625+00	56.7000007629394531	2
920	2020-10-06 15:32:53.19568+00	21.2000007629394531	1
921	2020-10-06 15:32:54.196004+00	56.7000007629394531	2
922	2020-10-06 15:32:57.198922+00	21.2000007629394531	1
923	2020-10-06 15:32:58.198087+00	56.7000007629394531	2
924	2020-10-06 15:33:01.203041+00	21.2000007629394531	1
925	2020-10-06 15:33:02.201273+00	56.7000007629394531	2
926	2020-10-06 15:33:05.201954+00	21.2000007629394531	1
927	2020-10-06 15:33:06.207956+00	56.7000007629394531	2
928	2020-10-06 15:33:09.234546+00	21.2000007629394531	1
929	2020-10-06 15:33:10.224373+00	56.7000007629394531	2
930	2020-10-06 15:33:13.208474+00	21.2000007629394531	1
931	2020-10-06 15:33:14.213198+00	56.7000007629394531	2
932	2020-10-06 15:33:17.216765+00	21.2000007629394531	1
933	2020-10-06 15:33:18.218005+00	56.7000007629394531	2
934	2020-10-06 15:33:21.228903+00	21.2000007629394531	1
935	2020-10-06 15:33:22.248033+00	56.7000007629394531	2
936	2020-10-06 15:33:25.223279+00	21.2000007629394531	1
937	2020-10-06 15:33:26.223491+00	56.6000022888183594	2
938	2020-10-06 15:33:29.230908+00	21.2000007629394531	1
939	2020-10-06 15:33:30.231853+00	56.7000007629394531	2
940	2020-10-06 15:33:33.235984+00	21.2000007629394531	1
941	2020-10-06 15:33:34.259431+00	56.6000022888183594	2
942	2020-10-06 15:33:37.260468+00	21.2000007629394531	1
943	2020-10-06 15:33:38.236853+00	56.6000022888183594	2
944	2020-10-06 15:33:41.240242+00	21.2000007629394531	1
945	2020-10-06 15:33:42.238926+00	56.6000022888183594	2
946	2020-10-06 15:33:45.24405+00	21.3000011444091797	1
947	2020-10-06 15:33:46.243557+00	56.7000007629394531	2
948	2020-10-06 15:33:49.276644+00	21.2000007629394531	1
949	2020-10-06 15:33:50.260537+00	56.6000022888183594	2
950	2020-10-06 15:33:53.274681+00	21.2000007629394531	1
951	2020-10-06 15:33:54.279791+00	56.6000022888183594	2
952	2020-10-06 15:33:57.280431+00	21.3000011444091797	1
953	2020-10-06 15:33:58.25939+00	56.6000022888183594	2
954	2020-10-06 15:34:01.261966+00	21.3000011444091797	1
955	2020-10-06 15:34:02.262272+00	56.6000022888183594	2
956	2020-10-06 15:34:05.29135+00	21.2000007629394531	1
957	2020-10-06 15:34:06.288177+00	56.5	2
958	2020-10-06 15:34:09.270941+00	21.2000007629394531	1
959	2020-10-06 15:34:10.268779+00	56.5	2
960	2020-10-06 15:34:13.274275+00	21.2000007629394531	1
961	2020-10-06 15:34:14.27451+00	56.5	2
962	2020-10-06 15:34:17.279278+00	21.2000007629394531	1
963	2020-10-06 15:34:18.27824+00	56.5	2
964	2020-10-06 15:34:21.278452+00	21.2000007629394531	1
965	2020-10-06 15:34:22.280724+00	56.5	2
966	2020-10-06 15:34:25.286091+00	21.3000011444091797	1
967	2020-10-06 15:34:26.282422+00	56.6000022888183594	2
968	2020-10-06 15:34:29.313228+00	21.2000007629394531	1
969	2020-10-06 15:34:30.312438+00	56.6000022888183594	2
970	2020-10-06 15:34:33.315975+00	21.2000007629394531	1
971	2020-10-06 15:34:34.29438+00	56.6000022888183594	2
972	2020-10-06 15:34:37.29784+00	21.2000007629394531	1
973	2020-10-06 15:34:38.306739+00	56.6000022888183594	2
974	2020-10-06 15:34:41.32274+00	21.2000007629394531	1
975	2020-10-06 15:34:42.30118+00	56.6000022888183594	2
976	2020-10-06 15:34:45.305255+00	21.2000007629394531	1
977	2020-10-06 15:34:46.301396+00	56.6000022888183594	2
978	2020-10-06 15:34:49.309595+00	21.2000007629394531	1
979	2020-10-06 15:34:50.309439+00	56.6000022888183594	2
980	2020-10-06 15:34:53.335494+00	21.2000007629394531	1
981	2020-10-06 15:34:54.312488+00	56.6000022888183594	2
982	2020-10-06 15:34:57.311184+00	21.2000007629394531	1
983	2020-10-06 15:34:58.31754+00	56.6000022888183594	2
984	2020-10-06 15:35:01.319873+00	21.2000007629394531	1
985	2020-10-06 15:35:02.340708+00	56.6000022888183594	2
986	2020-10-06 15:35:05.318353+00	21.2000007629394531	1
987	2020-10-06 15:35:06.316758+00	56.6000022888183594	2
988	2020-10-06 15:35:09.328123+00	21.2000007629394531	1
989	2020-10-06 15:35:10.327731+00	56.7000007629394531	2
990	2020-10-06 15:35:13.330916+00	21.2000007629394531	1
991	2020-10-06 15:35:14.331777+00	56.6000022888183594	2
992	2020-10-06 15:35:17.335811+00	21.2000007629394531	1
993	2020-10-06 15:35:18.334908+00	56.7000007629394531	2
994	2020-10-06 15:35:21.338803+00	21.2000007629394531	1
995	2020-10-06 15:35:22.333218+00	56.6000022888183594	2
996	2020-10-06 15:35:25.343385+00	21.2000007629394531	1
997	2020-10-06 15:35:26.338843+00	56.7000007629394531	2
998	2020-10-06 15:35:29.350192+00	21.2000007629394531	1
999	2020-10-06 15:35:30.34766+00	56.7000007629394531	2
1000	2020-10-06 15:35:33.34802+00	21.2000007629394531	1
1001	2020-10-06 15:35:34.350921+00	56.7000007629394531	2
1002	2020-10-06 15:35:37.38585+00	21.2000007629394531	1
1003	2020-10-06 15:35:38.355416+00	56.7000007629394531	2
1004	2020-10-06 15:35:41.353765+00	21.2000007629394531	1
1005	2020-10-06 15:35:42.356112+00	56.7000007629394531	2
1006	2020-10-06 15:35:45.362876+00	21.2000007629394531	1
1007	2020-10-06 15:35:46.38453+00	56.7000007629394531	2
1008	2020-10-06 15:35:49.367354+00	21.2000007629394531	1
1009	2020-10-06 15:35:50.384706+00	56.7000007629394531	2
1010	2020-10-06 15:35:53.399668+00	21.2000007629394531	1
1011	2020-10-06 15:35:54.37707+00	56.7000007629394531	2
1012	2020-10-06 15:35:57.378649+00	21.2000007629394531	1
1013	2020-10-06 15:35:58.374915+00	56.7000007629394531	2
1014	2020-10-06 15:36:01.381522+00	21.2000007629394531	1
1015	2020-10-06 15:36:02.38113+00	56.7000007629394531	2
1016	2020-10-06 15:36:05.408135+00	21.2000007629394531	1
1017	2020-10-06 15:36:06.408086+00	56.7000007629394531	2
1018	2020-10-06 15:36:09.389261+00	21.2000007629394531	1
1019	2020-10-06 15:36:10.383758+00	56.7000007629394531	2
1020	2020-10-06 15:36:13.405507+00	21.2000007629394531	1
1021	2020-10-06 15:36:14.393566+00	56.7000007629394531	2
1022	2020-10-06 15:36:17.397747+00	21.2000007629394531	1
1023	2020-10-06 15:36:18.397792+00	56.7000007629394531	2
1024	2020-10-06 15:36:21.394872+00	21.2000007629394531	1
1025	2020-10-06 15:36:22.423526+00	56.7000007629394531	2
1026	2020-10-06 15:36:25.404423+00	21.2000007629394531	1
1027	2020-10-06 15:36:26.425633+00	56.7000007629394531	2
1028	2020-10-06 15:36:29.461127+00	21.2000007629394531	1
1029	2020-10-06 15:36:30.459804+00	56.7000007629394531	2
1030	2020-10-06 15:36:33.440711+00	21.2000007629394531	1
1031	2020-10-06 15:36:34.440886+00	56.7000007629394531	2
1032	2020-10-06 15:36:37.467803+00	21.2000007629394531	1
1033	2020-10-06 15:36:38.466261+00	56.7000007629394531	2
1034	2020-10-06 15:36:41.449132+00	21.2000007629394531	1
1035	2020-10-06 15:36:42.449436+00	56.7000007629394531	2
1036	2020-10-06 15:36:45.45278+00	21.2000007629394531	1
1037	2020-10-06 15:36:46.451956+00	56.7000007629394531	2
1038	2020-10-06 15:36:49.456878+00	21.2000007629394531	1
1039	2020-10-06 15:36:50.456383+00	56.7000007629394531	2
1040	2020-10-06 15:36:53.460751+00	21.2000007629394531	1
1041	2020-10-06 15:36:54.481731+00	56.7000007629394531	2
1042	2020-10-06 15:36:57.463436+00	21.2000007629394531	1
1043	2020-10-06 15:36:58.47424+00	56.7000007629394531	2
1044	2020-10-06 15:37:01.468169+00	21.2000007629394531	1
1045	2020-10-06 15:37:02.489556+00	56.7000007629394531	2
1046	2020-10-06 15:37:05.472188+00	21.2000007629394531	1
1047	2020-10-06 15:37:06.473144+00	56.7000007629394531	2
1048	2020-10-06 15:37:09.478201+00	21.2000007629394531	1
1049	2020-10-06 15:37:10.501988+00	56.7000007629394531	2
1050	2020-10-06 15:37:13.493416+00	21.2000007629394531	1
1051	2020-10-06 15:37:14.507728+00	56.7000007629394531	2
1052	2020-10-06 15:37:17.488162+00	21.2000007629394531	1
1053	2020-10-06 15:37:18.498318+00	56.7000007629394531	2
1054	2020-10-06 15:37:21.491839+00	21.2000007629394531	1
1055	2020-10-06 15:37:22.514077+00	56.7000007629394531	2
1056	2020-10-06 15:37:25.496216+00	21.2000007629394531	1
1057	2020-10-06 15:37:26.495773+00	56.7000007629394531	2
1058	2020-10-06 15:37:29.500062+00	21.2000007629394531	1
1059	2020-10-06 15:37:30.497911+00	56.7000007629394531	2
1060	2020-10-06 15:37:33.525822+00	21.2000007629394531	1
1061	2020-10-06 15:37:34.52455+00	56.7000007629394531	2
1062	2020-10-06 15:37:37.508114+00	21.2000007629394531	1
1063	2020-10-06 15:37:38.528524+00	56.7000007629394531	2
1064	2020-10-06 15:37:41.511978+00	21.2000007629394531	1
1065	2020-10-06 15:37:42.511075+00	56.7000007629394531	2
1066	2020-10-06 15:37:45.516015+00	21.2000007629394531	1
1067	2020-10-06 15:37:46.515433+00	56.7000007629394531	2
1068	2020-10-06 15:37:49.541207+00	21.2000007629394531	1
1069	2020-10-06 15:37:50.519205+00	56.7000007629394531	2
1070	2020-10-06 15:37:53.52177+00	21.2000007629394531	1
1071	2020-10-06 15:37:54.525336+00	56.7000007629394531	2
1072	2020-10-06 15:37:57.529581+00	21.2000007629394531	1
1073	2020-10-06 15:37:58.529309+00	56.7000007629394531	2
1074	2020-10-06 15:38:01.554524+00	21.3000011444091797	1
1075	2020-10-06 15:38:02.533198+00	56.7999992370605469	2
1076	2020-10-06 15:38:05.557589+00	21.3000011444091797	1
1077	2020-10-06 15:38:06.537115+00	56.7000007629394531	2
1078	2020-10-06 15:38:09.56164+00	21.3000011444091797	1
1079	2020-10-06 15:38:10.541111+00	56.7000007629394531	2
1080	2020-10-06 15:38:13.543865+00	21.3000011444091797	1
1081	2020-10-06 15:38:14.545535+00	56.7000007629394531	2
1082	2020-10-06 15:38:17.549384+00	21.3000011444091797	1
1083	2020-10-06 15:38:18.549377+00	56.7000007629394531	2
1084	2020-10-06 15:38:21.572241+00	21.3000011444091797	1
1085	2020-10-06 15:38:22.564692+00	56.7000007629394531	2
1086	2020-10-06 15:38:25.557193+00	21.3000011444091797	1
1087	2020-10-06 15:38:26.573485+00	56.7000007629394531	2
1088	2020-10-06 15:38:29.584522+00	21.3000011444091797	1
1089	2020-10-06 15:38:30.56232+00	56.7000007629394531	2
1090	2020-10-06 15:38:33.567095+00	21.3000011444091797	1
1091	2020-10-06 15:38:34.566351+00	56.7000007629394531	2
1092	2020-10-06 15:38:37.581913+00	21.3000011444091797	1
1093	2020-10-06 15:38:38.590925+00	56.7000007629394531	2
1094	2020-10-06 15:38:41.576312+00	21.3000011444091797	1
1095	2020-10-06 15:38:42.576295+00	56.7000007629394531	2
1096	2020-10-06 15:38:45.587237+00	21.3000011444091797	1
1097	2020-10-06 15:38:46.58064+00	56.7000007629394531	2
1098	2020-10-06 15:38:49.583834+00	21.3000011444091797	1
1099	2020-10-06 15:38:50.584933+00	56.7000007629394531	2
1100	2020-10-06 15:38:53.590412+00	21.3000011444091797	1
1101	2020-10-06 15:38:54.611913+00	56.6000022888183594	2
1102	2020-10-06 15:38:57.594461+00	21.3000011444091797	1
1103	2020-10-06 15:38:58.598578+00	56.6000022888183594	2
1104	2020-10-06 15:39:01.596964+00	21.3000011444091797	1
1105	2020-10-06 15:39:02.619356+00	56.6000022888183594	2
1106	2020-10-06 15:39:05.628043+00	21.3000011444091797	1
1107	2020-10-06 15:39:06.606379+00	56.6000022888183594	2
1108	2020-10-06 15:39:09.612183+00	21.3000011444091797	1
1109	2020-10-06 15:39:10.61205+00	56.6000022888183594	2
1110	2020-10-06 15:39:13.615108+00	21.3000011444091797	1
1111	2020-10-06 15:39:14.61819+00	56.6000022888183594	2
1112	2020-10-06 15:39:17.642741+00	21.3000011444091797	1
1113	2020-10-06 15:39:18.621077+00	56.6000022888183594	2
1114	2020-10-06 15:39:21.636801+00	21.3000011444091797	1
1115	2020-10-06 15:39:22.626106+00	56.6000022888183594	2
1116	2020-10-06 15:39:25.629609+00	21.3000011444091797	1
1117	2020-10-06 15:39:26.652808+00	56.6000022888183594	2
1118	2020-10-06 15:39:29.634642+00	21.3000011444091797	1
1119	2020-10-06 15:39:30.63338+00	56.6000022888183594	2
1120	2020-10-06 15:39:33.637528+00	21.3000011444091797	1
1121	2020-10-06 15:39:34.658567+00	56.6000022888183594	2
1122	2020-10-06 15:39:37.641497+00	21.3000011444091797	1
1123	2020-10-06 15:39:38.642392+00	56.6000022888183594	2
1124	2020-10-06 15:39:41.666964+00	21.3000011444091797	1
1125	2020-10-06 15:39:42.645684+00	56.6000022888183594	2
1126	2020-10-06 15:39:45.649597+00	21.3000011444091797	1
1127	2020-10-06 15:39:46.649482+00	56.5	2
1128	2020-10-06 15:39:49.674276+00	21.3000011444091797	1
1129	2020-10-06 15:39:50.653565+00	56.5	2
1130	2020-10-06 15:39:53.657474+00	21.3000011444091797	1
1131	2020-10-06 15:39:54.656356+00	56.5	2
1132	2020-10-06 15:39:57.660774+00	21.3000011444091797	1
1133	2020-10-06 15:39:58.661441+00	56.5	2
1134	2020-10-06 15:40:01.664827+00	21.3000011444091797	1
1135	2020-10-06 15:40:02.665993+00	56.5	2
1136	2020-10-06 15:40:05.669854+00	21.3000011444091797	1
1137	2020-10-06 15:40:06.688797+00	56.5	2
1138	2020-10-06 15:40:09.672525+00	21.3000011444091797	1
1139	2020-10-06 15:40:10.672733+00	56.5	2
1140	2020-10-06 15:40:13.676538+00	21.3000011444091797	1
1141	2020-10-06 15:40:14.676607+00	56.5	2
1142	2020-10-06 15:40:17.681103+00	21.3000011444091797	1
1143	2020-10-06 15:40:18.681678+00	56.5	2
1144	2020-10-06 15:40:21.683894+00	21.3000011444091797	1
1145	2020-10-06 15:40:22.703848+00	56.5	2
1146	2020-10-06 15:40:25.688615+00	21.3000011444091797	1
1147	2020-10-06 15:40:26.708097+00	56.4000015258789062	2
1148	2020-10-06 15:40:29.692994+00	21.3000011444091797	1
1149	2020-10-06 15:40:30.691743+00	56.4000015258789062	2
1150	2020-10-06 15:40:33.696586+00	21.3000011444091797	1
1151	2020-10-06 15:40:34.696237+00	56.4000015258789062	2
1152	2020-10-06 15:40:37.699756+00	21.3000011444091797	1
1153	2020-10-06 15:40:38.700334+00	56.4000015258789062	2
1154	2020-10-06 15:40:41.723328+00	21.3000011444091797	1
1155	2020-10-06 15:40:42.703838+00	56.4000015258789062	2
1156	2020-10-06 15:40:45.708136+00	21.3000011444091797	1
1157	2020-10-06 15:40:46.707743+00	56.4000015258789062	2
1158	2020-10-06 15:40:49.711576+00	21.3000011444091797	1
1159	2020-10-06 15:40:50.7119+00	56.4000015258789062	2
1160	2020-10-06 15:40:53.723065+00	21.3000011444091797	1
1161	2020-10-06 15:40:54.739818+00	56.5	2
1162	2020-10-06 15:40:57.722031+00	21.3000011444091797	1
1163	2020-10-06 15:40:58.720986+00	56.4000015258789062	2
1164	2020-10-06 15:41:01.724932+00	21.3000011444091797	1
1165	2020-10-06 15:41:02.72533+00	56.4000015258789062	2
1166	2020-10-06 15:41:05.72901+00	21.3000011444091797	1
1167	2020-10-06 15:41:06.729878+00	56.4000015258789062	2
1168	2020-10-06 15:41:09.753742+00	21.3000011444091797	1
1169	2020-10-06 15:41:10.733025+00	56.4000015258789062	2
1170	2020-10-06 15:41:13.737786+00	21.2000007629394531	1
1171	2020-10-06 15:41:14.736967+00	56.4000015258789062	2
1172	2020-10-06 15:41:17.7411+00	21.3000011444091797	1
1173	2020-10-06 15:41:18.741426+00	56.5	2
1174	2020-10-06 15:41:21.747094+00	21.3000011444091797	1
1175	2020-10-06 15:41:22.746656+00	56.5	2
1176	2020-10-06 15:41:25.774752+00	21.2000007629394531	1
1177	2020-10-06 15:41:26.750152+00	56.5	2
1178	2020-10-06 15:41:29.775613+00	21.2000007629394531	1
1179	2020-10-06 15:41:30.761106+00	56.5	2
1180	2020-10-06 15:41:33.758751+00	21.2000007629394531	1
1181	2020-10-06 15:41:34.759072+00	56.5	2
1182	2020-10-06 15:41:37.772414+00	21.2000007629394531	1
1183	2020-10-06 15:41:38.759475+00	56.5	2
1184	2020-10-06 15:41:41.766197+00	21.2000007629394531	1
1185	2020-10-06 15:41:42.765827+00	56.5	2
1186	2020-10-06 15:41:45.770557+00	21.2000007629394531	1
1187	2020-10-06 15:41:46.769321+00	56.6000022888183594	2
1188	2020-10-06 15:41:49.774048+00	21.2000007629394531	1
1189	2020-10-06 15:41:50.773992+00	56.6000022888183594	2
1190	2020-10-06 15:41:53.778143+00	21.2000007629394531	1
1191	2020-10-06 15:41:54.778777+00	56.6000022888183594	2
1192	2020-10-06 15:41:57.78247+00	21.2000007629394531	1
1193	2020-10-06 15:41:58.780673+00	56.6000022888183594	2
1194	2020-10-06 15:42:01.785233+00	21.2000007629394531	1
1195	2020-10-06 15:42:02.785143+00	56.7000007629394531	2
1196	2020-10-06 15:42:05.801638+00	21.2000007629394531	1
1197	2020-10-06 15:42:06.790208+00	56.7000007629394531	2
1198	2020-10-06 15:42:09.792866+00	21.2000007629394531	1
1199	2020-10-06 15:42:10.794074+00	56.7000007629394531	2
1200	2020-10-06 15:42:13.794613+00	21.1000003814697266	1
1201	2020-10-06 15:42:14.796863+00	56.7000007629394531	2
1202	2020-10-06 15:42:17.80135+00	21.2000007629394531	1
1203	2020-10-06 15:42:18.801595+00	56.7000007629394531	2
1204	2020-10-06 15:42:21.815375+00	21.2000007629394531	1
1205	2020-10-06 15:42:22.827612+00	56.7000007629394531	2
1206	2020-10-06 15:42:25.810388+00	21.1000003814697266	1
1207	2020-10-06 15:42:26.809318+00	56.7000007629394531	2
1208	2020-10-06 15:42:29.812103+00	21.2000007629394531	1
1209	2020-10-06 15:42:30.813416+00	56.7999992370605469	2
1210	2020-10-06 15:42:33.817281+00	21.2000007629394531	1
1211	2020-10-06 15:42:34.837129+00	56.7999992370605469	2
1212	2020-10-06 15:42:37.820249+00	21.2000007629394531	1
1213	2020-10-06 15:42:38.832687+00	56.7999992370605469	2
1214	2020-10-06 15:42:41.831323+00	21.1000003814697266	1
1215	2020-10-06 15:42:42.825244+00	56.7999992370605469	2
1216	2020-10-06 15:42:45.822169+00	21.1000003814697266	1
1217	2020-10-06 15:42:46.829355+00	56.7999992370605469	2
1218	2020-10-06 15:42:49.835594+00	21.1000003814697266	1
1219	2020-10-06 15:42:50.834591+00	56.7999992370605469	2
1220	2020-10-06 15:42:53.838515+00	21.1000003814697266	1
1221	2020-10-06 15:42:54.838364+00	56.7999992370605469	2
1222	2020-10-06 15:42:57.853578+00	21.2000007629394531	1
1223	2020-10-06 15:42:58.842531+00	56.9000015258789062	2
1224	2020-10-06 15:43:01.847251+00	21.2000007629394531	1
1225	2020-10-06 15:43:02.845239+00	56.9000015258789062	2
1226	2020-10-06 15:43:05.849074+00	21.2000007629394531	1
1227	2020-10-06 15:43:06.850601+00	56.9000015258789062	2
1228	2020-10-06 15:43:09.854714+00	21.2000007629394531	1
1229	2020-10-06 15:43:10.854831+00	56.9000015258789062	2
1230	2020-10-06 15:43:13.885759+00	21.2000007629394531	1
1231	2020-10-06 15:43:14.884205+00	56.9000015258789062	2
1232	2020-10-06 15:43:17.862494+00	21.2000007629394531	1
1233	2020-10-06 15:43:18.884454+00	56.9000015258789062	2
1234	2020-10-06 15:43:21.870492+00	21.2000007629394531	1
1235	2020-10-06 15:43:22.865677+00	56.9000015258789062	2
1236	2020-10-06 15:43:25.869753+00	21.2000007629394531	1
1237	2020-10-06 15:43:26.869708+00	56.7999992370605469	2
1238	2020-10-06 15:43:29.873625+00	21.3000011444091797	1
1239	2020-10-06 15:43:30.895644+00	56.9000015258789062	2
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 56, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, false);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 14, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 19, true);


--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_alarmtrigger_id_seq', 1, false);


--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_farm_id_seq', 1, false);


--
-- Name: agribiot_object_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_object_id_seq', 12, true);


--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_parcel_id_seq', 1, false);


--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_risedalarm_id_seq', 1, false);


--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_sensor_id_seq', 1, false);


--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_sensorobject_id_seq', 1, false);


--
-- Name: agribiot_value_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_value_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: agribiot_alarmtrigger agribiot_alarmtrigger_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger
    ADD CONSTRAINT agribiot_alarmtrigger_pkey PRIMARY KEY (id);


--
-- Name: agribiot_farm agribiot_farm_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_farm
    ADD CONSTRAINT agribiot_farm_pkey PRIMARY KEY (id);


--
-- Name: agribiot_object agribiot_object_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_pkey PRIMARY KEY (id);


--
-- Name: agribiot_parcel agribiot_parcel_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel
    ADD CONSTRAINT agribiot_parcel_pkey PRIMARY KEY (id);


--
-- Name: agribiot_risedalarm agribiot_risedalarm_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_pkey PRIMARY KEY (id);


--
-- Name: agribiot_sensor agribiot_sensor_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor
    ADD CONSTRAINT agribiot_sensor_pkey PRIMARY KEY (id);


--
-- Name: agribiot_sensorobject agribiot_sensorobject_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_pkey PRIMARY KEY (id);


--
-- Name: agribiot_value agribiot_value_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value
    ADD CONSTRAINT agribiot_value_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: agribiot_alarmtrigger_probe_id_854b602b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmtrigger_probe_id_854b602b ON public.agribiot_alarmtrigger USING btree (probe_id);


--
-- Name: agribiot_object_parcel_id_adb1022e; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_parcel_id_adb1022e ON public.agribiot_object USING btree (parcel_id);


--
-- Name: agribiot_object_position_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_position_id ON public.agribiot_object USING gist ("position");


--
-- Name: agribiot_parcel_farm_id_b5346027; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_parcel_farm_id_b5346027 ON public.agribiot_parcel USING btree (farm_id);


--
-- Name: agribiot_parcel_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_parcel_geometry_id ON public.agribiot_parcel USING gist (geometry);


--
-- Name: agribiot_risedalarm_trigger_65bf5420; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_risedalarm_trigger_65bf5420 ON public.agribiot_risedalarm USING btree (trigger);


--
-- Name: agribiot_risedalarm_value_71960421; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_risedalarm_value_71960421 ON public.agribiot_risedalarm USING btree (value);


--
-- Name: agribiot_sensorobject_object_id_0d702adb; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensorobject_object_id_0d702adb ON public.agribiot_sensorobject USING btree (object_id);


--
-- Name: agribiot_sensorobject_sensor_id_36d39b3c; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensorobject_sensor_id_36d39b3c ON public.agribiot_sensorobject USING btree (sensor_id);


--
-- Name: agribiot_value_sensor_object_id_a7beb118; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_value_sensor_object_id_a7beb118 ON public.agribiot_value USING btree (sensor_object_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_alarmtrigger agribiot_alarmtrigge_probe_id_854b602b_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger
    ADD CONSTRAINT agribiot_alarmtrigge_probe_id_854b602b_fk_agribiot_ FOREIGN KEY (probe_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_object agribiot_object_parcel_id_adb1022e_fk_agribiot_parcel_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_parcel_id_adb1022e_fk_agribiot_parcel_id FOREIGN KEY (parcel_id) REFERENCES public.agribiot_parcel(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_parcel agribiot_parcel_farm_id_b5346027_fk_agribiot_farm_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel
    ADD CONSTRAINT agribiot_parcel_farm_id_b5346027_fk_agribiot_farm_id FOREIGN KEY (farm_id) REFERENCES public.agribiot_farm(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_risedalarm agribiot_risedalarm_trigger_65bf5420_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_trigger_65bf5420_fk_agribiot_ FOREIGN KEY (trigger) REFERENCES public.agribiot_alarmtrigger(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_risedalarm agribiot_risedalarm_value_71960421_fk_agribiot_value_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_value_71960421_fk_agribiot_value_id FOREIGN KEY (value) REFERENCES public.agribiot_value(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_sensorobject agribiot_sensorobject_object_id_0d702adb_fk_agribiot_object_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_object_id_0d702adb_fk_agribiot_object_id FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_sensorobject agribiot_sensorobject_sensor_id_36d39b3c_fk_agribiot_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_sensor_id_36d39b3c_fk_agribiot_sensor_id FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_value agribiot_value_sensor_object_id_a7beb118_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value
    ADD CONSTRAINT agribiot_value_sensor_object_id_a7beb118_fk_agribiot_ FOREIGN KEY (sensor_object_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

