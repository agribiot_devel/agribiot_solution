--
-- PostgreSQL database dump
--

-- Dumped from database version 11.9 (Debian 11.9-0+deb10u1)
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.agribiot_value DROP CONSTRAINT agribiot_value_sensor_object_id_a7beb118_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_sensor_id_36d39b3c_fk_agribiot_sensor_id;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_object_id_0d702adb_fk_agribiot_object_id;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_value_71960421_fk_agribiot_value_id;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_trigger_65bf5420_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_parcel DROP CONSTRAINT agribiot_parcel_farm_id_b5346027_fk_agribiot_farm_id;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_parcel_id_adb1022e_fk_agribiot_parcel_id;
ALTER TABLE ONLY public.agribiot_alarmtrigger DROP CONSTRAINT agribiot_alarmtrigge_probe_id_854b602b_fk_agribiot_;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
DROP INDEX public.agribiot_value_sensor_object_id_a7beb118;
DROP INDEX public.agribiot_sensorobject_sensor_id_36d39b3c;
DROP INDEX public.agribiot_sensorobject_object_id_0d702adb;
DROP INDEX public.agribiot_risedalarm_value_71960421;
DROP INDEX public.agribiot_risedalarm_trigger_65bf5420;
DROP INDEX public.agribiot_parcel_geometry_id;
DROP INDEX public.agribiot_parcel_farm_id_b5346027;
DROP INDEX public.agribiot_object_position_id;
DROP INDEX public.agribiot_object_parcel_id_adb1022e;
DROP INDEX public.agribiot_alarmtrigger_probe_id_854b602b;
DROP INDEX public.django_session_session_key_c0390e0f_like;
DROP INDEX public.django_session_expire_date_a5c62663;
DROP INDEX public.django_admin_log_user_id_c564eba6;
DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
DROP INDEX public.auth_user_username_6821ab7c_like;
DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
DROP INDEX public.auth_user_groups_group_id_97559544;
DROP INDEX public.auth_permission_content_type_id_2f476e4b;
DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
DROP INDEX public.auth_group_name_a6ea08ec_like;
ALTER TABLE ONLY public.agribiot_value DROP CONSTRAINT agribiot_value_pkey;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_pkey;
ALTER TABLE ONLY public.agribiot_sensor DROP CONSTRAINT agribiot_sensor_pkey;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_pkey;
ALTER TABLE ONLY public.agribiot_parcel DROP CONSTRAINT agribiot_parcel_pkey;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_pkey;
ALTER TABLE ONLY public.agribiot_farm DROP CONSTRAINT agribiot_farm_pkey;
ALTER TABLE ONLY public.agribiot_alarmtrigger DROP CONSTRAINT agribiot_alarmtrigger_pkey;
ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
ALTER TABLE public.agribiot_value ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_sensorobject ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_sensor ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_risedalarm ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_parcel ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_object ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_farm ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_alarmtrigger ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.agribiot_value_id_seq;
DROP TABLE public.agribiot_value;
DROP SEQUENCE public.agribiot_sensorobject_id_seq;
DROP TABLE public.agribiot_sensorobject;
DROP SEQUENCE public.agribiot_sensor_id_seq;
DROP TABLE public.agribiot_sensor;
DROP SEQUENCE public.agribiot_risedalarm_id_seq;
DROP TABLE public.agribiot_risedalarm;
DROP SEQUENCE public.agribiot_parcel_id_seq;
DROP TABLE public.agribiot_parcel;
DROP SEQUENCE public.agribiot_object_id_seq;
DROP TABLE public.agribiot_object;
DROP SEQUENCE public.agribiot_farm_id_seq;
DROP TABLE public.agribiot_farm;
DROP SEQUENCE public.agribiot_alarmtrigger_id_seq;
DROP TABLE public.agribiot_alarmtrigger;
DROP TABLE public.django_session;
DROP SEQUENCE public.django_migrations_id_seq;
DROP TABLE public.django_migrations;
DROP SEQUENCE public.django_content_type_id_seq;
DROP TABLE public.django_content_type;
DROP SEQUENCE public.django_admin_log_id_seq;
DROP TABLE public.django_admin_log;
DROP SEQUENCE public.auth_user_user_permissions_id_seq;
DROP TABLE public.auth_user_user_permissions;
DROP SEQUENCE public.auth_user_id_seq;
DROP SEQUENCE public.auth_user_groups_id_seq;
DROP TABLE public.auth_user_groups;
DROP TABLE public.auth_user;
DROP SEQUENCE public.auth_permission_id_seq;
DROP TABLE public.auth_permission;
DROP SEQUENCE public.auth_group_permissions_id_seq;
DROP TABLE public.auth_group_permissions;
DROP SEQUENCE public.auth_group_id_seq;
DROP TABLE public.auth_group;
DROP EXTENSION postgis;
--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO agribiot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO agribiot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO agribiot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO agribiot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO agribiot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO agribiot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO agribiot;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO agribiot;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO agribiot;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO agribiot;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO agribiot;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO agribiot;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO agribiot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO agribiot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO agribiot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO agribiot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO agribiot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO agribiot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_alarmtrigger (
    id integer NOT NULL,
    trigger_type character varying(100) NOT NULL,
    trigger_value double precision NOT NULL,
    probe_id integer
);


ALTER TABLE public.agribiot_alarmtrigger OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_alarmtrigger_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_alarmtrigger_id_seq OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_alarmtrigger_id_seq OWNED BY public.agribiot_alarmtrigger.id;


--
-- Name: agribiot_farm; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_farm (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_farm OWNER TO agribiot;

--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_farm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_farm_id_seq OWNER TO agribiot;

--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_farm_id_seq OWNED BY public.agribiot_farm.id;


--
-- Name: agribiot_object; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_object (
    id integer NOT NULL,
    "position" public.geography(Point,4326) NOT NULL,
    parcel_id integer NOT NULL
);


ALTER TABLE public.agribiot_object OWNER TO agribiot;

--
-- Name: agribiot_object_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_object_id_seq OWNER TO agribiot;

--
-- Name: agribiot_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_object_id_seq OWNED BY public.agribiot_object.id;


--
-- Name: agribiot_parcel; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_parcel (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    geometry public.geography(Polygon,4326) NOT NULL,
    farm_id integer NOT NULL
);


ALTER TABLE public.agribiot_parcel OWNER TO agribiot;

--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_parcel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_parcel_id_seq OWNER TO agribiot;

--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_parcel_id_seq OWNED BY public.agribiot_parcel.id;


--
-- Name: agribiot_risedalarm; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_risedalarm (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    trigger integer NOT NULL,
    value integer NOT NULL
);


ALTER TABLE public.agribiot_risedalarm OWNER TO agribiot;

--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_risedalarm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_risedalarm_id_seq OWNER TO agribiot;

--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_risedalarm_id_seq OWNED BY public.agribiot_risedalarm.id;


--
-- Name: agribiot_sensor; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_sensor (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    unit character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_sensor OWNER TO agribiot;

--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_sensor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_sensor_id_seq OWNER TO agribiot;

--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_sensor_id_seq OWNED BY public.agribiot_sensor.id;


--
-- Name: agribiot_sensorobject; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_sensorobject (
    id integer NOT NULL,
    object_id integer NOT NULL,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_sensorobject OWNER TO agribiot;

--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_sensorobject_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_sensorobject_id_seq OWNER TO agribiot;

--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_sensorobject_id_seq OWNED BY public.agribiot_sensorobject.id;


--
-- Name: agribiot_value; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_value (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    value double precision NOT NULL,
    sensor_object_id integer NOT NULL
);


ALTER TABLE public.agribiot_value OWNER TO agribiot;

--
-- Name: agribiot_value_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_value_id_seq OWNER TO agribiot;

--
-- Name: agribiot_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_value_id_seq OWNED BY public.agribiot_value.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: agribiot_alarmtrigger id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger ALTER COLUMN id SET DEFAULT nextval('public.agribiot_alarmtrigger_id_seq'::regclass);


--
-- Name: agribiot_farm id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_farm ALTER COLUMN id SET DEFAULT nextval('public.agribiot_farm_id_seq'::regclass);


--
-- Name: agribiot_object id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object ALTER COLUMN id SET DEFAULT nextval('public.agribiot_object_id_seq'::regclass);


--
-- Name: agribiot_parcel id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel ALTER COLUMN id SET DEFAULT nextval('public.agribiot_parcel_id_seq'::regclass);


--
-- Name: agribiot_risedalarm id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm ALTER COLUMN id SET DEFAULT nextval('public.agribiot_risedalarm_id_seq'::regclass);


--
-- Name: agribiot_sensor id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor ALTER COLUMN id SET DEFAULT nextval('public.agribiot_sensor_id_seq'::regclass);


--
-- Name: agribiot_sensorobject id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject ALTER COLUMN id SET DEFAULT nextval('public.agribiot_sensorobject_id_seq'::regclass);


--
-- Name: agribiot_value id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value ALTER COLUMN id SET DEFAULT nextval('public.agribiot_value_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add alarm trigger	7	add_alarmtrigger
26	Can change alarm trigger	7	change_alarmtrigger
27	Can delete alarm trigger	7	delete_alarmtrigger
28	Can view alarm trigger	7	view_alarmtrigger
29	Can add farm	8	add_farm
30	Can change farm	8	change_farm
31	Can delete farm	8	delete_farm
32	Can view farm	8	view_farm
33	Can add object	9	add_object
34	Can change object	9	change_object
35	Can delete object	9	delete_object
36	Can view object	9	view_object
37	Can add sensor	10	add_sensor
38	Can change sensor	10	change_sensor
39	Can delete sensor	10	delete_sensor
40	Can view sensor	10	view_sensor
41	Can add sensor object	11	add_sensorobject
42	Can change sensor object	11	change_sensorobject
43	Can delete sensor object	11	delete_sensorobject
44	Can view sensor object	11	view_sensorobject
45	Can add value	12	add_value
46	Can change value	12	change_value
47	Can delete value	12	delete_value
48	Can view value	12	view_value
49	Can add rised alarm	13	add_risedalarm
50	Can change rised alarm	13	change_risedalarm
51	Can delete rised alarm	13	delete_risedalarm
52	Can view rised alarm	13	view_risedalarm
53	Can add parcel	14	add_parcel
54	Can change parcel	14	change_parcel
55	Can delete parcel	14	delete_parcel
56	Can view parcel	14	view_parcel
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	agribiot	alarmtrigger
8	agribiot	farm
9	agribiot	object
10	agribiot	sensor
11	agribiot	sensorobject
12	agribiot	value
13	agribiot	risedalarm
14	agribiot	parcel
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2020-10-08 12:44:22.105891+00
2	auth	0001_initial	2020-10-08 12:44:22.141778+00
3	admin	0001_initial	2020-10-08 12:44:22.172896+00
4	admin	0002_logentry_remove_auto_add	2020-10-08 12:44:22.183221+00
5	admin	0003_logentry_add_action_flag_choices	2020-10-08 12:44:22.195651+00
6	contenttypes	0002_remove_content_type_name	2020-10-08 12:44:22.217967+00
7	auth	0002_alter_permission_name_max_length	2020-10-08 12:44:22.225895+00
8	auth	0003_alter_user_email_max_length	2020-10-08 12:44:22.239482+00
9	auth	0004_alter_user_username_opts	2020-10-08 12:44:22.248648+00
10	auth	0005_alter_user_last_login_null	2020-10-08 12:44:22.262217+00
11	auth	0006_require_contenttypes_0002	2020-10-08 12:44:22.264867+00
12	auth	0007_alter_validators_add_error_messages	2020-10-08 12:44:22.278172+00
13	auth	0008_alter_user_username_max_length	2020-10-08 12:44:22.295305+00
14	auth	0009_alter_user_last_name_max_length	2020-10-08 12:44:22.305095+00
15	auth	0010_alter_group_name_max_length	2020-10-08 12:44:22.315463+00
16	auth	0011_update_proxy_permissions	2020-10-08 12:44:22.32937+00
17	auth	0012_alter_user_first_name_max_length	2020-10-08 12:44:22.338121+00
18	sessions	0001_initial	2020-10-08 12:44:22.343605+00
19	agribiot	0001_initial	2020-10-08 12:44:22.437037+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: agribiot_alarmtrigger; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_alarmtrigger (id, trigger_type, trigger_value, probe_id) FROM stdin;
\.


--
-- Data for Name: agribiot_farm; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_farm (id, name) FROM stdin;
1	Château Guiraud
\.


--
-- Data for Name: agribiot_object; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_object (id, "position", parcel_id) FROM stdin;
1	0101000020E6100000FFFFFFFF831BD5BFB2AB88B383444640	1
2	0101000020E61000000000000076C0D4BF80657B1F63444640	1
3	0101000020E6100000000000009519D5BF509B56331C444640	1
4	0101000020E61000000000000094CFD4BFD90F6F56EE434640	1
5	0101000020E6100000000000008CF5D4BFF96AE24940444640	1
\.


--
-- Data for Name: agribiot_parcel; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_parcel (id, name, geometry, farm_id) FROM stdin;
1	335040000D0474	0103000020E610000001000000100000008C42DCE6D720D5BFFFBDCA9019444640A085A9D2BB1DD5BFD6BA1E9617444640475C5BD3171DD5BFE1BEC40E194446400F8EA3833F0DD5BFCFBAEB110F4446405E9AC706770CD5BF52FB63100E444640AD2DE175A2C9D4BF449957BFE3434640C4341D4B69C7D4BF03D8DBC1E343464040FCFCF7E0B5D4BFFF04172B6A4446408C570F4EFA31D5BF8FDDAA9097444640512BF125F936D5BFD08140C28B44464031DB04CE0825D5BFCF166B1382444640FCD3FCE71F33D5BF2A7E422C51444640DF5740FC5731D5BFBFFB9930504446409CF232D47231D5BF8AE8D7D64F444640A10040040B16D5BFA47B8CA83F4446408C42DCE6D720D5BFFFBDCA9019444640	1
2	335040000D0458	0103000020E6100000010000000A000000512BF125F936D5BFD08140C28B4446406F788BD18437D5BF952710768A44464044FAEDEBC039D5BFCD04C3B986444640C1B6346DB53DD5BF6B99B10D814446407C7C4276DE46D5BFD2DD1A7D71444640E855F88E754CD5BF6A50340F60444640F4CDECA98B4AD5BF291F27F15E444640FCD3FCE71F33D5BF2A7E422C5144464031DB04CE0825D5BFCF166B1382444640512BF125F936D5BFD08140C28B444640	1
3	335040000D0486	0103000020E61000000100000009000000CAD875148CEFD4BF16BD5301F743464031BF89D7ABFED4BF2FA52E19C7434640E384AEE9E607D5BFC7287403AA434640424937781508D5BFD49AE61DA7434640D71AA5A6B805D5BFFE8F5DB3A6434640C4D6C79E98D0D4BF9E78735D9D43464098816F3F85C8D4BFE395DA41DB4346406F92301D84CAD4BF88C89F5EDF434640CAD875148CEFD4BF16BD5301F7434640	1
4	335040000D0478	0103000020E61000000100000008000000A5373701E11ED5BF59709A99F44346401BB39190ED21D5BF5557F43DD94346406D7D47437C16D5BFA9B7ABFCD743464031BF89D7ABFED4BF2FA52E19C7434640CAD875148CEFD4BF16BD5301F74346407AAA436E861BD5BF1ABC541113444640BC8564B7851DD5BF833A408B00444640A5373701E11ED5BF59709A99F4434640	1
5	335040000D0453	0103000020E6100000010000000C0000001BB39190ED21D5BF5557F43DD9434640C72DE6E78626D5BF71EA5E82AE434640C7D8092FC129D5BF7A3DF3CD914346401B9A571A9D18D5BFEC408CC6904346402EBF78AB530AD5BFD5D6E3198F43464037E911FEA009D5BFF8C66BB98E4346408AA251CB2008D5BF28B3E66CA6434640424937781508D5BFD49AE61DA7434640E384AEE9E607D5BFC7287403AA43464031BF89D7ABFED4BF2FA52E19C74346406D7D47437C16D5BFA9B7ABFCD74346401BB39190ED21D5BF5557F43DD9434640	1
6	335040000D0491	0103000020E610000001000000210000003242D36DE495D5BF3B0CF7DBE84346409A006839758AD5BFA31122CFE44346403311DB824A81D5BF4A141049E3434640ADF13E332D56D5BFFDAA121BE24346401BB39190ED21D5BF5557F43DD9434640A5373701E11ED5BF59709A99F44346400A48FB1F602DD5BF344690EFF74346403F840200112CD5BF0D2ABBAAFD434640780E65A88A29D5BF6ED113E0054446403440B3356C25D5BF043BFE0B044446407A64BD625724D5BFB8C60CF90C4446401DED139BEA24D5BFE62ACC310F444640371CF1BFF025D5BFF747CE78114446403C7F7FEFDB35D5BF9934A1A41B44464064F72EEFBB33D5BF0C4D237722444640EB803518343ED5BF326C393C2944464089997D1EA33CD5BFC7B546A92944464022646A5C493CD5BF319413ED2A44464055CF937C363CD5BF6DACC43C2B444640B421FFCC203ED5BF7E5935632C4446403E271829EF3ED5BF80C1C99129444640CEA1B19BBE43D5BF2556EB692C444640FF52509B8246D5BF7E50BC6F21444640EDC1FFB16B56D5BFA9DE1AD82A444640B8DB3F602F5ED5BF2F54596F2F44464052C8DF073161D5BFE1B0D936314446404B5CC7B8E262D5BF34F1B336324446406F0ED76A0F7BD5BFEDC8A2FA184446404C09771D609CD5BF33EDAC27044446402AD725F444A1D5BF1EBD8685FF43464063B7CF2A33A5D5BF7DEA58A5F4434640EC8FE67E3D95D5BFF175638BEE4346403242D36DE495D5BF3B0CF7DBE8434640	1
7	335040000D0452	0103000020E61000000100000009000000ADF13E332D56D5BFFDAA121BE2434640DF23511ECC5CD5BF0278B06AB54346408DB3E908E066D5BF1F155A31B7434640C3BCC799266CD5BFD7F1046795434640C7ECD1C03431D5BFB583B64192434640C7D8092FC129D5BF7A3DF3CD91434640C72DE6E78626D5BF71EA5E82AE4346401BB39190ED21D5BF5557F43DD9434640ADF13E332D56D5BFFDAA121BE2434640	1
8	335040000D0482	0103000020E61000000100000016000000A30392B06FA7D5BFD3009475EE4346400850F81164A9D5BF95B31C8DE843464085668D30FBABD5BFDFBF7971E24346405F888B9246BBD5BF543FCAE3C4434640FAD51C2098A3D5BF20DC5328C14346404573AE72B29ED5BFD9017C5CC04346407BB9F4D48F72D5BF30D63730B9434640707A17EFC76DD5BF66796869B84346408DB3E908E066D5BF1F155A31B7434640DF23511ECC5CD5BF0278B06AB5434640ADF13E332D56D5BFFDAA121BE24346403311DB824A81D5BF4A141049E34346409A006839758AD5BFA31122CFE44346403242D36DE495D5BF3B0CF7DBE8434640939A2CA4B297D5BF7FA88F1BD9434640DD674B0CB89DD5BF8A50114CDA4346401521D0F46D9CD5BF216D8896E143464007CE1951DA9BD5BF85B6F704E4434640B01B5B74579BD5BF90FC1C7AE643464079D4F3C9E59AD5BF6BBEA5F7E8434640D8B04BF9B59AD5BF5F24592CEA434640A30392B06FA7D5BFD3009475EE434640	1
9	335040000D0475	0103000020E610000001000000050000008C42DCE6D720D5BFFFBDCA901944464022646A5C493CD5BF319413ED2A44464089997D1EA33CD5BFC7B546A92944464071B9B0242F21D5BFCA969B5E184446408C42DCE6D720D5BFFFBDCA9019444640	1
10	335040000D0484	0103000020E6100000010000000E000000D19A7A38DCA2D5BFEBF18CC756444640D7D6F445E7A1D5BFD5EDEC2B0F4446401CCAF55BE097D5BFD41BFF4F0F444640F3D544550298D5BF422C51AC094446402E5ADB6F487ED5BF35BD1F121944464029441BDBC67AD5BF4BF37D271C4446403D128A085A5CD5BFA0408E9C3B444640932694748357D5BFEA91ABFD424446409AC1CE030E57D5BFFB9A406C44444640726C3D433866D5BF380A5A924D444640A83D80A03770D5BFB9944E7F51444640DF933DF83F76D5BF53C9A596524446408D11E4FB3D8CD5BFBEC1172653444640D19A7A38DCA2D5BFEBF18CC756444640	1
11	335040000D0455	0103000020E610000001000000150000000AFA66F6D445D5BFE53EDE509D44464068E503A7E356D5BF65C39ACAA2444640AA66D652405AD5BF0B163DA6A44446405BC4C1EFF068D5BFC619C39CA04446409E6CB9E98571D5BF987384679D4446400562235A3C86D5BF1DA386808D444640CAE42F88A386D5BF1AF5B5D37B4446403C6876DD5B91D5BF77A9C76C7F4446407284B12A6794D5BFD7016B3068444640D982948E28A3D5BF2D211FF46C444640D19A7A38DCA2D5BFEBF18CC7564446408D11E4FB3D8CD5BFBEC1172653444640DF933DF83F76D5BF53C9A59652444640A83D80A03770D5BFB9944E7F51444640726C3D433866D5BF380A5A924D4446409AC1CE030E57D5BFFB9A406C444446409187742E7B48D5BFBA53951172444640B93C31467F43D5BF2761F07A7A444640AC0F351B753ED5BF5B1E108D93444640A23A67559547D5BFCBE1EE07974446400AFA66F6D445D5BFE53EDE509D444640	1
12	335040000D0472	0103000020E610000001000000240000007E6830575062D5BF6B48DC63E9444640F94E2734A465D5BFB1A371A8DF444640E7A90EB9196ED5BF02D2A34EE54446401480DA03087AD5BF3599F1B6D244464064DA4823B083D5BFDD22D51CC5444640E7CE02A3818ED5BFB2F6D26FBA4446406E5D7BB0D69AD5BF675F79909E4446407F1E59AFD895D5BF377980DD9F4446409662A29CC390D5BF16C330BB824446407B7F283D7892D5BF9BADBCE47F444640B7DC4F7C6B96D5BF3D55CF937C4446402B4EB51666A1D5BF24D0059A74444640D982948E28A3D5BF2D211FF46C4446407284B12A6794D5BFD7016B30684446403C6876DD5B91D5BF77A9C76C7F444640CAE42F88A386D5BF1AF5B5D37B4446400562235A3C86D5BF1DA386808D4446409E6CB9E98571D5BF987384679D4446405BC4C1EFF068D5BFC619C39CA0444640AA66D652405AD5BF0B163DA6A444464068E503A7E356D5BF65C39ACAA24446400AFA66F6D445D5BFE53EDE509D444640FF0B5FBAA445D5BF0943D3C89D444640954FEA268C41D5BF10C4C3C59B4446403C1CB85E2E3DD5BFC95FB58D9A444640351D4B69473BD5BFD58338C599444640657330F6B935D5BF0628B27B9744464055BBCBEAC234D5BF363CBD5296444640F8B53F619A33D5BF70BEC74F99444640FC7DB559AB2CD5BF48E17A14AE4446408EB2D9ECA32ED5BF3C61D394AE4446404A3A6F08FA30D5BF0C957F2DAF444640E08849134A3AD5BFCFE6278BB144464090977F3EDB34D5BFFC1C1F2DCE4446405262D7F6764BD5BF8473FC06DC4446407E6830575062D5BF6B48DC63E9444640	1
13	335040000D0494	0103000020E61000000100000016000000FCD3FCE71F33D5BF2A7E422C51444640F4CDECA98B4AD5BF291F27F15E44464041DE0610994DD5BFDABAE53455444640BD1E4C8A8F4FD5BF2724E362564446400E40B4A04154D5BF9AB4A9BA474446407DA4D299C555D5BFA8D02F0043444640E47F97BE7C57D5BF39CBD1883E4446400EA0DFF76F5ED5BFAE8A2606374446404B5CC7B8E262D5BF34F1B3363244464052C8DF073161D5BFE1B0D93631444640B8DB3F602F5ED5BF2F54596F2F444640EDC1FFB16B56D5BFA9DE1AD82A444640FF52509B8246D5BF7E50BC6F21444640CEA1B19BBE43D5BF2556EB692C4446403E271829EF3ED5BF80C1C99129444640B421FFCC203ED5BF7E5935632C44464055CF937C363CD5BF6DACC43C2B44464003DD4DA6C038D5BFC6CCF401374446408048BF7D1D38D5BF3CF71E2E394446409CF232D47231D5BF8AE8D7D64F444640DF5740FC5731D5BFBFFB993050444640FCD3FCE71F33D5BF2A7E422C51444640	1
14	335040000D0476	0103000020E610000001000000080000008C42DCE6D720D5BFFFBDCA9019444640A10040040B16D5BFA47B8CA83F4446409CF232D47231D5BF8AE8D7D64F4446408048BF7D1D38D5BF3CF71E2E3944464003DD4DA6C038D5BFC6CCF4013744464055CF937C363CD5BF6DACC43C2B44464022646A5C493CD5BF319413ED2A4446408C42DCE6D720D5BFFFBDCA9019444640	1
\.


--
-- Data for Name: agribiot_risedalarm; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_risedalarm (id, created, trigger, value) FROM stdin;
\.


--
-- Data for Name: agribiot_sensor; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_sensor (id, name, unit) FROM stdin;
\.


--
-- Data for Name: agribiot_sensorobject; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_sensorobject (id, object_id, sensor_id) FROM stdin;
\.


--
-- Data for Name: agribiot_value; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_value (id, created, value, sensor_object_id) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 56, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, false);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 14, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 19, true);


--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_alarmtrigger_id_seq', 1, false);


--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_farm_id_seq', 1, false);


--
-- Name: agribiot_object_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_object_id_seq', 12, true);


--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_parcel_id_seq', 1, false);


--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_risedalarm_id_seq', 1, false);


--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_sensor_id_seq', 1, false);


--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_sensorobject_id_seq', 1, false);


--
-- Name: agribiot_value_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_value_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: agribiot_alarmtrigger agribiot_alarmtrigger_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger
    ADD CONSTRAINT agribiot_alarmtrigger_pkey PRIMARY KEY (id);


--
-- Name: agribiot_farm agribiot_farm_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_farm
    ADD CONSTRAINT agribiot_farm_pkey PRIMARY KEY (id);


--
-- Name: agribiot_object agribiot_object_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_pkey PRIMARY KEY (id);


--
-- Name: agribiot_parcel agribiot_parcel_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel
    ADD CONSTRAINT agribiot_parcel_pkey PRIMARY KEY (id);


--
-- Name: agribiot_risedalarm agribiot_risedalarm_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_pkey PRIMARY KEY (id);


--
-- Name: agribiot_sensor agribiot_sensor_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor
    ADD CONSTRAINT agribiot_sensor_pkey PRIMARY KEY (id);


--
-- Name: agribiot_sensorobject agribiot_sensorobject_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_pkey PRIMARY KEY (id);


--
-- Name: agribiot_value agribiot_value_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value
    ADD CONSTRAINT agribiot_value_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: agribiot_alarmtrigger_probe_id_854b602b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmtrigger_probe_id_854b602b ON public.agribiot_alarmtrigger USING btree (probe_id);


--
-- Name: agribiot_object_parcel_id_adb1022e; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_parcel_id_adb1022e ON public.agribiot_object USING btree (parcel_id);


--
-- Name: agribiot_object_position_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_position_id ON public.agribiot_object USING gist ("position");


--
-- Name: agribiot_parcel_farm_id_b5346027; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_parcel_farm_id_b5346027 ON public.agribiot_parcel USING btree (farm_id);


--
-- Name: agribiot_parcel_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_parcel_geometry_id ON public.agribiot_parcel USING gist (geometry);


--
-- Name: agribiot_risedalarm_trigger_65bf5420; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_risedalarm_trigger_65bf5420 ON public.agribiot_risedalarm USING btree (trigger);


--
-- Name: agribiot_risedalarm_value_71960421; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_risedalarm_value_71960421 ON public.agribiot_risedalarm USING btree (value);


--
-- Name: agribiot_sensorobject_object_id_0d702adb; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensorobject_object_id_0d702adb ON public.agribiot_sensorobject USING btree (object_id);


--
-- Name: agribiot_sensorobject_sensor_id_36d39b3c; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensorobject_sensor_id_36d39b3c ON public.agribiot_sensorobject USING btree (sensor_id);


--
-- Name: agribiot_value_sensor_object_id_a7beb118; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_value_sensor_object_id_a7beb118 ON public.agribiot_value USING btree (sensor_object_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_alarmtrigger agribiot_alarmtrigge_probe_id_854b602b_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger
    ADD CONSTRAINT agribiot_alarmtrigge_probe_id_854b602b_fk_agribiot_ FOREIGN KEY (probe_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_object agribiot_object_parcel_id_adb1022e_fk_agribiot_parcel_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_parcel_id_adb1022e_fk_agribiot_parcel_id FOREIGN KEY (parcel_id) REFERENCES public.agribiot_parcel(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_parcel agribiot_parcel_farm_id_b5346027_fk_agribiot_farm_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel
    ADD CONSTRAINT agribiot_parcel_farm_id_b5346027_fk_agribiot_farm_id FOREIGN KEY (farm_id) REFERENCES public.agribiot_farm(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_risedalarm agribiot_risedalarm_trigger_65bf5420_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_trigger_65bf5420_fk_agribiot_ FOREIGN KEY (trigger) REFERENCES public.agribiot_alarmtrigger(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_risedalarm agribiot_risedalarm_value_71960421_fk_agribiot_value_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_value_71960421_fk_agribiot_value_id FOREIGN KEY (value) REFERENCES public.agribiot_value(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_sensorobject agribiot_sensorobject_object_id_0d702adb_fk_agribiot_object_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_object_id_0d702adb_fk_agribiot_object_id FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_sensorobject agribiot_sensorobject_sensor_id_36d39b3c_fk_agribiot_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_sensor_id_36d39b3c_fk_agribiot_sensor_id FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_value agribiot_value_sensor_object_id_a7beb118_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value
    ADD CONSTRAINT agribiot_value_sensor_object_id_a7beb118_fk_agribiot_ FOREIGN KEY (sensor_object_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

