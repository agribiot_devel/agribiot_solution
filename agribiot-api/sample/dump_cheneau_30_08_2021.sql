--
-- PostgreSQL database dump
--

-- Dumped from database version 11.12 (Debian 11.12-0+deb10u1)
-- Dumped by pg_dump version 11.12 (Debian 11.12-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectp_permission_id_71807bfc_fk_auth_perm;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectp_content_type_id_2e892405_fk_django_co;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobject_permission_id_36572738_fk_auth_perm;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobject_group_id_4bbbfb62_fk_auth_grou;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobject_content_type_id_7ade36b8_fk_django_co;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
ALTER TABLE ONLY public.agribiot_value DROP CONSTRAINT agribiot_value_sensor_object_id_97097252_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_sensor_id_4a836e2c_fk_agribiot_sensor_id;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_object_id_fcf9bc39_fk_agribiot_object_id;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_value_id_49e5444e_fk_agribiot_value_id;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_alarm_trigger_id_d6008608_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_rfiddeviceuser DROP CONSTRAINT agribiot_rfiddeviceuser_user_ptr_id_6733a39c_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteacces_sensorobject_id_3a831118_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteacces_sensor_id_a56784c0_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteacces_remoteaccess_id_b12c6c90_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteacces_remoteaccess_id_a33d91e7_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteacces_remoteaccess_id_382e8a77_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteacces_object_id_b8286ddc_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_remoteaccess DROP CONSTRAINT agribiot_remoteacces_foreign_users_207df1dc_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_parcel DROP CONSTRAINT agribiot_parcel_farm_id_63c02727_fk_agribiot_farm_id;
ALTER TABLE ONLY public.agribiot_operation DROP CONSTRAINT agribiot_operation_sensor_object_id_cb2c3e2f_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_operation_images DROP CONSTRAINT agribiot_operation_i_operationimage_id_73b154ff_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_operation_images DROP CONSTRAINT agribiot_operation_i_operation_id_32f4f572_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_operation DROP CONSTRAINT agribiot_operation_annotation_template__af355ffe_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_objectuser DROP CONSTRAINT agribiot_objectuser_user_ptr_id_d9b15f29_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_user_id_69163bcb_fk_agribiot_;
ALTER TABLE ONLY public.agribiot_gatewayuser DROP CONSTRAINT agribiot_gatewayuser_user_ptr_id_c1b9d969_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_foreignfarmuser DROP CONSTRAINT agribiot_foreignfarmuser_user_ptr_id_d62330a8_fk_auth_user_id;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelement_sensor_id_ab7c0bfe_fk_agribiot_sensor_id;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelement_object_id_c292df7b_fk_agribiot_object_id;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelemen_alarm_trigger_id_9ba193db_fk_agribiot_;
DROP INDEX public.guardian_userobjectpermission_user_id_d5c1e964;
DROP INDEX public.guardian_userobjectpermission_permission_id_71807bfc;
DROP INDEX public.guardian_userobjectpermission_content_type_id_2e892405;
DROP INDEX public.guardian_us_content_179ed2_idx;
DROP INDEX public.guardian_groupobjectpermission_permission_id_36572738;
DROP INDEX public.guardian_groupobjectpermission_group_id_4bbbfb62;
DROP INDEX public.guardian_groupobjectpermission_content_type_id_7ade36b8;
DROP INDEX public.guardian_gr_content_ae6aec_idx;
DROP INDEX public.django_session_session_key_c0390e0f_like;
DROP INDEX public.django_session_expire_date_a5c62663;
DROP INDEX public.django_admin_log_user_id_c564eba6;
DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
DROP INDEX public.authtoken_token_key_10f0b77e_like;
DROP INDEX public.auth_user_username_6821ab7c_like;
DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
DROP INDEX public.auth_user_groups_group_id_97559544;
DROP INDEX public.auth_permission_content_type_id_2f476e4b;
DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
DROP INDEX public.auth_group_name_a6ea08ec_like;
DROP INDEX public.agribiot_value_sensor_object_id_97097252;
DROP INDEX public.agribiot_sensorobject_sensor_id_4a836e2c;
DROP INDEX public.agribiot_sensorobject_object_id_fcf9bc39;
DROP INDEX public.agribiot_sensor_name_56d3fb1c_like;
DROP INDEX public.agribiot_risedalarm_value_id_49e5444e;
DROP INDEX public.agribiot_risedalarm_alarm_trigger_id_d6008608;
DROP INDEX public.agribiot_remoteaccess_sensors_sensor_id_a56784c0;
DROP INDEX public.agribiot_remoteaccess_sensors_remoteaccess_id_a33d91e7;
DROP INDEX public.agribiot_remoteaccess_sensor_objects_sensorobject_id_3a831118;
DROP INDEX public.agribiot_remoteaccess_sensor_objects_remoteaccess_id_b12c6c90;
DROP INDEX public.agribiot_remoteaccess_selected_objects_remoteaccess_id_382e8a77;
DROP INDEX public.agribiot_remoteaccess_selected_objects_object_id_b8286ddc;
DROP INDEX public.agribiot_remoteaccess_geometry_id;
DROP INDEX public.agribiot_remoteaccess_foreign_users_207df1dc;
DROP INDEX public.agribiot_parcel_geometry_id;
DROP INDEX public.agribiot_parcel_farm_id_63c02727;
DROP INDEX public.agribiot_operation_sensor_object_id_cb2c3e2f;
DROP INDEX public.agribiot_operation_position_id;
DROP INDEX public.agribiot_operation_images_operationimage_id_73b154ff;
DROP INDEX public.agribiot_operation_images_operation_id_32f4f572;
DROP INDEX public.agribiot_operation_annotation_template_id_af355ffe;
DROP INDEX public.agribiot_object_uuid_a3a0d421_like;
DROP INDEX public.agribiot_object_user_id_69163bcb;
DROP INDEX public.agribiot_object_position_id;
DROP INDEX public.agribiot_annotationtemplate_description_bbb0c561_like;
DROP INDEX public.agribiot_alarmtrigger_name_2a404727_like;
DROP INDEX public.agribiot_alarmelement_sensor_id_ab7c0bfe;
DROP INDEX public.agribiot_alarmelement_object_id_c292df7b;
DROP INDEX public.agribiot_alarmelement_geometry_id;
DROP INDEX public.agribiot_alarmelement_alarm_trigger_id_9ba193db;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectpermission_pkey;
ALTER TABLE ONLY public.guardian_userobjectpermission DROP CONSTRAINT guardian_userobjectpermi_user_id_permission_id_ob_b0b3d2fc_uniq;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobjectpermission_pkey;
ALTER TABLE ONLY public.guardian_groupobjectpermission DROP CONSTRAINT guardian_groupobjectperm_group_id_permission_id_o_3f189f7c_uniq;
ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_user_id_key;
ALTER TABLE ONLY public.authtoken_token DROP CONSTRAINT authtoken_token_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
ALTER TABLE ONLY public.agribiot_value DROP CONSTRAINT agribiot_value_pkey;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_pkey;
ALTER TABLE ONLY public.agribiot_sensorobject DROP CONSTRAINT agribiot_sensorobject_object_id_sensor_id_a7d20fea_uniq;
ALTER TABLE ONLY public.agribiot_sensor DROP CONSTRAINT agribiot_sensor_pkey;
ALTER TABLE ONLY public.agribiot_sensor DROP CONSTRAINT agribiot_sensor_name_key;
ALTER TABLE ONLY public.agribiot_risedalarm DROP CONSTRAINT agribiot_risedalarm_pkey;
ALTER TABLE ONLY public.agribiot_rfiddeviceuser DROP CONSTRAINT agribiot_rfiddeviceuser_pkey;
ALTER TABLE ONLY public.agribiot_remoteinstance DROP CONSTRAINT agribiot_remoteinstance_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteaccess_sensors_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteaccess_sensor_objects_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteaccess_selected_objects_pkey;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects DROP CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensorob_81219c57_uniq;
ALTER TABLE ONLY public.agribiot_remoteaccess_sensors DROP CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensor_i_0da2fc55_uniq;
ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects DROP CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_object_i_6a4c0d2b_uniq;
ALTER TABLE ONLY public.agribiot_remoteaccess DROP CONSTRAINT agribiot_remoteaccess_pkey;
ALTER TABLE ONLY public.agribiot_parcel DROP CONSTRAINT agribiot_parcel_pkey;
ALTER TABLE ONLY public.agribiot_operationimage DROP CONSTRAINT agribiot_operationimage_pkey;
ALTER TABLE ONLY public.agribiot_operation DROP CONSTRAINT agribiot_operation_pkey;
ALTER TABLE ONLY public.agribiot_operation_images DROP CONSTRAINT agribiot_operation_images_pkey;
ALTER TABLE ONLY public.agribiot_operation_images DROP CONSTRAINT agribiot_operation_image_operation_id_operationim_3636faaf_uniq;
ALTER TABLE ONLY public.agribiot_objectuser DROP CONSTRAINT agribiot_objectuser_pkey;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_uuid_key;
ALTER TABLE ONLY public.agribiot_object DROP CONSTRAINT agribiot_object_pkey;
ALTER TABLE ONLY public.agribiot_gatewayuser DROP CONSTRAINT agribiot_gatewayuser_pkey;
ALTER TABLE ONLY public.agribiot_foreignfarmuser DROP CONSTRAINT agribiot_foreignfarmuser_pkey;
ALTER TABLE ONLY public.agribiot_farm DROP CONSTRAINT agribiot_farm_pkey;
ALTER TABLE ONLY public.agribiot_annotationtemplate DROP CONSTRAINT agribiot_annotationtemplate_pkey;
ALTER TABLE ONLY public.agribiot_annotationtemplate DROP CONSTRAINT agribiot_annotationtemplate_description_key;
ALTER TABLE ONLY public.agribiot_alarmtrigger DROP CONSTRAINT agribiot_alarmtrigger_pkey;
ALTER TABLE ONLY public.agribiot_alarmtrigger DROP CONSTRAINT agribiot_alarmtrigger_name_key;
ALTER TABLE ONLY public.agribiot_alarmelement DROP CONSTRAINT agribiot_alarmelement_pkey;
ALTER TABLE public.guardian_userobjectpermission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.guardian_groupobjectpermission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_value ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_sensorobject ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_sensor ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_risedalarm ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteinstance ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess_sensors ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess_sensor_objects ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess_selected_objects ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_remoteaccess ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_parcel ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_operationimage ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_operation_images ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_operation ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_object ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_farm ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_annotationtemplate ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_alarmtrigger ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agribiot_alarmelement ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.guardian_userobjectpermission_id_seq;
DROP TABLE public.guardian_userobjectpermission;
DROP SEQUENCE public.guardian_groupobjectpermission_id_seq;
DROP TABLE public.guardian_groupobjectpermission;
DROP TABLE public.django_session;
DROP SEQUENCE public.django_migrations_id_seq;
DROP TABLE public.django_migrations;
DROP SEQUENCE public.django_content_type_id_seq;
DROP TABLE public.django_content_type;
DROP SEQUENCE public.django_admin_log_id_seq;
DROP TABLE public.django_admin_log;
DROP TABLE public.authtoken_token;
DROP SEQUENCE public.auth_user_user_permissions_id_seq;
DROP TABLE public.auth_user_user_permissions;
DROP SEQUENCE public.auth_user_id_seq;
DROP SEQUENCE public.auth_user_groups_id_seq;
DROP TABLE public.auth_user_groups;
DROP TABLE public.auth_user;
DROP SEQUENCE public.auth_permission_id_seq;
DROP TABLE public.auth_permission;
DROP SEQUENCE public.auth_group_permissions_id_seq;
DROP TABLE public.auth_group_permissions;
DROP SEQUENCE public.auth_group_id_seq;
DROP TABLE public.auth_group;
DROP SEQUENCE public.agribiot_value_id_seq;
DROP TABLE public.agribiot_value;
DROP SEQUENCE public.agribiot_sensorobject_id_seq;
DROP TABLE public.agribiot_sensorobject;
DROP SEQUENCE public.agribiot_sensor_id_seq;
DROP TABLE public.agribiot_sensor;
DROP SEQUENCE public.agribiot_risedalarm_id_seq;
DROP TABLE public.agribiot_risedalarm;
DROP TABLE public.agribiot_rfiddeviceuser;
DROP SEQUENCE public.agribiot_remoteinstance_id_seq;
DROP TABLE public.agribiot_remoteinstance;
DROP SEQUENCE public.agribiot_remoteaccess_sensors_id_seq;
DROP TABLE public.agribiot_remoteaccess_sensors;
DROP SEQUENCE public.agribiot_remoteaccess_sensor_objects_id_seq;
DROP TABLE public.agribiot_remoteaccess_sensor_objects;
DROP SEQUENCE public.agribiot_remoteaccess_selected_objects_id_seq;
DROP TABLE public.agribiot_remoteaccess_selected_objects;
DROP SEQUENCE public.agribiot_remoteaccess_id_seq;
DROP TABLE public.agribiot_remoteaccess;
DROP SEQUENCE public.agribiot_parcel_id_seq;
DROP TABLE public.agribiot_parcel;
DROP SEQUENCE public.agribiot_operationimage_id_seq;
DROP TABLE public.agribiot_operationimage;
DROP SEQUENCE public.agribiot_operation_images_id_seq;
DROP TABLE public.agribiot_operation_images;
DROP SEQUENCE public.agribiot_operation_id_seq;
DROP TABLE public.agribiot_operation;
DROP TABLE public.agribiot_objectuser;
DROP SEQUENCE public.agribiot_object_id_seq;
DROP TABLE public.agribiot_object;
DROP TABLE public.agribiot_gatewayuser;
DROP TABLE public.agribiot_foreignfarmuser;
DROP SEQUENCE public.agribiot_farm_id_seq;
DROP TABLE public.agribiot_farm;
DROP SEQUENCE public.agribiot_annotationtemplate_id_seq;
DROP TABLE public.agribiot_annotationtemplate;
DROP SEQUENCE public.agribiot_alarmtrigger_id_seq;
DROP TABLE public.agribiot_alarmtrigger;
DROP SEQUENCE public.agribiot_alarmelement_id_seq;
DROP TABLE public.agribiot_alarmelement;
DROP EXTENSION postgis;
DROP EXTENSION plpython3u;
--
-- Name: plpython3u; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpython3u WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpython3u; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpython3u IS 'PL/Python3U untrusted procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agribiot_alarmelement; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_alarmelement (
    id integer NOT NULL,
    trigger_type character varying(100) NOT NULL,
    trigger_value double precision NOT NULL,
    geometry public.geography(Polygon,4326),
    alarm_trigger_id integer NOT NULL,
    object_id integer,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_alarmelement OWNER TO agribiot;

--
-- Name: agribiot_alarmelement_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_alarmelement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_alarmelement_id_seq OWNER TO agribiot;

--
-- Name: agribiot_alarmelement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_alarmelement_id_seq OWNED BY public.agribiot_alarmelement.id;


--
-- Name: agribiot_alarmtrigger; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_alarmtrigger (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_alarmtrigger OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_alarmtrigger_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_alarmtrigger_id_seq OWNER TO agribiot;

--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_alarmtrigger_id_seq OWNED BY public.agribiot_alarmtrigger.id;


--
-- Name: agribiot_annotationtemplate; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_annotationtemplate (
    id integer NOT NULL,
    annotation_type character varying(4) NOT NULL,
    description text NOT NULL,
    photo character varying(100)
);


ALTER TABLE public.agribiot_annotationtemplate OWNER TO agribiot;

--
-- Name: agribiot_annotationtemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_annotationtemplate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_annotationtemplate_id_seq OWNER TO agribiot;

--
-- Name: agribiot_annotationtemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_annotationtemplate_id_seq OWNED BY public.agribiot_annotationtemplate.id;


--
-- Name: agribiot_farm; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_farm (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_farm OWNER TO agribiot;

--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_farm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_farm_id_seq OWNER TO agribiot;

--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_farm_id_seq OWNED BY public.agribiot_farm.id;


--
-- Name: agribiot_foreignfarmuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_foreignfarmuser (
    user_ptr_id integer NOT NULL,
    url_origin character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_foreignfarmuser OWNER TO agribiot;

--
-- Name: agribiot_gatewayuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_gatewayuser (
    user_ptr_id integer NOT NULL
);


ALTER TABLE public.agribiot_gatewayuser OWNER TO agribiot;

--
-- Name: agribiot_object; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_object (
    id integer NOT NULL,
    "position" public.geography(Point,4326) NOT NULL,
    uuid character varying(100) NOT NULL,
    description text NOT NULL,
    user_id integer
);


ALTER TABLE public.agribiot_object OWNER TO agribiot;

--
-- Name: agribiot_object_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_object_id_seq OWNER TO agribiot;

--
-- Name: agribiot_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_object_id_seq OWNED BY public.agribiot_object.id;


--
-- Name: agribiot_objectuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_objectuser (
    user_ptr_id integer NOT NULL
);


ALTER TABLE public.agribiot_objectuser OWNER TO agribiot;

--
-- Name: agribiot_operation; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_operation (
    id integer NOT NULL,
    imported timestamp with time zone NOT NULL,
    created timestamp with time zone NOT NULL,
    reader_uuid character varying(100) NOT NULL,
    tag_embedded_task_index integer NOT NULL,
    value double precision NOT NULL,
    "position" public.geography(Point,4326) NOT NULL,
    accuracy double precision NOT NULL,
    annotation_template_id integer NOT NULL,
    sensor_object_id integer NOT NULL
);


ALTER TABLE public.agribiot_operation OWNER TO agribiot;

--
-- Name: agribiot_operation_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_operation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_operation_id_seq OWNER TO agribiot;

--
-- Name: agribiot_operation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_operation_id_seq OWNED BY public.agribiot_operation.id;


--
-- Name: agribiot_operation_images; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_operation_images (
    id integer NOT NULL,
    operation_id integer NOT NULL,
    operationimage_id integer NOT NULL
);


ALTER TABLE public.agribiot_operation_images OWNER TO agribiot;

--
-- Name: agribiot_operation_images_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_operation_images_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_operation_images_id_seq OWNER TO agribiot;

--
-- Name: agribiot_operation_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_operation_images_id_seq OWNED BY public.agribiot_operation_images.id;


--
-- Name: agribiot_operationimage; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_operationimage (
    id integer NOT NULL,
    photo character varying(100)
);


ALTER TABLE public.agribiot_operationimage OWNER TO agribiot;

--
-- Name: agribiot_operationimage_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_operationimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_operationimage_id_seq OWNER TO agribiot;

--
-- Name: agribiot_operationimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_operationimage_id_seq OWNED BY public.agribiot_operationimage.id;


--
-- Name: agribiot_parcel; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_parcel (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    geometry public.geography(Polygon,4326) NOT NULL,
    farm_id integer NOT NULL
);


ALTER TABLE public.agribiot_parcel OWNER TO agribiot;

--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_parcel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_parcel_id_seq OWNER TO agribiot;

--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_parcel_id_seq OWNED BY public.agribiot_parcel.id;


--
-- Name: agribiot_remoteaccess; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess (
    id integer NOT NULL,
    access_type integer NOT NULL,
    target_type integer NOT NULL,
    geometry public.geography(Polygon,4326),
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    foreign_users integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_id_seq OWNED BY public.agribiot_remoteaccess.id;


--
-- Name: agribiot_remoteaccess_selected_objects; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess_selected_objects (
    id integer NOT NULL,
    remoteaccess_id integer NOT NULL,
    object_id integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess_selected_objects OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_selected_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_selected_objects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_selected_objects_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_selected_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_selected_objects_id_seq OWNED BY public.agribiot_remoteaccess_selected_objects.id;


--
-- Name: agribiot_remoteaccess_sensor_objects; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess_sensor_objects (
    id integer NOT NULL,
    remoteaccess_id integer NOT NULL,
    sensorobject_id integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess_sensor_objects OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensor_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_sensor_objects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_sensor_objects_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensor_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_sensor_objects_id_seq OWNED BY public.agribiot_remoteaccess_sensor_objects.id;


--
-- Name: agribiot_remoteaccess_sensors; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteaccess_sensors (
    id integer NOT NULL,
    remoteaccess_id integer NOT NULL,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_remoteaccess_sensors OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensors_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteaccess_sensors_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteaccess_sensors_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteaccess_sensors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteaccess_sensors_id_seq OWNED BY public.agribiot_remoteaccess_sensors.id;


--
-- Name: agribiot_remoteinstance; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_remoteinstance (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    token character varying(256) NOT NULL,
    url character varying(256) NOT NULL,
    color character varying(7) NOT NULL
);


ALTER TABLE public.agribiot_remoteinstance OWNER TO agribiot;

--
-- Name: agribiot_remoteinstance_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_remoteinstance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_remoteinstance_id_seq OWNER TO agribiot;

--
-- Name: agribiot_remoteinstance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_remoteinstance_id_seq OWNED BY public.agribiot_remoteinstance.id;


--
-- Name: agribiot_rfiddeviceuser; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_rfiddeviceuser (
    user_ptr_id integer NOT NULL
);


ALTER TABLE public.agribiot_rfiddeviceuser OWNER TO agribiot;

--
-- Name: agribiot_risedalarm; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_risedalarm (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    alarm_trigger_id integer NOT NULL,
    value_id integer NOT NULL
);


ALTER TABLE public.agribiot_risedalarm OWNER TO agribiot;

--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_risedalarm_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_risedalarm_id_seq OWNER TO agribiot;

--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_risedalarm_id_seq OWNED BY public.agribiot_risedalarm.id;


--
-- Name: agribiot_sensor; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_sensor (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    unit character varying(100) NOT NULL
);


ALTER TABLE public.agribiot_sensor OWNER TO agribiot;

--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_sensor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_sensor_id_seq OWNER TO agribiot;

--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_sensor_id_seq OWNED BY public.agribiot_sensor.id;


--
-- Name: agribiot_sensorobject; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_sensorobject (
    id integer NOT NULL,
    object_id integer NOT NULL,
    sensor_id integer NOT NULL
);


ALTER TABLE public.agribiot_sensorobject OWNER TO agribiot;

--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_sensorobject_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_sensorobject_id_seq OWNER TO agribiot;

--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_sensorobject_id_seq OWNED BY public.agribiot_sensorobject.id;


--
-- Name: agribiot_value; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.agribiot_value (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    value double precision NOT NULL,
    sensor_object_id integer NOT NULL
);


ALTER TABLE public.agribiot_value OWNER TO agribiot;

--
-- Name: agribiot_value_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.agribiot_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agribiot_value_id_seq OWNER TO agribiot;

--
-- Name: agribiot_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.agribiot_value_id_seq OWNED BY public.agribiot_value.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO agribiot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO agribiot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO agribiot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO agribiot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO agribiot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO agribiot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO agribiot;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO agribiot;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO agribiot;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO agribiot;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO agribiot;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO agribiot;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO agribiot;

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO agribiot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO agribiot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO agribiot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO agribiot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO agribiot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO agribiot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO agribiot;

--
-- Name: guardian_groupobjectpermission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.guardian_groupobjectpermission (
    id integer NOT NULL,
    object_pk character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.guardian_groupobjectpermission OWNER TO agribiot;

--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.guardian_groupobjectpermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guardian_groupobjectpermission_id_seq OWNER TO agribiot;

--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.guardian_groupobjectpermission_id_seq OWNED BY public.guardian_groupobjectpermission.id;


--
-- Name: guardian_userobjectpermission; Type: TABLE; Schema: public; Owner: agribiot
--

CREATE TABLE public.guardian_userobjectpermission (
    id integer NOT NULL,
    object_pk character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    permission_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.guardian_userobjectpermission OWNER TO agribiot;

--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: agribiot
--

CREATE SEQUENCE public.guardian_userobjectpermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guardian_userobjectpermission_id_seq OWNER TO agribiot;

--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: agribiot
--

ALTER SEQUENCE public.guardian_userobjectpermission_id_seq OWNED BY public.guardian_userobjectpermission.id;


--
-- Name: agribiot_alarmelement id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement ALTER COLUMN id SET DEFAULT nextval('public.agribiot_alarmelement_id_seq'::regclass);


--
-- Name: agribiot_alarmtrigger id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger ALTER COLUMN id SET DEFAULT nextval('public.agribiot_alarmtrigger_id_seq'::regclass);


--
-- Name: agribiot_annotationtemplate id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_annotationtemplate ALTER COLUMN id SET DEFAULT nextval('public.agribiot_annotationtemplate_id_seq'::regclass);


--
-- Name: agribiot_farm id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_farm ALTER COLUMN id SET DEFAULT nextval('public.agribiot_farm_id_seq'::regclass);


--
-- Name: agribiot_object id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object ALTER COLUMN id SET DEFAULT nextval('public.agribiot_object_id_seq'::regclass);


--
-- Name: agribiot_operation id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation ALTER COLUMN id SET DEFAULT nextval('public.agribiot_operation_id_seq'::regclass);


--
-- Name: agribiot_operation_images id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation_images ALTER COLUMN id SET DEFAULT nextval('public.agribiot_operation_images_id_seq'::regclass);


--
-- Name: agribiot_operationimage id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operationimage ALTER COLUMN id SET DEFAULT nextval('public.agribiot_operationimage_id_seq'::regclass);


--
-- Name: agribiot_parcel id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel ALTER COLUMN id SET DEFAULT nextval('public.agribiot_parcel_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess_selected_objects id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_selected_objects_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess_sensor_objects id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_sensor_objects_id_seq'::regclass);


--
-- Name: agribiot_remoteaccess_sensors id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteaccess_sensors_id_seq'::regclass);


--
-- Name: agribiot_remoteinstance id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteinstance ALTER COLUMN id SET DEFAULT nextval('public.agribiot_remoteinstance_id_seq'::regclass);


--
-- Name: agribiot_risedalarm id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm ALTER COLUMN id SET DEFAULT nextval('public.agribiot_risedalarm_id_seq'::regclass);


--
-- Name: agribiot_sensor id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor ALTER COLUMN id SET DEFAULT nextval('public.agribiot_sensor_id_seq'::regclass);


--
-- Name: agribiot_sensorobject id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject ALTER COLUMN id SET DEFAULT nextval('public.agribiot_sensorobject_id_seq'::regclass);


--
-- Name: agribiot_value id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value ALTER COLUMN id SET DEFAULT nextval('public.agribiot_value_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: guardian_groupobjectpermission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission ALTER COLUMN id SET DEFAULT nextval('public.guardian_groupobjectpermission_id_seq'::regclass);


--
-- Name: guardian_userobjectpermission id; Type: DEFAULT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission ALTER COLUMN id SET DEFAULT nextval('public.guardian_userobjectpermission_id_seq'::regclass);


--
-- Data for Name: agribiot_alarmelement; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_alarmelement (id, trigger_type, trigger_value, geometry, alarm_trigger_id, object_id, sensor_id) FROM stdin;
\.


--
-- Data for Name: agribiot_alarmtrigger; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_alarmtrigger (id, name) FROM stdin;
\.


--
-- Data for Name: agribiot_annotationtemplate; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_annotationtemplate (id, annotation_type, description, photo) FROM stdin;
1	OBSV	stade-A-bourgeonhiver	media/annotationtemplateimg/stade-A-bourgeonhiver_PRwHSCt.png
2	OBSV	stade-B-bourgeaudanslecoton	media/annotationtemplateimg/stade-B-bourgeaudanslecoton_KgXq2H8.png
3	OBSV	stade-C-pointeverte	media/annotationtemplateimg/stade-C-pointeverte_9ZA7knC.png
4	OBSV	stade-D-sortiefeuille	media/annotationtemplateimg/stade-D-sortiefeuille_O0tFbSV.png
5	OBSV	stade-E-feuillesetalees	media/annotationtemplateimg/stade-E-feuillesetalees_fcugZEB.png
6	OBSV	stade-F-grappesvisibles	media/annotationtemplateimg/stade-F-grappesvisibles_iNiXTJO.png
7	OBSV	stade-G-boutonsflorauxagglomerees	media/annotationtemplateimg/stade-G-boutonsflorauxagglomerees_QnW0Ri1.png
8	OBSV	stade-H-boutonsflorauxsepares	media/annotationtemplateimg/stade-H-boutonsflorauxsepares_hQ19TMe.png
9	OBSV	stade-I-floraison	media/annotationtemplateimg/stade-I-floraison_MV49idb.png
10	OBSV	stade-J-nouaison	media/annotationtemplateimg/stade-J-nouaison_12DRWhG.png
11	OBSV	stade-K-petitspois	media/annotationtemplateimg/stade-K-petitspois_dHD61bQ.png
12	TASK	Taillage	
13	TASK	Réparation faite	
14	TASK	fils tendus	
15	TASK	test	
16	TASK	1er traitement	
17	TASK	1er désherbage	
19	OBSV	changement de tag	
\.


--
-- Data for Name: agribiot_farm; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_farm (id, name) FROM stdin;
\.


--
-- Data for Name: agribiot_foreignfarmuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_foreignfarmuser (user_ptr_id, url_origin) FROM stdin;
\.


--
-- Data for Name: agribiot_gatewayuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_gatewayuser (user_ptr_id) FROM stdin;
4
\.


--
-- Data for Name: agribiot_object; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_object (id, "position", uuid, description, user_id) FROM stdin;
2	0101000020E610000003000040E3C7F4BFFFF17EA241924740	E2003412012A0100	Cru Mouzillon-Tillières	7
3	0101000020E61000000000008073BBF4BFFFF17EA241924740	E2003412012E0100	Sauvignon	8
4	0101000020E6100000FFFFFF7FDBBCF4BF97490B014A924740	E2003412012F0100	Muscadet Champ Garreau	9
26	0101000020E6100000CF748614C9C8F4BF63164E9060924740	E20034120136020002873CBE		21
27	0101000020E6100000D5773C35B0C8F4BF1171310E47924740	E20034120130020002873F31		22
28	0101000020E61000006EBC0187CCBCF4BFD46BAB404A924740	E2003412012C02000286A4E0		23
32	0101000020E6100000C5A5CFB3FEBBF4BF1C76A58632924740	E2003412013A02000286A4CE		26
33	0101000020E6100000D08BEADFBBBEF4BFED0DDF3E82924740	E2003412012F020002870C86		27
34	0101000020E6100000D0A58F2BBBC1F4BF96DDFE2289924740	E2003412013302000286F329		28
35	0101000020E6100000E522A57612C3F4BFA12FFF198C924740	E2003412012F020002870C8F		29
36	0101000020E6100000C8B3AAA955C9F4BF5234A4C48D924740	E2003412013802000286F341		30
31	0101000020E6100000010000C024BBF4BF135D6DDE41924740	E20034120139020002867D5A		25
29	0101000020E6100000010000E002BDF4BFE2C1FABC44924740	E20034120127020002873CB9		\N
\.


--
-- Data for Name: agribiot_objectuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_objectuser (user_ptr_id) FROM stdin;
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
\.


--
-- Data for Name: agribiot_operation; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_operation (id, imported, created, reader_uuid, tag_embedded_task_index, value, "position", accuracy, annotation_template_id, sensor_object_id) FROM stdin;
39	2021-06-18 15:15:16.379444+02	2021-06-18 17:04:31.453+02		6	0	0101000020E6100000CF748614C9C8F4BF63164E9060924740	3.21600008010864258	19	25
40	2021-06-18 15:15:17.138571+02	2021-06-18 17:06:32.327+02		5	0	0101000020E6100000D5773C35B0C8F4BF1171310E47924740	3.21600008010864258	19	26
41	2021-06-18 15:15:17.941641+02	2021-06-18 17:07:57.764+02		4	0	0101000020E61000006EBC0187CCBCF4BFD46BAB404A924740	3.21600008010864258	19	27
42	2021-06-18 15:15:18.247092+02	2021-06-18 17:08:08.336+02		4	0	0101000020E6100000C2FBAA5CA8BCF4BF93E2C20549924740	3.21600008010864258	19	28
43	2021-06-18 15:15:19.06859+02	2021-06-18 17:08:16.125+02		4	0	0101000020E61000002A90B84B0BBCF4BF3B0003BD47924740	3.21600008010864258	19	29
44	2021-06-18 15:15:19.825088+02	2021-06-18 17:08:41.158+02		4	0	0101000020E6100000C5A5CFB3FEBBF4BF1C76A58632924740	3.21600008010864258	19	30
45	2021-06-18 15:15:20.556813+02	2021-06-18 17:10:31.249+02		4	0	0101000020E6100000D08BEADFBBBEF4BFED0DDF3E82924740	3.21600008010864258	19	31
46	2021-06-18 15:15:21.314926+02	2021-06-18 17:11:10.035+02		4	0	0101000020E6100000D0A58F2BBBC1F4BF96DDFE2289924740	3.21600008010864258	19	32
47	2021-06-18 15:15:22.077047+02	2021-06-18 17:11:31.547+02		4	0	0101000020E6100000E522A57612C3F4BFA12FFF198C924740	3.21600008010864258	19	33
48	2021-06-18 15:15:22.834098+02	2021-06-18 17:12:41.558+02		4	0	0101000020E6100000C8B3AAA955C9F4BF5234A4C48D924740	3.21600008010864258	19	34
6	2021-06-18 11:39:42.392326+02	2021-02-10 15:15:25.908+01		2	0	0101000020E610000093FD0600A1C8F4BF84CB3D3747924740	1.29999995231627996	12	26
7	2021-06-18 11:39:42.406389+02	2021-03-03 15:20:59.069+01		2	0	0101000020E610000006DA301D4DC8F4BF96362F6147924740	1.79999995231627996	15	26
8	2021-06-18 11:39:42.420461+02	2021-03-03 15:21:21.601+01		2	0	0101000020E61000007F130A1170C8F4BF3E1F788F46924740	1.5	15	26
29	2021-06-18 14:02:25.006625+02	2021-06-18 14:53:39.291+02		3	0	0101000020E61000000B374B8FC7C8F4BF3BABC3D946924740	3.21600008010863991	19	26
33	2021-06-18 14:20:12.763138+02	2021-06-18 16:09:02.943+02		3	0	0101000020E61000004536CAD9BFBBF4BF9E2E41FC32924740	3.21600008010863991	19	30
9	2021-06-18 11:39:43.167234+02	2021-02-10 15:20:19.332+01		2	0	0101000020E61000008ECE0CF5CEBBF4BFDCD7817346924740	1.5	12	29
10	2021-06-18 11:39:43.181329+02	2021-03-03 10:37:49.764+01		2	0	0101000020E61000009E4471B4D0BBF4BF6C300DC347924740	2.5	13	29
11	2021-06-18 11:39:43.195413+02	2021-03-03 10:38:04.643+01		2	0	0101000020E6100000CEA5B8AAECBBF4BFB7019F1F46924740	1.5	14	29
12	2021-06-18 11:39:43.209419+02	2021-03-03 15:25:05.886+01		2	0	0101000020E6100000F31DE9F9C0BBF4BFD5B2B5BE48924740	2.90000009536742986	15	29
32	2021-06-18 14:20:12.710625+02	2021-06-18 16:08:06.47+02		3	0	0101000020E610000060FE6DC3E6BBF4BF5F29CB1047924740	3.21600008010863991	19	29
13	2021-06-18 11:39:43.967439+02	2021-02-10 15:22:30.566+01		2	0	0101000020E6100000FED6615FC5BCF4BF4970109A4A924740	1.10000002384185991	12	27
14	2021-06-18 11:39:43.981464+02	2021-03-03 15:26:08.572+01		2	0	0101000020E6100000BAA3EC1A1ABCF4BFF788981249924740	2.90000009536742986	15	27
30	2021-06-18 14:20:12.602849+02	2021-06-18 16:07:06.719+02		3	0	0101000020E6100000BACCFADEF0BCF4BF9E7831414B924740	3.21600008010863991	19	27
31	2021-06-18 14:20:12.656924+02	2021-06-18 16:07:53.269+02		3	0	0101000020E6100000653D10B45DBCF4BF3A6FE7EB47924740	3.21600008010863991	19	28
35	2021-06-18 14:20:12.871479+02	2021-06-18 16:14:19.282+02		3	0	0101000020E61000000C2E880EA2C1F4BF02CD316A88924740	3.21600008010863991	19	32
1	2021-06-18 11:39:41.564901+02	2021-02-10 15:09:45.239+01		2	0	0101000020E610000084284C0502CAF4BF82FA96395D924740	5.69999980926514027	12	25
2	2021-06-18 11:39:41.586554+02	2021-02-10 15:12:29.613+01		2	0	0101000020E6100000B07451078CC8F4BFFFE7305F5E924740	1.5	12	25
3	2021-06-18 11:39:41.604936+02	2021-03-03 10:33:53.778+01		2	0	0101000020E61000008670B9E7E6C8F4BF935A15CE5B924740	2.09999990463257014	13	25
4	2021-06-18 11:39:41.622793+02	2021-03-03 10:34:05.927+01		2	0	0101000020E6100000A35B9C1E01C9F4BF5F1941505B924740	1.79999995231627996	14	25
5	2021-06-18 11:39:41.640872+02	2021-03-03 15:19:23.629+01		2	0	0101000020E61000001F65D7D0E0C9F4BFED71CC9F5C924740	3.70000004768371982	15	25
\.


--
-- Data for Name: agribiot_operation_images; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_operation_images (id, operation_id, operationimage_id) FROM stdin;
2	35	5
\.


--
-- Data for Name: agribiot_operationimage; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_operationimage (id, photo) FROM stdin;
1	media/operation/JPEG_20210618_122047_7765383982344949183.jpg
2	media/operation/JPEG_20210618_122047_7765383982344949183_7X0FZSD.jpg
3	media/operation/JPEG_20210618_122047_7765383982344949183_xNPJAxM.jpg
4	media/operation/JPEG_20210618_141421_2764279012512451051.jpg
5	media/operation/JPEG_20210618_141421_2764279012512451051_w0Aughm.jpg
\.


--
-- Data for Name: agribiot_parcel; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_parcel (id, name, geometry, farm_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess (id, access_type, target_type, geometry, start_date, end_date, foreign_users) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess_selected_objects; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess_selected_objects (id, remoteaccess_id, object_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess_sensor_objects; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess_sensor_objects (id, remoteaccess_id, sensorobject_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteaccess_sensors; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteaccess_sensors (id, remoteaccess_id, sensor_id) FROM stdin;
\.


--
-- Data for Name: agribiot_remoteinstance; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_remoteinstance (id, name, token, url, color) FROM stdin;
\.


--
-- Data for Name: agribiot_rfiddeviceuser; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_rfiddeviceuser (user_ptr_id) FROM stdin;
5
\.


--
-- Data for Name: agribiot_risedalarm; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_risedalarm (id, created, alarm_trigger_id, value_id) FROM stdin;
\.


--
-- Data for Name: agribiot_sensor; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_sensor (id, name, unit) FROM stdin;
1	Température	°C
2	Humidité	%
3	tag	
\.


--
-- Data for Name: agribiot_sensorobject; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_sensorobject (id, object_id, sensor_id) FROM stdin;
25	26	3
26	27	3
27	28	3
28	29	3
29	31	3
30	32	3
31	33	3
32	34	3
33	35	3
34	36	3
\.


--
-- Data for Name: agribiot_value; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.agribiot_value (id, created, value, sensor_object_id) FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_group (id, name) FROM stdin;
1	farmUserGroup
2	gatewayGroup
3	RFIDDeviceGroup
4	objectGroup
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	2	59
2	2	84
3	3	40
4	3	37
5	3	107
6	3	104
7	3	67
8	3	64
9	3	84
10	1	44
11	1	103
12	1	91
13	1	80
14	1	58
15	1	55
16	1	56
17	1	57
18	1	84
19	1	81
20	1	82
21	1	83
22	1	111
23	1	108
24	1	109
25	1	110
26	1	36
27	1	33
28	1	34
29	1	35
30	1	95
31	1	92
32	1	93
33	1	94
34	1	45
35	1	48
36	1	47
37	1	96
38	1	99
39	1	98
40	1	68
41	1	71
42	1	70
43	1	37
44	1	40
45	1	39
46	1	104
47	1	107
48	1	106
49	1	72
50	1	73
51	1	75
52	1	74
53	1	50
54	1	51
55	1	53
56	1	52
57	1	64
58	1	65
59	1	67
60	1	66
61	1	59
62	1	49
63	1	76
64	1	54
65	4	88
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add Token	7	add_token
26	Can change Token	7	change_token
27	Can delete Token	7	delete_token
28	Can view Token	7	view_token
29	Can add token	8	add_tokenproxy
30	Can change token	8	change_tokenproxy
31	Can delete token	8	delete_tokenproxy
32	Can view token	8	view_tokenproxy
33	Can add alarm trigger	9	add_alarmtrigger
34	Can change alarm trigger	9	change_alarmtrigger
35	Can delete alarm trigger	9	delete_alarmtrigger
36	Can view alarm trigger	9	view_alarmtrigger
37	Can add annotation template	10	add_annotationtemplate
38	Can change annotation template	10	change_annotationtemplate
39	Can delete annotation template	10	delete_annotationtemplate
40	Can view annotation template	10	view_annotationtemplate
41	Can add farm	11	add_farm
42	Can change farm	11	change_farm
43	Can delete farm	11	delete_farm
44	Can view farm	11	view_farm
45	Can add foreign farm user	12	add_foreignfarmuser
46	Can change foreign farm user	12	change_foreignfarmuser
47	Can delete foreign farm user	12	delete_foreignfarmuser
48	Can view foreign farm user	12	view_foreignfarmuser
49	Can access auth token	12	access_foreign_user_token
50	Can add gateway user	13	add_gatewayuser
51	Can change gateway user	13	change_gatewayuser
52	Can delete gateway user	13	delete_gatewayuser
53	Can view gateway user	13	view_gatewayuser
54	Can access auth token	13	access_gateway_user_token
55	Can add object	14	add_object
56	Can change object	14	change_object
57	Can delete object	14	delete_object
58	Can view object	14	view_object
59	Can access auth token	14	access_object_token
60	Can add user	15	add_objectuser
61	Can change user	15	change_objectuser
62	Can delete user	15	delete_objectuser
63	Can view user	15	view_objectuser
64	Can add operation image	16	add_operationimage
65	Can change operation image	16	change_operationimage
66	Can delete operation image	16	delete_operationimage
67	Can view operation image	16	view_operationimage
68	Can add remote instance	17	add_remoteinstance
69	Can change remote instance	17	change_remoteinstance
70	Can delete remote instance	17	delete_remoteinstance
71	Can view remote instance	17	view_remoteinstance
72	Can add rfid device user	18	add_rfiddeviceuser
73	Can change rfid device user	18	change_rfiddeviceuser
74	Can delete rfid device user	18	delete_rfiddeviceuser
75	Can view rfid device user	18	view_rfiddeviceuser
76	Can access auth token	18	access_rfiddevice_user_token
77	Can add sensor	19	add_sensor
78	Can change sensor	19	change_sensor
79	Can delete sensor	19	delete_sensor
80	Can view sensor	19	view_sensor
81	Can add sensor object	20	add_sensorobject
82	Can change sensor object	20	change_sensorobject
83	Can delete sensor object	20	delete_sensorobject
84	Can view sensor object	20	view_sensorobject
85	View last associated value/operation	20	view_last_value
86	View all associated value/operation	20	view_all_value
87	View all associated value/operation in date range	20	view_date_range_value
88	Can add value	21	add_value
89	Can change value	21	change_value
90	Can delete value	21	delete_value
91	Can view value	21	view_value
92	Can add rised alarm	22	add_risedalarm
93	Can change rised alarm	22	change_risedalarm
94	Can delete rised alarm	22	delete_risedalarm
95	Can view rised alarm	22	view_risedalarm
96	Can add remote access	23	add_remoteaccess
97	Can change remote access	23	change_remoteaccess
98	Can delete remote access	23	delete_remoteaccess
99	Can view remote access	23	view_remoteaccess
100	Can add parcel	24	add_parcel
101	Can change parcel	24	change_parcel
102	Can delete parcel	24	delete_parcel
103	Can view parcel	24	view_parcel
104	Can add operation	25	add_operation
105	Can change operation	25	change_operation
106	Can delete operation	25	delete_operation
107	Can view operation	25	view_operation
108	Can add alarm element	26	add_alarmelement
109	Can change alarm element	26	change_alarmelement
110	Can delete alarm element	26	delete_alarmelement
111	Can view alarm element	26	view_alarmelement
112	Can add group object permission	27	add_groupobjectpermission
113	Can change group object permission	27	change_groupobjectpermission
114	Can delete group object permission	27	delete_groupobjectpermission
115	Can view group object permission	27	view_groupobjectpermission
116	Can add user object permission	28	add_userobjectpermission
117	Can change user object permission	28	change_userobjectpermission
118	Can delete user object permission	28	delete_userobjectpermission
119	Can view user object permission	28	view_userobjectpermission
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	!lcrPlmKSyClm9aQ7RxgKJSX1BweYkvGkKAxyLNvy	\N	f	AnonymousUser				f	t	2021-06-17 11:41:32.626971+02
4		\N	f	gateway			normal@gateway.fr	f	t	2021-06-17 11:41:38.120025+02
5		\N	f	rfidDevice1			normal@rfidDevice1.fr	f	t	2021-06-17 11:41:38.188182+02
6	pbkdf2_sha256$260000$JCaRk8OrVAuXg9nzISFCUj$VyBR4BYETFXCZ7xLm/AdG7yL62ttOhTxDqZ5i80ncgk=	\N	f	E2003412012C0100				f	t	2021-06-18 11:39:40.615953+02
7	pbkdf2_sha256$260000$V6K2xqZNvQCOEX3IXg873z$gIpOikMHfqe2/EgK/iUvz1PUm24qDZiSD5PrVrLPx+w=	\N	f	E2003412012A0100				f	t	2021-06-18 11:39:41.654502+02
8	pbkdf2_sha256$260000$wnjP0Dq58K966NRLVQyX4K$c8NMLZxjRBfJnvzRLfWtLridi23FpL9LOQfNQXygw7U=	\N	f	E2003412012E0100				f	t	2021-06-18 11:39:42.432557+02
9	pbkdf2_sha256$260000$V8dZSjO7mdZc5hltwkWMF8$6bmm7wn+X1ka683r4uAsa2h0KEIrwc+QSStTpu5TWS4=	\N	f	E2003412012F0100				f	t	2021-06-18 11:39:43.222358+02
10	pbkdf2_sha256$260000$Yv1GUrIM9ENalHeR9EyFrP$sxM9YZGSiUw79Xl1/k8blM/qWZN8Yif6JByaR9r8/pk=	\N	f	E2003412012C1700				f	t	2021-06-18 11:39:43.99564+02
11	pbkdf2_sha256$260000$riX5wPpIeNPLv1VDnDCr3j$Ce9D2V1E+tBnf8UHBWoQBjoq5y7CE+CIPzrnKHxNjuo=	\N	f	E200341201300100				f	t	2021-06-18 11:39:44.747426+02
3	pbkdf2_sha256$260000$8qq3roou11NIBObsJxdl2h$wm7j9SCgpRk6T49dxKWguBXLIvL7MHmE7oZp/lm0s6U=	2021-06-18 11:42:05.792632+02	f	agribiot			normal@user.fr	f	t	2021-06-17 11:41:37.438219+02
12	pbkdf2_sha256$260000$6TjTejcFCm2keEyDRFojDS$648qFZzdbPrmTtsYNuZBYPcTL0cRmgqAETnKuUrtzmg=	\N	f	E200341201360200				f	t	2021-06-18 12:34:20.525058+02
13	pbkdf2_sha256$260000$iwkS9TvXxlaguUMYnTdX5t$I2G7vft4wmQWthV8jZw0CoeCjki3moj/xSZo4CocBDQ=	\N	f	E200341201300200				f	t	2021-06-18 12:34:21.446464+02
14	pbkdf2_sha256$260000$SMlWvPhVvXGOIKvNiBTwAi$c9jySYjfN71Z52fxKxUgGloaMA+GnYclLAZNdxGlgrQ=	\N	f	E200341201390200				f	t	2021-06-18 12:34:22.199869+02
15	pbkdf2_sha256$260000$s7BKkRl74LqTyoNgrIbLlQ$qdonUxMh8Xx5n4FEkA/25qaxoeiPXBeYSRBR2/iiV0s=	\N	f	E2003412013A0200				f	t	2021-06-18 12:34:22.989527+02
16	pbkdf2_sha256$260000$gZiycAdvzD4CWIb73tznFI$elwCRHuxuUJA3q156UY0xxRteaIbrNmjZr26MUDJqU8=	\N	f	E200341201270200				f	t	2021-06-18 12:34:23.328523+02
17	pbkdf2_sha256$260000$hUise7s7Sxcofek3iYGmkl$gH2GhMUMrU9+dDmAyv8Jmuhr8N236qFfUQt/BMOcd0E=	\N	f	E2003412012C0200				f	t	2021-06-18 12:34:24.269191+02
18	pbkdf2_sha256$260000$ZB7Yfhfh76RfdOGcBHtPvd$Kiod2C4s10pi54sGNKfpWXCkiMkdIvWDCRN2X3DXKE0=	\N	f	E2003412012F0200				f	t	2021-06-18 12:34:25.029686+02
19	pbkdf2_sha256$260000$Qv4TRiCs2RSBZUqK7Rz3By$Sobsm4k3WeuqtfFdoy965Fh6Pee/eVNA6Sgw3mK09Y4=	\N	f	E200341201330200				f	t	2021-06-18 12:34:25.796225+02
20	pbkdf2_sha256$260000$kMHpkoKCEC0unQhrKU03W2$yI1CItlShPYy/Gfa8niScOSNtLF5ZJfI7CEPmawVRJQ=	\N	f	E200341201380200				f	t	2021-06-18 12:34:26.604391+02
2	pbkdf2_sha256$260000$Ajvf8BKRa0sOITA4dzZmil$Or+E/WsThTCBEIsEM2JmhCfXhuympq+iXM4X+XWa1h8=	2021-06-18 12:40:47.391508+02	t	root			root@root.fr	t	t	2021-06-17 11:41:36.716344+02
21	pbkdf2_sha256$260000$BCOSkPxXySmRxlyQ9IBBCl$ZfSsa2NaFOUKGytBHn49+t4wp7iMjplAp6WzTS4qBDY=	\N	f	E20034120136020002873CBE				f	t	2021-06-18 15:15:15.573881+02
22	pbkdf2_sha256$260000$18arpdCCvSIEvevTTc2EsG$nppEOYPvxVjCaOfYUkz1t94eDWJd+4Ic1z2oKBSFW7c=	\N	f	E20034120130020002873F31				f	t	2021-06-18 15:15:16.394236+02
23	pbkdf2_sha256$260000$DEJqPNK1jvMh078O9sszjT$MKbv4/b+rw2VH3QPTbXiT/78bx8e2f/Sojb7aJJWy4I=	\N	f	E2003412012C02000286A4E0				f	t	2021-06-18 15:15:17.154814+02
24	pbkdf2_sha256$260000$RKj9taYhiGpzr2c58zoeBR$HZy9IWUNVl/B8Y0Xk/F/9OBhDl5DA7FoIp6lrSJvRR0=	\N	f	E20034120127020002873CB9				f	t	2021-06-18 15:15:17.957079+02
25	pbkdf2_sha256$260000$Q7MB3kaUAKRUEwnwuM3XZb$h6EYXM91ISlBrPAkRm0MFdu4A92k1BFbfrIgsptGqyA=	\N	f	E20034120139020002867D5A				f	t	2021-06-18 15:15:18.262322+02
26	pbkdf2_sha256$260000$D48pZdmLRy7Oa6bXQVl5g5$4okNKwhfUB6kghZ062VdbH4XN4oDfqX9CVhi/Bsc88s=	\N	f	E2003412013A02000286A4CE				f	t	2021-06-18 15:15:19.084635+02
27	pbkdf2_sha256$260000$oeiFkah5NvKCXfGlg8M4av$0TJjI4HRCq65nTb67pQrfSTZSIH7AuBEswhBhG70/Yk=	\N	f	E2003412012F020002870C86				f	t	2021-06-18 15:15:19.839813+02
28	pbkdf2_sha256$260000$zLoJ4UNm9eXzrzcDdnA6Uk$cB8jCFhqbkdxEjeF3jOt2xCH/wctf97N2EVrImvOSaM=	\N	f	E2003412013302000286F329				f	t	2021-06-18 15:15:20.571555+02
29	pbkdf2_sha256$260000$dD2QXoLvLf2PSP2SNjAhGi$axZGvNyqpbcqbWoTTDixLxFK/YK3zoev9Kh1O++nm2o=	\N	f	E2003412012F020002870C8F				f	t	2021-06-18 15:15:21.330202+02
30	pbkdf2_sha256$260000$GaGL5qGalsbpifxo6mfkG8$RHt+0F9aAayIE4PmXoMmvHd1KRl/I4DUfwkH12vf2cc=	\N	f	E2003412013802000286F341				f	t	2021-06-18 15:15:22.091936+02
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
1	3	1
2	4	2
3	5	3
4	6	4
6	7	4
8	8	4
10	9	4
12	10	4
14	11	4
16	12	4
18	13	4
20	14	4
22	15	4
23	16	4
26	17	4
28	18	4
30	19	4
32	20	4
34	21	4
36	22	4
38	23	4
40	24	4
41	25	4
44	26	4
46	27	4
48	28	4
50	29	4
52	30	4
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
05dbec5b8675dde8dbaa672bb391b06965785436	2021-06-18 11:48:28.541991+02	5
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2021-06-18 12:41:20.771342+02	17	Operation object (17)	3		25	2
2	2021-06-18 12:41:20.810964+02	18	Operation object (18)	3		25	2
3	2021-06-18 12:41:20.819115+02	19	Operation object (19)	3		25	2
4	2021-06-18 12:41:20.825572+02	20	Operation object (20)	3		25	2
5	2021-06-18 12:41:20.831841+02	21	Operation object (21)	3		25	2
6	2021-06-18 12:41:20.836736+02	22	Operation object (22)	3		25	2
7	2021-06-18 12:41:20.841853+02	23	Operation object (23)	3		25	2
8	2021-06-18 12:41:20.84688+02	24	Operation object (24)	3		25	2
9	2021-06-18 12:41:20.851585+02	25	Operation object (25)	3		25	2
10	2021-06-18 12:41:20.856064+02	26	Operation object (26)	3		25	2
11	2021-06-18 12:41:20.860611+02	27	Operation object (27)	3		25	2
12	2021-06-18 13:59:41.421357+02	16	Object object (16)	3		14	2
13	2021-06-18 13:59:41.466267+02	15	Object object (15)	3		14	2
14	2021-06-18 13:59:41.470715+02	14	Object object (14)	3		14	2
15	2021-06-18 13:59:41.475093+02	13	Object object (13)	3		14	2
16	2021-06-18 13:59:41.479448+02	12	Object object (12)	3		14	2
17	2021-06-18 13:59:41.483771+02	10	Object object (10)	3		14	2
18	2021-06-18 13:59:41.488198+02	9	Object object (9)	3		14	2
19	2021-06-18 14:00:21.406337+02	8	Object object (8)	3		14	2
20	2021-06-18 14:00:21.442377+02	7	Object object (7)	3		14	2
21	2021-06-18 14:02:00.08832+02	5	Object object (5)	3		14	2
22	2021-06-18 15:18:57.879234+02	22	Object object (22)	3		14	2
23	2021-06-18 15:19:16.286057+02	18	Object object (18)	3		14	2
24	2021-06-18 15:20:05.010917+02	17	Object object (17)	3		14	2
25	2021-06-18 15:20:46.986493+02	21	Object object (21)	3		14	2
26	2021-06-18 15:22:05.762496+02	19	Object object (19)	3		14	2
27	2021-06-18 15:23:23.13847+02	20	Object object (20)	3		14	2
28	2021-06-18 15:25:04.042485+02	23	Object object (23)	3		14	2
29	2021-06-18 15:25:04.078372+02	6	Object object (6)	3		14	2
30	2021-06-18 15:25:45.343052+02	24	Object object (24)	3		14	2
31	2021-06-18 15:26:34.140478+02	25	Object object (25)	3		14	2
32	2021-06-18 15:27:53.417891+02	1	Object object (1)	3		14	2
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	authtoken	token
8	authtoken	tokenproxy
9	agribiot	alarmtrigger
10	agribiot	annotationtemplate
11	agribiot	farm
12	agribiot	foreignfarmuser
13	agribiot	gatewayuser
14	agribiot	object
15	agribiot	objectuser
16	agribiot	operationimage
17	agribiot	remoteinstance
18	agribiot	rfiddeviceuser
19	agribiot	sensor
20	agribiot	sensorobject
21	agribiot	value
22	agribiot	risedalarm
23	agribiot	remoteaccess
24	agribiot	parcel
25	agribiot	operation
26	agribiot	alarmelement
27	guardian	groupobjectpermission
28	guardian	userobjectpermission
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-06-17 11:41:28.727168+02
2	auth	0001_initial	2021-06-17 11:41:28.978776+02
3	admin	0001_initial	2021-06-17 11:41:29.053675+02
4	admin	0002_logentry_remove_auto_add	2021-06-17 11:41:29.086723+02
5	admin	0003_logentry_add_action_flag_choices	2021-06-17 11:41:29.120389+02
6	contenttypes	0002_remove_content_type_name	2021-06-17 11:41:29.200567+02
7	auth	0002_alter_permission_name_max_length	2021-06-17 11:41:29.262761+02
8	auth	0003_alter_user_email_max_length	2021-06-17 11:41:29.297278+02
9	auth	0004_alter_user_username_opts	2021-06-17 11:41:29.329907+02
10	auth	0005_alter_user_last_login_null	2021-06-17 11:41:29.365207+02
11	auth	0006_require_contenttypes_0002	2021-06-17 11:41:29.372324+02
12	auth	0007_alter_validators_add_error_messages	2021-06-17 11:41:29.405263+02
13	auth	0008_alter_user_username_max_length	2021-06-17 11:41:29.454044+02
14	auth	0009_alter_user_last_name_max_length	2021-06-17 11:41:29.494983+02
15	auth	0010_alter_group_name_max_length	2021-06-17 11:41:29.533832+02
16	auth	0011_update_proxy_permissions	2021-06-17 11:41:29.57179+02
17	auth	0012_alter_user_first_name_max_length	2021-06-17 11:41:29.605583+02
18	agribiot	0001_initial	2021-06-17 11:41:31.073553+02
19	authtoken	0001_initial	2021-06-17 11:41:31.208208+02
20	authtoken	0002_auto_20160226_1747	2021-06-17 11:41:31.414778+02
21	authtoken	0003_tokenproxy	2021-06-17 11:41:31.456908+02
22	guardian	0001_initial	2021-06-17 11:41:31.939855+02
23	guardian	0002_generic_permissions_index	2021-06-17 11:41:32.073842+02
24	sessions	0001_initial	2021-06-17 11:41:32.121061+02
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
i5mmz4kqzhqn1le9xmf69tlzwhfeun2v	.eJxVjDsOwyAQRO9CHSHM8k2Z3mdACyzBSYQlY1dR7h5bcpF0o3lv5s0CbmsNW6clTJldGbDLbxcxPakdID-w3Wee5rYuU-SHwk_a-Thnet1O9--gYq_7mrwQADgo5WSyclCSSrEExQpN0u9ROZ2Ndt5Z9IkKGIjZSC0waSsF-3wBxa83Jw:1luAyq:f5LVtBUe406hVxHkrRIGG_lt1yDlg7I4360n_GQSCKY	2021-07-02 11:40:04.714207+02
7ddd49ax0aeg4k3483frwvocygjf7lwg	.eJxVjDsOwyAQRO9CHSHM8k2Z3mdACyzBSYQlY1dR7h5bcpF0o3lv5s0CbmsNW6clTJldGbDLbxcxPakdID-w3Wee5rYuU-SHwk_a-Thnet1O9--gYq_7mrwQADgo5WSyclCSSrEExQpN0u9ROZ2Ndt5Z9IkKGIjZSC0waSsF-3wBxa83Jw:1luB0n:8nuD2XJOQwAiVNSdeoT5jBpYvY1OSojttiFt1RCmu04	2021-07-02 11:42:05.808608+02
rm24i3p85ew2kw5unug3e8td43k94eo2	.eJxVjDsOwjAQBe_iGlkb_2Io6TmD5fWucQDZUpxUiLuTSCmgfTPz3iLEdSlh7TyHicRFKHH63TCmJ9cd0CPWe5Op1WWeUO6KPGiXt0b8uh7u30GJvWy1ccpbGDWZqIg1Y86UCRyBsQNS0hkxAYAfzgYJzYaSAsyjZ3JZWfH5Avl9OKo:1luBvb:q5rb2UASf6NRbRAagnq-lyXstF9m5Xqkm3zKCbzM1Ec	2021-07-02 12:40:47.408387+02
\.


--
-- Data for Name: guardian_groupobjectpermission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.guardian_groupobjectpermission (id, object_pk, content_type_id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: guardian_userobjectpermission; Type: TABLE DATA; Schema: public; Owner: agribiot
--

COPY public.guardian_userobjectpermission (id, object_pk, content_type_id, permission_id, user_id) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Name: agribiot_alarmelement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_alarmelement_id_seq', 1, false);


--
-- Name: agribiot_alarmtrigger_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_alarmtrigger_id_seq', 1, false);


--
-- Name: agribiot_annotationtemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_annotationtemplate_id_seq', 19, true);


--
-- Name: agribiot_farm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_farm_id_seq', 1, false);


--
-- Name: agribiot_object_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_object_id_seq', 36, true);


--
-- Name: agribiot_operation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_operation_id_seq', 48, true);


--
-- Name: agribiot_operation_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_operation_images_id_seq', 2, true);


--
-- Name: agribiot_operationimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_operationimage_id_seq', 5, true);


--
-- Name: agribiot_parcel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_parcel_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_selected_objects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_selected_objects_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_sensor_objects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_sensor_objects_id_seq', 1, false);


--
-- Name: agribiot_remoteaccess_sensors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteaccess_sensors_id_seq', 1, false);


--
-- Name: agribiot_remoteinstance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_remoteinstance_id_seq', 1, false);


--
-- Name: agribiot_risedalarm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_risedalarm_id_seq', 1, false);


--
-- Name: agribiot_sensor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_sensor_id_seq', 3, true);


--
-- Name: agribiot_sensorobject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_sensorobject_id_seq', 34, true);


--
-- Name: agribiot_value_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.agribiot_value_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 4, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 65, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 119, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 53, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 30, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 32, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 28, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 24, true);


--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.guardian_groupobjectpermission_id_seq', 1, false);


--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: agribiot
--

SELECT pg_catalog.setval('public.guardian_userobjectpermission_id_seq', 1, false);


--
-- Name: agribiot_alarmelement agribiot_alarmelement_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelement_pkey PRIMARY KEY (id);


--
-- Name: agribiot_alarmtrigger agribiot_alarmtrigger_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger
    ADD CONSTRAINT agribiot_alarmtrigger_name_key UNIQUE (name);


--
-- Name: agribiot_alarmtrigger agribiot_alarmtrigger_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmtrigger
    ADD CONSTRAINT agribiot_alarmtrigger_pkey PRIMARY KEY (id);


--
-- Name: agribiot_annotationtemplate agribiot_annotationtemplate_description_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_annotationtemplate
    ADD CONSTRAINT agribiot_annotationtemplate_description_key UNIQUE (description);


--
-- Name: agribiot_annotationtemplate agribiot_annotationtemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_annotationtemplate
    ADD CONSTRAINT agribiot_annotationtemplate_pkey PRIMARY KEY (id);


--
-- Name: agribiot_farm agribiot_farm_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_farm
    ADD CONSTRAINT agribiot_farm_pkey PRIMARY KEY (id);


--
-- Name: agribiot_foreignfarmuser agribiot_foreignfarmuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_foreignfarmuser
    ADD CONSTRAINT agribiot_foreignfarmuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_gatewayuser agribiot_gatewayuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_gatewayuser
    ADD CONSTRAINT agribiot_gatewayuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_object agribiot_object_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_pkey PRIMARY KEY (id);


--
-- Name: agribiot_object agribiot_object_uuid_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_uuid_key UNIQUE (uuid);


--
-- Name: agribiot_objectuser agribiot_objectuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_objectuser
    ADD CONSTRAINT agribiot_objectuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_operation_images agribiot_operation_image_operation_id_operationim_3636faaf_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation_images
    ADD CONSTRAINT agribiot_operation_image_operation_id_operationim_3636faaf_uniq UNIQUE (operation_id, operationimage_id);


--
-- Name: agribiot_operation_images agribiot_operation_images_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation_images
    ADD CONSTRAINT agribiot_operation_images_pkey PRIMARY KEY (id);


--
-- Name: agribiot_operation agribiot_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation
    ADD CONSTRAINT agribiot_operation_pkey PRIMARY KEY (id);


--
-- Name: agribiot_operationimage agribiot_operationimage_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operationimage
    ADD CONSTRAINT agribiot_operationimage_pkey PRIMARY KEY (id);


--
-- Name: agribiot_parcel agribiot_parcel_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel
    ADD CONSTRAINT agribiot_parcel_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess agribiot_remoteaccess_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess
    ADD CONSTRAINT agribiot_remoteaccess_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteaccess_se_remoteaccess_id_object_i_6a4c0d2b_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_object_i_6a4c0d2b_uniq UNIQUE (remoteaccess_id, object_id);


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteaccess_se_remoteaccess_id_sensor_i_0da2fc55_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensor_i_0da2fc55_uniq UNIQUE (remoteaccess_id, sensor_id);


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteaccess_se_remoteaccess_id_sensorob_81219c57_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteaccess_se_remoteaccess_id_sensorob_81219c57_uniq UNIQUE (remoteaccess_id, sensorobject_id);


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteaccess_selected_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteaccess_selected_objects_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteaccess_sensor_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteaccess_sensor_objects_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteaccess_sensors_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteaccess_sensors_pkey PRIMARY KEY (id);


--
-- Name: agribiot_remoteinstance agribiot_remoteinstance_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteinstance
    ADD CONSTRAINT agribiot_remoteinstance_pkey PRIMARY KEY (id);


--
-- Name: agribiot_rfiddeviceuser agribiot_rfiddeviceuser_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_rfiddeviceuser
    ADD CONSTRAINT agribiot_rfiddeviceuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: agribiot_risedalarm agribiot_risedalarm_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_pkey PRIMARY KEY (id);


--
-- Name: agribiot_sensor agribiot_sensor_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor
    ADD CONSTRAINT agribiot_sensor_name_key UNIQUE (name);


--
-- Name: agribiot_sensor agribiot_sensor_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensor
    ADD CONSTRAINT agribiot_sensor_pkey PRIMARY KEY (id);


--
-- Name: agribiot_sensorobject agribiot_sensorobject_object_id_sensor_id_a7d20fea_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_object_id_sensor_id_a7d20fea_uniq UNIQUE (object_id, sensor_id);


--
-- Name: agribiot_sensorobject agribiot_sensorobject_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_pkey PRIMARY KEY (id);


--
-- Name: agribiot_value agribiot_value_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value
    ADD CONSTRAINT agribiot_value_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: guardian_groupobjectpermission guardian_groupobjectperm_group_id_permission_id_o_3f189f7c_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobjectperm_group_id_permission_id_o_3f189f7c_uniq UNIQUE (group_id, permission_id, object_pk);


--
-- Name: guardian_groupobjectpermission guardian_groupobjectpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobjectpermission_pkey PRIMARY KEY (id);


--
-- Name: guardian_userobjectpermission guardian_userobjectpermi_user_id_permission_id_ob_b0b3d2fc_uniq; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermi_user_id_permission_id_ob_b0b3d2fc_uniq UNIQUE (user_id, permission_id, object_pk);


--
-- Name: guardian_userobjectpermission guardian_userobjectpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermission_pkey PRIMARY KEY (id);


--
-- Name: agribiot_alarmelement_alarm_trigger_id_9ba193db; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_alarm_trigger_id_9ba193db ON public.agribiot_alarmelement USING btree (alarm_trigger_id);


--
-- Name: agribiot_alarmelement_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_geometry_id ON public.agribiot_alarmelement USING gist (geometry);


--
-- Name: agribiot_alarmelement_object_id_c292df7b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_object_id_c292df7b ON public.agribiot_alarmelement USING btree (object_id);


--
-- Name: agribiot_alarmelement_sensor_id_ab7c0bfe; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmelement_sensor_id_ab7c0bfe ON public.agribiot_alarmelement USING btree (sensor_id);


--
-- Name: agribiot_alarmtrigger_name_2a404727_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_alarmtrigger_name_2a404727_like ON public.agribiot_alarmtrigger USING btree (name varchar_pattern_ops);


--
-- Name: agribiot_annotationtemplate_description_bbb0c561_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_annotationtemplate_description_bbb0c561_like ON public.agribiot_annotationtemplate USING btree (description text_pattern_ops);


--
-- Name: agribiot_object_position_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_position_id ON public.agribiot_object USING gist ("position");


--
-- Name: agribiot_object_user_id_69163bcb; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_user_id_69163bcb ON public.agribiot_object USING btree (user_id);


--
-- Name: agribiot_object_uuid_a3a0d421_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_object_uuid_a3a0d421_like ON public.agribiot_object USING btree (uuid varchar_pattern_ops);


--
-- Name: agribiot_operation_annotation_template_id_af355ffe; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_annotation_template_id_af355ffe ON public.agribiot_operation USING btree (annotation_template_id);


--
-- Name: agribiot_operation_images_operation_id_32f4f572; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_images_operation_id_32f4f572 ON public.agribiot_operation_images USING btree (operation_id);


--
-- Name: agribiot_operation_images_operationimage_id_73b154ff; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_images_operationimage_id_73b154ff ON public.agribiot_operation_images USING btree (operationimage_id);


--
-- Name: agribiot_operation_position_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_position_id ON public.agribiot_operation USING gist ("position");


--
-- Name: agribiot_operation_sensor_object_id_cb2c3e2f; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_operation_sensor_object_id_cb2c3e2f ON public.agribiot_operation USING btree (sensor_object_id);


--
-- Name: agribiot_parcel_farm_id_63c02727; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_parcel_farm_id_63c02727 ON public.agribiot_parcel USING btree (farm_id);


--
-- Name: agribiot_parcel_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_parcel_geometry_id ON public.agribiot_parcel USING gist (geometry);


--
-- Name: agribiot_remoteaccess_foreign_users_207df1dc; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_foreign_users_207df1dc ON public.agribiot_remoteaccess USING btree (foreign_users);


--
-- Name: agribiot_remoteaccess_geometry_id; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_geometry_id ON public.agribiot_remoteaccess USING gist (geometry);


--
-- Name: agribiot_remoteaccess_selected_objects_object_id_b8286ddc; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_selected_objects_object_id_b8286ddc ON public.agribiot_remoteaccess_selected_objects USING btree (object_id);


--
-- Name: agribiot_remoteaccess_selected_objects_remoteaccess_id_382e8a77; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_selected_objects_remoteaccess_id_382e8a77 ON public.agribiot_remoteaccess_selected_objects USING btree (remoteaccess_id);


--
-- Name: agribiot_remoteaccess_sensor_objects_remoteaccess_id_b12c6c90; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensor_objects_remoteaccess_id_b12c6c90 ON public.agribiot_remoteaccess_sensor_objects USING btree (remoteaccess_id);


--
-- Name: agribiot_remoteaccess_sensor_objects_sensorobject_id_3a831118; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensor_objects_sensorobject_id_3a831118 ON public.agribiot_remoteaccess_sensor_objects USING btree (sensorobject_id);


--
-- Name: agribiot_remoteaccess_sensors_remoteaccess_id_a33d91e7; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensors_remoteaccess_id_a33d91e7 ON public.agribiot_remoteaccess_sensors USING btree (remoteaccess_id);


--
-- Name: agribiot_remoteaccess_sensors_sensor_id_a56784c0; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_remoteaccess_sensors_sensor_id_a56784c0 ON public.agribiot_remoteaccess_sensors USING btree (sensor_id);


--
-- Name: agribiot_risedalarm_alarm_trigger_id_d6008608; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_risedalarm_alarm_trigger_id_d6008608 ON public.agribiot_risedalarm USING btree (alarm_trigger_id);


--
-- Name: agribiot_risedalarm_value_id_49e5444e; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_risedalarm_value_id_49e5444e ON public.agribiot_risedalarm USING btree (value_id);


--
-- Name: agribiot_sensor_name_56d3fb1c_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensor_name_56d3fb1c_like ON public.agribiot_sensor USING btree (name varchar_pattern_ops);


--
-- Name: agribiot_sensorobject_object_id_fcf9bc39; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensorobject_object_id_fcf9bc39 ON public.agribiot_sensorobject USING btree (object_id);


--
-- Name: agribiot_sensorobject_sensor_id_4a836e2c; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_sensorobject_sensor_id_4a836e2c ON public.agribiot_sensorobject USING btree (sensor_id);


--
-- Name: agribiot_value_sensor_object_id_97097252; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX agribiot_value_sensor_object_id_97097252 ON public.agribiot_value USING btree (sensor_object_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: guardian_gr_content_ae6aec_idx; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_gr_content_ae6aec_idx ON public.guardian_groupobjectpermission USING btree (content_type_id, object_pk);


--
-- Name: guardian_groupobjectpermission_content_type_id_7ade36b8; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_groupobjectpermission_content_type_id_7ade36b8 ON public.guardian_groupobjectpermission USING btree (content_type_id);


--
-- Name: guardian_groupobjectpermission_group_id_4bbbfb62; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_groupobjectpermission_group_id_4bbbfb62 ON public.guardian_groupobjectpermission USING btree (group_id);


--
-- Name: guardian_groupobjectpermission_permission_id_36572738; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_groupobjectpermission_permission_id_36572738 ON public.guardian_groupobjectpermission USING btree (permission_id);


--
-- Name: guardian_us_content_179ed2_idx; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_us_content_179ed2_idx ON public.guardian_userobjectpermission USING btree (content_type_id, object_pk);


--
-- Name: guardian_userobjectpermission_content_type_id_2e892405; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_userobjectpermission_content_type_id_2e892405 ON public.guardian_userobjectpermission USING btree (content_type_id);


--
-- Name: guardian_userobjectpermission_permission_id_71807bfc; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_userobjectpermission_permission_id_71807bfc ON public.guardian_userobjectpermission USING btree (permission_id);


--
-- Name: guardian_userobjectpermission_user_id_d5c1e964; Type: INDEX; Schema: public; Owner: agribiot
--

CREATE INDEX guardian_userobjectpermission_user_id_d5c1e964 ON public.guardian_userobjectpermission USING btree (user_id);


--
-- Name: agribiot_alarmelement agribiot_alarmelemen_alarm_trigger_id_9ba193db_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelemen_alarm_trigger_id_9ba193db_fk_agribiot_ FOREIGN KEY (alarm_trigger_id) REFERENCES public.agribiot_alarmtrigger(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_alarmelement agribiot_alarmelement_object_id_c292df7b_fk_agribiot_object_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelement_object_id_c292df7b_fk_agribiot_object_id FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_alarmelement agribiot_alarmelement_sensor_id_ab7c0bfe_fk_agribiot_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_alarmelement
    ADD CONSTRAINT agribiot_alarmelement_sensor_id_ab7c0bfe_fk_agribiot_sensor_id FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_foreignfarmuser agribiot_foreignfarmuser_user_ptr_id_d62330a8_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_foreignfarmuser
    ADD CONSTRAINT agribiot_foreignfarmuser_user_ptr_id_d62330a8_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_gatewayuser agribiot_gatewayuser_user_ptr_id_c1b9d969_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_gatewayuser
    ADD CONSTRAINT agribiot_gatewayuser_user_ptr_id_c1b9d969_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_object agribiot_object_user_id_69163bcb_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_object
    ADD CONSTRAINT agribiot_object_user_id_69163bcb_fk_agribiot_ FOREIGN KEY (user_id) REFERENCES public.agribiot_objectuser(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_objectuser agribiot_objectuser_user_ptr_id_d9b15f29_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_objectuser
    ADD CONSTRAINT agribiot_objectuser_user_ptr_id_d9b15f29_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_operation agribiot_operation_annotation_template__af355ffe_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation
    ADD CONSTRAINT agribiot_operation_annotation_template__af355ffe_fk_agribiot_ FOREIGN KEY (annotation_template_id) REFERENCES public.agribiot_annotationtemplate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_operation_images agribiot_operation_i_operation_id_32f4f572_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation_images
    ADD CONSTRAINT agribiot_operation_i_operation_id_32f4f572_fk_agribiot_ FOREIGN KEY (operation_id) REFERENCES public.agribiot_operation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_operation_images agribiot_operation_i_operationimage_id_73b154ff_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation_images
    ADD CONSTRAINT agribiot_operation_i_operationimage_id_73b154ff_fk_agribiot_ FOREIGN KEY (operationimage_id) REFERENCES public.agribiot_operationimage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_operation agribiot_operation_sensor_object_id_cb2c3e2f_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_operation
    ADD CONSTRAINT agribiot_operation_sensor_object_id_cb2c3e2f_fk_agribiot_ FOREIGN KEY (sensor_object_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_parcel agribiot_parcel_farm_id_63c02727_fk_agribiot_farm_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_parcel
    ADD CONSTRAINT agribiot_parcel_farm_id_63c02727_fk_agribiot_farm_id FOREIGN KEY (farm_id) REFERENCES public.agribiot_farm(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess agribiot_remoteacces_foreign_users_207df1dc_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess
    ADD CONSTRAINT agribiot_remoteacces_foreign_users_207df1dc_fk_agribiot_ FOREIGN KEY (foreign_users) REFERENCES public.agribiot_foreignfarmuser(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteacces_object_id_b8286ddc_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteacces_object_id_b8286ddc_fk_agribiot_ FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_selected_objects agribiot_remoteacces_remoteaccess_id_382e8a77_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_selected_objects
    ADD CONSTRAINT agribiot_remoteacces_remoteaccess_id_382e8a77_fk_agribiot_ FOREIGN KEY (remoteaccess_id) REFERENCES public.agribiot_remoteaccess(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteacces_remoteaccess_id_a33d91e7_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteacces_remoteaccess_id_a33d91e7_fk_agribiot_ FOREIGN KEY (remoteaccess_id) REFERENCES public.agribiot_remoteaccess(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteacces_remoteaccess_id_b12c6c90_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteacces_remoteaccess_id_b12c6c90_fk_agribiot_ FOREIGN KEY (remoteaccess_id) REFERENCES public.agribiot_remoteaccess(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensors agribiot_remoteacces_sensor_id_a56784c0_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensors
    ADD CONSTRAINT agribiot_remoteacces_sensor_id_a56784c0_fk_agribiot_ FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_remoteaccess_sensor_objects agribiot_remoteacces_sensorobject_id_3a831118_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_remoteaccess_sensor_objects
    ADD CONSTRAINT agribiot_remoteacces_sensorobject_id_3a831118_fk_agribiot_ FOREIGN KEY (sensorobject_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_rfiddeviceuser agribiot_rfiddeviceuser_user_ptr_id_6733a39c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_rfiddeviceuser
    ADD CONSTRAINT agribiot_rfiddeviceuser_user_ptr_id_6733a39c_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_risedalarm agribiot_risedalarm_alarm_trigger_id_d6008608_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_alarm_trigger_id_d6008608_fk_agribiot_ FOREIGN KEY (alarm_trigger_id) REFERENCES public.agribiot_alarmtrigger(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_risedalarm agribiot_risedalarm_value_id_49e5444e_fk_agribiot_value_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_risedalarm
    ADD CONSTRAINT agribiot_risedalarm_value_id_49e5444e_fk_agribiot_value_id FOREIGN KEY (value_id) REFERENCES public.agribiot_value(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_sensorobject agribiot_sensorobject_object_id_fcf9bc39_fk_agribiot_object_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_object_id_fcf9bc39_fk_agribiot_object_id FOREIGN KEY (object_id) REFERENCES public.agribiot_object(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_sensorobject agribiot_sensorobject_sensor_id_4a836e2c_fk_agribiot_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_sensorobject
    ADD CONSTRAINT agribiot_sensorobject_sensor_id_4a836e2c_fk_agribiot_sensor_id FOREIGN KEY (sensor_id) REFERENCES public.agribiot_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agribiot_value agribiot_value_sensor_object_id_97097252_fk_agribiot_; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.agribiot_value
    ADD CONSTRAINT agribiot_value_sensor_object_id_97097252_fk_agribiot_ FOREIGN KEY (sensor_object_id) REFERENCES public.agribiot_sensorobject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobjectpermission guardian_groupobject_content_type_id_7ade36b8_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobject_content_type_id_7ade36b8_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobjectpermission guardian_groupobject_group_id_4bbbfb62_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobject_group_id_4bbbfb62_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobjectpermission guardian_groupobject_permission_id_36572738_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobject_permission_id_36572738_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjectpermission guardian_userobjectp_content_type_id_2e892405_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectp_content_type_id_2e892405_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjectpermission guardian_userobjectp_permission_id_71807bfc_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectp_permission_id_71807bfc_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjectpermission guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: agribiot
--

ALTER TABLE ONLY public.guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

